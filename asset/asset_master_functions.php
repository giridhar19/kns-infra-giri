<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_asset.php');

/*
PURPOSE : To add new Asset Master
INPUT 	: Material ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_asset_master($material_id,$remarks,$added_by)
{
	$asset_master_iresult = db_add_asset_master($material_id,$remarks,$added_by);
	
	if($asset_master_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Master Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Asset Master List
INPUT 	: Asset ID, Material ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Master, success or failure message
BY 		: Lakshmi
*/
function i_get_asset_master($asset_master_search_data)
{
	$asset_master_sresult = db_get_asset_master($asset_master_search_data);
	
	if($asset_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$asset_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Asset Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Asset Master 
INPUT 	: Asset ID, Asset Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_asset_master($asset_id,$asset_master_update_data)
{   
	$asset_master_sresult = db_update_asset_master($asset_id,$asset_master_update_data);
	
	if($asset_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Asset Type Master
INPUT 	: Asset Type, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_asset_type_master($asset_type,$remarks,$added_by)
{
	$asset_type_master_iresult = db_add_asset_type_master($asset_type,$remarks,$added_by);
	
	if($asset_type_master_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Type Master Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Asset Type Master List
INPUT 	: Asset Type ID, Asset Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Type Master, success or failure message
BY 		: Lakshmi
*/
function i_get_asset_type_master($asset_type_master_search_data)
{
	$asset_type_master_sresult = db_get_asset_type_master($asset_type_master_search_data);
	
	if($asset_type_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$asset_type_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Asset Type Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Asset Type Master 
INPUT 	: Asset type ID, Asset  Type Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_asset_type_master($asset_type_id,$asset_type_master_update_data)
{   
	$asset_type_master_sresult = db_update_asset_type_master($asset_type_id,$asset_type_master_update_data);
	
	if($asset_type_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Type Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Asset details
INPUT 	: Asset Master ID, Asset Type ID, Vendor ID, Invoice NO, Invoice Amount, Invoice Date, Other Charges, Location, Serial NO, Units, On Date, Life Of asset,
          Asset Used Till, Life Form, Depreciation Rate, Depreciation Amount, Wdv as on, Status, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_asset_details($asset_master_id,$asset_type_id,$vendor_id,$invoice_no,$invoice_amount,$invoice_date,$other_charges,$location,$serial_no,$units,$on_date,$life_of_asset,$asset_used_till,$life_form,$depreciation_rate,$depreciation_amount,$wdv_as_on,$status,$remarks,$added_by)
{
	$asset_details_iresult = db_add_asset_details($asset_master_id,$asset_type_id,$vendor_id,$invoice_no,$invoice_amount,$invoice_date,$other_charges,$location,$serial_no,$units,$on_date,$life_of_asset,$asset_used_till,$life_form,$depreciation_rate,$depreciation_amount,$wdv_as_on,$status,$remarks,$added_by);
	
	if($asset_details_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Details Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Asset Details List
INPUT 	: Details ID, Asset Master ID, Asset Type ID, Vendor ID, Invoice NO, Invoice Amount, Invoice Date, Other Changes, Location, Serial NO, Units, On Date, Life Of asset,
          Asset Used Till, Life Form, Depreciation Rate, Depreciation Amount, Wdv as on, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Details, success or failure message
BY 		: Lakshmi
*/
function i_get_asset_details($asset_details_search_data)
{
	$asset_details_sresult = db_get_asset_details($asset_details_search_data);
	
	if($asset_details_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$asset_details_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Asset Details Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Asset Details 
INPUT 	: Details ID, Asset Details Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_asset_details($details_id,$asset_details_update_data)
{   
	$asset_details_sresult = db_update_asset_details($details_id,$asset_details_update_data);
	
	if($asset_details_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Details Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Asset Transfer
INPUT 	: Details ID, Source Location, Dest Location, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_add_asset_transfer($details_id,$source_location,$dest_location,$remarks,$added_by)
{
	$asset_transfer_iresult = db_add_asset_transfer($details_id,$source_location,$dest_location,$remarks,$added_by);
	
	if($asset_transfer_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Transfer Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Asset Transfer List
INPUT 	: Transfer ID, Details ID, Source Location, Dest Location, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Transfer, success or failure message
BY 		: Lakshmi
*/
function i_get_asset_transfer($asset_transfer_search_data)
{
	$asset_transfer_sresult = db_get_asset_transfer($asset_transfer_search_data);
	
	if($asset_transfer_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$asset_transfer_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Asset Transfer Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Asset Transfer 
INPUT 	: Transfer ID, Transfer Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_asset_transfer($transfer_id,$asset_transfer_update_data)
{   
	$asset_transfer_sresult = db_update_asset_transfer($transfer_id,$asset_transfer_update_data);
	
	if($asset_transfer_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Asset Transfer Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
?>