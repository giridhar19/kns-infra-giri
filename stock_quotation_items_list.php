<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_method_planning_list.php
CREATED ON	: 14-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID','283');
/* DEFINES - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_TASK_METHOD_PLAN_LIST_FUNC_ID,'4','1');

	// Query String Data
	// Nothing

	// if(isset($_REQUEST['quote_id']))
	// {
	// 	$quote_id = $_REQUEST['quote_id'];
	// }
	// else
	// {
	// 	$quote_id = '';
	// }
	//
	// if(isset($_REQUEST['vendor_name']))
	// {
	// 	$vendor_name = $_REQUEST['vendor_name'];
	// }
	// else
	// {
	// 	$vendor_name = '';
	// }
	if(isset($_REQUEST['id']))
	{
		$quote_id = $_REQUEST['id'];
	}
	else
	{
		$quote_id = "";
	}

	if(isset($_REQUEST['name']))
	{
		$vendor_name = $_REQUEST['name'];
	}
	else
	{
		$vendor_name = "";
	}

  //Get Venor Type of Service List
	$stock_items_quotation_search_data = array("quote_id"=>$quote_id);
	$quotation_item_list = db_get_stock_items_quotations($stock_items_quotation_search_data);
	if($quotation_item_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$quotation_item_list_data = $quotation_item_list["data"];
		$quote_no = $quotation_item_list_data[0]["stock_quote_no"] ;
	}
	else
	{
		$alert = $quotation_item_list["data"];
		$alert_type = 0;
		$quote_no = "";
	}
}
else
{
	header("location:login.php");
}
?>
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<div>
	<h3  class="modal-title">Quotation Items List </h3></br>
	<h4  class="modal-title">Quotation No : <?php echo $quote_no ;?> </h4>
	<h4  class="modal-title">Vendor: <?php echo $vendor_name ;?> </h4>
 </div>
</div>
<div class="modal-body">
        <div class="widget-content">
      <table class="table table-striped table-bordered display nowrap" style="width:100%">
      <thead>
      <tr>
        <th>SL No</th>
        <th>Indent No</th>
        <th>Project</th>
        <th>Indent Qty</th>
        <th>Material Name</th>
        <th>Material Code</th>
        <th>Material Price</th>
        <th>Indent By</th>
        <th>Indent On</th>
		</tr>
		</thead>
		<tbody>
    <?php
		if($quotation_item_list["status"] == DB_RECORD_ALREADY_EXISTS)
		{
        $sl_no = 0;
				for($count = 0; $count < count($quotation_item_list_data); $count++)
				{
					$sl_no++;
				?>
				<tr>
				<td><?php echo $sl_no; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["quote_indent_no"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["quote_project_name"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["quote_material_qty"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["quote_material_name"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["quote_material_code"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["stock_material_price"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo $quotation_item_list_data[$count]["user_name"]; ?></td>
				<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($quotation_item_list_data[$count]["stock_indent_item_added_on"])); ?></td>
				</tr>
				<?php
				}
			}
			else
			{
				?>
				<td colspan="5">No Vendor data added yet!</td>
        <?php
      }
     ?>
    </tbody>
  </table>
  </div>
</div>
