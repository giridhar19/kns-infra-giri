<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
function get_conn_handle()
{
	$db     = 'kns_erp_live';
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';
	$error = "";

	$db_conn_handle = new mysqli($dbhost, $dbuser, $dbpass, $db);
    if ($db_conn_handle->connect_errno) {
    //echo "Failed to connect to MySQL: (" . $db_conn_handle->connect_errno . ") " . $db_conn_handle->connect_error;
		$db_conn_handle = DB_CONN_FAILURE;
		$error = $db_conn_handle->connect_error;
	}

	return $db_conn_handle;
	}

?>

<!DOCTYPE html>
<html lang="en" class="no-js">

<!-- Head -->
  <head>
  <!-- Meta data -->
    <meta charset="utf-8">
    <title>jsGantt Improved</title>
    <meta name="description" content="FREE javascript gantt - jsGantt Improved HTML, CSS and AJAX only">
    <meta name="keywords" content="jsgantt-improved free javascript gantt-chart html css ajax">
    <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- External resources -->
    <!-- jQuery + Ajax [required by Bootstrap] -->
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <!-- Required by smooth scrolling -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Bootstrap v4.0.0 Alpha -->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" />
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <!-- Font Awesome -->
      <script src="https://use.fontawesome.com/78d1e57168.js"></script>
    <!-- Google's Code Prettify -->
      <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&amp;skin=sunburst"></script>
    <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Cookie|Satisfy|Kelly+Slab|Overlock" rel="stylesheet">

  <!-- Internal resources -->
    <!-- Webpage -
      <link href="main.css" rel="stylesheet" type="text/css" />
      <script src="main.js" type="text/javascript"></script> ->
    <!-- jsGanttImproved App -->
      <link href="assets/dhtmlxgantt.css" rel="stylesheet" type="text/css"/>
      <script src="assets/dhtmlxgantt.js" type="text/javascript"></script>
  </head>

<!-- Content -->
  <body data-spy="scroll" data-target="#my-navbar-nav">
  <!-- Navigation bar -->

  <!-- Content -->
    <!-- Home -->

    <!-- Demo -->
      <div class="" id="demo">
        <h1>Demo</h1>
        <div id="embedded-Gantt">
          <script type="text/javascript">
var g = new JSGantt.GanttChart(document.getElementById('embedded-Gantt'), 'week');
if (g.getDivId() != null) {
  g.setCaptionType('Complete');  // Set to Show Caption (None,Caption,Resource,Duration,Complete)
  g.setQuarterColWidth(36);
  g.setDateTaskDisplayFormat('day dd month yyyy'); // Shown in tool tip box
  g.setDayMajorDateDisplayFormat('mon yyyy - Week ww') // Set format to display dates in the "Major" header of the "Day" view
  g.setWeekMinorDateDisplayFormat('dd mon') // Set format to display dates in the "Minor" header of the "Week" view
  g.setShowTaskInfoLink(1); // Show link in tool tip (0/1)
  g.setShowEndWeekDate(0); // Show/Hide the date for the last day of the week in header for daily view (1/0)
  g.setUseSingleCell(10000); // Set the threshold at which we will only use one cell per table row (0 disables).  Helps with rendering performance for large charts.
  g.setFormatArr('Day', 'Week', 'Month', 'Quarter'); // Even with setUseSingleCell using Hour format on such a large chart can cause issues in some browsers
  // Parameters                     (pID, pName,                  pStart,       pEnd,        pStyle,         pLink (unused)  pMile, pRes,       pComp, pGroup, pParent, pOpen, pDepend, pCaption, pNotes, pGantt)
  <?php
	$mysqli = get_conn_handle();
	$sqlQueryForProject = 'SELECT project_management_master_id as project_id, project_master_name as project_name, project_master_start_date as start_date
							FROM project_management_project_master WHERE project_master_active="1" AND project_management_master_id <> "19" ';

	//echo $sqlQueryForProject;
	$resultProjects = $mysqli->query( $sqlQueryForProject );
	//print_r( $resultProjects );
	while ( $rowProjects = $resultProjects->fetch_assoc() ) {
		 $projectId = $rowProjects['project_id'];
	?>
		g.AddTaskItem(new JSGantt.TaskItem( '<?php echo $projectId ;?> ' ,   '<?php echo $rowProjects['project_name'] ;?> ',     '<?php echo $rowProjects['start_date'] ;?> ',           '',          'ggroupblack',  '',                 0, '',    0,     1,      0,       0,     '',      '',      'Some Notes text', g ));
   <?php

		$sqlQueryForPrjPlan = 'SELECT pp.project_plan_id as project_id, pp.project_plan_project_id as pppid, project_plan_start_date as start_date, project_plan_end_date as end_date
								FROM project_plan as pp WHERE project_plan_active="1" and pp.project_plan_project_id = ' . $rowProjects['project_id'];

		//echo $sqlQueryForPrjPlan;
		$resultPrjPlan = $mysqli->query( $sqlQueryForPrjPlan );

		//print_r( $resultPrjPlan );
		$counter = 0;
		while ( $rowPrjPlan = $resultPrjPlan->fetch_assoc() ) {
			$projectPlanId = sprintf("%0d0%04d", $projectId, $rowPrjPlan['project_id']);
			$counter++;


	?>
		g.AddTaskItem(new JSGantt.TaskItem('<?php echo $projectPlanId ;?> ',  '<?php echo "PLAN " . $counter ;?>',         '<?php echo $rowPrjPlan['start_date'] ;?>','<?php echo $rowPrjPlan['end_date'] ;?>', 'ggroupblack',   '',                 0, '',   '10',   1,      <?php echo $projectId ;?>,       0,     '',      '',      '',      g));
	<?php
			//print_r( $rowPrjPlan );
			$sqlQueryForProcess = 'SELECT project_plan_process_id as process_id, project_plan_process_plan_id, project_process_master_name as process_name,
			project_plan_process_start_date as start_date, project_plan_process_end_date as end_date
			FROM project_plan_process
			INNER JOIN project_process_master
			ON project_plan_process_name = project_process_master_id
			WHERE project_plan_process_active = "1" and project_process_master_active ="1" and project_plan_process_plan_id = ' . $rowPrjPlan['project_id'];

			//echo $sqlQueryForProcess;
			$resultProcess = $mysqli->query( $sqlQueryForProcess );
			//print_r( $resultProcess );

			while ( $rowProcess = $resultProcess->fetch_assoc() ) {
				$processId = sprintf("%0d0%04d", $projectPlanId, $rowProcess['process_id']);

				$start_date = (strcmp($rowProcess['start_date'], '0000-00-00') !== 0)? $rowProcess['start_date']: '2017-01-01';
				$end_date = (strcmp($rowProcess['end_date'], '0000-00-00') !== 0)? $rowProcess['end_date']: '2018-01-01';

	?>
		g.AddTaskItem(new JSGantt.TaskItem('<?php echo $processId ;?> ',  '<?php echo $rowProcess['process_name'] ;?>', '<?php echo $start_date ;?>', '<?php echo $end_date;?>', 'gtaskpurple',   '',                 0, '',   '',   1,      <?php echo $projectPlanId ;?>,       0,     '',      '',      '',      g));
	<?php


			$sqlQueryForTask = 'SELECT project_process_task_id as task_id, project_process_id, project_task_master_name as task_name,
										project_process_actual_start_date as start_date, project_process_actual_end_date as end_date,
										project_process_task_completion as man_completion, project_process_machine_task_completion as machine_completion,
										project_process_contract_task_completion as contract_completion
									FROM project_plan_process_task
									INNER JOIN project_task_master
									ON 	project_task_master_id = project_process_task_type
									WHERE project_process_task_active = "1" and project_task_master_active ="1" and project_process_id = ' . $rowProcess['process_id'];

			//echo $sqlQueryForTask;
			$resultTasks = $mysqli->query( $sqlQueryForTask );
			//print_r( $resultProcess );

			while ( $rowTasks = $resultTasks->fetch_assoc() ) {
				//print_r( $rowTasks );
				$taskId = sprintf("%0d0%04d", $processId, $rowTasks['task_id']);
				$start_date = (strcmp($rowTasks['start_date'], '0000-00-00') !== 0)? $rowTasks['start_date']: '2017-01-01';
				$end_date = (strcmp($rowTasks['end_date'], '0000-00-00') !== 0)? $rowTasks['end_date']: '2018-01-01';

				$completionPer = 0;
				$completionTasksAvg =0;
				$sqlQuery = ' SELECT project_task_actual_manpower_task_id FROM project_task_actual_manpower WHERE project_task_actual_manpower_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				//echo "### : " . $result->num_rows . PHP_EOL;
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['man_completion'];
					$completionTasksAvg++;
				}

				$sqlQuery = ' SELECT project_task_actual_machine_plan_task_id FROM project_task_actual_machine_plan WHERE project_task_actual_machine_plan_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['machine_completion'];
					$completionTasksAvg++;
				}
				$sqlQuery = ' SELECT project_boq_actual_task_id FROM project_task_boq_actuals WHERE project_boq_actual_task_id = ' . $rowTasks['task_id'];
				$result = $mysqli->query( $sqlQuery );
				if( $result->num_rows > 0 ) {
					$completionPer = $completionPer + $rowTasks ['contract_completion'];
					$completionTasksAvg++;
				}
				$completionPer = $completionPer / $completionTasksAvg;
				//echo "@@@ : " . $completionPer . PHP_EOL;

	?>
		g.AddTaskItem(new JSGantt.TaskItem('<?php echo $taskId ;?> ',  '<?php echo $rowTasks['task_name'] ;?>', '<?php echo $start_date ;?>', '<?php echo $end_date ;?>', 'gtaskblue',   '',                 0, '',   '<?php echo $completionPer ;?>',   0,      <?php echo $processId ;?>,       0,     '',      '',      '',      g));
	<?php
				}
			}
		}
	}
   ?>
  /*
  g.AddTaskItem(new JSGantt.TaskItem(1,   'Define Chart API',     '',           '',          'ggroupblack',  '',                 0, 'Brian',    0,     1,      0,       1,     '',      '',      'Some Notes text', g ));
  g.AddTaskItem(new JSGantt.TaskItem(11,  'Chart Object',         '2017-02-20','2017-02-20', 'gmilestone',   '',                 1, 'Shlomy',   100,   0,      1,       1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(12,  'Task Objects',         '',           '',          'ggroupblack',  '',                 0, 'Shlomy',   40,    1,      1,       1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(121, 'Constructor Proc',     '2017-02-21','2017-03-09', 'gtaskblue',    '',                 0, 'Brian T.', 60,    0,      12,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(122, 'Task Variables',       '2017-03-06','2017-03-11', 'gtaskred',     '',                 0, 'Brian',    60,    0,      12,      1,     121,     '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(123, 'Task by Minute/Hour',  '2017-03-09','2017-03-14 12:00', 'gtaskyellow', '',            0, 'Ilan',     60,    0,      12,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(124, 'Task Functions',       '2017-03-09','2017-03-29', 'gtaskred',     '',                 0, 'Anyone',   60,    0,      12,      1,     '123SS', 'This is a caption', null, g));
  g.AddTaskItem(new JSGantt.TaskItem(2,   'Create HTML Shell',    '2017-03-24','2017-03-24', 'gtaskyellow',  '',                 0, 'Brian',    20,    0,      0,       1,     122,     '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(3,   'Code Javascript',      '',           '',          'ggroupblack',  '',                 0, 'Brian',    0,     1,      12,       1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(31,  'Define Variables',     '2017-02-25','2017-03-17', 'gtaskpurple',  '',                 0, 'Brian',    30,    0,      3,       1,     '',      'Caption 1','',   g));
  g.AddTaskItem(new JSGantt.TaskItem(32,  'Calculate Chart Size', '2017-03-15','2017-03-24', 'gtaskgreen',   '',                 0, 'Shlomy',   40,    0,      3,       1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(33,  'Draw Task Items',      '',           '',          'ggroupblack',  '',                 0, 'Someone',  40,    2,      3,       1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(332, 'Task Label Table',     '2017-03-06','2017-03-09', 'gtaskblue',    '',                 0, 'Brian',    60,    0,      33,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(333, 'Task Scrolling Grid',  '2017-03-11','2017-03-20', 'gtaskblue',    '',                 0, 'Brian',    0,     0,      33,      1,     '332',   '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(34,  'Draw Task Bars',       '',           '',          'ggroupblack',  '',                 0, 'Anybody',  60,    1,      3,       0,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(341, 'Loop each Task',       '2017-03-26','2017-04-11', 'gtaskred',     '',                 0, 'Brian',    60,    0,      34,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(342, 'Calculate Start/Stop', '2017-04-12','2017-05-18', 'gtaskpink',    '',                 0, 'Brian',    60,    0,      34,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(343, 'Draw Task Div',        '2017-05-13','2017-05-17', 'gtaskred',     '',                 0, 'Brian',    60,    0,      34,      1,     '',      '',      '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(344, 'Draw Completion Div',  '2017-05-17','2017-06-04', 'gtaskred',     '',                 0, 'Brian',    60,    0,      34,      1,     "342,343",'',     '',      g));
  g.AddTaskItem(new JSGantt.TaskItem(35,  'Make Updates',         '2017-07-17','2017-09-04', 'gtaskpurple',  '',                 0, 'Brian',    30,    0,      3,       1,     '333',   '',      '',      g));
	*/
  g.Draw();
} else {
  alert("Error, unable to create Gantt Chart");
}
          </script>
        </div>

      </div>


    <!-- Footer -->
      <div class="footer text-center">
        <p>© Copyright 2013-2017 jsGanttImproved<br />
        Designed with <a href="https://v4-alpha.getbootstrap.com" target="_blank">Bootstrap</a> and <a href="http://fontawesome.io" target="_blank">Font Awesome</a></p>
      </div>

  </body>

</html>
