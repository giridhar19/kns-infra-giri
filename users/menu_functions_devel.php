 <?php
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
?>
 <link href="bootstrap_aku.min.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.7.2.min.js"></script>
 <script type="text/javascript">
          $(document).ready(function(){
                  $(".dropdown").hover(            
                  function() {
                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
                    $(this).toggleClass('open');        
                  },
                  function() {
                    $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
                    $(this).toggleClass('open');       
                }
            );
			
        });
        </script>
   
        <div id="menu112">
        
        
<div class="container">
     <nav class="navbar navbar-fixed-top navbar-inverse ">
     <div class="container">
      <div class="navbar-header">
      	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
  			<span class="sr-only">Toggle navigation</span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  			<span class="icon-bar"></span>
  		</button>
  		<a class="navbar-brand" href="#" style="color: #ffffff;"><b>KNS<b></a>
        <div class="clearfix"></div>
  	</div>
  	
  	<div class="collapse navbar-collapse js-navbar-collapse">
  		<ul class="nav navbar-nav">
<?php


				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'Dashboard','','2','1');					

				if($perms_list['status'] == SUCCESS)

				{?>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dashboard<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <?php

					for($count = 0; $count < count($perms_list['data']); $count++)

					{?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						

					<?php

					}?>
            </ul>
          </li>
          <?php }?>
          
          
          	<li class="dropdown mega-dropdown">
  				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Legal - Liaison<span class="caret"></span></a>				
          <ul class="dropdown-menu mega-dropdown-menu">
          	<?php

				if(($role == 1) || ($role == 2) || ($role == 3))

				{

				?>
  		
  					
  					<li class="col-sm-3">
  						<ul>
                        <li class="dropdown-header"><b class="fa fa-legal">&nbsp;</b>Legal</li>
              	<?php if(($role == 1) || ($role == 2) || ($role == 3)){?>

                        <li><a href="pending_file_list.php">Files List</a></li>

					<?php }?>
  							   <?php if(($role == 1) || ($role == 2)){?>
            <li><a href="completed_file_list.php">Completed Files List</a></li>
            <?php }	?>
              <?php if(($role == 1) || ($role == 2) || ($role == 3) || ($role == 12)){ ?>
             <?php if(($role == 1) || ($role == 2) || ($role == 3)){?>
                  <li><a href="create_bulk_project_plan.php">Add Bulk Process</a></li>
            <li><a href="pending_project_list.php">Pending Process List</a></li>
            <li><a href="completed_project_list.php">Completed Process List</a></li>
            <li><a href="pending_bulk_project_list.php">Pending Bulk Process List</a></li>
            <li><a href="completed_bulk_project_list.php">Completed Bulk Process List</a></li>           
            <li><a href="legal_file_project_list.php">Project Land Bank</a></li>
            <li><a href="assigned_files_list.php">Land Approvals</a></li>
               <?php }?>
                 
                <li><a href="payment_request_list.php">Payment Request List</a></li>
            <li><a href="payment_issued_list.php">Payment Issued List</a></li>
             <?php if(($role == 1) || ($role == 2) || ($role == 3)){?>
  							<li class="divider"></li>
  							<li class="dropdown-header"><b class="fa fa-list-alt">&nbsp;</b>Tasks</li>
               <li><a href="pending_task_list.php">Pending Task List</a></li>
            <li><a href="completed_task_list.php">Completed Task List</a></li>
         <?php } }?>
  						</ul>
  					</li>
                    <?php }?>
                    <?php

				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'Legal Masters','','2','1');					

				if($perms_list['status'] == SUCCESS)

				{?>
  					<li class="col-sm-3">
  						<ul>
  							<li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Legal Masters</li>
  							<li><a href="add_task_type_master.php">Add Task Type</a></li>                      
  							<li><a href="task_type_list.php">Task Type List</a></li>
                <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-list">&nbsp;</b>Process</li>
                <li><a href="add_process_type_master.php">Add Process Type</a></li>
                <li><a href="process_type_list.php">Process Type List</a></li>              
                <li><a href="add_process_user_mapping.php">Assign User to Process</a></li>              
                <li><a href="process_user_mapping_list.php">User Process List</a></li>
                <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-commenting">&nbsp;</b>Reason</li>
                <li><a href="add_reason_master.php">Add Reason Type</a></li>            
                <li><a href="add_payment_reason_master.php">Add Payment Request Reason Type</a></li>            
                <li><a href="reason_master_list.php">Reason Type List</a></li>            
                <li><a href="add_payment_reason_master.php">Request Reason Type</a></li>            
                <li><a href="payment_request_reason_list.php">Payment Request Reason List</a></li>						
  						</ul>
  					</li>
                    <?php }?>
                    <?php if(($role == 1) || ($role == 2) || ($role == 3)){?>
  					<li class="col-sm-3">
  						<ul>
  							<li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Legal Reports</li>
                <li><a href="summary.php">Summary</a></li>
  							<li><a href="consolidated_report.php">Consolidated Report</a></li>
                <li><a href="bulk_consolidated_report.php">Consolidated Report - Bulk</a></li>
  							<li><a href="completed_process_report.php">Completed Processes</a></li>
                <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-dashboard">&nbsp;</b>Dashboard</li>
                <li><a href="dashboard.php">Process Dashboard</a></li>
                <li><a href="dashboard_tasks.php">Task Dashboard</a></li>  
                	  <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Katha Transfer -Buy
</li>                       
  						</ul>
  					</li>
<?php }?>

<li class="col-sm-3">
  						<ul>
                        <li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Registration</li>
                        <?php
				// Load menu based on user permissions				
				$perms_list = i_get_user_perms($user,'Registration','','2','1');					
				if($perms_list['status'] == SUCCESS)
				{?>
                           <?php
					
					for($count = 0; $count < count($perms_list['data']); $count++)
						
					{?>
				
                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						
						
					<?php
					
					} }?>
  							<li class="dropdown-header"><b class="fa fa-briefcase">&nbsp;</b>Katha_Transfer</li>
                  
                  <?php
				// Load menu based on user permissions				
				$perms_list = i_get_user_perms($user,'Katha_Transfer','','2','1');	
				
				if($perms_list['status'] == SUCCESS)
					
				{?>
                     <?php
					
					for($count = 0; $count < count($perms_list['data']); $count++)
					
					{?>
					
                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						
						
					<?php
					
					} }?>
                  
                              
  						</ul>
  					</li>

            

            
  				</ul>				
  			</li>
        

        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">BD<span class="caret"></span></a>       
          <ul class="dropdown-menu mega-dropdown-menu">
         
 <li class="col-sm-3">
                <ul>
                <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Land Bank</li>
                <?php

				if(($role == 1) || ($role == 11) || ($role == 12) || ($role == 2))

				{

				?>
                <li><a href="bd_add_project.php">Add BD Project</a></li>

					<li><a href="bd_project_list.php">BD Project List</a></li>

					<li><a href="bd_add_file.php">Add BD File</a></li>

					<li><a href="bd_file_list.php">Active BD File List</a></li>	

					<li><a href="bd_orphan_file_list.php">Inactive BD File List</a></li>
                    
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>BD</li>
                  
					<li><a href="bd_payment_request_list.php">Payment Request List</a></li>						

					<li><a href="bd_payment_report.php">Payment Report</a></li>	

 <?php }?>
           
 <?php 

				if($role == 1)

				{?>
                
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>BD Masters</li>
                  
                  
                        <li><a href="bd_add_funding_source.php">Add Funding Source</a></li>

						<li><a href="bd_add_village.php">Add Village</a></li>

						<li><a href="bd_add_land_status.php">Add Land Status</a></li>

						<li><a href="bd_add_account.php">Add Account Master</a></li>

						<li><a href="bd_add_delay_reason_master.php">Add Delay Reason</a></li>

						<li><a href="bd_add_court_case_type_master.php">Add Court Case Type</a></li>

						<li><a href="bd_add_court_establishment.php">Add Court Establishment Master</a></li>

						<li><a href="bd_add_court_status.php">Add Court Case Status Master</a></li>

						<li><a href="bd_add_diffident_master.php">Add Diffident Master</a></li>

						<li><a href="bd_add_plaintiff_master.php">Add Plaintiff Master</a></li>

                <?php }?>
                </ul></li>
                 <?php

				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'Survey Transactions','','2','1','0');					

				if($perms_list['status'] == SUCCESS)

				{?>
                <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Survey</li>
              
            
                
					<?php

					for($count = 0; $count < count($perms_list['data']); $count++)

					{?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						

					<?php

					}?>
                
                
              
                </ul>
                </li>
               <?php }?>
                   <?php

				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'Survey Masters','','2','1');					

				if($perms_list['status'] == SUCCESS)

				{?>
                 <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Survey Masters</li>
                
              
                <?php

					for($count = 0; $count < count($perms_list['data']); $count++)

					{?>

                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						

					<?php

					}?>
                
               
                <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Survey Transactions</li>
               
                </ul>
                </li>
                <?php }?>
                
                <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>APF</li>
                 <li><a href="apf_master_bank_list.php">APF Bank Master List</a></li>						
					                        <li><a href="apf_master_project_list.php">APF Project Master List</a></li>						
					                        <li><a href="apf_master_process_list.php">APF Process Master List</a></li>						
					                        <li><a href="apf_master_survey_list.php">APF Survey Master List</a></li>						
					                        <li><a href="apf_details_list.php">APF Details List</a></li>						
					                        <li><a href="apf_process_list.php">APF Process List</a></li>						
					                        <li><a href="apf_query_list.php">APF Query List</a></li>						
					                        <li><a href="apf_document_list.php">APF Document List</a></li>						
					                        <li><a href="apf_dashboard_pending.php">APF Dashboard Pending</a></li>						
					                        <li><a href="apf_dashboard_completed.php">APF Dashboard Completed</a></li>	
                  </ul>
                  
                  </li>
                
                
                
                 <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Court Case</li>
                
					<li><a href="bd_add_legal_court_case.php">Add Court Case</a></li>

					<li><a href="bd_court_case_list.php">Court Case List</a></li>	

					

					<li><a href="court_case_payment_request_list.php">Court Case Payment Request List</a></li>						

					<li><a href="court_payment_release_list.php?report=1">Court Case Payment Report</a></li>

					<li><a href="bd_court_case_fup_list.php">Court Case Follow Up</a></li>	

					<li><a href="bd_file_borrow_list.php">Registered Land Bank</a></li>	
                  </ul>
                  </li> 
          </ul>
        </li>
        
          
        
        
        <li class="dropdown mega-dropdown">
      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sales-CRM<span class="caret"></span></a>				
  				<ul class="dropdown-menu mega-dropdown-menu">
                 <?php if($role == 1){?>
  					<li class="col-sm-3">
      					<ul>
  							<li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>CRM Masters</li>
  							<li><a href="crm_add_bank.php">Add Bank</a></li>
                <li><a href="crm_bank_list.php">Bank List</a></li>
                <li><a href="crm_add_interest_status.php">Add Interest Status</a></li>
                <li><a href="crm_interest_status_list.php">Interest Status List</a></li>
                <li><a href="crm_add_reason.php">Add Reason</a></li>
                <li><a href="crm_reason_list.php">Reason List</a></li>
            <li><a href="crm_add_release_status.php">Add Release Status</a></li>
            <li><a href="crm_release_status_list.php">Release Status List</a></li>
            <li><a href="crm_add_site_status.php">Add Site Status</a></li>
            <li><a href="crm_site_status_list.php">Site Status List</a></li>
            <li><a href="crm_add_cancel_reason.php">Add Cancellation Reason</a></li>
            <li><a href="crm_cancel_reason_list.php">Cancellation Reason List</a></li>
            <li><a href="crm_add_site_type.php">Add Site Type</a></li>
            <li><a href="crm_site_type_list.php">Site Type List</a></li>
            <li><a href="crm_add_dimension.php">Add Dimension</a></li>
            <li><a href="crm_dimension_list.php">Dimension List</a></li>
            <li><a href="crm_add_site_cost.php">Add Site Costing</a></li>
            <li><a href="crm_add_site_discount.php">Add Discount Rates</a></li>
            <li><a href="crm_add_site_premium.php">Add Premium Values</a></li>
            <li><a href="crm_add_mgmnt_blocking_reason.php">Add Management Blocking Reason</a></li>
            <li><a href="crm_add_cab.php">Add Cab</a></li>
            <li><a href="crm_cab_list.php">Cab List</a></li>
  						</ul>
  					</li>
                    <?php }?>
                      <?php if(($role == 1) || ($role == 5) || ($role == 7) || ($role == 8) || ($role == 10)){?>
  					<li class="col-sm-3">
  						<ul>
  							<li class="dropdown-header"><b class="fa fa-check-circle-o">&nbsp;</b>CRM Projects</li>
  							<?php if($role == 1){?>
            <li><a href="crm_add_project.php">Add Project</a></li>
            <li><a href="crm_add_site_to_project.php">Add Site to Project</a></li>
            <li><a href="crm_add_survey_number.php">Add Survey to project</a></li>
            <li><a href="crm_import_sites.php">Import Sites</a></li>
            <?php }?>
            <?php if($role == 1) {?>
            <li><a href="crm_site_status_display.php">Site Availability Status</a></li>
            <?php }?>
            <li><a href="crm_site_status_report.php">Site Availability</a></li>
            <li><a href="crm_sale_status_report.php">Sale Status Report</a></li>
            <li><a href="crm_blocked_site_list.php">Blocked Site List</a></li>
            <li><a href="crm_mgmnt_blocked_site_list.php">Management Blocked Site List</a></li>
            <li><a href="crm_customer_profile_list.php">Customer Profile List</a></li>
            <li><a href="crm_pending_booking_list.php">Booking List - Pending Approval</a></li>
            <li><a href="crm_approved_booking_list.php">Booking List - Approved</a></li>
            <li><a href="crm_agreement_list.php">Agreement List</a></li>
            <li><a href="crm_registration_list.php">Registration List</a></li>
            <li><a href="crm_katha_transfer_list.php">Katha Transfer List</a></li>
            <li><a href="crm_cancelled_booking_list.php">Booking List - Cancelled</a></li>
            <?php if(($role == 1) || ($role == 10)){?>
            <li><a href="crm_all_booking_list.php">All Bookings</a></li>
            <?php }?>							
  						</ul>
  					</li>
                    <?php }?>
                     <?php if(($role == 1) || ($role == 5) || ($role == 6) || ($role == 7) || ($role == 8) || ($role == 9)){?>
  					<li class="col-sm-3">
  						<ul>
  							<li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>CRM Transactions</li>
                <li><a href="crm_add_enquiry.php">Add Enquiry</a></li>
                <li><a href="crm_enquiry_list.php">Enquiry List</a></li>
                 <?php if($role == 1){?><li><a href="crm_add_enquiry_source.php">Add Enquiry Source</a></li><?php }?>
               <?php if($role == 1){?>  <li><a href="crm_enquiry_source_list.php">Enquiry Source List</a></li><?php }?>
                <li><a href="crm_enquiry_fup_latest.php">Enquiry Follow Up List</a></li>
                <li><a href="crm_enquiry_fup_list.php">Enquiry Follow Up Dump</a></li>
                <li><a href="crm_assigned_enquiry_list.php">Assigned Enquiry List</a></li>
                <li><a href="crm_prospective_profile_list.php">Prospective Client List</a></li>
                   <li><a href="crm_enquiry_list_unq.php">Unqualifiable Enquiry List</a></li>
            <li><a href="crm_unq_enquiry_list.php">Unqualified Enquiry List</a></li>
                <?php if(($role == 1) || ($role == 5) || ($role == 6) || ($role == 8)){ ?>
            <li><a href="crm_import_enquiries.php">Import Enquiries</a></li>
            <?php }?>
                 <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-taxi">&nbsp;</b>Site Visit</li>
                <li><a href="crm_site_visit_plan_list.php">Tentative Site Visit Plan List</a></li>
            <li><a href="crm_site_visit_completed_list.php">Site Visit Completed List</a></li>
               <?php if(($role == 1) || ($role == 5) || ($role == 9)){?>
            <li><a href="crm_add_site_travel_plan.php">Add Site Travel Plan</a></li>
            <?php }?>
              <li><a href="crm_site_travel_plan_list.php">Site Travel Plan List</a></li>
            <li><a href="crm_add_site_travel_actual.php">Add Site Travel Actual</a></li>
               
                <li><a href="#"></a></li>
  							<li><a href="#">Calls to action</a></li>
  							<li><a href="#">Slide down on Hover</a></li>                
  						</ul>
  					</li>
                    <?php }?>
            <li class="col-sm-3">
      					<ul>
                <li class="dropdown-header"><b class="fa fa-bar-chart">&nbsp;</b>CRM Reports</li>
                <?php if($role == 1){ ?>
            <li><a href="crm_scheduler/crm_scheduler.php" target="_blank">Sales Performance Mails</a></li>
            <li><a href="crm_weekly_report_stm.php" target="_blank">Between Dates STM Report</a></li>
            <?php }?>
                 <?php if(($role == 1) || ($role == 5)){	?>
            <li><a href="crm_marketing_sales_summary_report.php">Marketing Sales Summary Report</a></li>
            <?php }?>
             <?php if(($role == 1) || ($role == 10) || ($role == 11)){?>
                <li><a href="crm_overall_report.php">Overall Report</a></li>                         
                <li><a href="crm_collections_report.php">Collection Report</a></li>                         
                <li><a href="crm_payment_follow_up_report.php">Payment Follow Up</a></li>                         
                <li><a href="crm_payment_no_follow_up_report.php">No Payment Follow Up Update</a></li>                         
                <li><a href="crm_payment_fup_update_report.php">Payment Follow Up Update</a></li>   
                <?php }?>                      
                <li><a href="crm_site_status_summary_report.php">Site Status Summary</a></li>                         
                <li><a href="crm_enquiry_fup_update_report.php">Follow Up Update</a></li>                         
                <li><a href="crm_marketing_report.php">Marketing Report</a></li>
                <li class="divider"></li>
                <li class="dropdown-header"><b class="fa fa-hashtag">&nbsp;</b>Misc.</li>
                <?php if(($role == 1) || ($role == 5)){?>
            <li><a href="crm_add_sales_target.php">Add Sales Target</a></li>
            <?php }?>                      
                <li><a href="crm_add_marketing_expenses.php">Add Marketing Expenses</a></li>
            <li><a href="crm_marketing_expenses_list.php">Marketing Expenses List</a></li>                                              
              </ul>
  					</li>
  				</ul>				
  			</li>
            
            
            
            <li class="dropdown mega-dropdown">
      		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Procurement<span class="caret"></span></a>				
  				<ul class="dropdown-menu mega-dropdown-menu">
                <?php
				
				// Load menu based on user permissions	
				
				$perms_list = i_get_user_perms($user,'Asset','','2','1');	
				
				if($perms_list['status'] == SUCCESS)
					
				{?>
                  <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Asset</li>
                  
					<?php
					
					for($count = 0; $count < count($perms_list['data']); $count++)
						
					{?>
					
                        
						<li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						
						
					<?php
					
					}?>
                  </ul>
                  </li>
                <?php }?>
                
                 <?php $perms_list = i_get_user_perms($user,'Stock Masters','','2','1');					
				if($perms_list['status'] == SUCCESS){?>
                
                
            <li class="col-sm-3">
                <ul>
              
                 <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Stock Masters</li>
                  <?php for($count = 0; $count < count($perms_list['data']); $count++){?>
            <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>
            <?php }?>
                </ul>
            </li>
            <?php }?>
                <?php $perms_list = i_get_user_perms($user,'Stock Transactions','','2','1');					
				if($perms_list['status'] == SUCCESS){?>
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Stock Transactions</li>
                   <?php
					for($count = 0; $count < count($perms_list['data']); $count++)
					{?>
            <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>
            <?php
					}?>                
                </ul>
            </li> 
            <?php }?>   
                
                <?php
				// Load menu based on user permissions				
				$perms_list = i_get_user_perms($user,'Stock Reports','','2','1');					
				if($perms_list['status'] == SUCCESS)
				{?>
                    <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Stock Reports</li>
                <?php
					for($count = 0; $count < count($perms_list['data']); $count++)
					{?>
                        <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>						
					<?php
					}?>
                     </ul>
            </li> 
                    <?php }?>
                </ul>
                
                </li>
 <?php

				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'PM Masters','','2','1');					

				if($perms_list['status'] == SUCCESS)

				{?>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Project Management<span class="caret"></span></a>       
          <ul class="dropdown-menu mega-dropdown-menu">
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-database">&nbsp;</b>Masters</li>
                  <li><a href="project_master_project_management_list.php">Project List</a></li>
                  <li><a href="project_master_process_list.php">Process List</a></li>
                  <li><a href="project_master_task_list.php">Task List</a></li>
                  <li><a href="project_contract_process_list.php">Contract Process List</a></li>
                  <li><a href="project_master_add_contract_rate.php">Contract Rate List</a></li> 
                  <li><a href="project_task_user_mapping_list.php">Task User List</a></li>
                  <li><a href="project_process_user_mapping_list.php">Process User List</a></li>
                  <li><a href="project_master_site_location_mapping_list.php">Project Location Masters</a></li>
                   <li class="divider"></li>
                  <li class="dropdown-header"><b class="fa fa-newspaper-o">&nbsp;</b>Contracts</li>
                  <li><a href="project_task_boq_list.php">Contract Plan List</a></li>
                  <li><a href="project_task_boq_actuals_list.php">Daily Contract Work List</a></li>
                  <li><a href="project_approved_contract_work.php">Daily Approved Contract Work List</a></li>
                  <li><a href="project_actual_contract_payment_list.php">Weekly Contract Payment List</a></li>
                  <li><a href="project_contract_payment_list.php">Issue Contract Payments</a></li>
                  <li><a href="project_accept_contract_payment_list.php">Accounts Accepted Contract Payment List</a></li>
                </ul>
            </li>

            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-users">&nbsp;</b>Manpower</li>
                  <li><a href="project_man_power_estimate_list.php">Manpower Planning</a></li>
                  <li><a href="project_master_man_power_rate_list.php">ManPower Rate List</a></li>
                  <li><a href="project_master_manpower_agency_list.php">ManPower Vendor List</a></li>
                  <li><a href="project_master_man_power_type_list.php">Manpower Type List</a></li>
                  <li><a href="project_task_actual_manpower_list.php">Daily Manpower List</a></li>
                  <li><a href="project_task_approved_actual_manpower_list.php">Daily Approved Manpower List</a></li>
                  <li><a href="project_actual_payment_manpower_list.php">Weekly Manpower Payment List</a></li>
                  <li><a href="project_accept_payment_manpower_list.php">Accounts Accepted Manpower payment List</a></li>
                  <li><a href="project_manpower_payment_list.php">Issues Manpower Payments</a></li>
                 
                </ul>
            </li>
            
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-truck">&nbsp;</b>Machine</li>
                  <li><a href="project_machine_vendor_master_list.php">Machine Vendor List</a></li>

                  <li><a href="project_master_machine_rate_list.php">Machine Rate List</a></li>
                  <li><a href="project_master_machine_type_list.php">Machine Type List</a></li>
                  <li><a href="project_master_machine_list.php">Machine List</a></li>                  
                  <li><a href="project_master_reason_list.php">Reason List</a></li>
                  <li><a href="project_machine_planning_list.php">Machine Planning List</a></li>
                  <li><a href="project_actual_machine_planning_list.php">Daily Machine List</a></li>
                  <li><a href="project_actual_approved_machine_list.php">Daily Approved Machine List</a></li>
                  <li><a href="project_actual_machine_payment_list.php">Weekly Machine Payment List</a></li>
                  <li><a href="project_accept_machine_payment_list.php">Accounts Accepted Machine Payment List</a></li>
                  <li><a href="project_issued_machine_payment_list.php">Issue Machine Payment</a></li>
                                    
                </ul>
            </li>

            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-building">&nbsp;</b>Projects</li>
                  <li><a href="project_completion_report.php">Project Completion Report List</a></li>                  
                  <li><a href="project_pending_task_list.php">Project Pending Task List</a></li>                  
                  <li><a href="dashboard_pause_report.php">Pause Report</a></li>                  
                  <li><a href="dashboard_project_progress.php">Project Progress Report</a></li>                  
                  <li><a href="project_upload_documents.php">Project Method Plan</a></li>                  
                  <li><a href="project_budget_list.php">Project Budget List</a></li>                  
                  <li><a href="project_approved_contract_list.php">Project Approved Contract List</a></li>                  
                  <li><a href="project_approved_machine_list.php">Project Approved Machine List</a></li>                  
                  <li><a href="project_approved_manpower_list.php">Project Approved Manpower List</a></li>                  
                  <li><a href="project_pending_contract_list.php">Project Pending Contract List</a></li>                  
                  <li><a href="project_completed_contract_list.php">Project Completed Contract List</a></li>                  
                  <li><a href="project_stock_module_mapping_list.php">Project Stock Mapping List</a></li>                  
                </ul>
            </li>

            
          </ul>
        </li>
<?php }?>
        <li class="dropdown mega-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stock<span class="caret"></span></a>       
          <ul class="dropdown-menu mega-dropdown-menu">
            <?php $perms_list = i_get_user_perms($user,'Stock Masters','','2','1');					
				if($perms_list['status'] == SUCCESS){?>
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-shopping-basket">&nbsp;</b>Masters</li>
                   <?php for($count = 0; $count < count($perms_list['data']); $count++){?>
            <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>
            <?php }?>
                </ul>
            </li>
            <?php }?>
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Transactions</li>
                  <li><a href="stock_indent_list.php">Indents</a></li>
                  <li><a href="stock_indent_item_for_approval.php">Indent Approvals</a></li>
                  <li><a href="stock_indent_issue_list.php">Indent Issue</a></li>
                  <li><a href="stock_indent_items_approved.php">Quotation Process</a></li>
                  <li><a href="stock_quotation_for_approval.php">Quotation Approvals</a></li>
                  <li><a href="stock_approved_quotations.php">Purchase Order Process</a></li>
                  <li><a href="stock_po_items_approval.php">Purchase Order Approvals</a></li>
                  <li><a href="stock_po_items_approved.php">Purchase Order Issue</a></li>
                  <li><a href="stock_grn_list.php">Goods Receipt Note</a></li>
                  <li><a href="stock_grn_engineer_inspection_list.php">GRN Inspection</a></li>
                  <li><a href="stock_move_to_payments_list.php">Accounts Approval</a></li>                  
                </ul>
            </li>
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Transactions</li>
                  <li><a href="#">WIP PO</a></li>
                  <li><a href="#">Pending Grn Inspection</a></li>
                  <li><a href="#">Moved Payment List</a></li>
                  <li><a href="#">Move to Management</a></li>
                  <li><a href="#">Management Payments List</a></li>
                  <li><a href="#">Release Payment</a></li>
                  <li><a href="#">Released Payment List</a></li>
                  <li><a href="#">Issue Payment</a></li>
                </ul>
            </li>
             <?php
				// Load menu based on user permissions				
				$perms_list = i_get_user_perms($user,'Stock Reports','','2','1');					
				if($perms_list['status'] == SUCCESS)
				{?>
            <li class="col-sm-3">
                <ul>
                  <li class="dropdown-header"><b class="fa fa-bar-chart">&nbsp;</b>Reports</li>
                   <?php
					for($count = 0; $count < count($perms_list['data']); $count++)
					{?>
            <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>
            <?php
					}?>
                </ul>
            </li>
            <?php }?>
          </ul>
        </li>

        <li class="dropdown mega-dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">HR<span class="caret"></span></a>
  <ul class="dropdown-menu mega-dropdown-menu" >
    <li class="col-sm-3">
      <ul>
        <li class="dropdown-header"><b class="fa fa-shopping-basket">&nbsp;</b>Current HR Menu</li>
         <li><a href="hr_attendance_report.php">Attendance Report</a></li>
            <li><a href="hr_add_absence_details.php">Apply For leave</a></li>
            <li><a href="hr_leave_summary.php">Leave Summary</a></li>
            <li><a href="hr_leaves_list_self.php">My Leaves</a></li>
            <li><a href="hr_leaves_list_reportees.php">My Reportees' Leaves</a></li>
            <li><a href="hr_add_out_pass_request.php">Apply for In/Out Pass</a></li>
            <li><a href="hr_out_pass_self.php">My In/Out Pass</a></li>
            <li><a href="hr_out_pass_reportees.php">Reportees' In/Out Pass</a></li>
            <li><a href="hr_holiday_list.php">Public Holiday List</a></li>
            <?php

						if(($role == "13") || ($user == "143620071466608200"))

						{

						?>
            <li><a href="hr_employee_list.php">Employee List</a></li>
            <li><a href="hr_add_attendance.php">Import Attendance</a></li>
            <li><a href="hr_attendance_export.php">Export Attendance</a></li>
            <li><a href="hr_admin_add_casual_leaves.php">Add Opening Casual Leaves</a></li>
            <li><a href="hr_admin_add_sick_leaves.php">Add Opening Sick Leaves</a></li>
            <li><a href="hr_admin_edit_casual_leaves.php">Edit Opening Casual Leaves</a></li>
            <li><a href="hr_admin_edit_sick_leaves.php">Edit Opening Sick Leaves</a></li>
            <li><a href="hr_admin_add_comp_off.php">Add Opening Comp Off</a></li>
            <li><a href="hr_add_holiday.php">Add Public Holiday</a></li>
            <?php

						}

						?>
      </ul>
    </li>
    <li class="col-sm-3">
      <ul>
        <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>General Tasks</li>
       
                <li><a href="add_general_task.php">Add General Task</a></li>
                <li><a href="general_pending_task_list.php">Pending Task List</a></li>
                <li><a href="general_completed_task_list.php">Completed Task List</a></li>
                <li><a href="general_pending_task_list_assigner.php">Tasks Assigned By Me</a></li>
                <li><a href="general_task_consolidated_report.php">Consolidated Report</a></li>
                <li><a href="general_task_summary.php">Task Summary</a></li>
                <?php if($role == 1){ ?><li><a href="add_department_master.php">Department Master</a></li>
                <li><a href="add_general_task_type_master.php">General Task Type master</a></li> <?php }?>
      </ul>
    </li>
       <?php

				// Load menu based on user permissions				

				$perms_list = i_get_user_perms($user,'Users','','2','1');					

				if($perms_list['status'] == SUCCESS)

				{?>  
    <li class="col-sm-3">
      <ul>
        <li class="dropdown-header"><b class="fa fa-inr">&nbsp;</b>Users</li>
        <?php

					for($count = 0; $count < count($perms_list['data']); $count++)

					{?>
            <li><a href="<?php echo $perms_list['data'][$count]['functionality_master_url']; ?>"><?php echo $perms_list['data'][$count]['functionality_master_function']; ?></a></li>
            <?php

					}?>
      </ul>
    </li>
    <?php }?>
    <li class="col-sm-3">
      <ul>
        
        <li class="dropdown-header"><b class="fa fa-handshake-o">&nbsp;</b>Meeting</li>
                <li><a href="add_meeting.php">Add Meeting</a></li>
                <li><a href="scheduled_meeting_list.php">Scheduled Meeting List</a></li>                         
             <?php if($role == 1){ ?>   <li><a href="meeting_report.php">Meeting Report</a></li>   <?php }?>
      </ul>
    </li>
  </ul>
</li>


             

  		</ul>
          <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">My account <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a  href="javascript:;">Help</a></li>
              <li><a href="#">Profile</a></li>
              <li class="divider"></li>
              <li><a href="out.php" style="color: red;">Logout</a></li>
            </ul>
          </li>
        </ul>        
  	</div><!-- /.nav-collapse -->
    </div>
    </nav>
  </div>
  </div>
<!-- /navbar -->
<div style=" margin:50px auto;"></div>