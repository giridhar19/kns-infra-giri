<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_doc_list.php
CREATED ON	: 13-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of BD documents of a file
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/$_SESSION['module'] = 'BD';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data	
	// Nothing here
	
	// Temp data
	$alert = "";
	
	// Search Filter Data
	if(isset($_POST['search_case_submit']))
	{
		$fup_date_start = $_POST['dt_fup_date_start'];		
		$fup_date_end   = $_POST['dt_fup_date_end'];				
	}
	else
	{
		$fup_date_start = '';
		$fup_date_end   = '';
	}
	
	// Get list of court cases
	$case_fup_data = array();
	if($fup_date_start != '')
	{
		$case_fup_data['fup_date_start'] = $fup_date_start;
	}
	if($fup_date_end != '')
	{
		$case_fup_data['fup_date_end'] = $fup_date_end;
	}
	
	$court_case_list = i_get_court_fup_latest($case_fup_data);
	if($court_case_list["status"] == SUCCESS)
	{
		$court_case_list_data = $court_case_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$court_case_list["data"];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>BD Court Case Follow Up List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Court Case Follow Up List</h3>	  
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="case_search_form" action="bd_court_case_fup_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_fup_date_start" value="<?php echo $fup_date_start; ?>" />
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_fup_date_end" value="<?php echo $fup_date_end; ?>" />
			  </span>
			  <input type="submit" name="search_case_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Case Type</th>
					<th style="word-wrap:break-word;">Case Number</th>
					<th style="word-wrap:break-word;">Survey Number</th>
					<th style="word-wrap:break-word;">Village</th>
					<th style="word-wrap:break-word;">Filing Date</th>
					<th style="word-wrap:break-word;">Plaintiff</th>
					<th style="word-wrap:break-word;">Defendant</th>
					<th style="word-wrap:break-word;">Jurisdiction</th>
					<th style="word-wrap:break-word;">Case Status</th>															
					<th style="word-wrap:break-word;">Last Updated By</th>						
					<th style="word-wrap:break-word;">Hearing Date</th>	
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Follow Up</th>																
				</tr>
				</thead>
				<tbody>
				 <?php
				if($court_case_list["status"] == SUCCESS)
				{				
					$sl_no = 0;
					for($count = 0; $count < count($court_case_list_data); $count++)
					{									
						$sl_no++;			

						if((strtotime($court_case_list_data[$count]['legal_court_case_follow_up_date'])) < strtotime(date('Y-m-d')))
						{
							$style = 'style="color:red;"';
						}
						else
						{
							$style = '';
						}
					?>
					<tr <?php echo $style; ?>>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["bd_court_case_type_master_type"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_survey_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($court_case_list_data[$count]["legal_court_case_date"])); ?></td>						
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_plaintiff"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["legal_court_case_diffident"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["bd_court_establishment_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]['bd_court_status_name']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $court_case_list_data[$count]["user_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($court_case_list_data[$count]['legal_court_case_follow_up_date'])); ?></td>
						<td style="word-wrap:break-word;"><a href="#" onclick="alert('Year: <?php echo $court_case_list_data[$count]["legal_court_case_year"].'\n\n'.$court_case_list_data[$count]["legal_court_case_details"]; ?>');">View</a></td>
						<td style="word-wrap:break-word;"><a href="bd_court_case_fup.php?case=<?php echo $court_case_list_data[$count]["legal_court_case_id"]; ?>" target="_blank">Follow Up</a></td>						
						</tr>
					<?php 							
					}
				}
				else
				{
				?>
				<td colspan="14">No Court Case added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>
