<?php
/* Database connection information */
function get_conn_handle()
{
  $db     = 'kns_erp_live';
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';

	try
	{
		$db_conn_handle = new PDO('mysql:host='.$dbhost.';dbname='.$db, $dbuser, $dbpass);
  }
    catch (PDOException $e)
	{
		$db_conn_handle = DB_CONN_FAILURE;
		$error = $e->getMessage();
	}
  return $db_conn_handle;
}

function filter_array_data($array, $key){
  $temp_array = array();
  $i = 0;
  $key_array = array();

  foreach($array as $val) {
      if (!in_array($val[$key], $key_array)) {
          $key_array[$i] = $val[$key];
          $temp_array[$val[$key]] = $val;
      }
      $i++;
  }
  return $temp_array;
}

$mysqli = get_conn_handle();

$query_for_project_process_task = 'SELECT DISTINCT project_task_master_name,project_task_master_id,project_process_task_id,project_process_task_location_id from project_plan_process_task PPPT inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type where project_process_id =833 and  project_process_task_active=1';
// $query_for_project_process_task = 'SELECT project_task_master_name,project_process_task_id,project_process_task_location_id from project_plan_process_task PPPT inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type where project_process_id = 658 and  project_process_task_active=1';
$query_for_project_process_task_results = $mysqli->query($query_for_project_process_task);
$query_for_project_process_task_data = $query_for_project_process_task_results->fetchAll();
$task_count = count($query_for_project_process_task_data);
$query_for_project_process_task_data = filter_array_data($query_for_project_process_task_data, 'project_task_master_id');
// print_r($query_for_project_process_task_data);
$total_avg_percentage = 0 ;
$road_items = array();
$task_planned_start_date = "";
$task_planned_end_date = "";
$task_actual_start_date = "";
$task_actual_end_date = "";
$task_name = array();
?>
<script>
  console.log('data ', <?= json_encode($query_for_project_process_task_data); ?>);
</script>
<?php
foreach ($query_for_project_process_task_data as $value) {
  $query_for_project_process_task_road = 'SELECT project_task_planning_task_id,project_task_planning_no_of_roads,project_site_location_mapping_master_name from task_planning PPPT where project_task_planning_task_id ='.$value["project_process_task_id"].' and  project_task_planning_active=1';
  $query_for_project_process_task_road_results = $mysqli->query($query_for_project_process_task_road);
  $query_for_project_process_task_road_data = $query_for_project_process_task_road_results->fetchAll();
  $road_date_array = array();
  $road_dates = array();
  for($rcount=0 ; $rcount < count($query_for_project_process_task_road_data) ; $rcount++)
  {
    $query_for_project_process_task_road_shadow = 'SELECT * from project_task_planning_shadow where project_task_planning_shadow_task_id ='.$value["project_process_task_id"].' and  project_task_planning_shadow_road_id='.$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"];
    $query_for_project_process_task_road_shadow_results = $mysqli->query($query_for_project_process_task_road);
    $query_for_project_process_task_road_shadow_data = $query_for_project_process_task_road_shadow_results->fetchAll();
    for($r=0 ; $r < count($query_for_project_process_task_road_shadow_data) ; $r++) {
    $road_dates = array('start_date' => $query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_new_start_date"],
                        'end_date'=>$query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_new_end_date"]);
  }
}
}
?>
