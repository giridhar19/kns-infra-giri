<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Data initialization
	$alert_type = -1;
	$alert      = "";
	
	/* QUERY STRING - START */
	if(isset($_GET["employee_id"]))
	{
		$hr_comp_off_employee = $_GET["employee_id"];
	}	
	else
	{
		$hr_comp_off_employee = "-1";
	}
	/* QUERY STRING - END */
	
	$self_employee_details = t_get_comp_off($hr_comp_off_employee);
	{
		if($self_employee_details["status"]==SUCCESS)
		{
			$self_employee_details_data=$self_employee_details["data"];
		}
	}

	// Get list of Comp offs already availed by this employee
	$fd_leave_filter_data = array('employee_id'=>$hr_comp_off_employee,'type'=>LEAVE_TYPE_COMP_OFF,'status'=>'1','duration' => '1');
	$comp_off_availed_list = i_get_leave_list($fd_leave_filter_data);
	if($comp_off_availed_list['status'] != SUCCESS)
	{
		$co_availed_count = 0;
	}
	else
	{
		$co_availed_count = count($comp_off_availed_list['data']);
	}
	
	$hd_leave_filter_data = array('employee_id'=>$hr_comp_off_employee,'type'=>LEAVE_TYPE_COMP_OFF,'status'=>'1','duration' => '2');
	$comp_off_availed_list = i_get_leave_list($hd_leave_filter_data);
	if($comp_off_availed_list['status'] != SUCCESS)
	{
		$co_availed_count = $co_availed_count + 0;
	}
	else
	{
		$co_availed_count = $co_availed_count + (count($comp_off_availed_list['data']));
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Comp off List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Comp off</h3>
            </div>
			
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Employee Code</th>
					<th>Employee Name</th>					
					<th>Worked Date</th>
					<th>Comp off Type</th>
					<th>Used Status</th>
				</tr>
				</thead>
				<tbody>
				<?php				
				if($self_employee_details["status"]==SUCCESS)
				{				
					for($count = 0; $count < count($self_employee_details_data); $count++)
					{
						if($co_availed_count > $count)
						{
							$availed = 'YES';
						}
						else
						{
							$availed = 'NO';
						}
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $self_employee_details_data[$count]["hr_employee_code"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $self_employee_details_data[$count]["hr_employee_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($self_employee_details_data[$count]["hr_attendance_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php if($self_employee_details_data[$count]["hr_comp_off_type"] == '1')
						{
							echo 'Full Day';
						}
						else
						{
							echo 'Half Day';
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $availed; ?></td>
					</tr>	
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="4">No leave applications!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->

<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
 
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
