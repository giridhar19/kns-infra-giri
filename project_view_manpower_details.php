<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

    $alert_type = -1;
    $alert = "";
if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    // Nothing

    if (isset($_GET['payment_manpower_id'])) {
        $payment_id = $_GET['payment_manpower_id'];
    } else {
        $payment_id = "";
    }

    $search_project   	 = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    }

    $search_vendor   	 = "";

    if (isset($_POST["search_vendor"])) {
        $search_vendor   = $_POST["search_vendor"];
    }


    // Temp data
    $project_payment_manpower_mapping_search_data = array("payment_id"=>$payment_id,"active"=>"1");
    $man_power_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);

    if ($man_power_list["status"] == SUCCESS) {
        $man_power_list_data = $man_power_list["data"];
    } else {
        $alert = $alert."Alert: ".$man_power_list["data"];
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task Actual Man Power List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_view_manpower_details.php">
			  <input type="hidden" name="payment_manpower_id" value="<?php echo $payment_id; ?>" />

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
              }
              ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task Name</th>
					<th>Road Name</th>
					<th>Msmrt</th>
					<th>UOM</th>
					<th>Date</th>
					<th>Agency</th>
					<th>Men Hrs</th>
					<th>Women Hrs</th>
					<th>Mason Hrs</th>
					<th>Others Hrs</th>
					<th>No of People</th>
					<th>Amount</th>

				</tr>
				</thead>
				<tbody>
				<?php
                if ($man_power_list["status"] == SUCCESS) {
                    ?>
					<input type="hidden" name="hd_quote_count" value="<?php echo count($man_power_list_data); ?>" />
					<?php
                    $sl_no = 0;
                    $man_power_men_rate = 0;
                    $man_power_women_rate = 0;
                    $man_power_mason_rate = 0;
                    $total_cost = 0;
                    for ($count = 0; $count < count($man_power_list_data); $count++) {
                        if (($man_power_list_data[$count]["project_task_actual_manpower_road_id"] != "No Roads")) {
                            $road_name = $man_power_list_data[$count]["project_site_location_mapping_master_name"];
                        } else {
                            $road_name = "No Roads";
                        }
                        $get_details_data = i_get_details('', $man_power_list_data[$count]["project_task_actual_manpower_task_id"], $man_power_list_data[$count]["project_task_actual_manpower_road_id"]);
                        $uom = $get_details_data["uom"];
                        $project_payment_manpower_mapping_search_data = array("manpower_id"=>$man_power_list_data[$count]["project_task_actual_manpower_id"]);
                        $payment_manpower_mapping_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
                        if ($payment_manpower_mapping_list["status"] == SUCCESS) {
                            $payment_manpower = true;
                        } else {
                            $payment_manpower = false;
                        }

                        $man_power_men_rate = $man_power_list_data[$count]["project_task_actual_manpower_men_rate"];
                        $man_power_women_rate = $man_power_list_data[$count]["project_task_actual_manpower_women_rate"];
                        $man_power_mason_rate = $man_power_list_data[$count]["project_task_actual_manpower_mason_rate"];
                        $total_men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
                        $total_women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
                        $total_mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
                        $total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;
                        $men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
                        $women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
                        $mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

                        $no_of_men = $men_hrs/8;
                        $no_of_women = $women_hrs/8;
                        $no_of_mason = $mason_hrs/8;

                        $total_hrs = $men_hrs + $women_hrs + $mason_hrs;
                        $no_of_people = $total_hrs/8; ?>
					<input type="hidden" name="hd_man_rate_<?php echo $count; ?>" value="<?php echo $man_power_men_rate; ?>" />
					<input type="hidden" name="hd_women_rate_<?php echo $count; ?>" value="<?php echo $man_power_women_rate; ?>" />
					<input type="hidden" name="hd_mason_rate_<?php echo $count; ?>" value="<?php echo $man_power_mason_rate; ?>" />

					<?php
                        $sl_no++; ?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_master_name"]; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_process_master_name"]; ?></td>
						<td><?php echo $man_power_list_data[$count]["project_task_master_name"]; ?></td>
						<td><?php echo $road_name; ?></td>
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_completed_msmrt"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $uom ; ?></td>

						<input type="hidden" name="hd_manpower_date_<?php echo $count; ?>" value="<?php echo date("d-M-Y", strtotime($man_power_list_data[$count][
                        "project_task_actual_manpower_date"])); ?>" />
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($man_power_list_data[$count][
                        "project_task_actual_manpower_date"])); ?></td>

						<input type="hidden" name="hd_vendor_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_agency"]; ?>" />
						<td><?php echo $man_power_list_data[$count]["project_manpower_agency_name"]; ?></td>

						<input type="hidden" name="hd_man_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"]; ?>
					  &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_men_rate ; ?>
					  Men :<?php echo $no_of_men; ?></td>

						<input type="hidden" name="hd_women_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"]; ?>
					&nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_women_rate ; ?> Women :<?php echo $no_of_women; ?></td>

						<input type="hidden" name="hd_mason_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?>" />
						<td style="word-wrap:break-word;"><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"]; ?>
					 &nbsp;&nbsp;&nbsp; Rate : <?php echo $man_power_mason_rate ; ?> Mason :<?php echo $no_of_mason ; ?></td>

						<input type="hidden" name="hd_others_count_<?php echo $count; ?>" value="<?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_others"]; ?>" />
						<td><?php echo $man_power_list_data[$count]["project_task_actual_manpower_no_of_others"]; ?></td>

						<td style="word-wrap:break-word;"><?php echo $no_of_people ; ?></td>

						<input type="hidden" name="hd_total_<?php echo $count; ?>" value="<?php echo $total_cost ; ?>" />
						<td><?php echo $total_cost ; ?></td>
					</tr>
					<?php
                    }
                } else {
                    ?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
                }
                 ?>

                </tbody>
              </table>

            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_actual_manpower(man_power_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function project_approve_task_actual_manpower(man_power_id,task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_man_power.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" +task_id+ "&action=approved");
		}
	}
}
</script>

  </body>

</html>
