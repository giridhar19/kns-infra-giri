<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */$_SESSION['module'] = 'HR';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_out_pass_submit"]))
	{
		$employee    = $_POST["hd_employee"];
		$date        = $_POST["dt_out_pass_date"];
		$out_time    = $_POST["num_out_time_hrs"].':'.$_POST["num_out_time_mins"];
		$finish_time = $_POST["num_finish_time_hrs"].':'.$_POST["num_finish_time_mins"];
		$remarks     = $_POST["txt_remarks"];
		$type        = $_POST["rd_type"];
		
		// Check for mandatory fields
		if(($date !="") && ($out_time !="") && ($finish_time !="") && ($type !=""))
		{		
			if((($_POST["num_out_time_hrs"]*60) + $_POST["num_out_time_mins"]) <= (($_POST["num_finish_time_hrs"]*60) + $_POST["num_finish_time_mins"]))
			{
				$out_pass_iresult = i_add_out_pass_request($employee,$date,$out_time,$finish_time,$remarks,$type,$user);
				
				if($out_pass_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
					$alert      = $alert."Your out pass request has been submitted for manager's approval";
				}
				else
				{
					$alert_type = 0;
					$alert      = $alert.$out_pass_iresult["data"];
				}	
			}
			else
			{
				$alert_type = 0;
				$alert      = 'Out Pass Start time cannot be greater than Out Pass Finish time.';
			}
		}
		else
		{
			$alert      = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of employees
	$employee_filter_data = array("employee_user"=>$user,"user_status"=>'1');
	$employee_list = i_get_employee_list($employee_filter_data);
	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Out Pass Request</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>HR</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Out Pass Request</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="apply_for_out_pass_form" class="form-horizontal" method="post" action="hr_add_out_pass_request.php">
								<input type="hidden" name="hd_employee" value="<?php echo $employee_list_data[0]["hr_employee_id"]; ?>" />
									<fieldset>										
													
										<div class="control-group">											
											<label class="control-label" for="dt_out_pass_date">Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_out_pass_date" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_out_time_hrs">In/Out Time*</label>
											<div class="controls">
												<input type="number" class="span3" name="num_out_time_hrs" min="0" max="23" step="1" pattern="\d*" required>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<input type="number" class="span3" name="num_out_time_mins" min="0" max="59" step="1" pattern="\d*" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="num_finish_time_hrs">Finish Time*</label>
											<div class="controls">
												<input type="number" class="span3" name="num_finish_time_hrs" min="0" max="23" step="1" pattern="\d*" required>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<input type="number" class="span3" name="num_finish_time_mins" min="0" pattern="\d*" max="59" step="1" required>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea class="span6" name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_type">Type*</label>
											<div class="controls">
												<input type="radio" name="rd_type" value="0" checked>&nbsp;&nbsp;&nbsp;Personal
												&nbsp;&nbsp;&nbsp;<input type="radio" name="rd_type" value="1">&nbsp;&nbsp;&nbsp;On Duty
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_out_pass_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>
