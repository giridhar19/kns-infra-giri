<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$boq_id    		  = $_POST["boq_id"];
	$task_id     	  = $_POST["task_id"];
	$active           = $_POST["action"];
	$checked_by_by    = $user;
	$checked_on       = date("Y-m-d H:i:s");
	
	$project_task_actual_boq_update_data = array("status"=>'Approved',"approved_by"=>$user,"approved_on"=>$checked_on);
	$delete_project_man_power_result = i_update_project_task_boq_actual($boq_id,$project_task_actual_boq_update_data);
	
	if($delete_project_man_power_result["status"] == FAILURE)
	{
		echo $delete_project_man_power_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>