<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10th Aug 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');

/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}	
	else
	{
		$file_id = "";
	}
	if(isset($_GET["payment_file_id"]))
	{
		$payment_file_id = $_GET["payment_file_id"];
	}	
	else
	{
		$payment_file_id = "";
	}
	/* QUERY STRING - END */
	// Get list of file payments
	$issued_amount = 0;
	$file_payment_list = i_get_file_payment_list($file_id,'');
	if($file_payment_list["status"] == SUCCESS)
	{
		$file_payment_list_data = $file_payment_list["data"];
		for($issue_count  = 0; $issue_count < count($file_payment_list_data); $issue_count++)
		{
			$issued_amount = $issued_amount + $file_payment_list_data[$issue_count]["file_payment_amount"];
		}
	}
	
	// Get list of files
	$bd_file_list = i_get_bd_files_list($payment_file_id,'','','','','','','1');

	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
		$land_cost = $bd_file_list_data[0]["bd_file_land_cost"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
		$land_cost = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_file_payment_request_submit"]))
	{
		$file_id    = $_POST["file_id"];
		$bd_file_id = $_POST["hd_bd_file_id"];
		$amount	    = $_POST["num_amount"];
		$reason     = $_POST["ddl_pay_type"];			
		$remarks    = $_POST["txt_remarks"];
		$payable_to = $_POST["stxt_done_to"];		
		$added_by   = $user;
		
		// Check for mandatory fields
		if(($file_id !="") && ($amount !="") && ($reason !=""))
		{
			$add_file_pay_request_result = i_add_bd_payment_request($bd_file_id,$amount,$reason,$remarks,$payable_to,$added_by);
			if($add_file_pay_request_result["status"] == SUCCESS)
			{
				$alert = $add_file_pay_request_result["data"];
				$alert_type = 1;
				
				$update_history_iresult = i_add_bd_payment_history($add_file_pay_request_result['data'],'1',$remarks,$added_by);
				if($update_history_iresult['status'] == SUCCESS)
				{
					$alert_type = 1;
					$alert      = 'Your request was sent to HOD for approval';
				}
				else
				{
					$alert_type = 0;
					$alert      = 'Your request was sent to HOD for approval. However, there was an issue in adding your request. Please contact the admin';
				}
			}
			else
			{
				$alert = $add_file_pay_request_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}	

// Get corresponding legal file details
$file_details = i_get_file_list('','','','','','','','','','',$file_id,'','','');
if($file_details['status'] == SUCCESS)
{
	$file_data = $file_details['data'];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Payment Request</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>  
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>File No: <?php echo $file_data[0]['bd_project_file_id']; ?>&nbsp;&nbsp;&nbsp;Project: <?php echo $file_data[0]['bd_project_name']; ?>&nbsp;&nbsp;&nbsp;Survey No: <?php echo $file_data[0]['bd_file_survey_no']; ?>&nbsp;&nbsp;&nbsp;Extent: <?php echo $file_data[0]['bd_file_extent']; ?> guntas &nbsp;&nbsp;&nbsp;&nbsp;Land cost : <?php echo $land_cost ;?>&nbsp;&nbsp;&nbsp;&nbsp;Issued Payment : <?php echo $issued_amount ; ?> </h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Make File Payment Request</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_file_form" class="form-horizontal" method="post" action="add_file_payment_request.php">
									<fieldset>
										<input type="hidden" name="file_id" value="<?php echo $file_id; ?>" />
										<input type="hidden" name="hd_bd_file_id" value="<?php echo $file_data[0]['bd_project_file_id']; ?>" />
										
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount *</label>
											<div class="controls">
												<input type="number" step="0.01" min="0" onkeyup="return check_validity('<?php echo $land_cost ;?>','<?php echo $issued_amount ;?>');" class="span6" name="num_amount" id="num_amount" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_pay_type">Payment Type *</label>
											<div class="controls">
												<select name="ddl_pay_type" class="span6" required>
												<option value="1">Land Lord Payment</option>
												<option value="2">Change of Land use Fee</option>
												<option value="3">Conversion Fee</option>
												<option value="4">Brokerage Amount</option>
												<option value="5">Stamp Duty</option>
												<option value="6">Registration Fee</option>
												<option value="7">Other Expenses</option>																				
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_done_to">Payable To *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_done_to" required="required" value="<?php echo $file_data[0]['bd_file_owner']; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_file_payment_request_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function check_validity(land_cost,issued_amount)
{
	var amount = parseInt(document.getElementById('num_amount').value);
	var land_cost1 = parseInt(land_cost);
	var issued_amount1 = parseInt(issued_amount);
	var total = (amount + issued_amount1);
	if(total > land_cost)
	{
		document.getElementById('num_amount').value = 0;
		alert('Cannot release greater than total value');
	}
}
</script>
  </body>

</html>