<?php
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	/** Include PHPExcel_IOFactory */
	require_once dirname(__FILE__) . '/../Legal/utilities/PHPExcel-1.8/Classes/PHPExcel.php';

	$filename = "./sample_budget_plan_for_import.xlsx";
	if (!file_exists($filename)) {
		exit("Source file not found\n");
	}

	$type = PHPExcel_IOFactory::identify($filename);
	$objReader = PHPExcel_IOFactory::createReader($type);
	$objPHPExcel = $objReader->load($filename);

	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
	    $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
	}
	echo "<pre>";
	print_r($worksheets);
