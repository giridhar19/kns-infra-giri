<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_APPROVED_CONTRACT_FUNC_ID', '287');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', PROJECT_APPROVED_CONTRACT_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', PROJECT_APPROVED_CONTRACT_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', PROJECT_APPROVED_CONTRACT_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_APPROVED_CONTRACT_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_APPROVED_CONTRACT_FUNC_ID, '6', '1');
?>
<script>
  window.permissions = {
    view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
    aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
  }
</script>
<?php
    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_contract = "";

    if (isset($_POST["search_contract"])) {
        $search_contract   = $_POST["search_contract"];
    // set here
    } else {
        $search_contract = "";
    }

    if (isset($_POST["search_task"])) {
        $search_task = $_POST["search_task"];
    // set here
    } else {
        $search_task = "";
    }

    $search_vendor = "";
    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor = $_REQUEST["search_vendor"];
    }

    $start_date = "";
    if (isset($_REQUEST["dt_start_date"])) {
        $start_date = $_REQUEST["dt_start_date"];
    }

    $end_date = "";
    if (isset($_REQUEST["dt_end_date"])) {
        $end_date = $_REQUEST["dt_end_date"];
    }

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    $project_contract_rate_master_search_data = array("active"=>'1',"vendor_id"=>$search_vendor,"process"=>$search_contract);
    $project_contract_rate_master_list = i_get_project_contract_rate_master($project_contract_rate_master_search_data);
    if ($project_contract_rate_master_list["status"] == SUCCESS) {
        $project_contract_rate_master_list_data = $project_contract_rate_master_list["data"];
    } else {
        $alert = $project_contract_rate_master_list["data"];
        $alert_type = 0;
    }

    // Get Project Contract Process modes already added
    $project_contract_process_search_data = array("active"=>'1');
    $project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
    if ($project_contract_process_list['status'] == SUCCESS) {
        $project_contract_process_list_data = $project_contract_process_list["data"];
    } else {
        $alert = $project_contract_process_list["data"];
        $alert_type = 0;
    }
    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_vendor_master_list["status"] == SUCCESS) {
        $project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"project_id"=>$search_project,"user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Project Daily Contract Work List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>

     <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
     <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
		 <script src="datatable/project_approved_contract_list_datatable.js?<?php echo time(); ?>"></script>
  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>

    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Daily Contract List</h3>
                  </div>

                  <div class="widget-header widget-toolbar">
                      <form class="form-inline" method="post" id="file_search_form">

                      <select id="search_vendor" name="search_vendor" class="form-control input-sm" style="max-width: 150px;">
                      <option value="">- - Select Vendor - -</option>
                      <?php
                                      for ($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++) {
                                          ?>
                      <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]) {
                                              ?> selected="selected" <?php
                                          } ?>><?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
                      <?php
                                      } ?>
                      </select>
                      <select id="search_project" name="search_project" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Project - -</option>
                        <?php
                                        for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                            ?>
                        <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                                ?> selected="selected" <?php
                                            } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
                        <?php
                                        } ?>
                        </select>
                        <select id="search_contract" name="search_contract" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Contract Process - -</option>
                        <?php
                              for ($process_count = 0; $process_count < count($project_contract_process_list_data); $process_count++) {
                                  ?>
                			  <option value="<?php echo $project_contract_process_list_data[$process_count]["project_contract_process_id"]; ?>" <?php if ($search_contract == $project_contract_process_list_data[$process_count]["project_contract_process_id"]) {
                                      ?> selected="selected" <?php
                                  } ?>><?php echo $project_contract_process_list_data[$process_count]["project_contract_process_name"]; ?></option>
                			  <?php
                              } ?>
                            </select>
                            <select id="search_task" name="search_task" class="form-control input-sm" style="max-width: 150px;">
                              <option value="">- - Select Contract Task - -</option>
                              <?php
                                    for ($work_count = 0; $work_count < count($project_contract_rate_master_list_data); $work_count++) {
                                        ?>
                      			  <option value="<?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]; ?>" <?php if ($search_task == $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_id"]) {
                                            ?> selected="selected" <?php
                                        } ?>><?php echo $project_contract_rate_master_list_data[$work_count]["project_contract_rate_master_work_task"]; ?></option>
                      			  <?php
                                    } ?>
                            </select>
                      <input type="date" id="start_date" name="dt_start_date" value="<?php echo $start_date; ?>"  class="form-control input-sm"/>
                      <input type="date" id="end_date" name="dt_end_date" value="<?php echo $end_date; ?>"  class="form-control input-sm"/>
                      <button type="button" class="btn btn-primary btn-sm" onclick="redrawTable()">Submit</button>
                      </form>
                    </div>
                  </div>
                <div class="widget-content" style="margin-top:15px;">
                <table id="example" class="table table-striped table-bordered display nowrap">
                  <thead>
                <tr>
                <th>#</th>
                <th>Agency</th>
                <th>Project</th>
                <th>Process</th>
                <th>Task Name</th>
                <th>UOM</th>
                <th>Work Type</th>
                <th>Road Name</th>
                <th>Contract Process</th>
                <th>Contract Task</th>
                <th>Location </th>
                <th>Number</th>
                <th>Length</th>
                <th>Breadth</th>
                <th>Depth</th>
                <th>Today Measurement</th>
                <th>%</th>
                <th>Rate</th>
                <th>Total</th>
                <th>Remarks</th>
                <th>Added By</th>
                <th>Added On</th>
                <th>Date</th>
                </tr>
                </thead>
                  </tbody>
                </table>
              </div>
            </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      <!-- </div> -->
      <!-- /container -->
    <!-- </div> -->

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
