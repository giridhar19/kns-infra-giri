<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: general_task_summary.php
CREATED ON	: 08-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans - Brief Summary
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Temp data
	$alert = "";
	
	// Initialize
	$search_task_type  = "";
	$search_status     = "";
	$search_user       = "";
	$search_department = "";
	$search_assigner   = "";
	
	// Form Data
	if(isset($_POST["task_search_submit"]))
	{
		$search_task_type  = $_POST["search_task_type"];
		$search_status     = $_POST["search_status"];
		$search_user       = $_POST["search_user"];
		$search_assigner   = $_POST["search_assigner"];
		$search_department = $_POST["search_department"];
	}

	if($search_user != "")
	{
		$task_user = $search_user;
	}
	else
	{
		if($role != "1")
		{
			$task_user = $user;
		}
		else
		{
			$task_user = "";
		}
	}
	
	if($search_assigner == "")
	{
		if($role != "1")
		{
			$search_assigner = '';
		}
		else
		{
			$search_assigner = $user;
		}
	}
	
	if($search_assigner == "-1")
	{
		$search_assigner = "";
	}

	$general_task_plan_list = i_get_gen_task_plan_list('',$search_task_type,$task_user,$search_department,'','','',$search_assigner,$search_status);
	if($general_task_plan_list["status"] == SUCCESS)
	{
		$general_task_plan_list_data = $general_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$general_task_plan_list["data"];
	}	
	
	// Get task type list
	$gen_task_type_list = i_get_gen_task_type_list('','1');
	if($gen_task_type_list["status"] == SUCCESS)
	{
		$gen_task_type_list_data = $gen_task_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$gen_task_type_list["data"];
		$alert_type = 0; // Failure
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get department list
	$department_list = i_get_department_list('','1');
	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
		$alert_type = 0; // Failure
	}	
	
	// Get user list
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>General Tasks - Summary</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>General Tasks - Consolidated Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of Tasks: <?php if($general_task_plan_list["status"] == SUCCESS)
			  {
				echo count($general_task_plan_list_data);
			  }
			  else
			  {
				echo '0';
			  }?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  <a href="add_general_task.php">Add Task</a></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
			<div class="widget-header" style="height:84px; padding-top:10px;">               
			  <form method="post" id="task_search_form" action="general_task_summary_other.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task_type">
			  <option value="">- - Select Task Type - -</option>
			  <?php
			  for($count = 0; $count < count($gen_task_type_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $gen_task_type_list_data[$count]["general_task_type_id"]; ?>" <?php if($search_task_type == $gen_task_type_list_data[$count]["general_task_type_id"]) { ?> selected="selected" <?php } ?>><?php echo $gen_task_type_list_data[$count]["general_task_type_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_status">
			  <option value="">- - Select Status - -</option>
			  <option value="0" <?php if($search_status == "0") { ?> selected="selected" <?php } ?>>Not Started</option>
			  <option value="1" <?php if($search_status == "1") { ?> selected="selected" <?php } ?>>Just Started</option>
			  <option value="2" <?php if($search_status == "2") { ?> selected="selected" <?php } ?>>In Progress</option>
			  <option value="3" <?php if($search_status == "3") { ?> selected="selected" <?php } ?>>Waiting</option>
			  <option value="4" <?php if($search_status == "4") { ?> selected="selected" <?php } ?>>Complete</option>
			  <option value="5" <?php if($search_status == "5") { ?> selected="selected" <?php } ?>>Deferred</option>
			  </select>
			  </span>
			  <?php if($role == 1)
			  {?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($task_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }
			  else
			  {
			  ?>
			  <input type="hidden" name="search_user" value="<?php echo $user; ?>" />
			  <?php
			  }
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_assigner">
			  <option value="-1">- - Select Assigned By - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_assigner == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			 
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_department">
			  <option value="">- - Select Department - -</option>
			  <?php
			  for($count = 0; $count < count($department_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if($search_department == $department_list_data[$count]["general_task_department_id"]) { ?> selected="selected" <?php } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="submit" name="task_search_submit" />
			  </span>
			  </form>			  
            </div>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>						
					<th>Task Details</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Assigned By</th>
					<th>Assigned To</th>					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				if($general_task_plan_list["status"] == SUCCESS)
				{
					$sl_count = 0;
					for($count = 0; $count < count($general_task_plan_list_data); $count++)
					{						
						$sl_count++;
						if(($general_task_plan_list_data[$count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "1970-01-01") && ($role == 3))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $general_task_plan_list_data[$count]["general_task_end_date"];
						}
						$start_date = $general_task_plan_list_data[$count]["general_task_planned_date"];
						
						$css_class = "#000000";
					?>					
					<tr style="color:<?php echo $css_class; ?>">
						<td><?php echo $sl_count; ?></td>						
						<td><?php echo $general_task_plan_list_data[$count]["general_task_details"]; ?></td>
						<td><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_start_date"],"d-M-Y"); ?></td>
						<td><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"d-M-Y"); ?></td>
						<td><?php echo $general_task_plan_list_data[$count]["assigner"]; ?></td>
						<td><?php echo $general_task_plan_list_data[$count]["assignee"]; ?></td>						
						<td><a href="general_task_assign.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>"><span style="color:black; text-decoration: underline;">Assign</span></a></td>
						<td><a href="edit_general_task.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>"><span style="color:black; text-decoration: underline;">Edit</span></a></td>
						<td><a href="#" onclick="return confirm_deletion(<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>);"><span style="color:black; text-decoration: underline;">Delete</span></a></td>
					</tr>
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				?>
				<form method="post" name="add_gen_task" action="add_general_task.php">
				<input type="hidden" name="ddl_gen_task_type" value="1" />
				<input type="hidden" name="ddl_department" value="12" />
				<tr>	
					<td><?php echo ++$sl_count; ?></td>
					<td><input type="text" name="txt_gen_task_details" placeholder="Enter Task Details" required /></td>
					<td colspan="2"><input type="date" name="dt_planned_end_date" /></td>
					<td colspan="2"><select name="ddl_assigned_to" required>
					<?php 
					for($count = 0; $count < count($user_list_data); $count++)
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $user)
					{
					?>
					
					selected="selected"
					
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>												
					<?php
					}
					?>
					</select></td>
					<td colspan="3"><button type="submit" class="btn btn-primary" name="add_general_task_submit">Submit</button></td>
				</tr>
				</form>
                </tbody>
              </table>
			  <br />			  
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(task_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "general_task_summary.php";
				}
			}

			xmlhttp.open("GET", "general_task_delete.php?task=" + task_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}
</script>

  </body>

</html>