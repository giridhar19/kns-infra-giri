<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$issue_id = $_GET["issue_id"];
$indent_id = $_GET["indent_id"];
//Get Issue List
$stock_issue_item_search_data = array("active"=>'1',"issue_id"=>$issue_id,"indent_id"=>$indent_id);
$issue_item_list = i_get_stock_issue_item($stock_issue_item_search_data);
if($issue_item_list["status"] == SUCCESS)
{
	$issue_item_list_data = $issue_item_list["data"];
	$issue_no = $issue_item_list_data[0]["stock_issue_no"];
	$item_id  = $issue_item_list_data[0]["stock_issue_item_material_id"];
	$issued_by = $issue_item_list_data[0]["added_by"];
	$issue_date = $issue_item_list_data[0]["stock_issue_issued_on"];
	$indent_no = $issue_item_list_data[0]["stock_indent_no"];
	$indent_date = $issue_item_list_data[0]["stock_indent_added_on"];
	$indent_by = $issue_item_list_data[0]["issued_to"];
}
else
{
	
}
$issue_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Issue Slip</title>
</head>

<body style="text-transform:capitalize">
<table border="1" style="border-collapse:collapse;" cellspacing="0" cellpadding="10" width="100%">
  <tr>
    <td colspan="8" valign="bottom"><p align="center">
    <img src="kns-logo.png" width="186" height="46" /><br />
<br />
 <strong style="text-transform:uppercase">Issue slip </strong></td>    
  </tr>
  <tr>
    <td colspan="4" valign="bottom">Indent nbr :'.$indent_no.'</td>
    <td colspan="4" valign="bottom">Issue nbr :'.$issue_no.'</td>
  </tr>
  <tr>
    <td colspan="4" valign="bottom">Indent date :'.date("d-M-Y",strtotime($indent_date)).'</td>
    <td colspan="4" valign="bottom">Issue date :'.date("d-M-Y",strtotime($issue_date)).'</td>
  </tr>
  <tr>
    <td colspan="4" valign="bottom">Indent by :'.$indent_by.'</td>
    <td colspan="4" valign="bottom">Issue by :'.$issued_by.'</td>
  </tr>
  <tr>
    <td width="86" valign="bottom">Sl.no</td>
    <td width="165" valign="bottom">Item code </td>
    <td width="118" valign="bottom">Item name </td>
    <td width="119" valign="bottom">Indent qty </td>
    <td width="113" valign="bottom">Issue qty </td>
    <td width="111" valign="bottom">Rate </td>
    <td width="115" valign="bottom">Value </td>
    <td width="129" valign="bottom">Type of material </td>
  </tr>';
		
for($count = 0; $count < count($issue_item_list["data"]); $count++)
{
	 $stock_indent_items_search_data = array("material_id"=>$issue_item_list_data[$count]["stock_issue_item_material_id"],"indent_id"=>$indent_id);
	 $indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);
	 if($indent_item_list["status"] == SUCCESS)
	 {
		 $indent_item_list = $indent_item_list["data"];
		 $indent_qty = $indent_item_list[0]["stock_indent_item_quantity"];
	 }
	$issue_qty = $issue_item_list_data[$count]["stock_issue_item_qty"];
	$rate = $issue_item_list_data[$count]["stock_material_price"];
	$value = $issue_qty * $rate;
	$issue_print_format = $issue_print_format.'<tr>
              <td style="height:30px;">'.($count + 1).'</td>
              <td>'.$issue_item_list_data[$count]["stock_material_name"].'</td>
              <td>'.$issue_item_list_data[$count]["stock_material_code"].'</td>
              <td>'.$indent_qty.'</td>
              <td>'.$issue_qty.'</td>
              <td>'.$rate.'</td>
              <td>'.$value.'</td>
              <td>'.$issue_item_list_data[$count]["stock_material_type"].'</td>
            </tr>';
};

$remarks = '';
for($count = 0; $count < count($issue_item_list["data"]); $count++)
{
	$remarks = $remarks.($count + 1).'. '.$issue_item_list_data[$count]["stock_issue_item_remarks"];
}

$issue_print_format = $issue_print_format.'

  <tr>
    <td width="86"><p align="center">Remarks: </td>
    <td colspan="7" valign="bottom">'.$remarks.'</td>
  </tr>
  <tr>
    <td colspan="4" valign="bottom">Issued by </td>
    <td colspan="4" valign="bottom">Received by </td>
  </tr>
</table>
</body>
</html>';

$mpdf = new mPDF();
$mpdf->WriteHTML($issue_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');
?>