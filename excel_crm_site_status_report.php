<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: excel_crm_site_status.php
CREATED ON	: 08-Feb-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Sites and their statuses
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$search_project     = $_GET["project"];	
	$search_site_type   = $_GET["site_type"];	
	$search_dimension   = $_GET["dimension"];
	$search_site_status = $_GET["site_status"];
	$search_rel_status  = $_GET["rel_status"];
	$site_number        = $_GET["site_num"];

	// Temp data
	$alert = "";
	$alert_type = "";		
	
	// Get site list	
	$site_list = i_get_site_list($site_number,$search_project,'',$search_site_type,$search_dimension,$search_site_status,$search_rel_status);
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 1; 
	// Set the first column
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row_count, "SL No"); 
	// Set the second column
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row_count, "Project"); 
	// Set the third column
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row_count, "Site No"); 
	// Set the fourth column
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row_count, "Block"); 
	// Set the fifth column
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row_count, "Wing"); 
	// Set the sixth column
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row_count, "Dimension"); 
	// Set the seventh column
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row_count, "Area"); 
	// Set the eighth column
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row_count, "Facing"); 
	// Set the ninth column
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row_count, "Status"); 
	// Set the tenth column
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row_count, "Release Status"); 
	// Increment the Excel row counter
	$row_count++; 	 
	/* Create excel sheet and write the column headers - END */
	
	if($site_list["status"] == SUCCESS)
	{
		$site_list_data = $site_list["data"];
		while($row_count <= (count($site_list_data) + 1))
		{ 
			// Set the first column
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row_count, $row_count - 1); 
			// Set the second column
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row_count, $site_list_data[$row_count - 2]["project_name"]); 
			// Set the third column
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row_count, $site_list_data[$row_count - 2]["crm_site_no"]); 
			// Set the fourth column
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row_count, $site_list_data[$row_count - 2]["crm_site_block"]); 
			// Set the fifth column
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row_count, $site_list_data[$row_count - 2]["crm_site_wing"]); 
			// Set the sixth column
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row_count, $site_list_data[$row_count - 2]["crm_dimension_name"]); 
			// Set the seventh column
			if($site_list_data[$row_count - 2]["crm_dimension_non_standard"] == "1")
			{
				$area = $site_list["data"][$row_count - 2]["crm_site_area"];
			}
			else
			{
				$area = ($site_list["data"][$row_count - 2]["crm_dimension_length"]*$site_list["data"][$row_count - 2]["crm_dimension_breadth"]);
			}
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row_count, $area); 
			// Set the eighth column
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row_count, $site_list_data[$row_count - 2]["crm_site_type_name"]); 
			// Set the ninth column
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row_count, $site_list_data[$row_count - 2]["status_name"]); 
			// Set the tenth column
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row_count, $site_list_data[$row_count - 2]["release_status"]); 
			// Increment the Excel row counter
			$row_count++; 
		} 

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="site_status_'.date("d-M-Y").'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save('php://output');
	}
	else
	{
		echo "No results for this search!";
	}
}
else
{
	header("location:login.php");
}	
?>