<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */$_SESSION['module'] = 'General Task';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	// Nothing here
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_meeting_submit"]))
	{		
		$department  = $_POST["ddl_department"];
		$agenda      = $_POST["stxt_agenda"];
		$date        = $_POST["dt_mtng_date"];
		$time        = $_POST["time_mtng_time"];
		$venue       = $_POST["stxt_venue"];
		$details     = $_POST["txt_details"];
		$file_upload = upload_file("document",$user);
		if($file_upload["status"] == FAILURE)
		{
			$doc_path = "";
			$alert = $alert.$file_upload["data"];
		}
		else
		{
			$doc_path = $file_upload["data"];
		}
		$participants = $_POST["cb_req_users"];
		
		// Check for mandatory fields
		if(($agenda !="") && ($date !="") && ($time !="") && ($department !=""))
		{
			if(strtotime($date." ".$time) >= strtotime(date("Y-m-d H:i:s")))
			{
				$mtng_time = strtotime($time);
				$meeting_data = array("department"=>$department,"start_date"=>$date,"end_date"=>$date,"start_time"=>date("H:i:s",strtotime('-60 minutes',$mtng_time)),"end_time"=>date("H:i:s",strtotime('+60 minutes',$mtng_time)));
				$meeting_list = i_get_meetings($meeting_data);			
				
				if($meeting_list["status"] == SUCCESS)
				{
					$meeting_conflict = true;
				}
				else
				{
					$meeting_conflict = false;
				}
			
				$add_mtng_result = i_add_meeting($department,$agenda,$doc_path,$date,$time,$venue,$details,$user);
				if($add_mtng_result["status"] == SUCCESS)
				{						
					$alert = "Meeting Request successfully sent";
					$alert_type = 1;
					
					if($meeting_conflict == true)
					{
						$alert = $alert.'<br /> Please note that there might be a schedule conflict of your meeting with the following meeting: <a href="meeting_details.php?meeting='.$meeting_list["data"][0]["meeting_id"].'">'.$meeting_list["data"][0]["meeting_no"].'</a>';
					}
				
					$email_id_list = "";
					// Add participants to the meeting
					for($user_count = 0; $user_count < count($participants); $user_count++)
					{
						$participant_result = i_add_meeting_participant($participants[$user_count],$add_mtng_result["data"],$user);
						
						if($participant_result["status"] != SUCCESS)
						{
							$alert = $alert."Request has not been sent to all users!";
						}
						else
						{
							$user_sresult = i_get_user_list($participants[$user_count],'','','');
							$email_id_list = $email_id_list.$user_sresult["data"][0]["user_email_id"].'||';
						}
					}
					$email_id_list = trim($email_id_list,'||');
					
					// Send email
					$subject = "Meeting Request sent by ".$loggedin_name." for ".date("d-M-Y",strtotime($date))." ".$time;
					$message = "Meeting Details:<br />
					Agenda: ".$agenda."<br />
					Date: ".date("d-M-Y",strtotime($date))."<br />
					Time: ".$time."<br />
					Venue: ".$venue;
					$mail_result = send_email($email_id_list,'',$subject,$message,'');					
				}
				else
				{
					$alert = $alert.$add_mtng_result["data"];
					$alert_type = 0;
				}	
			}
			else
			{
				$alert = "Meeting date time cannot be earlier than current date time";
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of file types
	$department_list = i_get_department_list('','1'); // Get the department list
	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Initiate Meeting</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Initiate Meeting</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Meeting Details</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_mtng_form" class="form-horizontal" method="post" action="add_meeting.php" enctype="multipart/form-data">								
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="ddl_department">Department *</label>
											<div class="controls">												
												<select name="ddl_department" required>
												<option value="">- - Select Department - -</option>
												<?php
												for($count = 0; $count < count($department_list_data); $count++)
												{
												?>
												<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>								
												<?php
												}
												?>														
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_agenda">Agenda *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_agenda" placeholder="Agenda of the meeting" required>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="dt_mtng_date">Date *</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_mtng_date" required>
												<p class="help-block">Scheduled date of the meeting</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="time_mtng_time">Time *</label>
											<div class="controls">
												<input type="time" class="span6" name="time_mtng_time" required>
												<p class="help-block">Scheduled time of the meeting</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_venue">Venue</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_venue">
												<p class="help-block">Venue of the meeting</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->									
										
										<div class="control-group">											
											<label class="control-label" for="txt_details">Details</label>
											<div class="controls">
												<textarea class="span6" name="txt_details"></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="document">Document</label>
											<div class="controls">
												<input type="file" class="span6" name="document">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_req_users">Attendance Required</label>
											<div class="controls">
											<table>
											<?php
											for($count = 0; $count < count($user_list_data); $count++)
											{
											?>											
											<tr>
											<td><?php echo $user_list_data[$count]["user_name"]; ?></td>
											<td>
												<input type="checkbox" class="span6" name="cb_req_users[]" value="<?php echo $user_list_data[$count]["user_id"]; ?>">
											</td>
											</tr>											
											<?php
											}
											?>
											</table>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_meeting_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>