<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["task_type"]))
	{
		$task_type = $_GET["task_type"];
	}
	else
	{
		$task_type = "";
	}

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}

	// Get list of task plans for this process plan
	if($role == 3)
	{
		$access_user = $user;
	}
	else
	{
		$access_user = "";
	}
	
	$legal_task_plan_list = i_get_task_plan_list('',$task_type,'','','0000-00-00','1',$access_user,'asc');	
	$legal_task_plan_list_data = array();
	if($legal_task_plan_list["status"] == SUCCESS)
	{		
		$array_count = 0;
		for($count = 0; $count < count($legal_task_plan_list["data"]); $count++)
		{
			$task_type_sresult = i_get_task_type_list('','','',$legal_task_plan_list["data"][$count]["task_plan_legal_type"]);
			// Get file details
			if($task_type_sresult["data"][0]["process_is_bulk"] != '1')
			{				
				$task_file = i_get_file_list('','','','','','','','','',$legal_task_plan_list["data"][$count]["task_plan_legal_id"]);
			}
			else
			{				
				$task_file = i_get_file_list('','','','','','','','','','','','','',$legal_task_plan_list["data"][$count]["task_plan_legal_id"]);			
			}							
			if($task_file["status"] == SUCCESS)
			{
				$legal_task_plan_list_data[$array_count] = $legal_task_plan_list["data"][$count];
				$array_count++;
			}
		}						
	}
	else
	{
		$alert = $alert."Alert: ".$legal_task_plan_list["data"];
	}

	/*if($process_plan_id != "")
	{
		// Get specific project details
		$process_plan_details = i_get_legal_process_plan_details($process_plan_id);
	}
	else
	{
		$process_plan_details = "";
	}*/	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Pending Task List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Pending Task List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="pending_task_list.php" method="post" id="task_update_form">
			<input type="hidden" name="process_plan" value="<?php echo $process_plan_id; ?>" />
			<input type="hidden" name="count" value="<?php echo count($legal_task_plan_list_data); ?>" />
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Work Description</th>
					<th>Planned End Date</th>
					<th>Start Date</th>
					<th>Actual End Date</th>
					<th>Days</th>
					<th>Reason</th>
					<th>Doc</th>
					<th>Assigned To</th>								
				</tr>
				</thead>
				<tbody>
				 <?php
				if(($legal_task_plan_list["status"] == SUCCESS) && ((count($legal_task_plan_list_data)) > 0))
				{
					$sl_count = 0;
					for($count = 0; $count < count($legal_task_plan_list_data); $count++)
					{
						if($legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"] != '')
						{
							$task_process_data = i_get_legal_process_plan_details($legal_task_plan_list_data[$count]["task_plan_legal_process_plan_id"]);
							$file_no_disp   = $task_process_data['data']['file_number'];
							$survey_no_disp = $task_process_data['data']['file_survey_number'];
						}
						else if($legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"] != '')
						{
							// Fetch and concatenate file and survey numbers of all files in the bulk process
							$legal_bulk_files_search_data = array('legal_bulk_process_files_process_id'=>$legal_task_plan_list_data[$count]["task_plan_legal_bulk_process_plan_id"]);
							$bulk_process_files_list = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);
							
							for($fcount = 0; $fcount < count($bulk_process_files_list['data']); $fcount++)
							{
								$file_no_disp   = $file_no_disp.$bulk_process_files_list['data'][$fcount]['file_number'].',';
								$survey_no_disp = $survey_no_disp.$bulk_process_files_list['data'][$fcount]['file_survey_number'].',';
							}
							$file_no_disp   = trim($file_no_disp,',');
							$survey_no_disp = trim($survey_no_disp,',');
						}
						$sl_count++;
						if(($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "0000-00-00") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "") && ($legal_task_plan_list_data[$count]["task_plan_actual_end_date"] != "1969-12-31") && ($legal_task_plan_list_data[$count]["task_plan_planned_end_date"] != "1970-01-01") && ($role == 3))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $legal_task_plan_list_data[$count]["task_plan_actual_end_date"];
						}
						$start_date = $legal_task_plan_list_data[$count]["task_plan_planned_end_date"];
						
						$variance = get_date_diff($start_date,$end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FF0000";
								$font_class = "#000000";
							}
							else						
							{
								$css_class = "#FFA500";
								$font_class = "#000000";
							}
						}
						else
						{
							if((get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FFFFFF";
								$font_class = "#000000";
							}
							else
							{	$css_class = "#00FF00";
								$font_class = "#000000";
							}
						}
					?>
					<tr bgcolor="<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $legal_task_plan_list_data[$count]["task_type_name"]; ?><br /><br /><strong><?php echo $file_no_disp; ?></strong><br /><br />
						Sy No: <?php echo $survey_no_disp; ?></td>						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_planned_end_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_start_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($legal_task_plan_list_data[$count]["task_plan_actual_end_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php echo $variance["data"];?></td>
						
						<td style="word-wrap:break-word;"><span style="color:black; text-decoration: underline;"><?php echo $legal_task_plan_list_data[$count]["reason"]; ?></span>
						</td>
						
						<td style="word-wrap:break-word;"><?php if($legal_task_plan_list_data[$count]["task_plan_document_path"] != "") { ?><a href="documents/<?php echo $legal_task_plan_list_data[$count]["task_plan_document_path"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">Download</span></a><?php } ?></td>
						
						<td style="word-wrap:break-word;"><?php echo $legal_task_plan_list_data[$count]["user_name"]; ?></td>															
						
					</tr>
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>