<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: Attendance Report

CREATED ON	: 26-Mar-2016

CREATED BY	: Nitin Kashyap

PURPOSE     : Attendance Report

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];





	// Temp data

	$alert      = "";

	$alert_type = "";		
	
	$is_not_started = "";
	// Query String
	
	if(isset($_GET["file_id"]))

	{

		$file_id = $_GET["file_id"];

	}

	else

	{

		$file_id = "";

	}

	if(isset($_GET["village"]))

	{

		$village = $_GET["village"];

	}

	else

	{

		$village = "";

	}

	

	if(isset($_GET["survey"]))

	{

		$survey_no = $_GET["survey"];

	}

	else

	{

		$survey_no = "";

	}

	

	if(isset($_GET["project_id"]))

	{

		$project_id = $_GET["project_id"];

	}

	else

	{

		$project_id = "";

	}
	
	if(isset($_GET["owner"]))

	{

		$owner = $_GET["owner"];

	}

	else

	{

		$owner = "";

	}
	
	if(isset($_GET["owner_status"]))

	{

		$owner_status = $_GET["owner_status"];

	}

	else

	{

		$owner_status = "";

	}
	
	if(isset($_GET["process_status"]))

	{

		$process_status = $_GET["process_status"];

	}

	else

	{

		$process_status = "";

	}
	
	

	// Get list of files
	$bd_file_list = i_get_bd_files_list($file_id,$project_id,$survey_no,$owner,$village,$owner_status,$process_status,'1');

	

	/* Create excel sheet and write the column headers - START */

	// Instantiate a new PHPExcel object

	$objPHPExcel = new PHPExcel(); 

	// Set the active Excel worksheet to sheet 0

	$objPHPExcel->setActiveSheetIndex(0); 

	// Initialise the Excel row number

	$row_count = 1; 

	// Excel column identifier array

	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S');

	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "FILE ID");

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "SURVEY NO"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "SURVEY ALIAS NO"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "SURVEY SALES DEED DATE"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "PROJECT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "LAND OWNER"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "LAND OWNER NO"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "VILLAGE"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "EXTENT"); 	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "LAND STATUS"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "LAND COST"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "BROKERAGE AMOUNT"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "PAID AMOUNT"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "PAYABLE"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, "PROCESS STATUS"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, "ACCOUNT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, "JDA%"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, "ADDED BY"); 

	

	$style_array = array('font' => array('bold' => true));

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[17].$row_count)->applyFromArray($style_array);

	$row_count++;

	if($bd_file_list["status"] == SUCCESS)

	{

		$bd_file_list_data = $bd_file_list['data'];

		$sl_no           = 0;

		

		for($bd_file_count = 0; $bd_file_count < count($bd_file_list_data); $bd_file_count++)

		{

			$sl_no++;
			
			// Corresponding Legal File Details
			$legal_file_sresult = i_get_file_list('',$bd_file_list_data[$bd_file_count]["bd_project_file_id"],'','','','','','','','','','');
			if($legal_file_sresult['status'] == SUCCESS)
			{
				$is_legal_file = true;
				$legal_file_no = $legal_file_sresult['data'][0]['file_number']; 
				$legal_file_id = $legal_file_sresult['data'][0]['file_id']; 
				// Get list of file payments
				$file_payment_list = i_get_file_payment_list($legal_file_id,'');
	
				$total = $bd_file_list_data[$bd_file_count]["bd_file_land_cost"];
				if($file_payment_list["status"] == SUCCESS)
				{
					// Get specific file payment details
					$file_payment_list_data = $file_payment_list["data"];
					for($pay_count = 0 ; $pay_count < count($file_payment_list_data) ; $pay_count++)
					{
						$paid = $paid + $file_payment_list_data[$pay_count]["file_payment_amount"];
					}
					$balance_amount = $total - $paid ;
				}
				else
				{
					$alert = "Invalid File!";
					$paid = 0;
				}
			}
			else
			{
				$is_legal_file = false;
				$legal_file_no = 'NO LEGAL FILE';
				$legal_file_id = '';
				$paid = 0;
			}
			if($legal_file_sresult["status"] == SUCCESS)
			{
				$process_name =  $legal_file_sresult["data"][0]["process_name"];
			}
			else
			{
				$process_name =  $bd_file_list_data[$bd_file_count]["process_name"];
			}
			$payable = $total - $paid;
			$file_handover_data = i_get_file_handover_details($legal_file_id);
			
				if(($is_not_started == '') || ($is_legal_file == false))
				{
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $legal_file_no); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_survey_no"]); 
					
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_survey_sale_deed"]); 
		
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_survey_sale_deed_date"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $bd_file_list_data[$bd_file_count]["bd_project_name"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_owner"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_owner_phone_no"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $bd_file_list_data[$bd_file_count]["village_name"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_extent"]); 			

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_owner_status_name"]);

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_land_cost"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $bd_file_list_data[$bd_file_count]["bd_file_brokerage_amount"]); 
					
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $paid);
					
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, $payable); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, $process_name); 	

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[15].$row_count, $bd_file_list_data[$bd_file_count]["bd_own_account_master_account_name"]); 			

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[16].$row_count, $bd_file_list_data[$bd_file_count]["bd_project_file_jda_share_percent"]); 

					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[17].$row_count, $bd_file_list_data[$bd_file_count]["user_name"]);

					$row_count++;
				}

		}

	}

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[17].$row_count)->getAlignment()->setWrapText(true); 	

	

	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[17].$row_count);

	$row_count++;

	

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[17].($row_count - 1))->getAlignment()->setWrapText(true); 	

	/* Create excel sheet and write the column headers - END */

	

	

	header('Content-Type: application/vnd.ms-excel'); 

	header('Content-Disposition: attachment;filename="LandBank - Live.xls"'); 

	header('Cache-Control: max-age=0'); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

	$objWriter->save('php://output');

}		

else

{

	header("location:login.php");

}	

?>