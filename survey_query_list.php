<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('SURVEY_QUERY_RESPONSE_LIST_FUNC_ID','204');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',SURVEY_QUERY_RESPONSE_LIST_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',SURVEY_QUERY_RESPONSE_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',SURVEY_QUERY_RESPONSE_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',SURVEY_QUERY_RESPONSE_LIST_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_GET['query_id']))
	{
		$query_id = $_GET['query_id'];
	}
	else
	{
		$query_id = "";
	}
	
	$search_process      = "";
	$search_village      = "";
	$search_project      = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_process    = $_POST["search_process"];
		$search_village    = $_POST["search_village"];
		$search_project    = $_POST["search_project"];
		$process_id        = $_POST["hd_process_id"];
	
	}

	// Get survey Process modes already added
	$survey_process_search_data = array("active"=>'1');
	$survey_process_list = i_get_survey_process($survey_process_search_data);
	if($survey_process_list['status'] == SUCCESS)
	{
		$survey_process_list_data = $survey_process_list['data'];
	}	
	else
	{
		$alert = $survey_process_list["data"];
		$alert_type = 0;
	}
	
	// Get survey Process Master modes already added
	$survey_process_master_search_data = array("active"=>'1',"process_type"=>'1');
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list['status'] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list['data'];
	}	

	else
	{
		$alert = $survey_process_master_list["data"];
		$alert_type = 0;
	}
	
	 // Get Village Master modes already added
	$village_master_list = i_get_village_list('');
	if($village_master_list['status'] == SUCCESS)
	{
		$village_master_list_data = $village_master_list["data"];
	}
	else
	{
		$alert = $village_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey already added
	$survey_project_master_search_data = array("active"=>'1');
	$survey_project_master_list = i_get_survey_project_master($survey_project_master_search_data);
	if($survey_project_master_list['status'] == SUCCESS)
	{
		$survey_project_master_list_data = $survey_project_master_list['data'];
	}
	else
	{
		$alert = $survey_project_master_list["data"];
		$alert_type = 0;
	}
	
	// Get survey Query Response modes already added
	$survey_query_search_data = array("active"=>'1',"query_id"=>$query_id,"process_id"=>$process_id,"process_name"=>$search_process,"village_name"=>$search_village,"project_name"=>$search_project);
	$survey_query_response_list = i_get_survey_query($survey_query_search_data);
	if($survey_query_response_list['status'] == SUCCESS)
	{
		$survey_query_response_list_data = $survey_query_response_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Query List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      			<h3>Survey - Query List</h3><?php if($add_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="survey_add_query.php?process_id=<?php echo $process_id ;?>">Survey - Add Query</a></span><?php } ?>
	  		</div> <!-- /widget-header -->
			<?php 
			if($process_id == '')
			{?>
			<div class="widget-header" style="height:50px; padding-top:10px;">		
			<form method="post" id="file_search_form" action="survey_query_list.php">
			<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($survey_project_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $survey_project_master_list_data[$project_count]["survey_project_master_id"]; ?>" <?php if($search_project == $survey_project_master_list_data[$project_count]["survey_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $survey_project_master_list_data[$project_count]["survey_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($survey_count = 0; $survey_count < count($survey_process_master_list_data); $survey_count++)
			  {
			  ?>
			  <option value="<?php echo $survey_process_master_list_data[$survey_count]["survey_process_master_id"]; ?>" <?php if($search_process == $survey_process_master_list_data[$survey_count]["survey_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $survey_process_master_list_data[$survey_count]["survey_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_village">
			  <option value="">- - Select Village - -</option>
			  <?php
			  for($village_count = 0; $village_count < count($village_master_list_data); $village_count++)
			  {
			  ?>
			  <option value="<?php echo $village_master_list_data[$village_count]["village_id"]; ?>" <?php if($search_village == $village_master_list_data[$village_count]["village_id"]) { ?> selected="selected" <?php } ?>><?php echo $village_master_list_data[$village_count]["village_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
			   <?php
			  }
			  ?>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								</div>
								
							</div> 
							<?php
							if($view_perms_list['status'] == SUCCESS)
							{
							?>
							
							<table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th style="word-wrap:break-word;">Project</th>
					<th style="word-wrap:break-word;">Village</th>
					<th style="word-wrap:break-word;">Query</th>
					<th style="word-wrap:break-word;">Query By</th>
					<th style="word-wrap:break-word;">Query Date</th>
					<th style="word-wrap:break-word;">Leadtime</th>
					<th style="word-wrap:break-word;">Latest Response</th>
					<th style="word-wrap:break-word;">Follow Up Date</th>
					<th style="word-wrap:break-word;">Remarks</th>
					<th style="word-wrap:break-word;">Added By</th>					
					<th style="word-wrap:break-word;">Added On</th>									
					<th colspan="3" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_query_response_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($survey_query_response_list_data); $count++)
					{
						$sl_no++;
			
						//Get Leadtime
						$survey_response_search_data = array("query_id"=>$survey_query_response_list_data[$count]["survey_query_id"],"status"=>'Completed');
						$survey_response_list = i_get_survey_response($survey_response_search_data);
						if($survey_response_list["status"] == SUCCESS)
						{
							$survey_response_list_data = $survey_response_list["data"];
							$response_date = $survey_response_list_data[0]['survey_response_added_on'];
						}
						else
						{
							$response_date = date('Y-m-d');
						}
						 
						$query_date = $survey_query_response_list_data[$count]["survey_query_date"];
						$leadtime = get_date_diff($query_date,$response_date);
						
						// Get latest response
						$survey_latest_response_search_data = array("query_id"=>$survey_query_response_list_data[$count]["survey_query_id"],"active"=>'1');
						$survey_latest_response_list = i_get_survey_response($survey_latest_response_search_data);
						if($survey_latest_response_list["status"] == SUCCESS)
						{
							$survey_latest_response_list_data = $survey_latest_response_list["data"];
							$response_text     = $survey_latest_response_list_data[0]['survey_response'];
							if($survey_latest_response_list_data[0]['survey_response_status'] == 'Completed')
							{
								$response_fup_date = 'COMPLETED';
							}
							else
							{
								$response_fup_date = date('d-M-Y',strtotime($survey_latest_response_list_data[0]['survey_response_follow_up_date']));								
							}
						}
						else
						{
							$response_text     = 'NO RESPONSE';
							$response_fup_date = 'NO RESPONSE';
						}
						?>
						<tr>
						<td><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["survey_project_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["village_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["survey_query"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["survey_query_by"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($query_date,"d-M-Y"); ?></td>
						<td><?php echo $leadtime["data"] ;?></td>
						<td style="word-wrap:break-word;"><?php echo $response_text; ?></td>
						<td style="word-wrap:break-word;"><?php echo $response_fup_date; ?></td>
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["survey_query_remarks"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $survey_query_response_list_data[$count]["added_by"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($response_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_edit_query_response('<?php echo $survey_query_response_list_data[$count]["survey_query_id"]; ?>','<?php echo $survey_query_response_list_data[$count]["survey_query_process_id"]; ?>');">Edit</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if(($survey_query_response_list_data[$count]["survey_query_active"] == "1") || ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return delete_query_response('<?php echo $survey_query_response_list_data[$count]["survey_query_id"]; ?>','<?php echo $survey_query_response_list_data[$count]["survey_query_process_id"]; ?>');">Delete</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if($view_perms_list['status'] == SUCCESS){?><a style="padding-right:10px" href="#" onclick="return go_to_survey_response('<?php echo $survey_query_response_list_data[$count]["survey_query_id"]; ?>');">Responses</a><?php } ?></td>
						</tr>
						<tr>						
						<td colspan="15"><?php echo $survey_query_response_list_data[$count]["survey_process_master_name"]; ?></td>
						</tr>
						<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No survey Query added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			    <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function delete_query_response(query_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_query_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_delete_query.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("query_id=" + query_id + "&action=0");
		}
	}	
}
function go_to_edit_query_response(query_id,process_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_edit_query.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","query_id");
	hiddenField1.setAttribute("value",query_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","process_id");
	hiddenField2.setAttribute("value",process_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_survey_response(query_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "survey_response.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","query_id");
	hiddenField1.setAttribute("value",query_id);

	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>
