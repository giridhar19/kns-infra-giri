<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/$_SESSION['module'] = 'Legal Masters';
// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$alert      = "";
	$alert_type = "-1";

	// Search
	if(isset($_POST["process_filter_submit"]))
	{
		$process = $_POST["ddl_process_type"];
	}
	else if(isset($_GET["process"]))
	{
		$process = $_GET["process"];
	}
	else
	{
		$process = "-1";
	}

	// Get list of task types
	$task_type_list = i_get_task_type_list('',$process,'','');

	if($task_type_list["status"] == SUCCESS)
	{
		$task_type_list_data = $task_type_list["data"];
		$task_count = count($task_type_list_data);
	}
	else
	{
		$alert = $alert."Alert: ".$task_type_list["data"];
		$task_count = 0;
	}
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','1','');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Task Type List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">		
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Task Type List (Count: <?php echo $task_count; ?>)</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="task_type_filter" action="task_type_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_process_type">
			  <option value="">- - Select Process - -</option>
			  <?php
				for($count = 0; $count < count($process_type_list_data); $count++)
				{
					?>
					<option value="<?php echo $process_type_list_data[$count]["process_master_id"]; ?>" <?php 
					if($process == $process_type_list_data[$count]["process_master_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $process_type_list_data[$count]["process_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>						
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="process_filter_submit" />
			  </span>
			  </form>			  
            </div>            
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>Task Name</th>
					<th>Process</th>
					<th>Active</th>	
					<th>Expected Duration</th>
					<th>Is Document Mandatory</th>
					<th>Added By</th>						
					<th>Added On</th>
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				<?php
				if($task_type_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($task_type_list_data); $count++)
					{
						if($task_type_list_data[$count]["task_type_active"] == 1)
						{
							$action = 0;
							$text   = "Disable";
							$status = "Enabled";
						}
						else
						{
							$action = 1;
							$text   = "Enable";
							$status = "Disabled";
						}
						
						if($task_type_list_data[$count]["task_type_is_document_mandatory"] == 1)
						{
							$document = "Yes";
						}
						else
						{
							$document = "No";
						}
					?>
					<tr>
						<td><?php echo $task_type_list_data[$count]["task_type_name"]; ?></td>	
						<td><?php echo $task_type_list_data[$count]["process_name"]; ?></td>	
						<td><?php echo $status; ?></td>
						<td><?php echo $task_type_list_data[$count]["task_type_maximum_expected_duration"]; ?></td>	
						<td><?php echo $document; ?></td>	
						<td><?php echo $task_type_list_data[$count]["user_name"]; ?></td>	
						<td><?php echo date("d-M-Y",strtotime($task_type_list_data[$count]["task_type_added_on"])); ?></td>
						<td><a href="task_type_enable_disable.php?task_type=<?php echo $task_type_list_data[$count]["task_type_id"]; ?>&action=<?php echo $action; ?>&process=<?php echo $process; ?>"><?php echo $text; ?></a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="8">No task type added yet</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>
