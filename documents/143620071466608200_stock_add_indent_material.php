<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Capture the form data
	if(isset($_GET["indent_id"]))
		{		
			$indent_id = $_GET["indent_id"];				
		}
		else
		{
			$indent_id = "-1";
	}
	if(isset($_POST["add_material_submit"]))
	{
		$material_id   = $_POST["ddl_material"];
		$uom 		   = $_POST["ddl_uom"];
		$quantity	   = $_POST["num_qty"];
		$remarks 	   = $_POST["txt_remarks"];
	
		// Check for mandatory fields
		if(($material_id != "") && ($uom != "") && ($quantity != ""))
		{
			$indent_iresult = i_add_stock_indent_items($material_id,'1',$quantity,$uom,$remarks,$user);
			
			if($indent_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $indent_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	//Get Uom List
	$stock_unit_search_data = array();
	$uom_list = i_get_stock_unit_measure_list($stock_unit_search_data);
	if($uom_list["status"] == SUCCESS)
	{
		$uom_list_data = $uom_list["data"];
	}
	else
	{
		$alert = $uom_list["data"];
		$alert_type = 0;
	}
	
	//Get Material List
	$stock_material_search_data = array();
	$materail_list = i_get_material_list($stock_material_search_data);
	if($materail_list["status"] == SUCCESS)
	{
		$materail_list_data = $materail_list["data"];
	}
	else
	{
		$alert = $materail_list["data"];
		$alert_type = 0;
	}
	
	// Get Indent Item Details
	$stock_indent_items_search_data = array("active"=>'1');
	$indent_item_list = i_get_indent_items_list($stock_indent_items_search_data);
	if($indent_item_list["status"] == SUCCESS)
	{
		$indent_item_list_data = $indent_item_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$indent_item_list["data"];
	}
	
	//Get Indent List
	$stock_indent_search_data = array("indent_id"=>$indent_id);
	$indent_list = i_get_stock_indent_list($stock_indent_search_data);
	if($indent_list["status"] == SUCCESS)
	{
		$indent_list_data = $indent_list["data"];
	}
	else
	{
		$alert = $indent_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Indent Material</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Indent Details</h3>
						<div class="pull-right"><a style="padding-right:10px" href="stock_edit_indent.php?indent_id=<?php echo $indent_id ;?>" >Edit </a></div>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>					
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>
											<?php if($indent_list["status"] == SUCCESS)
											{?>
											  <tr>
											  <td width="25%">Project</td>
											  <td width="70%"><?php echo $indent_list_data[0]["stock_indent_project"]; ?> </td>
											  </tr>											 
											  <tr>
											  <td>Requested By</td>										  
											  <td><?php echo $indent_list_data[0]["stock_indent_requested_by"]; ?></td>	
											  </tr>
											  <tr>
											  <td>Requested On</td>										  
											  <td><?php echo $indent_list_data[0]["stock_indent_requested_on"]; ?></td>
											  </tr>
											  <tr>
											  <td>Department</td>										  
											  <td><?php echo $indent_list_data[0]["general_task_department_name"]; ?></td>
											  </tr>
											  <tr>
											  <td>Required Date</td>										  
											  <td><?php echo $indent_list_data[0]["stock_indent_material_required_date"]; ?></td>
											  </tr>
											  <tr>
											  <td>Status</td>										  
											  <td><?php echo $indent_list_data[0]["stock_indent_status"]; ?></td>
											  </tr>
											  <?php 
											}
											?>
											  </tr>
											  </tbody>
											</table>
											</fieldset>
										</div>
									</div>
								
								
								
								 <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Material List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="task_list.php" method="post" id="task_update_form">			
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Material</th>
					<th>Indent</th>
					<th>Quantity</th>
					<th>Unit Of Measure</th>
					<th>Added By</th>
					<th>Added On</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				 if($indent_item_list["status"] == SUCCESS)
				 {
					for($count = 0; $count < count($indent_item_list_data); $count++)
					{					
					?>				
					<tr>
						<td><?php echo $indent_item_list_data[$count]["stock_material_name"]; ?></td>
						<td><?php echo $indent_item_list_data[$count]["stock_indent_id"]; ?></td>
						<td><?php echo $indent_item_list_data[$count]["stock_indent_item_quantity"]; ?></td>
						<td><?php echo $indent_item_list_data[$count]["stock_unit_name"]; ?></td>
						<td><?php echo $indent_item_list_data[$count]["user_name"]; ?></td>
						<td><?php echo date("d-M-Y",strtotime($indent_item_list_data[$count]["stock_indent_item_added_on"])); ?></td>
						<td><?php if(($indent_item_list_data[$count]["stock_indent_item_active"] == "1")){?><a href="#" onclick="return delete_indent_item(<?php echo $indent_item_list_data[$count]["stock_indent_item_id"]; ?>);">Delete</a><?php } ?></td>
					</tr>
					<?php 		
					}
				}
				else
				{
				?>
				<td colspan="2">No Items added</td>
				<?php
				}
				 ?>	

                </tbody>
				</table>
				<br/>		
				<div class="modal-body">
			    <div class="row">
                <center><a href="#" data-toggle="modal" data-target="#myModal"><strong>+ Add Material</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</center>
				  </div>
				  </div>			  
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
               <br><br>
               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:20px;">
                     <div class="modal-content">
                       
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Add Material</h4>
                           </div>
                            <form method="post" action="stock_add_indent_material.php">
										<div class="control-group">											
											<label class="control-label">Material Type</label>
											<div class="controls">
												<select id="ddl_material_type" name="ddl_material_type" onchange="return get_products();">
												<option>- - -Select Material- - -</option>
												<option value="Returnable">Returnable</option>
												<option value="NonReturnable">NonReturnable</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group" id="product_area">											
											<label class="control-label" for="ddl_material">Material</label>
											<div class="controls">
												<select class="form-control" id="ddl_material" name="ddl_material" required>
												<option>- - -Select Material- - -</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
											<div class="control-group">											
											<label class="control-label" for="ddl_uom">Unit Of Measure</label>
											<div class="controls">
												<select name="ddl_uom" required>
												<option>- - -Select Department- - -</option>
												<?php
												for($count = 0; $count < count($uom_list_data); $count++)
												{
												?>
												<option value="<?php echo $uom_list_data[$count]["stock_unit_id"]; ?>"><?php echo $uom_list_data[$count]["stock_unit_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_qty">Quantity</label>
											<div class="controls">
												<input type="number" name="num_qty" placeholder="Quantity" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" name="txt_remarks" placeholder="Remarks" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                           <div class="modal-footer">                              
                            <button type="submit" name="add_material_submit" class="btn btn-primary">Save changes</button>					  
                           </div>
                           </form>
                     </div>
                  </div>
               </div>
						  
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function get_products(count,material)
{			
	product_type = document.getElementById("ddl_material_type").value;			
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{				
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{																	
			document.getElementById("product_area").innerHTML = xmlhttp.responseText;					
		}
	}			
	
	xmlhttp.open("POST", "select_material.php");   // file name where delete code is written
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("product_type=" + product_type);
}
	
function delete_indent_item(indent_item_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				alert(xmlhttp.responseText); 	 
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_add_indent_material.php";
					}
				}
			}

			xmlhttp.open("POST", "delete_indent_item.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("indent_item_id=" + indent_item_id + "&action=0");
		}
	}	
}
</script>
</body>
</html>
