<?php

/**

 * @author Nitin kashyap

 * @copyright 2015

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_transactions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_sale_process.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');



/*

PURPOSE : To add customer profile

INPUT 	: Booking ID, Names (max. of 4 names), Relationship (max. of 4 relationship details), Contact No (max. of 4 contact numbers), Email (max. of 4 emails), DOB 9max. of 4 DOBs), Anniversary (max. of 2 anniversaries), Designation, Funding Type, Bank (mandatory if funding type is loan), Agreement Address, Residential Address, Office Address, PAN No (max. of 4 PAN numbers), GPA Holder Name, GPA Address, GPA email, GPA Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_customer_profile($booking_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_no_one,$contact_no_two,$contact_no_three,$contact_no_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$loan_bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks,$added_by)

{	

	$cust_profile_sresult = db_get_customer_profile('',$booking_id,'','','','','','','','','','','');

	

	if($cust_profile_sresult["status"] == DB_NO_RECORD)

	{

		$customer_profile_iresult = db_add_customer_profile($booking_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_no_one,$contact_no_two,$contact_no_three,$contact_no_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$loan_bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks,$added_by);

			

		if($customer_profile_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = $customer_profile_iresult["data"];

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Customer Profile already entered for this booking!";

	}

		

	return $return;

}



/*

PURPOSE : To get customer profile list

INPUT 	: Profile ID, Booking ID, Name, Contact No, Email, DOB, Anniversary, Funding Type, PAN No, Added By, Site Availability/Sold Status

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_cust_profile_list($profile_id,$booking_id,$name,$contact_no,$email,$dob,$anniversary,$funding_type,$pan_no,$added_by,$site_status="",$project_user="")

{	
	$cust_profile_sresult = db_get_customer_profile($profile_id,$booking_id,$name,$contact_no,$email,$dob,$anniversary,$funding_type,$pan_no,$added_by,'','',$site_status,$project_user);	

	

	if($cust_profile_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $cust_profile_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No customer profile added!";

	}

		

	return $return;

}



/*

PURPOSE : To update customer profile

INPUT 	: Cust Profile ID, Name (1 to 4), Relationship (1 to 4), Contact Number (1 to 4), Email (1 to 4), DOB (1 to 4), Anniversary (1 to 2), Company, Designation, Funding Type, Bank, Agreement Address, Residential Address, Office Address, PAN No (1 to 4), GPA Holder, GPA Contact No, GPA Address, GPA Email, GPA Remarks

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_cust_profile($cust_details_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_one,$contact_two,$contact_three,$contact_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks)

{	

	$cust_profile_uresult = db_update_cust_details($cust_details_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_one,$contact_two,$contact_three,$contact_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks);

	

	if($cust_profile_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $cust_profile_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add payment schedule

INPUT 	: Booking ID, Date, Amount, Milestone, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_pay_schedule($booking_id,$date,$amount,$milestone,$remarks,$added_by)

{	

	// Check for duplicate

	$scheduled_pay_details = db_get_pay_schedule($booking_id,$date,$date,'','','');



	if($scheduled_pay_details['status'] == DB_NO_RECORD)

	{

		// Check for already scheduled payments

		$scheduled_pay_details = db_get_pay_schedule($booking_id,'','','','','');

		$already_planned = 0;

		if($scheduled_pay_details['status'] == DB_RECORD_ALREADY_EXISTS)

		{

			for($count = 0; $count < count($scheduled_pay_details['data']); $count++)

			{

				$already_planned = $already_planned + $scheduled_pay_details['data'][$count]['crm_payment_schedule_amount'];

			}

		}

		else

		{

			$already_planned = 0;

		}	

		

		// Check for total payment to be received

		$booking_details = i_get_site_booking($booking_id,'','','','','','','','','','','','','','','','');

		if($booking_details['status'] == SUCCESS)

		{

			if($booking_details['data'][0]['crm_booking_consideration_area'] != '0.00')

			{

				$total_receivable = ($booking_details['data'][0]['crm_booking_consideration_area'] * $booking_details['data'][0]['crm_booking_rate_per_sq_ft']);

			}

			else		

			{

				$total_receivable = ($booking_details['data'][0]["crm_site_area"] * $booking_details['data'][0]['crm_booking_rate_per_sq_ft']);

			}

		}

		else

		{

			$total_receivable = 0;

		}	

		

		if($total_receivable >= ($already_planned + $amount))

		{

			$pay_schedule_iresult = db_add_payment_schedule($booking_id,$date,$amount,$milestone,$remarks,$added_by);

				

			if($pay_schedule_iresult["status"] == SUCCESS)

			{	

				$pay_fup_iresult = db_add_payment_follow_up($booking_id,$date." "."10:00:00","Amount: ".$amount,SYSTEM_GEN_FOLLOW_UP,$added_by);

				

				if($pay_fup_iresult["status"] == SUCCESS)

				{

					$return["status"] = SUCCESS;

					$return["data"]   = "Payment Schedule Successfully Added";

				}

				else

				{

					$return["status"] = SUCCESS;

					$return["data"]   = "Payment Schedule Successfully Added, but follow up not scheduled properly!";

				}

			}

			else

			{

				$return["status"] = FAILURE;

				$return["data"]   = "There was an internal error. Please try again later!";

			}

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "You are exceeding the actual site value";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "You cannot enter 2 payment schedules for the same day";

	}

		

	return $return;

}



/*

PURPOSE : To add payment

INPUT 	: Booking ID, Date, Amount, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_payment($booking_id,$amount,$milestone,$tds,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by)

{	

	// Generate Receipt No

	$receipt_no = p_generate_receipt_no();



	$payment_iresult = db_add_payment($booking_id,$receipt_no,$amount,$milestone,$tds,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by);

		

	if($payment_iresult["status"] == SUCCESS)

	{

		$payment_track_iresult = db_add_payment_track($payment_iresult["data"],PAYMENT_RECEIVED,$remarks,$added_by);

			

		if($payment_track_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Payment Successfully Added";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Payment Track Successfully Added, but payment tracking not initiated!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get payment schedules

INPUT 	: Booking ID, Payment Start Date, Payment End Date, Added By, Milestone, Order of sorting, Schedule ID, Project

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_pay_schedule($booking_id,$payment_start_date,$payment_end_date,$added_by,$milestone='',$order='',$schedule_id='',$project='')

{	

	$pay_schedule_sresult = db_get_pay_schedule($booking_id,$payment_start_date,$payment_end_date,$added_by,'','',$milestone,$order,$schedule_id,$project);

	

	if($pay_schedule_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $pay_schedule_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment schedule added!";

	}

		

	return $return;

}



/*

PURPOSE : To add payment terms

INPUT 	: Booking ID, Maintenance Charges, Water Charges, Club House Charges, Documentation Charges, Other Charges, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_payment_terms($booking_id,$main_charges,$water_charges,$club_house_charges,$documentation_charges,$other_charges,$added_by)

{	

	$payment_terms_iresult = db_add_payment_terms($booking_id,$main_charges,$water_charges,$club_house_charges,$documentation_charges,$other_charges,$added_by);

		

	if($payment_terms_iresult["status"] == SUCCESS)

	{	

		$return["status"] = SUCCESS;

		$return["data"]   = "Payment Terms Successfully Added";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get payment terms

INPUT 	: Booking ID

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_payment_terms($booking_id)

{	

	$payment_terms_sresult = db_get_payment_terms($booking_id,'','','');

	

	if($payment_terms_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_terms_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment terms added!";

	}

		

	return $return;

}



/*

PURPOSE : To get payments

INPUT 	: Payment ID, Booking ID, Mode, Receipt Date (Start), Receipt Date (End), Project

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_payment($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date,$project='')

{	

	$payment_sresult = db_get_payment($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date,'','','',$project);

	

	if($payment_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment added!";

	}

		

	return $return;

}



/*

PURPOSE : To get payment total

INPUT 	: Payment ID, Booking ID, Mode, Receipt Date (Start), Receipt Date (End)

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_payment_total($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date)

{	

	$payment_sresult = db_get_payment_total($payment_id,$booking_id,$mode,$rec_start_date,$rec_end_date,'','','');

	

	if($payment_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment added!";

	}

		

	return $return;

}



/*

PURPOSE : To get payment follow up

INPUT 	: Booking ID, Payment Start Date, Payment End Date, Added By, Work Start Date, Work End Date,Updated By, Show Not followed up also, Order

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_payment_fup_list($fup_id,$booking_id,$fup_start,$fup_end,$added_by,$start_date,$end_date,$work_start_date='',$work_end_date='',$updated_by='',$show_no_fup=false,$order='')

{	

	$payment_fup_sresult = db_get_payment_follow_up($fup_id,$booking_id,$fup_start,$fup_end,$added_by,$start_date,$end_date,$work_start_date,$work_end_date,$updated_by,$show_no_fup,$order);

	

	if($payment_fup_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_fup_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment follow up added!";

	}

		

	return $return;

}



/*

PURPOSE : To add payment follow up

INPUT 	: Booking ID, Date, Amount, Milestone, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_payment_fup($booking_id,$date_time,$notes,$added_by)

{	

	$payment_fup_iresult = db_add_payment_follow_up($booking_id,$date_time,$notes,USER_ADDED_FOLLOW_UP,$added_by);

		

	if($payment_fup_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Payment Follow Up Successfully Added";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To update payment follow up details

INPUT 	: Follow Up, Remarks, Updated By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_payment_fup($fup_id,$remarks,$updated_by)

{	

	$payment_fup_uresult = db_update_payment_fup_details($fup_id,$remarks,$updated_by);

	

	if($payment_fup_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_fup_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Track Status

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_payment_track($pay_track_id,$payment_id,$booking_id,$track_status)

{	

	$payment_track_sresult = db_get_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,'','','');

	

	if($payment_track_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_track_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment track added!";

	}

		

	return $return;

}



/*

PURPOSE : To add payment track

INPUT 	: Payment ID, Status, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_payment_track($payment_id,$status,$remarks,$added_by)

{	

	$payment_track_iresult = db_add_payment_track($payment_id,$status,$remarks,$added_by);

			

	if($payment_track_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Payment Track Successfully Added";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later";

	}

		

	return $return;

}



/*

PURPOSE : To add cancellation payment

INPUT 	: Booking ID, Amount, Mode, Instrument Date, Bank, Branch, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_cancellation_payment($booking_id,$amount,$mode,$instrument_date,$bank,$branch,$remarks,$added_by)

{	

	$cancel_payment_iresult = db_add_cancellation_payment($booking_id,$amount,$mode,$instrument_date,$bank,$branch,$added_by);

		

	if($cancel_payment_iresult["status"] == SUCCESS)

	{

		$payment_track_iresult = db_add_cancel_payment_track($cancel_payment_iresult["data"],PAYMENT_MADE,$remarks,$added_by);

			

		if($payment_track_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Payment Successfully Added";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Payment Track Successfully Added, but payment tracking not initiated!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To add cancellation payment track

INPUT 	: Payment ID, Status, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_cancel_payment_track($payment_id,$status,$remarks,$added_by)

{	

	$cancel_payment_track_iresult = db_add_cancel_payment_track($payment_id,$status,$remarks,$added_by);

			

	if($cancel_payment_track_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Cancellation Payment Track Successfully Added";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later";

	}

		

	return $return;

}



/*

PURPOSE : To get cancellation payments

INPUT 	: Payment ID, Booking ID, Mode, Instrument Date (Start), Instrument Date (End), Added By, Added Date (Start), Added Date (End)

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_cancel_payment($payment_id,$booking_id,$mode,$instru_start_date,$instru_end_date,$added_by,$start_date,$end_date)

{	

	$payment_sresult = db_get_cancel_payment($payment_id,$booking_id,$mode,$instru_start_date,$instru_end_date,$added_by,$start_date,$end_date);

	

	if($payment_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment added!";

	}

		

	return $return;

}



/*

PURPOSE : To get cancellation payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Track Status

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_cancel_payment_track($pay_track_id,$payment_id,$booking_id,$track_status)

{	

	$payment_track_sresult = db_get_cancel_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,'','','');

	

	if($payment_track_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_track_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No payment track added!";

	}

		

	return $return;

}



/*

PURPOSE : To add other payment

INPUT 	: Booking ID, Amount, Type, Mode, Instrument Date, Bank, Branch, Receipt Date, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_other_payment($booking_id,$amount,$type,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by)

{	

	$other_payment_iresult = db_add_other_payment($booking_id,$amount,$type,$mode,$instrument_date,$bank,$branch,$receipt_date,$remarks,$added_by);

		

	if($other_payment_iresult["status"] == SUCCESS)

	{

		$other_payment_track_iresult = db_add_payment_track($other_payment_iresult["data"],OTHER_PAYMENT_RECEIVED,$remarks,$added_by);

			

		if($other_payment_track_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Payment Successfully Added";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "Payment Successfully Added, but payment tracking not initiated!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get other payment done

INPUT 	: Booking ID, Type

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_other_payment($booking_id,$type,$payment_id="")

{	

	$other_payment_sresult = db_get_other_payment($payment_id,$booking_id,'',$type,'','','','','');

	

	if($other_payment_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $other_payment_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No other payment added!";

	}

		

	return $return;

}



/*

PURPOSE : To get other payment done

INPUT 	: Booking ID, Type

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_other_payment_total($booking_id,$type)

{	

	$other_payment_sresult = db_get_other_payment_total('',$booking_id,'',$type,'','','','','');

	

	if($other_payment_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $other_payment_sresult["data"][0]["other_paid_total"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "0";

	}

		

	return $return;

}



/*

PURPOSE : To get other payment track

INPUT 	: Payment Track ID, Payment ID, Booking ID, Track Status

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_other_payment_track($pay_track_id,$payment_id,$booking_id,$track_status)

{	

	$other_payment_track_sresult = db_get_other_payment_track($pay_track_id,$payment_id,$booking_id,$track_status,'','','');

	

	if($other_payment_track_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $other_payment_track_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No other payment track added!";

	}

		

	return $return;

}



/*

PURPOSE : To add Katha Transfer related transaction

INPUT 	: Site ID, Transaction Type, Date, Remarks, Added By

OUTPUT 	: Katha Transfer Transaction ID, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_kt_transaction($site_id,$transaction_type,$transaction_date,$remarks,$added_by)

{	

	$kt_transaction_data = array("site_id"=>$site_id,"type"=>$transaction_type);

	$kt_transactions_sresult = db_get_kt_transactions($kt_transaction_data);

		

	if($kt_transactions_sresult["status"] == DB_NO_RECORD)

	{

		$kt_transaction_iresult = db_add_katha_transfer_transaction($site_id,$transaction_type,$transaction_date,$remarks,$added_by);

			

		if($kt_transaction_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Katha Transfer transaction successfully added";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To get Katha Transfer Transaction details

INPUT 	: Katha Transfer Transaction Data Array

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_kt_transactions($kt_transaction_data)

{		

	$kt_transaction_sresult = db_get_kt_transactions($kt_transaction_data);

	

	if($kt_transaction_sresult["status"] == DB_RECORD_ALREADY_EXISTS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $kt_transaction_sresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "No Katha Transfer step added yet!";

	}

		

	return $return;

}



/*

PURPOSE : To update payment details

INPUT 	: Payment ID, Payment Data Array

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_payment_details($payment_id,$payment_data)

{	

	$payment_uresult = db_update_payment_details($payment_id,$payment_data);

	

	if($payment_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To update other payment details

INPUT 	: Other Payment ID, Other Payment Data Array

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_other_payment_details($payment_id,$payment_data)

{	

	$payment_uresult = db_update_other_payment_details($payment_id,$payment_data);

	

	if($payment_uresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_uresult["data"];

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an internal error. Please try again later!";

	}

		

	return $return;

}



/*

PURPOSE : To update payment schedule details

INPUT 	: Schedule ID, Schedule Data, Previous Schedule Data, Updated By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_payment_schedule($schedule_id,$schedule_data,$old_schedule_data,$updated_by)

{	

	// Check for already scheduled payments

	$scheduled_pay_details = db_get_pay_schedule($old_schedule_data['booking'],'','','','','');

	$already_planned = 0;

	if($scheduled_pay_details['status'] == DB_RECORD_ALREADY_EXISTS)

	{

		for($count = 0; $count < count($scheduled_pay_details['data']); $count++)

		{

			if($schedule_id != $scheduled_pay_details['data'][$count]['crm_payment_schedule_id'])

			{

				$already_planned = $already_planned + $scheduled_pay_details['data'][$count]['crm_payment_schedule_amount'];

			}

		}

	}

	else

	{

		$already_planned = 0;

	}	

	

	// Check for total payment to be received

	$booking_details = i_get_site_booking($old_schedule_data['booking'],'','','','','','','','','','','','','','','','');

	if($booking_details['status'] == SUCCESS)

	{

		if($booking_details['data'][0]['crm_booking_consideration_area'] != '0.00')

		{

			$total_receivable = ($booking_details['data'][0]['crm_booking_consideration_area'] * $booking_details['data'][0]['crm_booking_rate_per_sq_ft']);

		}

		else		

		{

			$total_receivable = ($booking_details['data'][0]["crm_site_area"] * $booking_details['data'][0]['crm_booking_rate_per_sq_ft']);

		}

	}

	else

	{

		$total_receivable = 0;

	}



	if($total_receivable >= ($already_planned + $schedule_data['amount']))

	{

		$schedule_uresult = db_update_payment_schedule_details($schedule_id,$schedule_data);

		

		if($schedule_uresult["status"] == SUCCESS)

		{

			$fup_sresult = i_get_payment_fup_list('',$old_schedule_data['booking'],$old_schedule_data['old_date'].' 10:00:00',$old_schedule_data['old_date'].' 10:00:00','','','');

			

			$fup_id = '-1';

			if($fup_sresult['status'] == SUCCESS)

			{						

				for($count = 0; $count < count($fup_sresult['data']); $count++)

				{						

					if($fup_sresult['data'][$count]['crm_payment_follow_up_type'] == SYSTEM_GEN_FOLLOW_UP)

					{					

						$fup_id = $fup_sresult['data'][$count]['crm_payment_follow_up_id'];

					}

				}

			}

			

			if($fup_id != '-1')

			{

				$payment_fup_uresult = db_update_payment_fup_details($fup_id,'',$updated_by,$schedule_data['date'].' 10:00:00','Amount: '.$schedule_data['amount']);

			}

			

			$return["status"] = SUCCESS;

			$return["data"]   = $schedule_uresult["data"];

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Scheduled amount cannot be more than total receivable";

	}

		

	return $return;

}

/*

PURPOSE : To geenreate receipt no

INPUT 	: NOTHING

OUTPUT 	: Receipt No

BY 		: Nitin Kashyap

*/

function p_generate_receipt_no()

{

	// Get latest receipt based on receipt no

	$latest_receipt_sresult = db_get_payment('','','','','','','','');

	

	if($latest_receipt_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

	{

		$latest_receipt_no = $latest_receipt_sresult['data'][0]['crm_payment_receipt_no'];

		$new_receipt_no    = $latest_receipt_no + 1;

	}

	else // If no receipt no, return 1

	{

		$new_receipt_no    = 1;

	}	

	

	return $new_receipt_no;

}



/* PRIVATE FUNCTIONS - END */
?>