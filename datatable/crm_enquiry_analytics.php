<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
   session_start();
   define('CRM_ANALYTICS_FUNC_ID', '384');
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $user = $_SESSION["loggedin_user"];
  $role = $_SESSION["loggedin_role"];
  $approve_perms_list = i_get_user_perms($user, '', CRM_ANALYTICS_FUNC_ID, '6', '1');
  $aColumns = array(
    'user_name',
    'user_id',
    'user_email_id',
    'user_password',
    'user_active',
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "user_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = "";
 	if ( isset( $_GET['order'][0]["column"] ) )
 	{
 		$sOrder = "ORDER BY ";
 		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
 		{
 				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
 				($_GET['order'][$i]["dir"]=='asc' ? 'asc' : 'desc') .", ";
 		}

 		$sOrder = substr_replace( $sOrder, "", -2 );
 		if ( $sOrder == "ORDER BY" )
 		{
 			$sOrder = "";
 		}
 	}
	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
  $sWhere = "Where ";
  if($approve_perms_list["status"]==0 || $role==1) {
    $sWhere .= "`user_active`=1 AND `user_crm_active`=1";
   }
   else{
     $sWhere .= "`user_id`= $user AND `user_active`=1 AND `user_crm_active`=1";
   }

   if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
     {
       $sWhere = "WHERE (";
       for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
         $sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        if($approve_perms_list["status"]==0 || $role==1) {
          $sWhere .= ") AND `user_active`=1 AND `user_crm_active`=1";
         }
         else{
           $sWhere .= ") AND `user_id`= $user AND `user_active`=1 AND `user_crm_active`=1";
         }
     	}

             	/* Individual column filtering */
    if(isset($_GET['assigned_to']) && $_GET['assigned_to'] != '') {
        $sWhere .= " AND `user_id` = ". $_GET['assigned_to'];
    }
	/*
	 * SQL queries
	 * Get data to display
	 */

   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];
  /*
   * Output
   */
   // print_r($rResult);
  $output = array(
    "iTotalDisplayRecords" => $iFilteredTotal,
    "query"=>$sQuery,
    "aaData" => array(),
  );
   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
    $output['aaData'][] = $row;
  }
  echo json_encode($output,JSON_PARTIAL_OUTPUT_ON_ERROR);
 ?>
