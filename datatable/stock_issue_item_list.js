function checkFileAttached(){
  $('#formImport').submit();
}

$(document).ready(function() {
  function createToolTip(str, length){
    var substr = (str.length <= 20)? str : str.substr(0, 20)+'...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="'+str+'">'+substr+'</abbr>'
  }

  var table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_indent_issue_list_datatable.php',
    fnRowCallback: function(row, full, index){
      $(row).attr('id', 'row_'+index);
      // get the stock qty
      $.ajax({
       url: 'ajax/stock_get_material_stock.php',
       data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
       dataType: 'json',
       success: function(stock_response) {
         console.log(stock_response);
           $('span#stock_qty_' + index).html(stock_response);
       }
       });
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.search_status = $('#search_status').val();
      aoData.table = "stock_indent_items_list";
      aoData.search_project = getVars('search_project');
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
     aoData.sSearch = aoData.search.value;
   }
    },
    // fixedColumns: {
    //   leftColumns: 4
    // },
    drawCallback: function(){
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function(){
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'indent_no'
      },
      {
        orderable: false,
        data: 'material_name'
      },
      {
        orderable: false,
        data: 'material_code'
      },
      {
        orderable: false,
        data: 'uom'
      },
      {
        orderable: false,
        data: 'project_name'
      },
      {
        orderable: false,
        data: 'material_qty'
      },
      {
        orderable: false,
        data: function() {
          return '<span id="stock_qty_' + arguments[3].row + '">loading..</span>';
        },
      },

      {
        orderable: false,
          data : function(data, type, full) {
              return moment(data.added_on).format('DD-MMM-YYYY');
          }
      },
      {
        orderable: false,
        data: 'indent_by'
      },
      {
        orderable: false,
        data : function(data, type, full) {
          return '<a href="stock_indent_issue.php?indent_id='+data.indent_id+'&project_id='+data.project_id+' " target=_blank> <span class="glyphicon glyphicon-share-alt"></span></a>';
        }
      },
      {
        orderable: false,
        data : function(data, type, full) {
          return '<a href="stock_issue_list.php?indent_id='+data.indent_id+'" target=_blank> <span class="glyphicon glyphicon-eye-open"></span></a>';
        }
      }
    ]
    // columnDefs: [
    //   {
    //     orderable: false,
    //     targets: [7],
    //     data: function(data, type, full, meta) {
    //       var classname = columnsMapping[meta.col]+'_'+meta.row;
    //       return '<span class="'+classname+'">loading..</span>';
    //     }
    //   }
    // ]
  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}
