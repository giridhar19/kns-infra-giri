//globalization variable for data rendaring
var table;
var holiday_list_data = [];
var executed = false;
//data render function
function redrawTable() {
  resetUploadForm();
  table.fnDraw();
}


function exportData() {
  var project_id = $("#project_id").val();
  if (project_id == "") {
    alert("select project to continue");
  } else {
    $('#hd_project_id').val(project_id);
    $('#formImport')[0].action = 'task_planning_excel.php';
    $('#formImport')[0].submit();
  }
}

function get_holiday_list() {
  $.ajax({
    url: 'ajax/get_holiday_list.php',
    data: {
      date_start: new Date().getFullYear() + '-01-01',
      date_end: new Date().getFullYear() + '-12-31'
    },
    dataType: 'json',
    success: function(response) {
      var temp = [];
      for (var date in response) {
        temp.push(response[date]['hr_holiday_date']);
      }
      holiday_list_data = temp;
      return temp;
    }
  });
}

function resetUploadForm() {
  $("#input-import_file").val('');
  $('#upload-button').show();
  $('#processing-button').hide();
  get_plan_dates($("#project_id").val());
}

function checkFileAttached() {
  var project_id = $("#project_id").val();
  if (project_id == "") {
    alert("Select Project to Import");
    return false;
  } else {
    $('#hd_project_id').val(project_id);
    $('#formImport')[0].action = './import_project_task_plan.php?cache_token=' + (new Date()).getTime();
    $('#formImport')[0].submit();
    $('#upload-button').hide();
    $('#processing-button').show();
  }
}

function getCellValue() {
  var meta = arguments[3];
  console.log(arguments)
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">loading..</span>';
}

function colorizeCell() {
  if (arguments[4] < 11) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 10) {
    $(arguments[0]).addClass('red');
  }
}

function edit_task_plan_data() {
  var rowData = table.api().data()[$('#selected_row_id').val()];
  var qty = $('#quantity').val();
  var start_date = $('#plan_start_date').val();
  var end_date = $('#plan_end_date').val();
  var planning_id = $('#planning_id').val();
  var per_day_output = $('#output').val();
  var road_id = rowData.project_task_planning_no_of_roads;
  var task_id = rowData.project_task_planning_task_id;
  if ((start_date.trim() == '') || (end_date.trim() == '') || (qty.trim() == '') ||
    ($('#resource_type').val().trim() == '') || ($('#resource_name').val().trim() == '')) {
    alert('Please input required field');
    return false;
  }
  if ((new Date(start_date).getDay() == 0) || (new Date(end_date).getDay() == 0)) {
    alert('Dates cannot be set to Sunday');
    return false;
  }
  var actual_measurement = $('span#total' + $('#selected_row_id').val()).html();
  if (!executed) {
    executed = true;
    holiday_list_data = get_holiday_list();
  }
  for (var i in holiday_list_data) {
    if (moment(end_date).format('') == moment(holiday_list_data[i]).format('') ||
      moment(start_date).format('') == moment(holiday_list_data[i]).format('')) {
      alert("start date or end date cannot be on public holiday");
      return false;
    }
  }

  // return false;

  if (qty <= 0 || parseFloat(qty).toFixed(2) < parseFloat(actual_measurement)) {
    alert("measurement cannot be zero or less than actual measurement");
    return false;
  }

  if (end_date < start_date) {
    alert("End date cannot be lesser than start date");
    return false;
  }
  var diff_days = findDateDiff(new Date(start_date), new Date(end_date));
  var no_of_objects = ((qty / per_day_output) / diff_days);
  var data = {
    quantity: qty,
    start_date: start_date,
    end_date: end_date,
    no_of_objects: no_of_objects,
    total_days: diff_days,
    planning_id: planning_id,
    road_id: road_id,
    task_id: task_id,
    project_id: $('#project_id').val(),
    resource_type: $('#resource_type').val(),
    resource_name: $('#resource_name').val()
  };
  $.ajax({
    url: 'project_edit_task_plan.php',
    method: 'POST',
    data: data,
    success: function(response) {
      $("#exampleModal").modal('hide');
      table.api().data().row($('#selected_row_id').val()).draw(false);
    }
  });
}

function showModal(row_id) {
  var rowData = table.api().data()[row_id];
  var per_day_out = $('span#output' + $('#selected_row_id').val()).html();
  $('#exampleModal').modal('show');
  $('#selected_row_id').val(row_id);
  $('#exampleModal').on('shown.bs.modal', function() {
    $('span#project_name').html(rowData.project_master_name);
    $('span#process_name').html(rowData.project_process_master_name);
    $('span#task_name').html(rowData.project_task_master_name);
    $('span#uom').html(rowData.project_uom_name);
    $('#planning_id').val(rowData.project_task_planning_id);
    $('#resource_type').val(rowData.project_task_planning_object_type);
    $('#task_id').val(rowData.project_task_master_id);
    $('#quantity').val(rowData.project_task_planning_measurment);
    $('#output').val(per_day_out);
    $('#plan_start_date').val(rowData.project_task_planning_start_date);
    $('#plan_end_date').val(rowData.project_task_planning_end_date);
    var actual_measurement = $('span#total' + $('#selected_row_id').val()).html();
    if (actual_measurement > 0 && rowData.project_task_planning_start_date !== '0000-00-00') {
      $('#plan_start_date').attr('disabled', 'disabled');
    } else {
      $('#plan_start_date').removeAttr('disabled');
    }
    getResourceName(rowData);
    $('#resource_type').on('change', rowData, getResourceName);
  });
  // remove the Modal content from DOM
  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $('#exampleModal').off('shown.bs.modal');
    $('#exampleModal').off('hidden.bs.modal');
    $('form#edit_task_plan_form')[0].reset();
  });
}

function getResourceName(rowData) {
  if (rowData.data) {
    rowData = rowData.data;
  }
  $.ajax({
    url: 'project_get_cw_list.php',
    data: {
      object_type: $('#resource_type').val(),
      task_id: $('#task_id').val()
    },
    dataType: 'json',
    success: function(response) {
      $("#resource_name").empty();
      if (response.length) {
        $("#resource_name").append("<option value=''>Select Resource Name</option>");
        for (var i = 0; i < response.length; i++) {
          var id;
          var name;
          if (response[i]["project_object_output_object_type"] == "CW") {
            id = response[i]['project_cw_master_id'];
            name = response[i]['project_cw_master_name'];
          } else {
            id = response[i]['project_machine_type_master_id'];
            name = response[i]['project_machine_type_master_name'];
          }

          var compare_value = '';
          if (rowData.project_task_planning_object_type == "CW") {
            compare_value = rowData.project_cw_master_name;
          } else {
            compare_value = rowData.project_machine_master_name;
          }

          if (compare_value == name) {
            $("#resource_name").append("<option value='" + id + "' selected>" + name + "</option>").val();
          } else {
            $("#resource_name").append("<option value='" + id + "'>" + name + "</option>").val();
          }
        }
      }
    }
  });
}

$(document).ready(function() {
  get_process($('#project_id').val());
  get_process_task($('#project_id').val(), $('#ddl_process_id').val());
  get_plan_dates($('#project_id').val());
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
    createdCell: colorizeCell
  };


  function createToolTip(str, length) {
    var substr = (str.length <= length) ? str : str.substr(0, length) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    scrollX: true,
    ajax: 'datatable/project_task_planning.php',
    fnRowCallback: function(row, full, index) {
      if ($('#project_id').val() != "" ||
        $('#search_process').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      //get total measurment
      $.ajax({
        url: 'ajax/project_get_actual_total_measurment.php',
        data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
        dataType: 'json',
        success: function(actual_response) {
          // first print the actual
          $('span#total' + index).html(parseFloat(actual_response).toFixed(2));
        }
      });
      var object_name;
      if (full.project_task_planning_object_type == 'CW')
        object_name = full.project_cw_master_name;
      else {
        object_name = full.project_machine_master_name;
      }
      $.ajax({
        type: "GET",
        data: "object_name=" + object_name + "&task_id=" + full.project_task_master_id + "&object_type=" + full.project_task_planning_object_type,
        url: "project_get_object_details.php",
        cache: false,
        dataType: 'json',
        success: function(response) {
          if (response) {
            $('span#output' + index).html(response[1]);
          }
        }
      });
      if ((moment(full['project_task_planning_end_date']).format() < moment(full['project_task_planning_start_date']).format()) || (full['project_task_planning_end_date'] === '0000-00-00')) {
        $(row).css("color", "red");
      }
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.search_process = $('#search_process').val();
      aoData.search_task = $('#search_task').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    processing: true,
    searchDelay: 1500,
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data, type) {
          if (type === 'export') {
            return data.project_process_master_name;
          }
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type) {
          if (type === 'export') {
            return data.project_task_master_name;
          }
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: `uom_name`
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['project_task_planning_no_of_roads'] == 'No Roads') {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: `project_task_planning_measurment`
      },
      {
        orderable: false,
        data: function(data, type) {
          if (type === 'export') {
            return $("tr").find('span#total' + arguments[3].row).html();
          }
          return '<span id="total' + arguments[3].row + '">loading..</span>';
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.project_task_planning_object_type !== 'empty') {
            return data.project_task_planning_object_type;
          }
          return " ";
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.project_task_planning_object_type == 'CW')
            return data.project_cw_master_name;
          else if (data.project_task_planning_object_type == 'MC') {
            return data.project_machine_master_name;
          } else {
            return " ";
          }
        }
      },
      {
        orderable: false,
        data: `project_task_planning_no_of_object`
      },
      {
        orderable: false,
        data: function(data, type) {
          if (type === 'export') {
            return $("tr").find('span#output' + arguments[3].row).html();
          }
          return '<span id="output' + arguments[3].row + '">0</span>';
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.project_task_planning_start_date !== '0000-00-00' && data.project_task_planning_end_date !== '0000-00-00') {
            return findDateDiff(new Date(data.project_task_planning_start_date), new Date(data.project_task_planning_end_date));
          } else {
            return 0;
          }
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.project_task_planning_start_date == '0000-00-00') {
            return '00/00/0000';
          }
          return moment(data.project_task_planning_start_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.project_task_planning_end_date == '0000-00-00') {
            return '00/00/0000';
          }
          return moment(data.project_task_planning_end_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: `project_task_planning_id`
      },
      {
        orderable: false,
        data: function() {
          if (window.permissions.edit) {
            return '<button onclick="showModal(' + arguments[3].row + ')"> <span class="glyphicon glyphicon-pencil"></span></button>';
          }
          return '***';
        }
      },
    ]
  });
  $('#submit_button').on('click', function() {
    if ($('#project_id').val())
      get_plan_dates($('#project_id').val());
  });

  $("#project_id").change(function() {
    var project_id = $(this).val();
    get_process(project_id);
  });

  $("#search_process").change(function() {
    var project_id = $("#project_id").val();
    var process_id = $(this).val();
    get_process_task(project_id, process_id);
  });
});

function findDateDiff(start_date, end_date) {
  var iWeeks, iDateDiff, iAdjust = 0;
  // if (start_date < end_date) return -1; // error code if dates transposed
  var iWeekday1 = start_date.getDay(); // day of week
  var iWeekday2 = end_date.getDay();
  iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1; // change Sunday from 0 to 7
  iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
  if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1; // adjustment if both days on weekend
  iWeekday1 = (iWeekday1 > 6) ? 6 : iWeekday1; // only count weekdays
  iWeekday2 = (iWeekday2 > 6) ? 6 : iWeekday2;

  // calculate differnece in weeks (1000mS * 60sec * 60min * 24hrs * 7 days = 604800000)
  iWeeks = Math.floor((end_date.getTime() - start_date.getTime()) / 604800000)

  if (iWeekday1 <= iWeekday2) {
    iDateDiff = (iWeeks * 6) + (iWeekday2 - iWeekday1)
  } else {
    iDateDiff = ((iWeeks + 1) * 6) - (iWeekday1 - iWeekday2)
  }
  iDateDiff -= iAdjust // take into account both days on weekend
  iDateDiff = iDateDiff + 1
  return iDateDiff;
}

function get_plan_dates(project_id) {
  $.ajax({
    url: 'ajax/get_plan_dates.php',
    data: {
      project_id: project_id
    },
    dataType: 'json',
    success: function(response) {
      var start_date = new Date(response[0][0]);
      var end_date = new Date(response[0][1]);
      $("span#plan_start_date").html(moment(response[0][0]).format('DD/MM/YYYY'));
      $("span#plan_end_date").html(moment(response[0][1]).format('DD/MM/YYYY'));
      iDateDiff = findDateDiff(start_date, end_date);
      $("span#plan_date_diff").html(iDateDiff);
    }
  });
}

function get_process(project_id) {
  $.ajax({
    url: 'ajax/project_get_process.php',
    data: {
      project_id: project_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_process").empty();
      $("#search_process").append("<option value=''>Select Process</option>");
      for (var i = 0; i < response.length; i++) {
        var id = response[i]['process_master_id'];
        var name = response[i]['process_name'];
        $("#search_process").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_process_id').val();
        $('#search_process option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}

function get_process_task(project_id, process_id) {
  $.ajax({
    url: 'ajax/project_get_task_data.php',
    data: {
      project_id: project_id,
      process_id: process_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_task").empty();
      if (response.length) {
        $("#search_task").append("<option value=''>Select Task</option>");
        for (var i = 0; i < response.length; i++) {
          if (i == response.length - 1) {
            var id = response[i]['project_task_master_id'];
            var name = response[i]['project_task_master_name'];
          } else {
            if (response[i]['project_task_master_name'] == response[i + 1]['project_task_master_name'])
              continue;
          }
          var id = response[i]['project_task_master_id'];
          var name = response[i]['project_task_master_name'];
          $("#search_task").append("<option value='" + id + "'>" + name + "</option>");
          var selectedVal = $('#ddl_task_id').val();
          $('#search_task option').map(function() {
            if ($(this).val() == selectedVal) return this;
          }).attr('selected', 'selected');
        }
      }
    }
  })
}