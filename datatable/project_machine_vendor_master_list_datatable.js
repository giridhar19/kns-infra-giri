var table;

function tableDraw() {
  table.fnDraw(false);
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function delete_project_machine_vendor_master(master_id) {
  var ok = confirm("Are you sure you want to Delete?");
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          tableDraw();
        }
      }
    }
    xmlhttp.open("POST", "project_delete_machine_vendor_master.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("master_id=" + master_id + "&action=0");
  }
}

var columns = [{
    className: 'noVis',
    "orderable": false,
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_name"
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_code"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_machine_vendor_master_contact_person;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_machine_vendor_master_contact_number;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_machine_vendor_master_address;
    }
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_email"
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_pan_number;
    },
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_tin_number;
    },
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_gst_number;
    },
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_account_holder_name"
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_bank_name;
    },
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_branch;
    },
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_account_number;
    },
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_ifsc_code"
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_secondary_acc_holder_name;
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_secondary_acc_bank;
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_secondary_acc_branch;
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_machine_vendor_master_secondary_acc_number;
    }
  },
  {
    "orderable": false,
    "data": "project_machine_vendor_master_secondary_acc_ifsc_code"
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type === 'export') {
        return data.project_machine_vendor_master_remarks;
      }
      return createToolTip(data.project_machine_vendor_master_remarks);
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type === 'export') {
        return data.added_by;
      }
      return createToolTip(data.added_by);
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_machine_vendor_master_added_on).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type === 'export') {
        return data.updated_by;
      }
      if (data.updated_by == null) {
        return '';
      }
      return createToolTip(data.updated_by);
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      if (data.project_machine_vendor_master_last_updated_on == '0000-00-00 00:00:00') {
        return '00-00-0000 00:00';
      }
      return moment(data.project_machine_vendor_master_last_updated_on).
      format('DD-MM-YYYY hh:mm A');
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.delete) {
        return `<a style="color:red" href="#" onclick=delete_project_machine_vendor_master(${data.project_machine_vendor_master_id})>
        <span class="red glyphicon glyphicon-trash"></span></a>`;
      }
      return '***';
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a target = _blank href=project_edit_machine_vendor_master.php?master_id=${data.project_machine_vendor_master_id}>
        <span class="glyphicon glyphicon-pencil"></span></a>`;
      }
      return '***';
    }
  },
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_machine_vendor_master_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_machine_vendor_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });
});