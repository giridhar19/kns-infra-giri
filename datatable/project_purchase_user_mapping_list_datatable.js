var table;
$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/project_purchase_user_mapping_list.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
    },
    fnServerParams: function(aoData) {
      aoData.search_project = $('#search_project').val();
      aoData.table = "purchase_user_mapping";
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'stock_project_name'
      },
      {
        orderable: false,
        data: `project_user`
      },
      {
        orderable: false,
        data: 'project_purchase_user_mapping_remarks'
      },
      {
        orderable: false,
        data: `project_purchase_user_mapping_added_on`
      },
      {
        orderable: false,
        data: `added_by`
      }
    ]
  });
});