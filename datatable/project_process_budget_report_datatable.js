var table;
var columnsMapping = {
  '3': 'planned_mp',
  '4': 'planned_mc',
  '5': 'planned_cw',
  '6': 'planned_material',
  '7': 'planned_total',

  '8': 'total_mp_cost',
  '9': 'total_mc_cost',
  '10': 'total_actual_cw_cost',
  '11': 'total_actual_material_cost',
  '12': 'total_actual_cost',

  '13': 'variance_mp_cost',
  '14': 'variance_mc_cost',
  '15': 'variance_actual_cw_cost',
  '16': 'variance_actual_material_cost',
  '17': 'variance_total'
};

var diffMapping = {
  "total_mp_cost": "planned_mp",
  "total_mc_cost": "planned_mc",
  "total_actual_cw_cost": "planned_cw",
  "total_actual_material_cost": "planned_material",
};

function closeModalWindow() {
  $('#exampleModal').modal('hide');
  window.setTimeout(function() {
    alert('File uploaded success');
  }, 500);
}

function upload(process_id, project_id) {
  $('#exampleModal').modal({
    remote: 'project_upload_documents.php?process_id=' + process_id + '&project_id=' + project_id
  });

  $('#exampleModal').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function uploadFile() {
  if (($('#file_remarks_doc').val().trim() == '') ||
    ($('#stxt_remarks').val().trim() == '')) {
    alert('Please input required field');
    return false;
  }

  document.forms[0].submit();
  return false;
}

function openFiles(process_id, project_id) {
  $('#exmplModal').modal({
    remote: 'project_task_method_planning_list.php?process_id=' + process_id + '&project_id=' + project_id
  });

  $('#exmplModal').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function drawTable() {
  table.fnDraw();
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function colorizeCell() {
  if (arguments[4] < 8) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 12) {
    $(arguments[0]).addClass('red');
  }
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
    createdCell: colorizeCell
  };

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    searchDelay: 1500,
    dataSrc: 'aaData',
    processing: true,
    ajax: 'datatable/project_process_budget_report.php',
    pageLength: 25,
    dom: 'lBfrtip',
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);

      // get the planned
      $.ajax({
        url: 'ajax/project_get_budget_planned_process_data.php',
        data: "project_id=" + full['project_id'] + "&process_id=" + full['project_plan_process_id'],
        dataType: 'json',
        success: function(planned_response) {
          $.each(planned_response, function(key, value) {
            $('span.' + key + '_' + index).html(parseInt(value));
          });

          // get the actuals
          $.ajax({
            url: 'ajax/project_get_process_actual_budget_data.php',
            data: "process_id=" + full['project_plan_process_id'],
            dataType: 'json',
            success: function(actual_response) {
              $.each(actual_response, function(key, value) {
                // first print the actual
                $('span.' + key + '_' + index).html(parseInt(value));

                // next print the diff
                // diff = planned - actual
                var diff_value = planned_response[diffMapping[key]] - value;
                var className = key.replace('total', 'variance') + '_' + index;
                $('span.' + className).html(parseInt(diff_value));
                if (diff_value < 0) {
                  $(row).css("color", "red");
                }
              })
              var variance_total = (parseInt($("tr").find('span.' + 'variance_mp_cost' + '_' + index).html())) + (parseInt($("tr").find('span.' + 'variance_mc_cost' + '_' + index).html())) +
                parseInt(($("tr").find('span.' + 'variance_actual_cw_cost' + '_' + index).html())) + (parseInt($("tr").find('span.' + 'variance_actual_material_cost' + '_' + index).html()));
              $('span.' + 'variance_total' + '_' + index).html(parseInt(variance_total));
            }
          });
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.table = "project_process_budget_details";
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data, type) {
          if (type === 'export') {
            return data.project_process_master_name;
          }
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a target="_blank" href="project_budget_report_list.php?project_id=' + data.project_id + '&search_process=' + data.project_process_master_id + '"><span class="glyphicon glyphicon-eye-open"></span></a>';
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (window.permissions.edit) {
            return '<a  href="#"><span id="upload" class="glyphicon glyphicon-paperclip"></span></a>';
          }
          return '***';
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a  href="#"><span id="view" class="glyphicon glyphicon-folder-open"></span></a>';
        }
      },
    ],
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'upload') {
      upload(rowData['project_plan_process_id'], rowData['project_id']);
    } else if (event.target.tagName == 'SPAN' && event.target.id == 'view') {
      openFiles(rowData['project_plan_process_id'], rowData['project_id']);
    }
  });
});