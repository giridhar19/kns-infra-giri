var table;
$(document).ready(function() {
  // get_process($('#project_id').val());
  // var commonCellDefinition = {
  //   orderable: false,
  //   data: getCellValue,
  //   createdCell: colorizeCell
  // };
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    searchDelay: 1500,
    dataSrc: 'aaData',
    processing: true,
    ajax: 'datatable/project_task_planning_history_datatable.php',
    scrollX: true,

    scrollCollapse: true,
    fixedHeader: true,
    pageLength: 25,
    dom: 'lBfrtip',
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.table = "task_planning_shadow_list";
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: 'project_name'
      },
      {
        orderable: false,
        data: `process_name`
      },
      {
        orderable: false,
        data: `task_name`
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['road_id'] == 'No Roads') {
            return 'No Roads';
          }
          return data['road_name'];
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.old_start_date == '0000-00-00 00:00:00') {
            return '00/00/0000';
          }
          return moment(data.old_start_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.old_end_date == '0000-00-00 00:00:00') {
            return '00/00/0000';
          }
          return moment(data.old_end_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.new_start_date == '0000-00-00 00:00:00') {
            return '00/00/0000';
          }
          return moment(data.new_start_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: function(data) {
          if (data.new_end_date == '0000-00-00 00:00:00') {
            return '00/00/0000';
          }
          return moment(data.new_end_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: `changed_by`
      },
      {
        orderable: false,
        data: function(data) {
          if (data.changed_on == '0000-00-00 00:00:00') {
            return '00/00/0000';
          }
          return moment(data.changed_on).format('DD/MM/YYYY');
        }
      },
    ]
  });
  $("#project_id").change(function() {
    var project_id = $(this).val();
    get_process(project_id);
  })
});
