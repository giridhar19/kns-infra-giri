$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/persona_salary_list_datatable.php',
    "iDisplayLength": 20,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    dom: 'Bfrtip',
    buttons: [
        'excelHtml5',
        'csvHtml5',
    ],
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "persona_salary_list"
      });
      aoData.push({
        "name": "search_department",
        "value": getVars('search_department')
      });
    },
    "aaSorting": [
      // [1, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `company_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": 'employee_code'
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": `user_name`
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `department_name`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mData": `designation_name`
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": 'manager_name'
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": `user_location`
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `basic`
      },
      {
        "bSortable": false,
        "aTargets": [9],
        "mData": `hra`
      },
      {
        "bSortable": false,
        "aTargets": [10],
        "mData": `conveyance_allowance`
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": `medical_reimbursement`
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mData": `educational_allowance`
      },
      {
        "bSortable": false,
        "aTargets": [13],
        "mData": `special_allowance`
      },
      // Monthly Gross
      {
        "bSortable": false,
        "aTargets": [14],
        "mRender": function(data, type, full) {
          var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance'])
                  + parseInt(full['educational_allowance'] || 0) + parseInt(full['medical_reimbursement'] || 0 );
          return (gross).toLocaleString('en-IN');
        }
      },
      // PF Employee
      {
        "bSortable": false,
        "aTargets": [15],
        "mRender": function(data, type, full) {
          return (parseInt(full['pf_amount'])).toLocaleString('en-IN');
        }
      },
      // ESI deduction amount
      {
        "bSortable": false,
        "aTargets": [16],
        "mRender": function(data, type, full) {
          return parseInt(full['esi_employee_amount'] || 0).toLocaleString('en-IN');
        }
      },
      // PT
      {
        "bSortable": false,
        "aTargets": [17],
        "mRender": function(data, type, full) {
          var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance'])
                  + parseInt(full['educational_allowance'] || 0) + parseInt(full['medical_reimbursement'] || 0 );
          return parseInt(gross) >= 15000 ? '200' : '0';
        }
      },
      // TDS
      {
        "bSortable": false,
        "aTargets": [18],
        "mRender": function(data, type, full) {
          return 0;
        }
      },
      // Total Deduction
      {
        "bSortable": false,
        "aTargets": [19],
        "mRender": function(data, type, full) {
          var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance'])
                  + parseInt(full['educational_allowance'] || 0) + parseInt(full['medical_reimbursement'] || 0 );
          var pt = parseInt(gross) >= 15000 ? 200 : '0'
          return ((parseInt(full['pf_amount'])) + parseInt(full['esi_employee_amount'] || 0) + pt).toLocaleString('en-IN');
        }
      },
      // Take Home
      {
        "bSortable": false,
        "aTargets": [20],
        "mRender": function(data, type, full) {
          var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance'])
                  + parseInt(full['educational_allowance']) + parseInt(full['medical_reimbursement'] || 0);
          var pt = parseInt(gross) >= 15000 ? 200 : 0
          var deduction = (parseInt(full['pf_amount']) + parseInt(full['esi_employee_amount'] || 0) + pt);
          return (parseInt(((gross) - deduction))).toLocaleString('en-IN');
        }
      },
      // Statutory
      {
        "bSortable": false,
        "aTargets": [21],
        "mData": `statutory_bonus`
      },
      // Employer PF AMount
      {
        "bSortable": false,
        "aTargets": [22],
        "mData": `pf_amount`
      },
      // ESI Company Amount
      {
        "bSortable": false,
        "aTargets": [23],
        "mData": `esi_company_amount`
      },
      // Monthly CTC
      {
        "bSortable": false,
        "aTargets": [24],
        "mRender": function(data, type, full) {
          var gross = parseInt(full['basic']) + parseInt(full['hra']) + parseInt(full['conveyance_allowance']) + parseInt(full['special_allowance'])
                  + parseInt(full['educational_allowance']) + parseInt(full['medical_reimbursement'] || 0);
          var monthly_ctc = gross + parseInt(full['pf_amount']) + parseInt(full['esi_company_amount']) + parseInt(full['statutory_bonus']);
          return monthly_ctc;
        }
      },
      // Bank Details
      {
        "bSortable": false,
        "aTargets": [25, 26, 27, 28, 29],
        "mRender": function(data, type, full, meta) {
          if(meta.col == 25) {
            return full['bank_name'];
          } else if(meta.col == 26) {
            return full['bank_account_number'];
          }
          else if(meta.col == 27) {
            return full['ifsc_code'];
          }
          else if(meta.col == 28) {
            return full['bank_branch'];
          }
          else if(meta.col == 29) {
            return full['bank_city'];
          }
        }
      },
      {
        "bSortable": false,
        "aTargets": [30],
        "mRender": function(data, type, full) {
          return '<a href="#" id="edit">Edit</a>';
        }
      }
    ]
  });

  $('#example tbody').on('click', 'tr', function(event) {

    if (event.target.tagName == 'A' && event.target.id == 'edit') {

      var rowData = table.api().row(this).data();
      go_to_persona_edit_list(rowData['persona_salary_user_id']);
    }
  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  console.log('getVars ',  key, element);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(department_id) {
console.log('setVars ', department_id);

  if(department_id) {
    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("id", 'search_department');
    hiddenField1.setAttribute("value", department_id);
    document.body.appendChild(hiddenField1);
  }
}

function go_to_persona_edit_list(user_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "edit_persona_salary.php");
  form.setAttribute("target", "blank")

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "persona_salary_user_id");
  hiddenField1.setAttribute("value", user_id);

  form.appendChild(hiddenField1);

  document.body.appendChild(form);
  form.submit();
}
