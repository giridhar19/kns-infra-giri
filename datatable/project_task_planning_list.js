//globalization variable for data rendaring
var table;
//data render function
function redrawTable() {
  table.fnDraw();
}

var columnsMapping = {
  '8': 'planned_mp',
  '9': 'planned_mc',
  '10': 'planned_cw',

  '11': 'total_mp_cost',
  '12': 'total_mc_cost',
  '13': 'total_actual_cw_cost',
};

var buttonsMapping = {
  '20': 'pause_button'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  return '<span class="' + classname + '">loading..</span>';
}

function colorizeCell() {
  if (arguments[4] < 11) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 10) {
    $(arguments[0]).addClass('red');
  }
}

$(document).ready(function() {
  get_process($('#project_id').val());
  get_process_task($('#project_id').val(), $('#ddl_task_id').val());
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
    createdCell: colorizeCell
  };


  function createToolTip(str, length) {
    var substr = (str.length <= length) ? str : str.substr(0, length) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    ajax: 'datatable/project_task_planning_datatable.php',
    fnRowCallback: function(row, full, index) {
      $("#status").prop('value', 'Save');
      if ($(row).attr('id')) {
        return false;
      } else {
        $(row).attr('id', 'row_' + index);
        // get the planned
        $.ajax({
          url: 'ajax/project_get_budget_planned_data.php',
          data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
          dataType: 'json',
          success: function(planned_response) {
            $.each(planned_response, function(key, value) {
              $('span.' + key + '_' + index).html(parseInt(value));
            });

            // get the actuals
            $.ajax({
              url: 'ajax/project_get_actual_data.php',
              data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
              dataType: 'json',
              success: function(actual_response) {
                $.each(actual_response, function(key, value) {
                  // first print the actual
                  $('span.' + key + '_' + index).html(parseInt(value));
                })
              }
            });

            //get total measurment
            $.ajax({
              url: 'ajax/project_get_actual_total_measurment.php',
              data: "task_id=" + full['project_task_planning_task_id'] + "&road_id=" + full['project_task_planning_no_of_roads'],
              dataType: 'json',
              success: function(actual_response) {
                // first print the actual
                $('span#total' + index).html(parseFloat(actual_response).toFixed(2));
              }
            });
            if ((moment(full['project_task_planning_end_date']).format() < moment().format()) || (full['project_task_planning_end_date'] === '0000-00-00')) {
              $(row).css("color", "red");
            }
            if (getStatus(full.pause_status) == 'Pause') {
              $("tr").find('#pause_button' + '_' + index).addClass("btn-danger");
            } else {
              $("tr").find('#pause_button' + '_' + index).addClass("btn-primary");
            }
          }
        });
      }
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.search_process = $('#search_process').val();
      aoData.search_task = $('#search_task').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    processing: true,
    searchDelay: 1500,
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['project_task_planning_no_of_roads'] == 'No Roads') {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: `uom_name`
      },
      {
        orderable: false,
        data: `project_task_planning_measurment`
      },
      {
        orderable: false,
        data: function() {
          return '<span id="total' + arguments[3].row + '">loading..</span>';
        }
      },
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      {
        orderable: false,
        targets: [14],
        data: function(data) {
          return createToolTip(data.user_name, 7);
        }
      },
      {
        orderable: false,
        targets: [15],
        data: function(data) {
          return createToolTip(data.project_task_planning_added_on, 7);
        }
      },
      {
        orderable: false,
        targets: [16],
        data: 'project_task_planning_total_days'

      },
      {
        orderable: false,
        targets: [17],
        data: function(data) {
          if (data.project_task_planning_start_date == '0000-00-00') {
            return '00/00/0000';
          }
          return moment(data.project_task_planning_start_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        targets: [18],
        data: function(data) {
          if (data.project_task_planning_end_date == '0000-00-00') {
            return '00/00/0000';
          }
          return moment(data.project_task_planning_end_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        targets: [19],
        data: function(data) {
          return '<button type="button" class="btn btn-primary btn-xs" onclick="update_actual(' + arguments[3].row + ')">Actual</button>';
        }
      },
      {
        orderable: false,
        targets: [20],
        data: function(data) {
          var status = getStatus(data.pause_status);
          var meta = arguments[3];
          var id = buttonsMapping[meta.col] + '_' + meta.row;
          return '<button type="button" id="' + id + '" class="btn btn-primary btn-xs" onclick="update_pause(' + arguments[3].row + ')">' + status + '</button>';
        }
      }
    ]
  });
  $("#project_id").change(function() {
    var project_id = $(this).val();
    get_process(project_id);
  })
  $("#search_process").change(function() {
    var project_id = $("#project_id").val();
    var process_id = $(this).val();
    get_process_task(project_id, process_id);
  })
});

function get_process(project_id) {
  $.ajax({
    url: 'ajax/project_get_process.php',
    data: {
      project_id: project_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_process").empty();
      $("#search_process").append("<option value=''>Select Process</option>");
      for (var i = 0; i < response.length; i++) {
        var id = response[i]['process_master_id'];
        var name = response[i]['process_name'];
        $("#search_process").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_process_id').val();
        $('#search_process option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}

function get_process_task(project_id, process_id) {
  $.ajax({
    url: 'ajax/project_get_task_data.php',
    data: {
      project_id: project_id,
      process_id: process_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_task").empty();
      $("#search_task").append("<option value=''>Select Task</option>");
      for (var i = 0; i < response.length; i++) {
        if (i == response.length - 1) {
          var id = response[i]['project_task_master_id'];
          var name = response[i]['project_task_master_name'];
        } else {
          if (response[i]['project_task_master_name'] == response[i + 1]['project_task_master_name'])
            continue;
        }
        var id = response[i]['project_task_master_id'];
        var name = response[i]['project_task_master_name'];
        $("#search_task").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_task_id').val();
        $('#search_task option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}

function getStatus(status) {
  if (status == "Pause") {
    status = "Release";
  } else {
    status = "Pause";
  }
  return status;
}

function update_pause(row_id) {
  var rowData = table.fnGetData()[row_id];
  var road_id = rowData['project_task_planning_no_of_roads'];
  project_process_task_id = rowData['project_task_planning_task_id'];
  project_id = rowData['project_management_master_id'];
  location_id = rowData['project_task_planning_no_of_roads'];
  var status = getStatus(rowData['pause_status']);
  var form = document.createElement("form");
  form.setAttribute("method", "GET");
  form.setAttribute("action", "project_update_delay_reason_master.php");
  form.setAttribute("target", "blank");

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "project_process_task_id");
  hiddenField1.setAttribute("value", project_process_task_id);

  var hiddenField2 = document.createElement("input");
  hiddenField2.setAttribute("type", "hidden");
  hiddenField2.setAttribute("name", "status");
  hiddenField2.setAttribute("value", status);

  var hiddenField3 = document.createElement("input");
  hiddenField3.setAttribute("type", "hidden");
  hiddenField3.setAttribute("name", "road_id");
  hiddenField3.setAttribute("value", location_id);

  var hiddenField4 = document.createElement("input");
  hiddenField4.setAttribute("type", "hidden");
  hiddenField4.setAttribute("name", "project_id");
  hiddenField4.setAttribute("value", project_id);

  form.appendChild(hiddenField1);
  form.appendChild(hiddenField2);
  form.appendChild(hiddenField3);
  form.appendChild(hiddenField4);

  document.body.appendChild(form);
  form.submit();
}

function update_actual(row_id) {
  var rowData = table.fnGetData()[row_id];
  var manpower_plan = parseInt($('.planned_mp_' + row_id).html());
  var machine_plan = parseInt($('.planned_mc_' + row_id).html());
  var contract_plan = parseInt($('.planned_cw_' + row_id).html());
  var actual_manpower = parseInt($('.total_mp_cost_' + row_id).html());
  var actual_machine = parseInt($('.total_mc_cost_' + row_id).html());
  var actual_contract = parseInt($('.total_actual_cw_cost_' + row_id).html());
  var total_plan_buget = parseInt($('.planned_mp_' + row_id).html()) + parseInt($('.planned_mc_' + row_id).html()) + parseInt($('.planned_cw_' + row_id).html());
  var total_actual_buget = parseInt($('.total_mp_cost_' + row_id).html()) + parseInt($('.total_mc_cost_' + row_id).html()) + parseInt($('.total_actual_cw_cost_' + row_id).html());
  // date condition
  if (moment().set({
      hour: 0,
      minute: 0,
      second: 0,
      millisecond: 0
    }).format('x') > moment(rowData['project_task_planning_end_date']).format('x')) {
    showMessage('Update Plan End Date');
  } else if (rowData['project_task_planning_end_date'] === '0000-00-00' || rowData['project_task_planning_start_date'] === '0000-00-00') {
    showMessage('Update Plan End Date');
  } else if (total_plan_buget == 0) {
    showMessage('Update Plan Buget');
  } else if (machine_plan > actual_machine || manpower_plan > actual_manpower || contract_plan > actual_contract) {
    $('#row_id').val(row_id);
    $('#myModal1').modal('show');
  } else if (rowData.pause_status == 'Pause') {
    showMessage('Release Pause Status');
  } else if (total_actual_buget > total_plan_buget) {
    showMessage('Update Actual Buget Plan');
  } else {
    $('#row_id').val(row_id);
    $('#myModal1').modal('show');
  }
}

function showMessage(alert) {
  $('#myModal2').modal('show');
  $('#modalContent').show().html(alert);
}

function go_to_project_task_actual() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];
  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_mp_' + row_id).html()) > (parseInt($('.total_mp_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_manpower.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Man Power Budget');
  }
}

function go_to_project_task_actual_machine_planning() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];

  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_mc_' + row_id).html()) > (parseInt($('.total_mc_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_machine_plan.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Machine Budget');
  }
}

function go_to_project_task_boq_actuals() {
  var row_id = parseInt($('#row_id').val());
  var data = table.fnGetData()[row_id];

  project_id = data.project_task_planning_project_id;
  project_process_task_id = data.project_task_planning_task_id;
  location_id = data.project_task_planning_no_of_roads;

  if (parseInt($('.planned_cw_' + row_id).html()) > (parseInt($('.total_actual_cw_cost_' + row_id).html()))) {
    var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq_actuals.php");
    form.setAttribute("target", "blank");

    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("name", "project_process_task_id");
    hiddenField1.setAttribute("value", project_process_task_id);

    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("type", "hidden");
    hiddenField2.setAttribute("name", "project_id");
    hiddenField2.setAttribute("value", project_id);

    var hiddenField3 = document.createElement("input");
    hiddenField3.setAttribute("type", "hidden");
    hiddenField3.setAttribute("name", "location_id");
    hiddenField3.setAttribute("value", location_id);

    form.appendChild(hiddenField1);
    form.appendChild(hiddenField2);
    form.appendChild(hiddenField3);

    document.body.appendChild(form);
    form.submit();
    $('#myModal1').modal('hide');
  } else {
    showMessage('Update Plan Contract Budget');
  }
}