var table;
var columnsMapping = {
  '9': 'follow_up_count',
  '10': 'site_count',
};

function drawTable() {
  table.draw();
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  return '<span class="' + classname + '">Loading..</span>';
}

function go_to_enquiry(enquiry_id, unqualified_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "post");
  form.setAttribute("target", "_blank");
  form.setAttribute("action", "crm_add_enquiry.php");
  var hiddenField = document.createElement("input");
  hiddenField.setAttribute("type", "hidden");
  hiddenField.setAttribute("name", "hd_navigate_enquiry_id");
  hiddenField.setAttribute("value", enquiry_id);
  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "hd_navigate_unqual_id");
  hiddenField1.setAttribute("value", unqualified_id);
  form.appendChild(hiddenField);
  form.appendChild(hiddenField1);
  document.body.appendChild(form);
  form.submit();
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
  };

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_unq_enquiry_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      $.ajax({
        url: 'ajax/get_follow_up_data.php',
        data: "enquiry_id=" + full.enquiry_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'follow_up_count' + '_' + index).html(response.fup_count);
          $('span.' + 'site_count' + '_' + index).html(response.sv_count);
        }
      });
    },
    "language": {
      "infoFiltered": " "
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "unqualified_leads";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();
      aoData.project_id = $('#ddl_project').val();
      aoData.search_cell = $('#cell').val();
      aoData.search_reason = $('#search_reason').val();
      aoData.search_source = $('#ddl_search_source').val();
      aoData.search_status = $('#ddl_search_int_status').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    "columns": [{
        "orderable": false,
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          return data.name;
        }
      },
      {
        "orderable": false,
        targets: [2],
        "data": "cell"
      },
      {
        "orderable": false,
        "data": function(data) {
          return data.enquiry_number;
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          return createToolTip(data.project_name, 20);
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          return data.email;
        }
      },
      {
        "orderable": false,
        "data": "enquiry_source_master_name"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (data.walk_in == "1") {
            return 'Yes';
          }
          return 'No';
        }
      },
      {
        "orderable": false,
        "data": function(data) {
          return moment(data.added_on).format("DD/MM/YYYY");
        }
      },
      commonCellDefinition,
      commonCellDefinition,
      {
        "orderable": false,
        "data": function(data, type) {
          return createToolTip(data.unqualified_assigned_to, 20);
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          return moment(data.crm_unqualified_lead_added_on).format("DD/MM/YYYY");
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          var diff_time = moment(data.crm_unqualified_lead_added_on).format("x") - moment(data.added_on).format('x');
          return Math.round(diff_time / (3600 * 1000 * 24));
        }
      },
      {
        "orderable": false,
        "data": function(data, type) {
          return createToolTip(data.reason_name, 20)
        }
      },
      {
        "orderable": false,
        "data": function(data, type, meta) {
          if (!window.permissions.edit) {
            return '***';
          }
          return `<a href="#" onclick=go_to_enquiry(${data.enquiry_id},${data.crm_unqualified_lead_id})> <span class="glyphicon glyphicon-repeat"></span></a>`
        }
      },
    ],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
  });
});