<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
*/

/*
PURPOSE : To get payment reason list
INPUT 	: Payment Reason ID, Payment Reason Name, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of payment reasons
BY 		: Nitin Kashyap
*/
function db_get_payment_reason_list($pay_reason_id,$pay_reason_name,$active,$added_by,$start_date,$end_date)
{
	$get_pay_reason_list_squery_base = "select * from payment_request_reason_master PRM left outer join users U on U.user_id = PRM.payment_request_reason_added_by";
	
	$get_pay_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_pay_reason_list_sdata = array();
	
	if($pay_reason_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason_id = :reason_id";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason_id = :reason_id";				
		}
		
		//Data
		$get_pay_reason_list_sdata[':reason_id']  = $pay_reason_id;
		
		$filter_count++;
	}
	
	if($pay_reason_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason=:pay_reason_name";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason=:pay_reason_name";				
		}
		
		// Data
		$get_pay_reason_list_sdata[':pay_reason_name']  = $pay_reason_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason_active=:active";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason_active=:active";				
		}
		
		// Data
		$get_pay_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason_added_by=:added_by";				
		}
		
		// Data
		$get_pay_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason_added_on >= :start_date";				
		}
		
		//Data
		$get_pay_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." where payment_request_reason_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_pay_reason_list_squery_where = $get_pay_reason_list_squery_where." and payment_request_reason_added_on <= :end_date";				
		}
		
		//Data
		$get_pay_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_pay_reason_list_squery = $get_pay_reason_list_squery_base.$get_pay_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_pay_reason_list_sstatement = $dbConnection->prepare($get_pay_reason_list_squery);
		
		$get_pay_reason_list_sstatement -> execute($get_pay_reason_list_sdata);
		
		$get_pay_reason_list_sdetails = $get_pay_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_pay_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_pay_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_pay_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new payment request reason
INPUT 	: Reason, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_payment_request_reason($reason,$added_by)
{
	// Query
    $request_reason_iquery = "insert into payment_request_reason_master (payment_request_reason,payment_request_reason_active,payment_request_reason_added_by,payment_request_reason_added_on) values (:reason,:active,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $request_reason_istatement = $dbConnection->prepare($request_reason_iquery);
        
        // Data		
        $request_reason_idata = array(':reason'=>$reason,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $request_reason_istatement->execute($request_reason_idata);
		$request_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $request_reason_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To add new payment request
INPUT 	: Task, Amount, Reason, Remarks, Added By
OUTPUT 	: Payment Request ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_payment_request($task_id,$amount,$reason,$remarks,$added_by)
{
	// Query
    $payment_request_iquery = "insert into legal_payment_request (legal_payment_request_task,legal_payment_request_amount,legal_payment_request_status,legal_payment_request_reason,legal_payment_request_remarks,legal_payment_request_added_by,legal_payment_request_added_on) values (:task,:amount,:status,:reason,:remarks,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $payment_request_istatement = $dbConnection->prepare($payment_request_iquery);
        
        // Data		
        $payment_request_idata = array(':task'=>$task_id,':amount'=>$amount,':status'=>'1',':reason'=>$reason,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $payment_request_istatement->execute($payment_request_idata);
		$payment_request_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $payment_request_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To get payment request list
INPUT 	: Payment Request Data Array
OUTPUT 	: Payment Request List
BY 		: Nitin Kashyap
*/
function db_get_payment_request_list($pay_request_data)
{
	// Extract all data
	$pay_request_id = "";
	$task           = "";
	$task_type      = "";
	$process        = "";
	$file_no        = "";
	$status         = "";
	$reason         = "";
	$added_by       = "";
	$start_date     = "";	
	$end_date       = "";
	
	if(array_key_exists("request",$pay_request_data))
	{	
		$pay_request_id = $pay_request_data["request"];
	}
	
	if(array_key_exists("task",$pay_request_data))
	{	
		$task = $pay_request_data["task"];
	}
	
	if(array_key_exists("task_type",$pay_request_data))
	{	
		$task_type = $pay_request_data["task_type"];
	}
	
	if(array_key_exists("process",$pay_request_data))
	{	
		$process = $pay_request_data["process"];
	}
	
	if(array_key_exists("file_number",$pay_request_data))
	{	
		$file_no = $pay_request_data["file_number"];
	}
	
	if(array_key_exists("status",$pay_request_data))
	{	
		$status = $pay_request_data["status"];
	}
	
	if(array_key_exists("reason",$pay_request_data))
	{	
		$reason = $pay_request_data["reason"];
	}
	
	if(array_key_exists("added_by",$pay_request_data))
	{	
		$added_by = $pay_request_data["added_by"];
	}
	
	if(array_key_exists("start_date",$pay_request_data))
	{	
		$start_date = $pay_request_data["start_date"];
	}
	
	if(array_key_exists("end_date",$pay_request_data))
	{	
		$end_date = $pay_request_data["end_date"];
	}

	$get_pay_request_squery_base = "select *,PM.process_name as process_name,BPM.process_name as bulk_process_name from legal_payment_request LPR inner join users U on U.user_id = LPR.legal_payment_request_added_by inner join payment_request_reason_master PRRM on PRRM.payment_request_reason_id = LPR.legal_payment_request_reason inner join task_plan_legal TPL on TPL.task_plan_legal_id = LPR.legal_payment_request_task inner join task_type_master TTM on TTM.task_type_id = TPL.task_plan_legal_type left outer join process_plan_legal PPL on PPL.process_plan_legal_id = TPL.task_plan_legal_process_plan_id left outer join process_master PM on PM.process_master_id = PPL.process_plan_type left outer join files F on F.file_id = PPL.process_plan_legal_file_id left outer join legal_bulk_process LBP on LBP.legal_bulk_process_id = TPL.task_plan_legal_bulk_process_plan_id left outer join process_master BPM on BPM.process_master_id = LBP.legal_bulk_process_type";		
	
	$get_pay_request_squery_where = "";
	
	$get_pay_request_squery_order = "";		
	
	$filter_count = 0;
	
	// Data
	$get_pay_request_sdata = array();
	
	if($pay_request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_id=:request_id";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_id=:request_id";				
		}
		
		// Data
		$get_pay_request_sdata[':request_id']  = $pay_request_id;
		
		$filter_count++;
	}
	
	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_task=:task_id";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_task=:task_id";				
		}
		
		// Data
		$get_pay_request_sdata[':task_id']  = $task;
		
		$filter_count++;
	}
	
	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where TPL.task_plan_legal_type = :task_type_id";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and TPL.task_plan_legal_type = :task_type_id";				
		}
		
		// Data
		$get_pay_request_sdata[':task_type_id']  = $task_type;
		
		$filter_count++;
	}
	
	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where (PPL.process_plan_type = :process or LBP.legal_bulk_process_type = :process)";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and (PPL.process_plan_type = :process or LBP.legal_bulk_process_type = :process)";
		}
		
		// Data
		$get_pay_request_sdata[':process']  = $process;
		
		$filter_count++;
	}
	
	if($file_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where F.file_number = :file_no";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and F.file_number = :file_no";				
		}
		
		// Data
		$get_pay_request_sdata[':file_no']  = $file_no;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_status=:status";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_status=:status";				
		}
		
		// Data
		$get_pay_request_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_reason=:reason";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_reason=:reason";				
		}
		
		// Data
		$get_pay_request_sdata[':reason']  = $reason;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_added_by=:added_by";				
		}
		
		// Data
		$get_pay_request_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_added_on >= :start_date";				
		}
		
		// Data
		$get_pay_request_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where legal_payment_request_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and legal_payment_request_added_on <= :end_date";				
		}
		
		// Data
		$get_pay_request_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	$get_pay_request_squery_order = ' order by legal_payment_request_added_on desc';
	
	$get_pay_request_squery = $get_pay_request_squery_base.$get_pay_request_squery_where.$get_pay_request_squery_order;			
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_pay_request_sstatement = $dbConnection->prepare($get_pay_request_squery);
		
		$get_pay_request_sstatement -> execute($get_pay_request_sdata);
		
		$get_pay_request_sdetails = $get_pay_request_sstatement -> fetchAll();
		
		if(FALSE === $get_pay_request_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_pay_request_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_pay_request_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update payment request
INPUT 	: Payment Request ID, Payment Request Data Array
OUTPUT 	: Payment Request ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_payment_request_status($payment_request_id,$payment_request_data)
{
	if(array_key_exists("amount",$payment_request_data))
	{	
		$amount = $payment_request_data["amount"];
	}
	else
	{
		$amount = "";
	}
	
	if(array_key_exists("status",$payment_request_data))
	{	
		$status = $payment_request_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("reason",$payment_request_data))
	{	
		$reason = $payment_request_data["reason"];
	}
	else
	{
		$reason = "";
	}
	
	// Query
    $payment_request_uquery_base = "update legal_payment_request set";  
	
	$payment_request_uquery_set = "";
	
	$payment_request_uquery_where = " where legal_payment_request_id=:request_id";
	
	$payment_request_udata = array(":request_id"=>$payment_request_id);
	
	$filter_count = 0;
	
	if($amount != "")
	{
		$payment_request_uquery_set = $payment_request_uquery_set." legal_payment_request_amount=:amount,";
		$payment_request_udata[":amount"] = $amount;
		$filter_count++;
	}
	
	if($status != "")
	{
		$payment_request_uquery_set = $payment_request_uquery_set." legal_payment_request_status=:status,";
		$payment_request_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($reason != "")
	{
		$payment_request_uquery_set = $payment_request_uquery_set." legal_payment_request_reason=:reason,";
		$payment_request_udata[":reason"] = $reason;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$payment_request_uquery_set = trim($payment_request_uquery_set,',');
	}
	
	$payment_request_uquery = $payment_request_uquery_base.$payment_request_uquery_set.$payment_request_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $payment_request_ustatement = $dbConnection->prepare($payment_request_uquery);		
        
        $payment_request_ustatement -> execute($payment_request_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $payment_request_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new payment request update
INPUT 	: Request ID, Status, Remarks, Added By
OUTPUT 	: Payment Request Update ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_payment_request_history($request_id,$status,$remarks,$added_by)
{
	// Query
    $payment_request_history_iquery = "insert into legal_payment_request_history (legal_payment_request_id,legal_payment_request_status,legal_payment_request_update_remarks,legal_payment_request_updated_by,legal_payment_request_updated_on) values (:request,:status,:remarks,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $payment_request_history_istatement = $dbConnection->prepare($payment_request_history_iquery);
        
        // Data		
        $payment_request_history_idata = array(':request'=>$request_id,':status'=>$status,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $payment_request_history_istatement->execute($payment_request_history_idata);
		$payment_request_history_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $payment_request_history_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To add new payment release
INPUT 	: Request ID, Amount, Mode, Remarks, Bill Submit Date, Added By
OUTPUT 	: Payment Release ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_payment_release($request_id,$amount,$mode,$remarks,$bill_sub_date,$added_by)
{
	// Query
    $payment_release_iquery = "insert into legal_payments_issued (legal_payment_issued_request,legal_payment_issued_amount,legal_payment_issued_mode,legal_payment_issued_remarks,legal_payment_issued_bill_submit_date,legal_payment_issued_by,legal_payment_issued_on) values (:request,:amount,:mode,:remarks,:bill_date,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $payment_release_istatement = $dbConnection->prepare($payment_release_iquery);
        
        // Data		
        $payment_release_idata = array(':request'=>$request_id,':amount'=>$amount,':mode'=>$mode,':remarks'=>$remarks,':bill_date'=>$bill_sub_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $payment_release_istatement->execute($payment_release_idata);
		$payment_release_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $payment_release_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To get payment released list
INPUT 	: Payment Released Data Array
OUTPUT 	: Payment Released List
BY 		: Nitin Kashyap
*/
function db_get_payment_released_list($pay_release_data)
{
	// Extract all data
	$pay_release_id = "";
	$request_id     = "";
	$mode           = "";
	$task_type      = "";
	$process        = "";
	$file_no        = "";
	$added_by       = "";
	$start_date     = "";	
	$end_date       = "";
	
	if(array_key_exists("release",$pay_release_data))
	{	
		$pay_release_id = $pay_release_data["release"];
	}
	
	if(array_key_exists("request",$pay_release_data))
	{	
		$request_id = $pay_release_data["request"];
	}
	
	if(array_key_exists("mode",$pay_release_data))
	{	
		$mode = $pay_release_data["mode"];
	}
	
	if(array_key_exists("task_type",$pay_release_data))
	{	
		$task_type = $pay_release_data["task_type"];
	}
	
	if(array_key_exists("process",$pay_release_data))
	{	
		$process = $pay_release_data["process"];
	}
	
	if(array_key_exists("file_number",$pay_release_data))
	{	
		$file_no = $pay_release_data["file_number"];
	}
	
	if(array_key_exists("added_by",$pay_release_data))
	{	
		$added_by = $pay_release_data["added_by"];
	}
	
	if(array_key_exists("start_date",$pay_release_data))
	{	
		$start_date = $pay_release_data["start_date"];
	}
	
	if(array_key_exists("end_date",$pay_release_data))
	{	
		$end_date = $pay_release_data["end_date"];
	}

	$get_pay_release_squery_base = "select *,PM.process_name as process_name,BPM.process_name as bulk_process_name from legal_payments_issued LPI inner join payment_mode_master PMM on PMM.payment_mode_id = LPI.legal_payment_issued_mode inner join legal_payment_request LPR on LPR.legal_payment_request_id = LPI.legal_payment_issued_request inner join users U on U.user_id = LPR.legal_payment_request_added_by inner join payment_request_reason_master PRRM on PRRM.payment_request_reason_id = LPR.legal_payment_request_reason inner join task_plan_legal TPL on TPL.task_plan_legal_id = LPR.legal_payment_request_task inner join task_type_master TTM on TTM.task_type_id = TPL.task_plan_legal_type left outer join process_plan_legal PPL on PPL.process_plan_legal_id = TPL.task_plan_legal_process_plan_id left outer join process_master PM on PM.process_master_id = PPL.process_plan_type left outer join files F on F.file_id = PPL.process_plan_legal_file_id left outer join legal_bulk_process LBP on LBP.legal_bulk_process_id = TPL.task_plan_legal_bulk_process_plan_id left outer join process_master BPM on BPM.process_master_id = LBP.legal_bulk_process_type";		
	
	$get_pay_release_squery_where = "";
	
	$get_pay_release_squery_order = "";		
	
	$filter_count = 0;
	
	// Data
	$get_pay_release_sdata = array();
	
	if($pay_release_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_id=:release_id";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_id=:release_id";				
		}
		
		// Data
		$get_pay_release_sdata[':release_id']  = $pay_release_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_request=:request_id";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_request=:request_id";				
		}
		
		// Data
		$get_pay_release_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_mode = :mode";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_mode = :mode";				
		}
		
		// Data
		$get_pay_release_sdata[':mode']  = $mode;
		
		$filter_count++;
	}
	
	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where TPL.task_plan_legal_type = :task_type_id";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and TPL.task_plan_legal_type = :task_type_id";				
		}
		
		// Data
		$get_pay_release_sdata[':task_type_id']  = $task_type;
		
		$filter_count++;
	}
	
	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where (PPL.process_plan_type = :process or LBP.legal_bulk_process_type = :process)";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and (PPL.process_plan_type = :process or LBP.legal_bulk_process_type = :process)";
		}
		
		// Data
		$get_pay_release_sdata[':process']  = $process;
		
		$filter_count++;
	}
	
	if($file_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." where F.file_number = :file_no";								
		}
		else
		{
			// Query
			$get_pay_request_squery_where = $get_pay_request_squery_where." and F.file_number = :file_no";				
		}
		
		// Data
		$get_pay_request_sdata[':file_no']  = $file_no;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_by=:added_by";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_by=:added_by";				
		}
		
		// Data
		$get_pay_release_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_on >= :start_date";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_on >= :start_date";				
		}
		
		// Data
		$get_pay_release_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_payment_issued_on <= :end_date";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_payment_issued_on <= :end_date";				
		}
		
		// Data
		$get_pay_release_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_pay_release_squery_order = ' order by legal_payment_issued_on desc';
	$get_pay_release_squery = $get_pay_release_squery_base.$get_pay_release_squery_where.$get_pay_release_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_pay_release_sstatement = $dbConnection->prepare($get_pay_release_squery);
		
		$get_pay_release_sstatement -> execute($get_pay_release_sdata);
		
		$get_pay_release_sdetails = $get_pay_release_sstatement -> fetchAll();
		
		if(FALSE === $get_pay_release_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_pay_release_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_pay_release_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get payment request history list
INPUT 	: Payment History Data Array
OUTPUT 	: Payment History List
BY 		: Nitin Kashyap
*/
function db_get_payment_request_history($pay_history_data)
{
	// Extract all data
	$request_id = "";
	
	if(array_key_exists("request",$pay_history_data))
	{	
		$request_id = $pay_history_data["request"];
	}

	$get_pay_request_history_squery_base = "select *,LPRH.legal_payment_request_status as pay_status,PM.process_name as process_name,BPM.process_name as bulk_process_name from legal_payment_request_history LPRH inner join legal_payment_request LPR on LPR.legal_payment_request_id = LPRH.legal_payment_request_id inner join users U on U.user_id = LPRH.legal_payment_request_updated_by inner join payment_request_reason_master PRRM on PRRM.payment_request_reason_id = LPR.legal_payment_request_reason inner join task_plan_legal TPL on TPL.task_plan_legal_id = LPR.legal_payment_request_task inner join task_type_master TTM on TTM.task_type_id = TPL.task_plan_legal_type left outer join process_plan_legal PPL on PPL.process_plan_legal_id = TPL.task_plan_legal_process_plan_id left outer join process_master PM on PM.process_master_id = PPL.process_plan_type left outer join files F on F.file_id = PPL.process_plan_legal_file_id left outer join legal_bulk_process LBP on LBP.legal_bulk_process_id = TPL.task_plan_legal_bulk_process_plan_id left outer join process_master BPM on BPM.process_master_id = LBP.legal_bulk_process_type";
	
	$get_pay_request_history_squery_where = "";
	
	$get_pay_request_history_squery_order = "";		
	
	$filter_count = 0;
	
	// Data
	$get_pay_request_history_sdata = array();
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_request_history_squery_where = $get_pay_request_history_squery_where." where LPRH.legal_payment_request_id=:request_id";								
		}
		else
		{
			// Query
			$get_pay_request_history_squery_where = $get_pay_request_history_squery_where." and LPRH.legal_payment_request_id=:request_id";				
		}
		
		// Data
		$get_pay_request_history_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	$get_pay_request_history_squery = $get_pay_request_history_squery_base.$get_pay_request_history_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_pay_request_history_sstatement = $dbConnection->prepare($get_pay_request_history_squery);
		
		$get_pay_request_history_sstatement -> execute($get_pay_request_history_sdata);
		
		$get_pay_request_history_sdetails = $get_pay_request_history_sstatement -> fetchAll();
		
		if(FALSE === $get_pay_request_history_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_pay_request_history_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_pay_request_history_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
?>