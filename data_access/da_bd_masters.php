<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/



/*

PURPOSE : To add new fund source

INPUT 	: Fund Source Name, Added By

OUTPUT 	: Fund Source id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_fund_source($fund_source_name,$added_by)

{

	// Query

    $fund_source_iquery = "insert into bd_fund_source_master (bd_fund_source_name,bd_fund_source_active,bd_fund_source_added_by,bd_fund_source_added_on) values (:fund_source_name,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $fund_source_istatement = $dbConnection->prepare($fund_source_iquery);

        

        // Data

        $funs_source_idata = array(':fund_source_name'=>$fund_source_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $fund_source_istatement->execute($funs_source_idata);

		$fund_source_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $fund_source_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get fund source list

INPUT 	: Bank Name, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of banks

BY 		: Nitin Kashyap

*/

function db_get_fund_source_list($fund_source_name,$active,$added_by,$start_date,$end_date)

{

	$get_fund_source_list_squery_base = "select * from bd_fund_source_master";

	

	$get_fund_source_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_fund_source_list_sdata = array();

	

	if($fund_source_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." where bd_fund_source_name=:fund_source_name";								

		}

		else

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." and bd_fund_source_name=:fund_source_name";				

		}

		

		// Data

		$get_fund_source_list_sdata[':fund_source_name']  = $fund_source_name;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." where bd_fund_source_active=:active";								

		}

		else

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." and bd_fund_source_active=:active";				

		}

		

		// Data

		$get_fund_source_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." where bd_fund_source_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." and bd_fund_source_added_by=:added_by";				

		}

		

		// Data

		$get_fund_source_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." where bd_fund_source_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." and bd_fund_source_added_on >= :start_date";				

		}

		

		//Data

		$get_fund_source_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." where bd_fund_source_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_fund_source_list_squery_where = $get_fund_source_list_squery_where." and bd_fund_source_added_on <= :end_date";				

		}

		

		//Data

		$get_fund_source_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_fund_source_list_squery = $get_fund_source_list_squery_base.$get_fund_source_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_fund_source_list_sstatement = $dbConnection->prepare($get_fund_source_list_squery);

		

		$get_fund_source_list_sstatement -> execute($get_fund_source_list_sdata);

		

		$get_fund_source_list_sdetails = $get_fund_source_list_sstatement -> fetchAll();

		

		if(FALSE === $get_fund_source_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_fund_source_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_fund_source_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new village

INPUT 	: Village Name, nearest Landmark, Added By

OUTPUT 	: Village id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_village($village_name,$landmark,$added_by)

{

	// Query

    $village_iquery = "insert into village_master (village_name,village_nearest_landmark,village_added_by,village_added_on) values (:name,:landmark,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $village_istatement = $dbConnection->prepare($village_iquery);

        

        // Data

        $village_idata = array(':name'=>$village_name,':landmark'=>$landmark,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $village_istatement->execute($village_idata);

		$village_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $village_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get village list

INPUT 	: Village Name, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of villages

BY 		: Nitin Kashyap

*/

function db_get_village_list($village_name,$added_by,$start_date,$end_date)

{

	$get_village_list_squery_base = "select * from village_master";

	

	$get_village_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_village_list_sdata = array();

	

	if($village_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." where village_name=:village_name";								

		}

		else

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." and village_name=:village_name";				

		}

		

		// Data

		$get_village_list_sdata[':village_name']  = $village_name;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." where village_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." and village_added_by=:added_by";				

		}

		

		// Data

		$get_village_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." where village_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." and village_added_on >= :start_date";				

		}

		

		//Data

		$get_village_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." where village_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_village_list_squery_where = $get_village_list_squery_where." and village_added_on <= :end_date";				

		}

		

		//Data

		$get_village_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_village_list_squery_order = ' order by village_name asc';

	$get_village_list_squery = $get_village_list_squery_base.$get_village_list_squery_where.$get_village_list_squery_order;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_village_list_sstatement = $dbConnection->prepare($get_village_list_squery);

		

		$get_village_list_sstatement -> execute($get_village_list_sdata);

		

		$get_village_list_sdetails = $get_village_list_sstatement -> fetchAll();

		

		if(FALSE === $get_village_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_village_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_village_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new owner status

INPUT 	: Owner Status Name, Added By

OUTPUT 	: Village id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_owner_status($owner_status,$added_by)

{

	// Query

    $owner_status_iquery = "insert into bd_file_owner_status (bd_file_owner_status_name,bd_file_owner_status_active,bd_file_owner_status_added_by,bd_file_owner_status_added_on) values (:name,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $owner_status_istatement = $dbConnection->prepare($owner_status_iquery);

        

        // Data

        $owner_status_idata = array(':name'=>$owner_status,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $owner_status_istatement->execute($owner_status_idata);

		$owner_status_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $owner_status_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get owner status list

INPUT 	: Owner Status Name, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of owner status

BY 		: Nitin Kashyap

*/

function db_get_owner_status_list($owner_status_name,$added_by,$start_date,$end_date)

{

	$get_owner_status_list_squery_base = "select * from bd_file_owner_status";

	

	$get_owner_status_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_owner_status_list_sdata = array();

	

	if($owner_status_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." where bd_file_owner_status_name=:owner_status";						

		}

		else

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." and bd_file_owner_status_name=:owner_status";				

		}

		

		// Data

		$get_owner_status_list_sdata[':owner_status']  = $owner_status_name;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." where bd_file_owner_status_added_by=:added_by";						

		}

		else

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." and bd_file_owner_status_added_by=:added_by";				

		}

		

		// Data

		$get_owner_status_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." where bd_file_owner_status_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." and bd_file_owner_status_added_on >= :start_date";				

		}

		

		//Data

		$get_owner_status_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." where bd_file_owner_status_added_on <= :end_date";				

		}

		else

		{

			// Query

			$get_owner_status_list_squery_where = $get_owner_status_list_squery_where." and bd_file_owner_status_added_on <= :end_date";				

		}

		

		//Data

		$get_owner_status_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_owner_status_list_squery = $get_owner_status_list_squery_base.$get_owner_status_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_owner_status_list_sstatement = $dbConnection->prepare($get_owner_status_list_squery);

		

		$get_owner_status_list_sstatement -> execute($get_owner_status_list_sdata);

		

		$get_owner_status_list_sdetails = $get_owner_status_list_sstatement -> fetchAll();

		

		if(FALSE === $get_owner_status_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_owner_status_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_owner_status_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get DELAY REASONS

INPUT 	: Reason Name, Reason Priority, Reason Added By

OUTPUT 	: SUCCESS or Failure message,if it is Success it will give Existing data.

BY 		: Sonakshi

*/

function db_add_delay_reason_master($delay_reason_name,$dealy_reason_priority,$delay_reason_added_by)

{

	$reason_iquery = "insert into bd_delay_reason_master (bd_delay_reason_name,bd_delay_reason_priority,bd_delay_reason_active,bd_delay_reason_added_by,bd_delay_reason_added_on) values (:delay_reason_name,:delay_reason_priority,:delay_reason_active,:delay_reason_added_by,:delay_reason_added_on)";

	

	try{

		

		$dbconnection      = get_conn_handle();

		$reason_istatement = $dbconnection->prepare($reason_iquery);

		

		$reason_idata = array(':delay_reason_name'=>$delay_reason_name,':delay_reason_priority'=>'0',':delay_reason_active'=>'1',':delay_reason_added_by'=>$delay_reason_added_by,':delay_reason_added_on'=>date("Y-m-d H:i:s"));		

		

		$dbconnection->beginTransaction();

		$reason_istatement->execute($reason_idata);

		$delay_reson_master_id = $dbconnection->lastInsertId();

		$dbconnection->commit(); 

        

		$return["status"] = SUCCESS;

		$return["data"]   = $delay_reson_master_id;		

	}

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get DELAY REASONS

INPUT 	: REASON ID AND REASON NAME.

OUTPUT 	: SUCCESS or Failure message,if it is Success it will give Existing data.

BY 		: Sonakshi

*/

function db_get_delay_reason_master($delay_reason_search_data)

{

	// Extract all input parameters

	if(array_key_exists("delay_reason_master_id",$delay_reason_search_data))

	{

		$delay_reason_master_id = $delay_reason_search_data["delay_reason_master_id"];

	}

	else

	{

		$delay_reason_master_id = "";

	}

	

	if(array_key_exists("delay_reason_name",$delay_reason_search_data))

	{

		$delay_reason_name = $delay_reason_search_data["delay_reason_name"];

	}

	else

	{

		$delay_reason_name = "";

	}

	

	if(array_key_exists("delay_reason_priority",$delay_reason_search_data))

	{

		$delay_reason_priority = $delay_reason_search_data["delay_reason_priority"];

	}

	else

	{

		$delay_reason_priority = "";

	}

	

	if(array_key_exists("delay_reason_active",$delay_reason_search_data))

	{

		$delay_reason_active = $delay_reason_search_data["delay_reason_active"];

	}

	else

	{

		$delay_reason_active = "";

	}

	

	if(array_key_exists("delay_reason_added_by",$delay_reason_search_data))

	{

		$delay_reason_added_by = $delay_reason_search_data["delay_reason_added_by"];

	}

	else

	{

		$delay_reason_added_by = "";

	}

	

	if(array_key_exists("delay_reason_added_on	",$delay_reason_search_data))

	{

		$delay_reason_added_on= $delay_reason_search_data["delay_reason_added_on"];

	}

	else

	{

		$delay_reason_added_on= "";

	}

	

	$get_delay_reason_squery_base = "select * from bd_delay_reason_master";

	

	$get_delay_reason_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_delay_reason_sdata = array();

	

	if($delay_reason_master_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_master_id = :delay_reason_master_id";

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_master_id = :delay_reason_master_id";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_master_id']  = $delay_reason_master_id;

		

		$filter_count++;

	}

	

	if($delay_reason_name!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_name = :delay_reason_name";								

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_name = :delay_reason_name";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_name']  = $delay_reason_name;

		

		$filter_count++;

	}

	

	if($delay_reason_priority != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_priority = :delay_reason_priority";

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_priority = :delay_reason_priority";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_priority']  = $delay_reason_priority;

		

		$filter_count++;

	}

	

	if($delay_reason_active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_active = :delay_reason_active";

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_active = :delay_reason_active";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_active']  = $delay_reason_active;

		

		$filter_count++;

	}



	

	if($delay_reason_added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_added_by = :delay_reason_added_by";								

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_added_by = :delay_reason_added_by";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_added_by'] = $delay_reason_added_by;

		

		$filter_count++;

	}

	

	if($delay_reason_added_on != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." where bd_delay_reason_added_on = :delay_reason_added_on	";								

		}

		else

		{

			// Query

			$get_delay_reason_squery_where = $get_delay_reason_squery_where." and bd_delay_reason_added_on = :delay_reason_added_on	";				

		}

		

		// Data

		$get_delay_reason_sdata[':delay_reason_added_on'] = $delay_reason_added_on;

		

		$filter_count++;

	}

	

	$get_delay_reason_squery_order = " order by bd_delay_reason_priority DESC, bd_delay_reason_name ASC";

	$get_delay_reason_squery = $get_delay_reason_squery_base.$get_delay_reason_squery_where.$get_delay_reason_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_delay_reason_sstatement = $dbConnection->prepare($get_delay_reason_squery);

		

		$get_delay_reason_sstatement -> execute($get_delay_reason_sdata);

		

		$get_delay_reason_sdetails = $get_delay_reason_sstatement -> fetchAll();

		

		if(FALSE === $get_delay_reason_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_delay_reason_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_delay_reason_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add court case type

INPUT 	: Case Type, Added by

OUTPUT 	: Court Case Type ID, success or failure message

BY 		: Sonakshi D

*/

function db_add_court_case_type($case_type,$added_by)

{

	// Query

    $case_type_iquery = "insert into bd_court_case_type_master (bd_court_case_type_master_type,bd_court_case_type_active,bd_court_case_type_added_by,bd_court_case_type_added_on) values (:case_type,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $case_type_istatement = $dbConnection->prepare($case_type_iquery);

        

        // Data

        $case_type_idata = array(':case_type'=>$case_type,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $case_type_istatement->execute($case_type_idata);

		$court_case_type_master_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $court_case_type_master_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Court Case Type list

INPUT 	: Case Type, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Court Case Types

BY 		: Sonakshi D

*/

function db_get_court_case_type($case_type,$active,$added_by,$start_date,$end_date)

{

	$get_court_case_type_list_squery_base = "select * from bd_court_case_type_master";

	

	$get_court_case_type_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_court_case_type_list_sdata = array();

	

	if($case_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." where bd_court_case_type_master_type=:case_type";								

		}

		else

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." and bd_court_case_type_master_type=:case_type";				

		}

		

		// Data

		$get_court_case_type_list_sdata[':case_type']  = $case_type;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." where bd_court_case_type_active=:active";

		}

		else

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." and bd_court_case_type_active=:active";				

		}

		

		// Data

		$get_court_case_type_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." where bd_court_case_type_added_by=:added_by";

		}

		else

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." and bd_court_case_type_added_by=:added_by";				

		}

		

		// Data

		$get_court_case_type_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." where bd_court_case_type_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." and bd_court_case_type_added_on >= :start_date";				

		}

		

		//Data

		$get_court_case_type_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." where bd_court_case_type_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_court_case_type_list_squery_where = $get_court_case_type_list_squery_where." and bd_court_case_type_added_on <= :end_date";				

		}

		

		//Data

		$get_court_case_type_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_court_case_type_list_squery = $get_court_case_type_list_squery_base.$get_court_case_type_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_court_case_type_list_sstatement = $dbConnection->prepare($get_court_case_type_list_squery);

		

		$get_court_case_type_list_sstatement -> execute($get_court_case_type_list_sdata);

		

		$get_court_case_type_list_sdetails = $get_court_case_type_list_sstatement -> fetchAll();

		

		if(FALSE === $get_court_case_type_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_court_case_type_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_court_case_type_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new Establishmnet

INPUT 	: Establishmnet ID, Address, Added By

OUTPUT 	: Establishment ID, success or failure message

BY 		: Sonakshi D

*/

function db_add_bd_court_establishment($establishment_name,$address,$added_by)

{

	// Query

    $establishment_iquery = "insert into bd_court_establishment_master (bd_court_establishment_name,bd_court_establishment_address,bd_court_establishment_active,bd_court_establishment_added_by,bd_court_establishment_added_on) values (:establishment_name,:address,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $establishment_istatement = $dbConnection->prepare($establishment_iquery);

        

        // Data		

        $establishment_idata = array(':establishment_name'=>$establishment_name,':address'=>$address,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $establishment_istatement->execute($establishment_idata);	

		$bd_court_establishment_id = $dbConnection -> lastInsertId();		

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = "Successfully Inserted";		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get establishmnet list

INPUT 	: establishment ID, File ID, Added By, Start Date, End Date

OUTPUT 	: List of Establishment

BY 		: Sonakshi D

*/

function db_get_court_establishment($establishment_name,$address,$active,$added_by,$start_date,$end_date)

{

	$get_establishment_list_squery_base = "select * from bd_court_establishment_master";

	

	$get_establishment_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_establishment_list_sdata = array();

	

	if($establishment_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_name=:establishment_name";						

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_name=:establishment_name";				

		}

		

		// Data

		$get_establishment_list_sdata[':establishment_name']  = $establishment_name;

		

		$filter_count++;

	}

	

	if($address != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_address=:address";					

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_address=:address";				

		}

		

		// Data

		$get_establishment_list_sdata[':address']  = $address;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_active=:active";					

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_active=:active";				

		}

		

		// Data

		$get_establishment_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_added_by=:added_by";					

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_added_by=:added_by";				

		}

		

		// Data

		$get_establishment_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_added_on >= :start_date";			

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_added_on >= :start_date";				

		}

		

		//Data

		$get_establishment_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." where bd_court_establishment_added_on <= :end_date";				

		}

		else

		{

			// Query

			$get_establishment_list_squery_where = $get_establishment_list_squery_where." and bd_court_establishment_added_on <= :end_date";				

		}

		

		//Data

		$get_establishment_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}		

	

	$get_establishment_list_squery = $get_establishment_list_squery_base.$get_establishment_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_establishment_list_sstatement = $dbConnection->prepare($get_establishment_list_squery);

		

		$get_establishment_list_sstatement -> execute($get_establishment_list_sdata);

		

		$get_establishment_list_sdetails = $get_establishment_list_sstatement -> fetchAll();

		

		if(FALSE === $get_establishment_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_establishment_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_establishment_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}







/*

PURPOSE : To add new Court Status

INPUT 	: Status Name, Added By

OUTPUT 	: Status id, success or failure message

BY 		: Sonakshi D

*/

function db_add_court_status($status_name,$added_by)

{

	// Query

    $status_iquery = "insert into bd_court_status_master (bd_court_status_name,bd_court_status_master_active,bd_court_status_master_added_by,bd_court_status_master_added_on) values (:status_name,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $status_istatement = $dbConnection->prepare($status_iquery);

        

        // Data

        $status_idata = array(':status_name'=>$status_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $status_istatement->execute($status_idata);

		$bd_court_status_master_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_court_status_master_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Court Status list

INPUT 	: Status Name, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Court Status

BY 		: Sonakshi D

*/

function db_get_court_status_list($status_name,$active,$added_by,$start_date,$end_date)

{

	$get_court_status_list_squery_base = "select * from bd_court_status_master";

	

	$get_court_status_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_court_status_list_sdata = array();

	

	if($status_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." where bd_court_status_name=:status_name";								

		}

		else

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." and bd_court_status_name=:status_name";				

		}

		

		// Data

		$get_court_status_list_sdata[':status_name']  = $status_name;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." where bd_court_status_master_active=:active";								

		}

		else

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." and bd_court_status_master_active=:active";				

		}

		

		// Data

		$get_court_status_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." where bd_court_status_master_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." and bd_court_status_master_added_by=:added_by";				

		}

		

		// Data

		$get_court_status_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." where bd_court_status_master_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." and bd_court_status_master_added_on >= :start_date";				

		}

		

		//Data

		$get_court_status_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." where bd_court_status_master_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_court_status_list_squery_where = $get_court_status_list_squery_where." and bd_court_status_master_added_on <= :end_date";				

		}

		

		//Data

		$get_court_status_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_court_status_list_squery = $get_court_status_list_squery_base.$get_court_status_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_court_status_list_sstatement = $dbConnection->prepare($get_court_status_list_squery);

		

		$get_court_status_list_sstatement -> execute($get_court_status_list_sdata);

		

		$get_court_status_list_sdetails = $get_court_status_list_sstatement -> fetchAll();

		

		if(FALSE === $get_court_status_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_court_status_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_court_status_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new Account Name

INPUT 	: Account Name, Contact Number, Added By

OUTPUT 	: Account Master ID, success or failure message

BY 		: Sonakshi D

*/

function db_add_account($account_name,$contact,$added_by)

{

	// bd_

    $account_iquery = "insert into bd_own_account_master (bd_own_account_master_account_name,bd_own_account_master_contact_no,bd_own_account_master_active,bd_own_account_master_added_by,bd_own_account_master_added_on) values (:name,:contact,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $account_istatement = $dbConnection->prepare($account_iquery);

        

        // Data

        $account_idata = array(':name'=>$account_name,':contact'=>$contact,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $account_istatement->execute($account_idata);

		$bd_own_accunt_master_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_own_accunt_master_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Account list

INPUT 	: Account Name, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Account

BY 		: Sonakshi D

*/

function db_get_account_list($account_name,$active,$added_by,$start_date,$end_date)

{

	$get_account_list_squery_base = "select * from bd_own_account_master";

	

	$get_account_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_account_list_sdata = array();

	

	if($account_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." where bd_own_account_master_account_name=:account_name";								

		}

		else

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." and bd_own_account_master_account_name=:account_name";				

		}

		

		// Data

		$get_account_list_sdata[':account_name']  = $account_name;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." where bd_own_account_master_active=:active";								

		}

		else

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." and bd_own_account_master_active=:active";				

		}

		

		// Data

		$get_account_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." where bd_own_account_master_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." and bd_own_account_master_added_by=:added_by";				

		}

		

		// Data

		$get_account_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." where bd_own_account_master_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." and bd_own_account_master_added_on >= :start_date";				

		}

		

		//Data

		$get_account_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." where bd_own_account_master_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_account_list_squery_where = $get_account_list_squery_where." and bd_own_account_master_added_on <= :end_date";				

		}

		

		//Data

		$get_account_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_account_list_squery = $get_account_list_squery_base.$get_account_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_account_list_sstatement = $dbConnection->prepare($get_account_list_squery);

		

		$get_account_list_sstatement -> execute($get_account_list_sdata);

		

		$get_account_list_sdetails = $get_account_list_sstatement -> fetchAll();

		

		if(FALSE === $get_account_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_account_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_account_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new plaintiff

INPUT 	: Plaintiff Name,Active,Added by

OUTPUT 	: Plaintiff id, success or failure message

BY 		: Sonakshi D

*/

function db_add_plaintiff($plaintiff_name,$added_by)

{

	// Query

    $plaintiff_iquery = "insert into bd_court_plaintiff_master (bd_court_plaintiff_name,bd_court_plaintiff_active,bd_court_plaintiff_added_on,bd_court_plaintiff_added_by) values (:plaintiff_name,:active,:now,:added_by)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $plaintiff_istatement = $dbConnection->prepare($plaintiff_iquery);

        

        // Data

        $plaintiff_idata = array(':plaintiff_name'=>$plaintiff_name,':active'=>'1',':now'=>date("Y-m-d H:i:s"),':added_by'=>$added_by);	

		$dbConnection->beginTransaction();

        $plaintiff_istatement->execute($plaintiff_idata);

		$bd_court_plaintiff_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_court_plaintiff_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Plaintiff List

INPUT 	: Plaintiff Name, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Plaintiff

BY 		: Sonakshi D

*/

function db_get_plaintiff($plaintiff_name,$active,$added_by,$start_date,$end_date)

{

	$get_plaintiff_list_squery_base = "select * from bd_court_plaintiff_master";

	

	$get_plaintiff_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_plaintiff_list_sdata = array();

	

	if($plaintiff_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." where bd_court_plaintiff_name=:plaintiff_name";								

		}

		else

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." and bd_court_plaintiff_name=:plaintiff_name";				

		}

		

		// Data

		$get_plaintiff_list_sdata[':plaintiff_name']  = $plaintiff_name;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." where bd_court_plaintiff_active=:active";								

		}

		else

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." and bd_court_plaintiff_active=:active";				

		}

		

		// Data

		$get_plaintiff_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." where bd_court_plaintiff_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." and bd_court_plaintiff_added_by=:added_by";				

		}

		

		// Data

		$get_plaintiff_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." where bd_court_plaintiff_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." and bd_court_plaintiff_added_on >= :start_date";				

		}

		

		//Data

		$get_plaintiff_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." where bd_court_plaintiff_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_plaintiff_list_squery_where = $get_plaintiff_list_squery_where." and bd_court_plaintiff_added_on <= :end_date";				

		}

		

		//Data

		$get_plaintiff_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_plaintiff_list_squery = $get_plaintiff_list_squery_base.$get_plaintiff_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_plaintiff_list_sstatement = $dbConnection->prepare($get_plaintiff_list_squery);

		

		$get_plaintiff_list_sstatement -> execute($get_plaintiff_list_sdata);

		

		$get_plaintiff_list_sdetails = $get_plaintiff_list_sstatement -> fetchAll();

		

		if(FALSE === $get_plaintiff_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_plaintiff_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_plaintiff_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new diffident

INPUT 	: Diffident Name,Active,Added by

OUTPUT 	: Diffident id, success or failure message

BY 		: Sonakshi D

*/

function db_add_diffident($diffident_name,$added_by)

{

	// Query

    $diffident_iquery = "insert into bd_court_diffident_master (bd_court_diffident_name,bd_court_diffident_active,bd_court_diffident_added_by,bd_court_diffident_added_on) values (:diffident_name,:active,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $diffident_istatement = $dbConnection->prepare($diffident_iquery);

        

        // Data

        $diffident_idata = array(':diffident_name'=>$diffident_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $diffident_istatement->execute($diffident_idata);

		$db_court_diffident_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $db_court_diffident_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Diffident List

INPUT 	: Diffident Name, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Diffident

BY 		: Sonakshi D

*/

function db_get_diffident($diffident_name,$active,$added_by,$start_date,$end_date)

{

	$get_diffident_list_squery_base = "select * from bd_court_diffident_master";

	

	$get_diffident_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_diffident_list_sdata = array();

	

	if($diffident_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." where bd_court_diffident_name=:diffident_name";								

		}

		else

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." and bd_court_diffident_name=:diffident_name";				

		}

		

		// Data

		$get_diffident_list_sdata[':diffident_name']  = $diffident_name;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." where bd_court_diffident_active=:active";								

		}

		else

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." and bd_court_diffident_active=:active";				

		}

		

		// Data

		$get_diffident_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." where bd_court_diffident_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." and bd_court_diffident_added_by=:added_by";				

		}

		

		// Data

		$get_diffident_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." where bd_court_diffident_added_on >= :start_date";						

		}

		else

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." and bd_court_diffident_added_on >= :start_date";				

		}

		

		//Data

		$get_diffident_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." where bd_court_diffident_added_on <= :end_date";						

		}

		else

		{

			// Query

			$get_diffident_list_squery_where = $get_diffident_list_squery_where." and bd_court_diffident_added_on <= :end_date";				

		}

		

		//Data

		$get_diffident_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_diffident_list_squery = $get_diffident_list_squery_base.$get_diffident_list_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_diffident_list_sstatement = $dbConnection->prepare($get_diffident_list_squery);

		

		$get_diffident_list_sstatement -> execute($get_diffident_list_sdata);

		

		$get_diffident_list_sdetails = $get_diffident_list_sstatement -> fetchAll();

		

		if(FALSE === $get_diffident_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_diffident_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_diffident_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}

?>