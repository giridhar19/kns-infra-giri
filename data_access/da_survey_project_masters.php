<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new Survey Process Master
INPUT 	: Name, Order, Process Type, Remarks, Added By
OUTPUT 	: master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_process_master($name,$order,$process_type,$remarks,$added_by)
{
	// Query
   $survey_process_master_iquery = "insert into survey_process_master (survey_process_master_name,survey_process_master_order,survey_process_master_type,survey_process_master_active,survey_process_master_remarks,survey_process_master_added_by,
   survey_process_master_added_on) values (:name,:order,:process_type,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_process_master_istatement = $dbConnection->prepare($survey_process_master_iquery);
        
        // Data
        $survey_process_master_idata = array(':name'=>$name,':order'=>$order,':process_type'=>$process_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_process_master_istatement->execute($survey_process_master_idata);
		$survey_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Process Master List
INPUT 	: Master ID, Name, Order, process Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Master
BY 		: Lakshmi
*/
function db_get_survey_process_master($survey_process_master_search_data)
{  
	if(array_key_exists("master_id",$survey_process_master_search_data))
	{
		$master_id = $survey_process_master_search_data["master_id"];
	}
	else
	{
		$master_id= "";
	}
	
	if(array_key_exists("name",$survey_process_master_search_data))
	{
		$name = $survey_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("order",$survey_process_master_search_data))
	{
		$order = $survey_process_master_search_data["order"];
	}
	else
	{
		$order = "";
	}
	
	if(array_key_exists("process_type",$survey_process_master_search_data))
	{
		$process_type = $survey_process_master_search_data["process_type"];
	}
	else
	{
		$process_type = "";
	}
	
	if(array_key_exists("process_name_check",$survey_process_master_search_data))
	{
		$process_name_check = $survey_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$survey_process_master_search_data))
	{
		$active = $survey_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_process_master_search_data))
	{
		$added_by = $survey_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_process_master_search_data))
	{
		$start_date= $survey_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_process_master_search_data))
	{
		$end_date= $survey_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_process_master_list_squery_base = "select * from survey_process_master SPM inner join users U on U.user_id = SPM.survey_process_master_added_by";
	
	$get_survey_process_master_list_squery_where = "";
	$get_survey_process_master_list_squery_order_by = " order by survey_process_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_survey_process_master_list_sdata = array();
	
	if($master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_id = :master_id";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_id = :master_id";				
		}
		
		// Data
		$get_survey_process_master_list_sdata[':master_id'] = $master_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_name = :name";		
				// Data
				$get_survey_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_name like :name";						

				// Data
				$get_survey_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_name like :name";						

				// Data
				$get_survey_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_name = :name";	
					
				// Data
				$get_survey_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." or survey_process_master_name like :name";				
				// Data
				$get_survey_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_name like :name";
				
				// Data
				$get_survey_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	if($order != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_order = :order";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_order = :order";				
		}
		
		// Data
		$get_survey_process_master_list_sdata[':order']  = $order;
		
		$filter_count++;
	}
	
	if($process_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_type = :process_type";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_type = :process_type";				
		}
		
		// Data
		$get_survey_process_master_list_sdata[':process_type']  = $process_type;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_active = :active";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_active = :active";				
		}
		
		// Data
		$get_survey_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_added_by = :added_by";
		}
		
		//Data
		$get_survey_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." where survey_process_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_process_master_list_squery_where = $get_survey_process_master_list_squery_where." and survey_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_survey_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_process_master_list_squery = $get_survey_process_master_list_squery_base.$get_survey_process_master_list_squery_where.$get_survey_process_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_process_master_list_sstatement = $dbConnection->prepare($get_survey_process_master_list_squery);
		
		$get_survey_process_master_list_sstatement -> execute($get_survey_process_master_list_sdata);
		
		$get_survey_process_master_list_sdetails = $get_survey_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey Process Master 
INPUT 	: Master ID, Survey Process Master  Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_process_master($master_id,$survey_process_master_update_data)
{
	if(array_key_exists("name",$survey_process_master_update_data))
	{	
		$name = $survey_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("order",$survey_process_master_update_data))
	{	
		$order = $survey_process_master_update_data["order"];
	}
	else
	{
		$order = "";
	}
	
	if(array_key_exists("process_type",$survey_process_master_update_data))
	{	
		$process_type = $survey_process_master_update_data["process_type"];
	}
	else
	{
		$process_type = "";
	}
	
	if(array_key_exists("active",$survey_process_master_update_data))
	{	
		$active = $survey_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_process_master_update_data))
	{	
		$remarks = $survey_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_process_master_update_data))
	{	
		$added_by = $survey_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_process_master_update_data))
	{	
		$added_on = $survey_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_process_master_update_uquery_base = "update survey_process_master set";  
	
	$survey_process_master_update_uquery_set = "";
	
	$survey_process_master_update_uquery_where = " where survey_process_master_id = :master_id";
	
	$survey_process_master_update_udata = array(":master_id"=>$master_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_name = :name,";
		$survey_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($order != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_order = :order,";
		$survey_process_master_update_udata[":order"] = $order;
		$filter_count++;
	}
	
	if($process_type != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_type = :process_type,";
		$survey_process_master_update_udata[":process_type"] = $process_type;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_active = :active,";
		$survey_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_remarks = :remarks,";
		$survey_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_added_by = :added_by,";
		$survey_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_process_master_update_uquery_set = $survey_process_master_update_uquery_set." survey_process_master_added_on = :added_on,";
		$survey_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_process_master_update_uquery_set = trim($survey_process_master_update_uquery_set,',');
	}
	
	$survey_process_master_update_uquery = $survey_process_master_update_uquery_base.$survey_process_master_update_uquery_set.$survey_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_process_master_update_ustatement = $dbConnection->prepare($survey_process_master_update_uquery);		
        
        $survey_process_master_update_ustatement -> execute($survey_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_delay_reason_master($name,$remarks,$added_by)
{
	// Query
   $survey_delay_reason_master_iquery = "insert into survey_delay_reason_master (survey_delay_reason_master_name,survey_delay_reason_master_active,survey_delay_reason_master_remarks,survey_delay_reason_master_added_by,survey_delay_reason_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_delay_reason_master_istatement = $dbConnection->prepare($survey_delay_reason_master_iquery);
        
        // Data
        $survey_delay_reason_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_delay_reason_master_istatement->execute($survey_delay_reason_master_idata);
		$survey_delay_reason_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_delay_reason_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Delay Reason Master
BY 		: Lakshmi
*/
function db_get_survey_delay_reason_master($survey_delay_reason_master_search_data)
{  
	if(array_key_exists("reason_id",$survey_delay_reason_master_search_data))
	{
		$reason_id = $survey_delay_reason_master_search_data["reason_id"];
	}
	else
	{
		$reason_id= "";
	}
	
	if(array_key_exists("name",$survey_delay_reason_master_search_data))
	{
		$name = $survey_delay_reason_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("reason_name_check",$survey_delay_reason_master_search_data))
	{
		$reason_name_check = $survey_delay_reason_master_search_data["reason_name_check"];
	}
	else
	{
		$reason_name_check = "";
	}
	
	if(array_key_exists("active",$survey_delay_reason_master_search_data))
	{
		$active = $survey_delay_reason_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_delay_reason_master_search_data))
	{
		$added_by = $survey_delay_reason_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_delay_reason_master_search_data))
	{
		$start_date= $survey_delay_reason_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_delay_reason_master_search_data))
	{
		$end_date= $survey_delay_reason_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_delay_reason_master_list_squery_base = "select * from survey_delay_reason_master SDRM inner join users U on U.user_id = SDRM.survey_delay_reason_master_added_by";
	
	$get_survey_delay_reason_master_list_squery_where = "";
	$get_survey_delay_reason_master_list_squery_order_by = " order by survey_delay_reason_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_survey_delay_reason_master_list_sdata = array();
	
	if($reason_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_id = :reason_id";								
		}
		else
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_id = :reason_id";				
		}
		
		// Data
		$get_survey_delay_reason_master_list_sdata[':reason_id'] = $reason_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($reason_name_check == '1')
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_name = :name";		
				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = $name;
			}
			else if($reason_name_check == '2')
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_name like :name";						

				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_name like :name";						

				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($reason_name_check == '1')
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_name = :name";	
					
				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = $name;
			}
			else if($reason_name_check == '2')
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." or survey_delay_reason_master_name like :name";				
				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_name like :name";
				
				// Data
				$get_survey_delay_reason_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_active = :active";								
		}
		else
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_active = :active";				
		}
		
		// Data
		$get_survey_delay_reason_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_added_by = :added_by";
		}
		
		//Data
		$get_survey_delay_reason_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_delay_reason_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." where survey_delay_reason_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_delay_reason_master_list_squery_where = $get_survey_delay_reason_master_list_squery_where." and survey_delay_reason_master_added_on <= :end_date";
		}
		
		//Data
		$get_survey_delay_reason_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_delay_reason_master_list_squery = $get_survey_delay_reason_master_list_squery_base.$get_survey_delay_reason_master_list_squery_where.$get_survey_delay_reason_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_delay_reason_master_list_sstatement = $dbConnection->prepare($get_survey_delay_reason_master_list_squery);
		
		$get_survey_delay_reason_master_list_sstatement -> execute($get_survey_delay_reason_master_list_sdata);
		
		$get_survey_delay_reason_master_list_sdetails = $get_survey_delay_reason_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_delay_reason_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_delay_reason_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_delay_reason_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update Survey Delay Reason Master 
INPUT 	: Reason ID, Survey Delay Reason Master Update Array
OUTPUT 	: Reason ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_delay_reason_master($reason_id,$survey_delay_reason_master_update_data)
{
	if(array_key_exists("name",$survey_delay_reason_master_update_data))
	{	
		$name = $survey_delay_reason_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$survey_delay_reason_master_update_data))
	{	
		$active = $survey_delay_reason_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_delay_reason_master_update_data))
	{	
		$remarks = $survey_delay_reason_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_delay_reason_master_update_data))
	{	
		$added_by = $survey_delay_reason_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_delay_reason_master_update_data))
	{	
		$added_on = $survey_delay_reason_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_delay_reason_master_update_uquery_base = "update survey_delay_reason_master set";  
	
	$survey_delay_reason_master_update_uquery_set = "";
	
	$survey_delay_reason_master_update_uquery_where = " where survey_delay_reason_master_id = :reason_id";
	
	$survey_delay_reason_master_update_udata = array(":reason_id"=>$reason_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$survey_delay_reason_master_update_uquery_set = $survey_delay_reason_master_update_uquery_set." survey_delay_reason_master_name = :name,";
		$survey_delay_reason_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_delay_reason_master_update_uquery_set = $survey_delay_reason_master_update_uquery_set." survey_delay_reason_master_active = :active,";
		$survey_delay_reason_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_delay_reason_master_update_uquery_set = $survey_delay_reason_master_update_uquery_set." survey_delay_reason_master_remarks = :remarks,";
		$survey_delay_reason_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_delay_reason_master_update_uquery_set = $survey_delay_reason_master_update_uquery_set." survey_delay_reason_master_added_by = :added_by,";
		$survey_delay_reason_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_delay_reason_master_update_uquery_set = $survey_delay_reason_master_update_uquery_set." survey_delay_reason_master_added_on = :added_on,";
		$survey_delay_reason_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_delay_reason_master_update_uquery_set = trim($survey_delay_reason_master_update_uquery_set,',');
	}
	
	$survey_delay_reason_master_update_uquery = $survey_delay_reason_master_update_uquery_base.$survey_delay_reason_master_update_uquery_set.$survey_delay_reason_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_delay_reason_master_update_ustatement = $dbConnection->prepare($survey_delay_reason_master_update_uquery);		
        
        $survey_delay_reason_master_update_ustatement -> execute($survey_delay_reason_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $reason_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Master
INPUT 	: Project, Parent Survey ID, Village, Survey No, Land Owner, Extent, Remarks, Added By, Updated By, Updated On
OUTPUT 	: Survey ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_master($project,$parent_survey_id,$village,$survey_no,$land_owner,$extent,$remarks,$added_by,$updated_by,$updated_on)
{
	// Query
   $survey_master_iquery = "insert into survey_master
   (survey_master_project,survey_master_parent_survey_id,survey_master_village,survey_master_survey_no,survey_master_land_owner,survey_master_extent,survey_master_active,survey_master_remarks,survey_master_added_by,
   survey_master_updated_by,survey_master_updated_on,survey_master_added_on) values(:project,:parent_survey_id,:village,:survey_no,:land_owner,:extent,:active,:remarks,:added_by,:updated_by,:updated_on,:added_on)";  
 
    try
    {
        $dbConnection = get_conn_handle();
        $survey_master_istatement = $dbConnection->prepare($survey_master_iquery);
        
        // Data
        $survey_master_idata = array(':project'=>'',':parent_survey_id'=>$parent_survey_id,':village'=>$village,':survey_no'=>$survey_no,':land_owner'=>$land_owner,
		':extent'=>$extent,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':updated_by'=>$updated_by,':updated_on'=>$updated_on,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_master_istatement->execute($survey_master_idata);
		$survey_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Master list
INPUT 	: Survey ID, village, Survey No, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Master
BY 		: Lakshmi
*/
function db_get_survey_master($survey_master_search_data)
{  
	if(array_key_exists("survey_id",$survey_master_search_data))
	{
		$survey_id = $survey_master_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("project",$survey_master_search_data))
	{
		$project = $survey_master_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("parent_survey_id",$survey_master_search_data))
	{
		$parent_survey_id = $survey_master_search_data["parent_survey_id"];
	}
	else
	{
		$parent_survey_id = "";
	}
	
	if(array_key_exists("village",$survey_master_search_data))
	{
		$village = $survey_master_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("survey_no",$survey_master_search_data))
	{
		$survey_no = $survey_master_search_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	
	if(array_key_exists("land_owner",$survey_master_search_data))
	{
		$land_owner = $survey_master_search_data["land_owner"];
	}
	else
	{
		$land_owner = "";
	}
	
	if(array_key_exists("extent",$survey_master_search_data))
	{
		$extent = $survey_master_search_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("active",$survey_master_search_data))
	{
		$active = $survey_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_master_search_data))
	{
		$added_by = $survey_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("survey_village",$survey_master_search_data))
	{
		$survey_village = $survey_master_search_data["survey_village"];
	}
	else
	{
		$survey_village = "";
	}
	
	if(array_key_exists("start_date",$survey_master_search_data))
	{
		$start_date = $survey_master_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_master_search_data))
	{
		$end_date = $survey_master_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("is_mapped",$survey_master_search_data))
	{
		$is_mapped = $survey_master_search_data["is_mapped"];
	}
	else
	{
		$is_mapped = "";
	}
	
	if(array_key_exists("remarks",$survey_master_search_data))
	{
		$remarks = $survey_master_search_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("updated_by",$survey_master_search_data))
	{
		$updated_by = $survey_master_search_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_master_search_data))
	{
		$updated_on = $survey_master_search_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}

	$get_survey_master_list_squery_base = "select SM.survey_master_id as survey_master_id,SM.survey_master_parent_survey_id as parent_survey_no,ASM.survey_master_survey_no as parent_survey,SM.survey_master_extent as extent,SM.survey_master_survey_no as survey_no,SM.survey_master_land_owner as land_owner,VM.village_name as village_name,VM.village_id as village_id,SM.survey_master_added_on as added_on,SM.survey_master_remarks as remarks,SM.survey_master_updated_by as updated_by,SM.survey_master_updated_on as updated_on,U.user_name as user_name,SM.survey_master_active as survey_master_active from survey_master SM inner join users U on U.user_id = SM.survey_master_added_by inner join village_master VM on VM.village_id = SM.survey_master_village left outer join survey_master ASM on ASM.survey_master_id = SM.survey_master_parent_survey_id";
	
	$get_survey_master_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_master_list_sdata = array();
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_id = :survey_id";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_id = :survey_id";				
		}
		
		// Data
		$get_survey_master_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_updated_by = :updated_by";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_updated_by = :updated_by";				
		}
		
		// Data
		$get_survey_master_list_sdata[':updated_by'] = $updated_by;
		
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_updated_on = :updated_on";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_updated_on = :updated_on";				
		}
		
		// Data
		$get_survey_master_list_sdata[':updated_on'] = $updated_on;
		
		$filter_count++;
	}
	
	if($remarks != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_remarks = :remarks";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_remarks = :remarks";				
		}
		
		// Data
		$get_survey_master_list_sdata[':remarks'] = $remarks;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_project = :project";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_project = :project";				
		}
		
		// Data
		$get_survey_master_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($parent_survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_parent_survey_id = :parent_survey_id";							
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_parent_survey_id = :parent_survey_id";				
		}
		
		// Data
		$get_survey_master_list_sdata[':parent_survey_id'] = $parent_survey_id;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_village = :village";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_village = :village";				
		}
		
		// Data
		$get_survey_master_list_sdata[':village'] = $village;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_survey_no like :survey_no";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_survey_no like :survey_no";				
		}
		
		// Data
		$get_survey_master_list_sdata[':survey_no'] = '%'.$survey_no.'%';
		
		$filter_count++;
	}
	
	if($land_owner != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_land_owner = :land_owner";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_land_owner = :land_owner";				
		}
		
		// Data
		$get_survey_master_list_sdata[':land_owner'] = $land_owner;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_extent = :extent";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_extent = :extent";				
		}
		
		// Data
		$get_survey_master_list_sdata[':extent'] = $extent;
		
		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_active = :active";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_active = :active";				
		}
		
		// Data
		$get_survey_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_added_by = :added_by";
		}
		
		//Data
		$get_survey_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_added_on <= :end_date";
		}
		
		//Data
		$get_survey_master_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($is_mapped != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." where SM.survey_master_id not in (select survey_file_bd_file_id from survey_file SAF inner join survey_details SAD on SAD.survey_details_id = SAF.survey_file_details_id where SAF.survey_file_active = 1 and SAD.survey_details_village = :survey_village)";								
		}
		else
		{
			// Query
			$get_survey_master_list_squery_where = $get_survey_master_list_squery_where." and SM.survey_master_id not in (select survey_file_bd_file_id from survey_file SAF inner join survey_details SAD on SAD.survey_details_id = SAF.survey_file_details_id where SAF.survey_file_active = 1 and SAD.survey_details_village = :survey_village)";
		}
		
		//Data
		$get_survey_master_list_sdata[':survey_village']  = $survey_village;
		
		$filter_count++;
	}
	
	$get_survey_master_list_squery = $get_survey_master_list_squery_base.$get_survey_master_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_master_list_sstatement = $dbConnection->prepare($get_survey_master_list_squery);
		
		$get_survey_master_list_sstatement -> execute($get_survey_master_list_sdata);
		
		$get_survey_master_list_sdetails = $get_survey_master_list_sstatement -> fetchAll();		
	
		if(FALSE === $get_survey_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update Survey Master 
INPUT 	: Survey ID, Survey Master Update Array
OUTPUT 	: Survey ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_master($survey_id,$survey_master_update_data)
{
	
	if(array_key_exists("project",$survey_master_update_data))
	{	
		$project = $survey_master_update_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("updated_by",$survey_master_update_data))
	{	
		$updated_by = $survey_master_update_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_master_update_data))
	{	
		$updated_on = $survey_master_update_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}
	
	if(array_key_exists("parent_survey_id",$survey_master_update_data))
	{	
		$parent_survey_id = $survey_master_update_data["parent_survey_id"];
	}
	else
	{
		$parent_survey_id = "";
	}
	
	if(array_key_exists("village",$survey_master_update_data))
	{	
		$village = $survey_master_update_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("survey_no",$survey_master_update_data))
	{	
		$survey_no = $survey_master_update_data["survey_no"];
	}
	else
	{
		$survey_no = "";
	}
	
	if(array_key_exists("land_owner",$survey_master_update_data))
	{	
		$land_owner = $survey_master_update_data["land_owner"];
	}
	else
	{
		$land_owner = "";
	}
	
	if(array_key_exists("extent",$survey_master_update_data))
	{	
		$extent = $survey_master_update_data["extent"];
	}
	else
	{
		$extent = "";
	}
	
	if(array_key_exists("active",$survey_master_update_data))
	{	
		$active = $survey_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_master_update_data))
	{	
		$remarks = $survey_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_master_update_data))
	{	
		$added_by = $survey_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_master_update_data))
	{	
		$added_on = $survey_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_master_update_uquery_base = "update survey_master set ";  
	
	$survey_master_update_uquery_set = "";
	
	$survey_master_update_uquery_where = " where survey_master_id = :survey_id";
	
	$survey_master_update_udata = array(":survey_id"=>$survey_id);
	
	$filter_count = 0;
	
	if($village != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_village = :village,";
		$survey_master_update_udata[":village"] = $village;
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_survey_no = :survey_no,";
		$survey_master_update_udata[":survey_no"] = $survey_no;
		$filter_count++;
	}
	
	if($land_owner != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_land_owner = :land_owner,";
		$survey_master_update_udata[":land_owner"] = $land_owner;
		$filter_count++;
	}
	
	if($extent != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_extent = :extent,";
		$survey_master_update_udata[":extent"] = $extent;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_active = :active,";
		$survey_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_remarks = :remarks,";
		$survey_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_added_by = :added_by,";
		$survey_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_added_on = :added_on,";
		$survey_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_updated_by = :updated_by,";
		$survey_master_update_udata[":updated_by"] = $updated_by;		
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		$survey_master_update_uquery_set = $survey_master_update_uquery_set." survey_master_updated_on = :updated_on,";
		$survey_master_update_udata[":updated_on"] = $updated_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_master_update_uquery_set = trim($survey_master_update_uquery_set,',');
	}
	
	$survey_master_update_uquery = $survey_master_update_uquery_base.$survey_master_update_uquery_set.$survey_master_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_master_update_ustatement = $dbConnection->prepare($survey_master_update_uquery);		
        
        $survey_master_update_ustatement -> execute($survey_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

/*
PURPOSE : To add new Survey Document Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Document Type ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_document_type_master($name,$remarks,$added_by)
{
	// Query
   $survey_document_type_master_iquery = "insert into survey_document_type_master (survey_document_type_master_name,survey_document_type_master_active,survey_document_type_master_remarks,survey_document_type_master_added_by,survey_document_type_master_added_on) values (:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_document_type_master_istatement = $dbConnection->prepare($survey_document_type_master_iquery);
        
        // Data
        $survey_document_type_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_document_type_master_istatement->execute($survey_document_type_master_idata);
		$survey_document_type_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_document_type_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Document Type Master List
INPUT 	: Document Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Document Type Master
BY 		: Lakshmi
*/
function db_get_survey_document_type_master($survey_document_type_master_search_data)
{  
	if(array_key_exists("document_type_id",$survey_document_type_master_search_data))
	{
		$document_type_id = $survey_document_type_master_search_data["document_type_id"];
	}
	else
	{
		$document_type_id= "";
	}
	
	if(array_key_exists("name",$survey_document_type_master_search_data))
	{
		$name = $survey_document_type_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("document_type_name_check",$survey_document_type_master_search_data))
	{
		$document_type_name_check = $survey_document_type_master_search_data["document_type_name_check"];
	}
	else
	{
		$document_type_name_check = "";
	}
	
	if(array_key_exists("active",$survey_document_type_master_search_data))
	{
		$active = $survey_document_type_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_document_type_master_search_data))
	{
		$added_by = $survey_document_type_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_document_type_master_search_data))
	{
		$start_date= $survey_document_type_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_document_type_master_search_data))
	{
		$end_date= $survey_document_type_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_document_type_master_list_squery_base = "select * from survey_document_type_master SDTM inner join users U on U.user_id = SDTM.survey_document_type_master_added_by";
	
	$get_survey_document_type_master_list_squery_where = "";
	$get_survey_document_type_master_list_squery_order_by = " order by survey_document_type_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_survey_document_type_master_list_sdata = array();
	
	if($document_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_id = :document_type_id";								
		}
		else
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_id = :document_type_id";				
		}
		
		// Data
		$get_survey_document_type_master_list_sdata[':document_type_id'] = $document_type_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($document_type_name_check == '1')
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_name = :name";		
				// Data
				$get_survey_document_type_master_list_sdata[':name']  = $name;
			}
			else if($document_type_name_check == '2')
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_name like :name";						

				// Data
				$get_survey_document_type_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_name like :name";						

				// Data
				$get_survey_document_type_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($document_type_name_check == '1')
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_name = :name";	
					
				// Data
				$get_survey_document_type_master_list_sdata[':name']  = $name;
			}
			else if($document_type_name_check == '2')
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." or survey_document_type_master_name like :name";				
				// Data
				$get_survey_document_type_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_name like :name";
				
				// Data
				$get_survey_document_type_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_active = :active";								
		}
		else
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_active = :active";				
		}
		
		// Data
		$get_survey_document_type_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_added_by = :added_by";
		}
		
		//Data
		$get_survey_document_type_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_document_type_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." where survey_document_type_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_document_type_master_list_squery_where = $get_survey_document_type_master_list_squery_where." and survey_document_type_master_added_on <= :end_date";
		}
		
		//Data
		$get_survey_document_type_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_document_type_master_list_squery = $get_survey_document_type_master_list_squery_base.$get_survey_document_type_master_list_squery_where.$get_survey_document_type_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_document_type_master_list_sstatement = $dbConnection->prepare($get_survey_document_type_master_list_squery);
		
		$get_survey_document_type_master_list_sstatement -> execute($get_survey_document_type_master_list_sdata);
		
		$get_survey_document_type_master_list_sdetails = $get_survey_document_type_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_document_type_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_document_type_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_document_type_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey Document Type Master 
INPUT 	: Document Type ID, Document Type Master Update Array
OUTPUT 	: Document Type ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_document_type_master($document_type_id,$survey_document_type_master_update_data)
{
	if(array_key_exists("name",$survey_document_type_master_update_data))
	{	
		$name = $survey_document_type_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$survey_document_type_master_update_data))
	{	
		$active = $survey_document_type_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_document_type_master_update_data))
	{	
		$remarks = $survey_document_type_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_document_type_master_update_data))
	{	
		$added_by = $survey_document_type_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_document_type_master_update_data))
	{	
		$added_on = $survey_document_type_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_document_type_master_update_uquery_base = "update survey_document_type_master set";  
	
	$survey_document_type_master_update_uquery_set = "";
	
	$survey_document_type_master_update_uquery_where = " where survey_document_type_master_id = :document_type_id";
	
	$survey_document_type_master_update_udata = array(":document_type_id"=>$document_type_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$survey_document_type_master_update_uquery_set = $survey_document_type_master_update_uquery_set." survey_document_type_master_name = :name,";
		$survey_document_type_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_document_type_master_update_uquery_set = $survey_document_type_master_update_uquery_set." survey_document_type_master_active = :active,";
		$survey_document_type_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_document_type_master_update_uquery_set = $survey_document_type_master_update_uquery_set." survey_document_type_master_remarks = :remarks,";
		$survey_document_type_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_document_type_master_update_uquery_set = $survey_document_type_master_update_uquery_set." survey_document_type_master_added_by = :added_by,";
		$survey_document_type_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_document_type_master_update_uquery_set = $survey_document_type_master_update_uquery_set." survey_document_type_master_added_on = :added_on,";
		$survey_document_type_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_document_type_master_update_uquery_set = trim($survey_document_type_master_update_uquery_set,',');
	}
	
	$survey_document_type_master_update_uquery = $survey_document_type_master_update_uquery_base.$survey_document_type_master_update_uquery_set.$survey_document_type_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_document_type_master_update_ustatement = $dbConnection->prepare($survey_document_type_master_update_uquery);		
        
        $survey_document_type_master_update_ustatement -> execute($survey_document_type_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_type_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Project ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_project_master($name,$remarks,$added_by)
{
	// Query
   $survey_project_master_iquery = "insert into survey_project_master (survey_project_master_name,survey_project_master_active,survey_project_master_remarks,survey_project_master_added_by,survey_project_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_project_master_istatement = $dbConnection->prepare($survey_project_master_iquery);
        
        // Data
        $survey_project_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_project_master_istatement->execute($survey_project_master_idata);
		$survey_project_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_project_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey project Master List
INPUT 	: Project ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey project Master
BY 		: Lakshmi
*/
function db_get_survey_project_master($survey_project_master_search_data)
{  
	if(array_key_exists("project_id",$survey_project_master_search_data))
	{
		$project_id = $survey_project_master_search_data["project_id"];
	}
	else
	{
		$project_id= "";
	}
	
	if(array_key_exists("name",$survey_project_master_search_data))
	{
		$name = $survey_project_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("project_name_check",$survey_project_master_search_data))
	{
		$project_name_check = $survey_project_master_search_data["project_name_check"];
	}
	else
	{
		$project_name_check = "";
	}
	
	if(array_key_exists("active",$survey_project_master_search_data))
	{
		$active = $survey_project_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_project_master_search_data))
	{
		$added_by = $survey_project_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_project_master_search_data))
	{
		$start_date= $survey_project_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_project_master_search_data))
	{
		$end_date= $survey_project_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_project_master_list_squery_base = "select * from survey_project_master SPMM inner join users U on U.user_id = SPMM.survey_project_master_added_by";
	
	$get_survey_project_master_list_squery_where = "";
	$get_survey_project_master_list_squery_order_by = " order by survey_project_master_name ASC";
	$filter_count = 0;
	
	// Data
	$get_survey_project_master_list_sdata = array();
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_id = :project_id";								
		}
		else
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_id = :project_id";				
		}
		
		// Data
		$get_survey_project_master_list_sdata[':project_id'] = $project_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($project_name_check == '1')
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_name = :name";		
				// Data
				$get_survey_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_name like :name";						

				// Data
				$get_survey_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_name like :name";						

				// Data
				$get_survey_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($project_name_check == '1')
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_name = :name";	
					
				// Data
				$get_survey_project_master_list_sdata[':name']  = $name;
			}
			else if($project_name_check == '2')
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." or survey_project_master_name like :name";				
				// Data
				$get_survey_project_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_name like :name";
				
				// Data
				$get_survey_project_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_active = :active";								
		}
		else
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_active = :active";				
		}
		
		// Data
		$get_survey_project_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_added_by = :added_by";
		}
		
		//Data
		$get_survey_project_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_project_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." where survey_project_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_project_master_list_squery_where = $get_survey_project_master_list_squery_where." and survey_project_master_added_on <= :end_date";
		}
		
		//Data
		$get_survey_project_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_project_master_list_squery = $get_survey_project_master_list_squery_base.$get_survey_project_master_list_squery_where.$get_survey_project_master_list_squery_order_by;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_project_master_list_sstatement = $dbConnection->prepare($get_survey_project_master_list_squery);
		
		$get_survey_project_master_list_sstatement -> execute($get_survey_project_master_list_sdata);
		
		$get_survey_project_master_list_sdetails = $get_survey_project_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_project_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_project_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_project_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey Project Master 
INPUT 	: Project ID, Survey Project Master Update Array
OUTPUT 	: Project ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_project_master($project_id,$survey_project_master_update_data)
{
	if(array_key_exists("name",$survey_project_master_update_data))
	{	
		$name = $survey_project_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$survey_project_master_update_data))
	{	
		$active = $survey_project_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_project_master_update_data))
	{	
		$remarks = $survey_project_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_project_master_update_data))
	{	
		$added_by = $survey_project_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_project_master_update_data))
	{	
		$added_on = $survey_project_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_project_master_update_uquery_base = "update survey_project_master set";  
	
	$survey_project_master_update_uquery_set = "";
	
	$survey_project_master_update_uquery_where = " where survey_project_master_id = :project_id";
	
	$survey_project_master_update_udata = array(":project_id"=>$project_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$survey_project_master_update_uquery_set = $survey_project_master_update_uquery_set." survey_project_master_name = :name,";
		$survey_project_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_project_master_update_uquery_set = $survey_project_master_update_uquery_set." survey_project_master_active = :active,";
		$survey_project_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_project_master_update_uquery_set = $survey_project_master_update_uquery_set." survey_project_master_remarks = :remarks,";
		$survey_project_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_project_master_update_uquery_set = $survey_project_master_update_uquery_set." survey_project_master_added_by = :added_by,";
		$survey_project_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_project_master_update_uquery_set = $survey_project_master_update_uquery_set." survey_project_master_added_on = :added_on,";
		$survey_project_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_project_master_update_uquery_set = trim($survey_project_master_update_uquery_set,',');
	}
	
	$survey_project_master_update_uquery = $survey_project_master_update_uquery_base.$survey_project_master_update_uquery_set.$survey_project_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_project_master_update_ustatement = $dbConnection->prepare($survey_project_master_update_uquery);		
        
        $survey_project_master_update_ustatement -> execute($survey_project_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

?>