<?php

/**

 * @author Nitin

 * @copyright 2016

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');



// if(function_exists($_GET['f'])) {
//   $data = array("date_start"=>$_GET['date_start'],"date_end"=>$_GET['date_end']);
//   echo json_encode(db_get_holiday_list($data)["data"]);
// } else {
//   echo 'Method Not Exist';
// }

/*

PURPOSE : To add new holiday

INPUT 	: Date, Holiday Name, Mandatory or not, Added By

OUTPUT 	: Holiday id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_holiday($holiday_date,$holiday_name,$type,$holiday_added_by)

{

	// Query

    $holiday_iquery = "insert into hr_holiday_master (hr_holiday_date,hr_holiday_name,hr_holiday_type,hr_holiday_status,hr_holiday_added_by,hr_holiday_added_on) values (:date,:name,:type,:status,:added_by,:now)";



    try

    {

        $dbConnection = get_conn_handle();



        $holiday_istatement = $dbConnection->prepare($holiday_iquery);



        // Data

        $holiday_idata = array(':date'=>$holiday_date,':name'=>$holiday_name,':type'=>$type,':status'=>'1',':added_by'=>$holiday_added_by,':now'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();

        $holiday_istatement->execute($holiday_idata);

		$holiday_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $holiday_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get holidays list

INPUT 	: Holiday Data Array: Holiday ID, Name, Category, Active

OUTPUT 	: List of holidays

BY 		: Nitin Kashyap

*/

function db_get_holiday_list($holiday_filter_data)

{

	$get_holiday_list_squery_base = "select * from hr_holiday_master HM inner join users U on U.user_id = HM.hr_holiday_added_by";



	$get_holiday_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_holiday_list_sdata = array();



	if(array_key_exists("holiday_id",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_id=:holiday_id";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_id=:holiday_id";

		}



		// Data

		$get_holiday_list_sdata[':holiday_id']  = $holiday_filter_data["holiday_id"];



		$filter_count++;

	}



	if(array_key_exists("date_start",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_date >= :date_start";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_date >= :date_start";

		}



		// Data

		$get_holiday_list_sdata[':date_start']  = $holiday_filter_data["date_start"];



		$filter_count++;

	}



	if(array_key_exists("date_end",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_date <= :date_end";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_date <= :date_end";

		}



		// Data

		$get_holiday_list_sdata[':date_end']  = $holiday_filter_data["date_end"];



		$filter_count++;

	}



	if(array_key_exists("type",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_type = :type";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_type = :type";

		}



		// Data

		$get_holiday_list_sdata[':type']  = $holiday_filter_data["type"];



		$filter_count++;

	}



	if(array_key_exists("status",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_status = :status";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_status = :status";

		}



		// Data

		$get_holiday_list_sdata[':status']  = $holiday_filter_data["status"];



		$filter_count++;

	}



	if(array_key_exists("added_by",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_added_by = :added_by";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_added_by = :added_by";

		}



		// Data

		$get_holiday_list_sdata[':added_by']  = $holiday_filter_data["added_by"]["value"];



		$filter_count++;

	}



	if(array_key_exists("start_date",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_added_on >= :start_date";

		}



		// Data

		$get_holiday_list_sdata[':start_date']  = $holiday_filter_data["start_date"];



		$filter_count++;

	}



	if(array_key_exists("end_date",$holiday_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." where hr_holiday_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_holiday_list_squery_where = $get_holiday_list_squery_where." and hr_holiday_added_on <= :end_date";

		}



		// Data

		$get_holiday_list_sdata[':end_date']  = $holiday_filter_data["end_date"];



		$filter_count++;

	}



	$get_holiday_list_squery_order = " order by hr_holiday_date asc";

	$get_holiday_list_squery = $get_holiday_list_squery_base.$get_holiday_list_squery_where.$get_holiday_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_holiday_list_sstatement = $dbConnection->prepare($get_holiday_list_squery);



		$get_holiday_list_sstatement -> execute($get_holiday_list_sdata);



		$get_holiday_list_sdetails = $get_holiday_list_sstatement -> fetchAll();



		if(FALSE === $get_holiday_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_holiday_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_holiday_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	return $return;

}


/*

PURPOSE : To update holiday details

INPUT 	: Holiday ID, Holiday Data Array

OUTPUT 	: Holiday ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_holiday_details($holiday_id,$holiday_data)

{

	if(array_key_exists("date",$holiday_data))

	{

		$date = $holiday_data["date"];

	}

	else

	{

		$date = "";

	}



	if(array_key_exists("name",$holiday_data))

	{

		$name = $holiday_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("status",$holiday_data))

	{

		$status = $holiday_data["status"];

	}

	else

	{

		$status = "";

	}



	// Query

    $holiday_uquery_base = "update hr_holiday_master set";



	$holiday_uquery_set = "";



	$holiday_uquery_where = " where hr_holiday_id=:holiday_id";



	$holiday_udata = array(":holiday_id"=>$holiday_id);



	$filter_count = 0;



	if($date != "")

	{

		$holiday_uquery_set = $holiday_uquery_set." hr_holiday_date=:date,";

		$holiday_udata[":date"] = $date;

		$filter_count++;

	}



	if($name != "")

	{

		$holiday_uquery_set = $holiday_uquery_set." hr_holiday_name=:name,";

		$holiday_udata[":name"] = $name;

		$filter_count++;

	}



	if($status != "")

	{

		$holiday_uquery_set = $holiday_uquery_set." hr_holiday_status=:status,";

		$holiday_udata[":status"] = $status;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$holiday_uquery_set = trim($holiday_uquery_set,',');

	}



	$holiday_uquery = $holiday_uquery_base.$holiday_uquery_set.$holiday_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $holiday_ustatement = $dbConnection->prepare($holiday_uquery);



        $holiday_ustatement -> execute($holiday_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $holiday_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}

?>
