<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/



/*

PURPOSE : To add new payment request

INPUT 	: file_id,amount,reason,remarks,payable_to,added_by

OUTPUT 	: Payment id, success or failure message

BY 		: Punith

*/

function db_add_bd_payment_request($file_id,$amount,$reason,$remarks,$payable_to,$added_by)

{

	// Query

    $process_type_iquery = "insert into bd_payment_requests (bd_payment_requests_file_id,bd_payment_requests_amount,bd_payment_requests_reason,bd_payment_requests_remarks,bd_payment_requests_payable_to,bd_payment_requests_status,bd_payment_requests_added_by,bd_payment_requests_added_on) values (:file_id,:amount,:reason,:remarks,:payable_to,:status,:added_by,:now)";  



    try

    {

        $dbConnection = get_conn_handle();

        

        $process_type_istatement = $dbConnection->prepare($process_type_iquery);

        

        // Data

        $process_type_idata = array(':file_id'=>$file_id,':amount'=>$amount,':reason'=>$reason,':remarks'=>$remarks,':payable_to'=>$payable_to,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $process_type_istatement->execute($process_type_idata);

		$bd_payment_requests_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_payment_requests_id;		

    }

    catch(PDOException $e)

    {

   // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}

/*

PURPOSE : To get payment list

INPUT 	: File_ID, Amount, Reason, Payable To, Added By, Start date, End date

OUTPUT 	: List of payments

BY 		: punith

*/

function db_get_bd_payment_request($payment_request_data)

{

	$get_payment_request_squery_base = "select * from bd_payment_requests BPR left outer join bd_project_files BPF on BPF.bd_project_file_id = BPR.bd_payment_requests_file_id left outer join bd_projects_master BPM on BPM.bd_project_id = BPF.bd_mapped_project_id inner join users U on U.user_id = BPR.bd_payment_requests_added_by";

	

	$get_payment_request_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_payment_request_sdata = array();

	

	if(array_key_exists("request",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_id=:request_id";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_id=:request_id";				

		}

		

		// Data

		$get_payment_request_sdata[':request_id']  = $payment_request_data['request'];

		

		$filter_count++;

	}

	

	if(array_key_exists("file_id",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_file_id=:file_id";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_file_id=:file_id";				

		}

		

		// Data

		$get_payment_request_sdata[':file_id']  = $payment_request_data['file_id'];

		

		$filter_count++;

	}

	

	if(array_key_exists("amount",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_amount=:amount";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_amount=:amount";				

		}

		

		// Data

		$get_payment_request_sdata[':amount']  = $payment_request_data['amount'];

		

		$filter_count++;

	}

	

	if(array_key_exists("reason",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_reason=:reason";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_reason=:reason";				

		}

		

		// Data

		$get_payment_request_sdata[':reason']  = $payment_request_data['reason'];

		

		$filter_count++;

	}

	

	if(array_key_exists("status",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_status=:status";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_status=:status";				

		}

		

		// Data

		$get_payment_request_sdata[':status']  = $payment_request_data['status'];

		

		$filter_count++;

	}

	

	if(array_key_exists("added_by",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_added_by=:added_by";				

		}

		

		// Data

		$get_payment_request_sdata[':added_by']  = $payment_request_data['added_by'];

		

		$filter_count++;

	}

	

	if(array_key_exists("start_date",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_added_on >= :start_date";				

		}

		

		//Data

		$get_payment_request_sdata[':start_date']  = $payment_request_data['start_date'];

		

		$filter_count++;

	}



	if(array_key_exists("end_date",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." where bd_payment_requests_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_payment_request_squery_where = $get_payment_request_squery_where." and bd_payment_requests_added_on <= :end_date";				

		}

		

		//Data

		$get_payment_request_sdata[':end_date']  = $payment_request_data['end_date'];

		

		$filter_count++;

	}

	

	$get_payment_request_squery = $get_payment_request_squery_base.$get_payment_request_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_payment_request_sstatement = $dbConnection->prepare($get_payment_request_squery);

		

		$get_payment_request_sstatement -> execute($get_payment_request_sdata);

		

		$get_payment_request_sdetails = $get_payment_request_sstatement -> fetchAll();

		

		if(FALSE === $get_payment_request_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_payment_request_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_payment_request_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update BD file details

INPUT 	: id,file_id,amount,reason,remarks,payable_to,added_by,added_on

OUTPUT 	: Payment ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_bd_payment_request($bd_payment_request_id,$bd_payment_request_data)

{

	// Query

    $bd_file_payment_uquery_base = "update bd_payment_requests set";  

	

	$bd_file_payment_uquery_set = "";

	

	$bd_file_payment_uquery_where = " where bd_payment_requests_id=:payment_request_id";

	

	$bd_file_payment_udata = array(":payment_request_id"=>$bd_payment_request_id);

	

	$filter_count = 0;

	

	if(array_key_exists("file_id",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_file_id=:file_id,";

		$bd_file_payment_udata[":file_id"] = $bd_payment_request_data['file_id'];

		$filter_count++;

	}

	

	if(array_key_exists("amount",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_amount=:amount,";

		$bd_file_payment_udata[":amount"] = $bd_payment_request_data['amount'];

		$filter_count++;

	}

	

	if(array_key_exists("reason",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_reason=:reason,";

		$bd_file_payment_udata[":reason"] = $bd_payment_request_data['reason'];

		$filter_count++;

	}

	

	if(array_key_exists("remarks",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_remarks=:remarks,";

		$bd_file_payment_udata[":remarks"] = $bd_payment_request_data['remarks'];

		$filter_count++;

	}



	if(array_key_exists("payable_to",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_payable_to=:payable_to,";

		$bd_file_payment_udata[":payable_to"] = $bd_payment_request_data['payable_to'];

		$filter_count++;

	}

	

	if(array_key_exists("status",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_payment_requests_status=:status,";

		$bd_file_payment_udata[":status"] = $bd_payment_request_data['status'];

		$filter_count++;

	}

	

	if(array_key_exists("added_by",$bd_payment_request_data))

	{

		$bd_file_payment_uquery_set = $bd_file_payment_uquery_set." bd_file_added_by=:added_by,";

		$bd_file_payment_udata["added_by"] = $bd_payment_request_data['added_by'];

		$filter_count++;

	}	

		

	if($filter_count > 0)

	{

		$bd_file_payment_uquery_set = trim($bd_file_payment_uquery_set,',');

	}

	

	$bd_file_payment_uquery = $bd_file_payment_uquery_base.$bd_file_payment_uquery_set.$bd_file_payment_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $bd_file_payment_ustatement = $dbConnection->prepare($bd_file_payment_uquery);		

        

        $bd_file_payment_ustatement -> execute($bd_file_payment_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_payment_request_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add new BD payment request update

INPUT 	: Request ID, Status, Remarks, Added By

OUTPUT 	: Payment Request Update ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_bd_payment_request_history($request_id,$status,$remarks,$added_by)

{

	// Query

    $payment_request_history_iquery = "insert into bd_payment_request_history (bd_payment_request_id,bd_payment_request_status,bd_payment_request_update_remarks,bd_payment_request_updated_by,bd_payment_request_updated_on) values (:request,:status,:remarks,:added_by,:now)";

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $payment_request_history_istatement = $dbConnection->prepare($payment_request_history_iquery);

        

        // Data		

        $payment_request_history_idata = array(':request'=>$request_id,':status'=>$status,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		

		$dbConnection->beginTransaction();

        $payment_request_history_istatement->execute($payment_request_history_idata);

		$payment_request_history_id = $dbConnection->lastInsertId();

		$dbConnection->commit();

		

		$return["status"] = SUCCESS;

		$return["data"]   = $payment_request_history_id;		

	}

    catch(PDOException $e)

    {

		// Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

	}

    return $return;

}



/*

PURPOSE : To get BD payment request history list

INPUT 	: Payment History Data Array

OUTPUT 	: Payment History List

BY 		: Nitin Kashyap

*/

function db_get_bd_payment_request_history($pay_history_data)

{

	// Extract all data

	$request_id = "";

	

	if(array_key_exists("request",$pay_history_data))

	{	

		$request_id = $pay_history_data["request"];

	}



	$get_pay_request_history_squery_base = "select * from bd_payment_request_history";

	

	$get_pay_request_history_squery_where = "";

	

	$get_pay_request_history_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_pay_request_history_sdata = array();

	

	if($request_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pay_request_history_squery_where = $get_pay_request_history_squery_where." where bd_payment_request_id=:request_id";								

		}

		else

		{

			// Query

			$get_pay_request_history_squery_where = $get_pay_request_history_squery_where." and bd_payment_request_id=:request_id";				

		}

		

		// Data

		$get_pay_request_history_sdata[':request_id']  = $request_id;

		

		$filter_count++;

	}

	

	$get_pay_request_history_squery = $get_pay_request_history_squery_base.$get_pay_request_history_squery_where;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_pay_request_history_sstatement = $dbConnection->prepare($get_pay_request_history_squery);

		

		$get_pay_request_history_sstatement -> execute($get_pay_request_history_sdata);

		

		$get_pay_request_history_sdetails = $get_pay_request_history_sstatement -> fetchAll();

		

		if(FALSE === $get_pay_request_history_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_pay_request_history_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_pay_request_history_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new payment request

INPUT 	: Borrow_id, Amount, Reason, Remarks, Payable to, Added_by

OUTPUT 	: Payment ID, success or failure message

BY 		: Punith

*/

function db_add_bd_borrow_payment_request($borrow_id,$amount,$reason,$remarks,$payable_to,$added_by)

{

	// Query

    $borrow_payment_iquery = "insert into bd_file_borrow_payments (bd_file_borrow_id,bd_file_borrow_payment_amount,bd_file_borrow_payment_reason,bd_file_borrow_payment_remarks,bd_file_borrow_payment_payable_to,bd_file_borrow_payment_status,bd_file_borrow_payment_added_by,bd_file_borrow_payment_added_on) values (:borrow_id,:amount,:reason,:remarks,:payable_to,:status,:added_by,:now)";  



    try

    {

        $dbConnection = get_conn_handle();

        

        $borrow_payment_istatement = $dbConnection->prepare($borrow_payment_iquery);

        

        // Data

        $borrow_payment_idata = array(':borrow_id'=>$borrow_id,':amount'=>$amount,':reason'=>$reason,':remarks'=>$remarks,':payable_to'=>$payable_to,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $borrow_payment_istatement->execute($borrow_payment_idata);

		$bd_file_borrow_payment_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $bd_file_borrow_payment_id;		

    }

    catch(PDOException $e)

    {

   // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get repayment payment list

INPUT 	: Borrow ID, Amount, Reason, Payable To, Added By, Start date, End date

OUTPUT 	: List of payments

BY 		: Punith

*/

function db_get_bd_borrow_payment_request($payment_request_data)

{

	$get_borrow_payment_request_squery_base = "select * from bd_file_borrow_payments BPR inner join users U on U.user_id = BPR.bd_file_borrow_payment_added_by";

	

	$get_borrow_payment_request_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_payment_request_sdata = array();

	

	if(array_key_exists("request",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_id=:request_id";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_id=:request_id";				

		}

		

		// Data

		$get_payment_request_sdata[':request_id']  = $payment_request_data['request'];

		

		$filter_count++;

	}

	

	if(array_key_exists("borrow_id",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_id=:borrow_id";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_id=:borrow_id";				

		}

		

		// Data

		$get_payment_request_sdata[':borrow_id']  = $payment_request_data['borrow_id'];

		

		$filter_count++;

	}

	

	if(array_key_exists("amount",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_amount=:amount";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_amount=:amount";				

		}

		

		// Data

		$get_payment_request_sdata[':amount']  = $payment_request_data['amount'];

		

		$filter_count++;

	}

	

	if(array_key_exists("reason",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_reason=:reason";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_reason=:reason";				

		}

		

		// Data

		$get_payment_request_sdata[':reason']  = $payment_request_data['reason'];

		

		$filter_count++;

	}

	

	if(array_key_exists("status",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_status=:status";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_status=:status";				

		}

		

		// Data

		$get_payment_request_sdata[':status']  = $payment_request_data['status'];

		

		$filter_count++;

	}

	

	if(array_key_exists("added_by",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_added_by=:added_by";				

		}

		

		// Data

		$get_payment_request_sdata[':added_by']  = $payment_request_data['added_by'];

		

		$filter_count++;

	}

	

	if(array_key_exists("start_date",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_added_on >= :start_date";				

		}

		

		//Data

		$get_payment_request_sdata[':start_date']  = $payment_request_data['start_date'];

		

		$filter_count++;

	}



	if(array_key_exists("end_date",$payment_request_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." where bd_file_borrow_payment_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_borrow_payment_request_squery_where = $get_borrow_payment_request_squery_where." and bd_file_borrow_payment_added_on <= :end_date";				

		}

		

		//Data

		$get_payment_request_sdata[':end_date']  = $payment_request_data['end_date'];

		

		$filter_count++;

	}

	

	$get_borrow_payment_request_squery = $get_borrow_payment_request_squery_base.$get_borrow_payment_request_squery_where;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_borrow_payment_request_sstatement = $dbConnection->prepare($get_borrow_payment_request_squery);

		

		$get_borrow_payment_request_sstatement -> execute($get_payment_request_sdata);

		

		$get_borrow_payment_request_sdetails = $get_borrow_payment_request_sstatement -> fetchAll();

		

		if(FALSE === $get_borrow_payment_request_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_borrow_payment_request_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_borrow_payment_request_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update BD file details

INPUT 	: Borrow Payment ID, Borrow Payment Data

OUTPUT 	: Payment ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_borrow_bd_payment_request($borrow_payment_id,$bd_borrow_payment_data)

{

	// Query

    $bd_borrow_payment_uquery_base = "update bd_file_borrow_payments set";  

	

	$bd_borrow_payment_uquery_set = "";

	

	$bd_borrow_payment_uquery_where = " where bd_file_borrow_payment_id = :borrow_payment_id";

	

	$bd_borrow_payment_udata = array(":borrow_payment_id"=>$borrow_payment_id);

	

	$filter_count = 0;

	

	if(array_key_exists("borrow_id",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_id = :borrow_id,";

		$bd_borrow_payment_udata[":borrow_id"] = $bd_borrow_payment_data['borrow_id'];

		$filter_count++;

	}

	

	if(array_key_exists("amount",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_payment_amount=:amount,";

		$bd_borrow_payment_udata[":amount"] = $bd_borrow_payment_data['amount'];

		$filter_count++;

	}

	

	if(array_key_exists("reason",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_payment_reason=:reason,";

		$bd_borrow_payment_udata[":reason"] = $bd_borrow_payment_data['reason'];

		$filter_count++;

	}

	

	if(array_key_exists("remarks",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_payment_remarks=:remarks,";

		$bd_borrow_payment_udata[":remarks"] = $bd_borrow_payment_data['remarks'];

		$filter_count++;

	}



	if(array_key_exists("payable_to",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_payment_payable_to=:payable_to,";

		$bd_borrow_payment_udata[":payable_to"] = $bd_borrow_payment_data['payable_to'];

		$filter_count++;

	}

	

	if(array_key_exists("status",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_borrow_payment_status=:status,";

		$bd_borrow_payment_udata[":status"] = $bd_borrow_payment_data['status'];

		$filter_count++;

	}

	

	if(array_key_exists("added_by",$bd_borrow_payment_data))

	{

		$bd_borrow_payment_uquery_set = $bd_borrow_payment_uquery_set." bd_file_added_by=:added_by,";

		$bd_borrow_payment_udata["added_by"] = $bd_borrow_payment_data['added_by'];

		$filter_count++;

	}	

		

	if($filter_count > 0)

	{

		$bd_borrow_payment_uquery_set = trim($bd_borrow_payment_uquery_set,',');

	}

	

	$bd_borrow_payment_uquery = $bd_borrow_payment_uquery_base.$bd_borrow_payment_uquery_set.$bd_borrow_payment_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $bd_borrow_payment_ustatement = $dbConnection->prepare($bd_borrow_payment_uquery);		

        

        $bd_borrow_payment_ustatement -> execute($bd_borrow_payment_data);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $borrow_payment_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}

?>