<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	// Capture the form data
	if(isset($_POST["add_tds_deduction_submit"]))
	{
		$deduction_type  		  = $_POST["ddl_tds_type"];
		$deduction_percentage     = $_POST["num_deduction"];
		$deduction_vendor_id     = $_POST["ddl_vendor_id"];
		$remarks      		  	  = $_POST["stxt_remarks"];
if($deduction_percentage!="" && $deduction_percentage>0  && $remarks!=""){

	$tds_deduction_result = i_add_project_tds_deduction_master($deduction_type,$deduction_percentage,$deduction_vendor_id,date("Y-m-d"),$remarks,$user);
	if($tds_deduction_result["status"] == SUCCESS)
	{
		$alert_type=1;
		$alert="Tds Deduction added successfully";
	}
}
	else
	{
		$alert_type=0;
		$alert="Kindly Fill all the fields";
	}
}
}
else
{
	header("location:login.php");
}
?>
<style>
  .input-sms{
	margin: 10px !important;
}
.table-top{
	margin-top: 28px;
  border-top: 1px solid #D5D5D5 !important;
}
</style>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>TDS Deduction Master</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js"></script>
  <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
  <link href="./css/style.css" rel="stylesheet">
  <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
  <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="./bootstrap_aku.min.css" rel="stylesheet">

</head>

<body>

  <?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
?>

  <div class="main">

    <div class="main-inner">

      <div class="container">

        <div class="row">

          <div class="span12">

            <div class="widget ">

              <div class="widget-header">
                <i class="icon-user"></i>
              </div> <!-- /widget-header -->

              <div class="widget-content table-top">

                <ul class="nav nav-tabs">
                  <li>
                    <a href="#formcontrols" data-toggle="tab">TDS Deduction Master</a>
                  </li>
                </ul>
                <br>
                <div class="control-group">
                  <div class="controls">
                    <?php
								if($alert_type == 0) // Failure
								{
								?>
                    <div class="alert alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>
                        <?php echo $alert; ?></strong>
                    </div>
                    <?php
								}
								?>

                    <?php
								if($alert_type == 1) // Success
								{
								?>
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <?php echo $alert; ?>
                      <?php
								}
								?>
                    </div> <!-- /controls -->
                  </div> <!-- /control-group -->
                  <div>
                    <form id="add_quotation_compare_form" class="form-horizontal" method="post" action="project_add_tds_master.php">
                      <input type="hidden" name="hd_manpower_id" value="<?php echo $manpower_id; ?>" />
                      <div class="form-group input-sms">
                        <label class="control-label" for="ddl_tds_type">Deduction Type*</label>
                        <select class="form-control" name="ddl_tds_type" id="ddl_tds_type" required>
                          <option value="">- - -Select Deduction Type- - -</option>
                          <option value="Manpower">Manpower</option>
                          <option value="Machine">Machine</option>
                          <option value="Contract">Contract</option>
                        </select>
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
                        <label class="control-label" for="ddl_tds_type">Vendor List*</label>
                        <select class="form-control" id="ddl_vendor_id" name="ddl_vendor_id" required>
                          <option value="">- - -Select Vendor- - -</option>
                        </select>
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
                        <label class="control-label" for="num_deduction">Deduction Percentage*</label>
                        <input class="form-control" type="number" name="num_deduction">
                      </div> <!-- /control-group -->

                      <div class="form-group input-sms">
                        <label class="control-label" for="stxt_remarks">Remarks</label>
                        <div>
                          <input class="form-control" type="text" name="stxt_remarks" placeholder="Remarks">
                        </div> <!-- /control-group -->

                        <div class="form-actions input-sms">
                          <input type="submit" class="btn btn-primary" name="add_tds_deduction_submit" value="Submit" />
                          <button type="reset" class="btn">Cancel</button>
                        </div> <!-- /form-actions -->

                    </form>
                  </div>
                </div>

              </div> <!-- /widget-content -->

            </div> <!-- /widget -->

          </div> <!-- /span8 -->




        </div> <!-- /row -->

      </div> <!-- /container -->

    </div> <!-- /main-inner -->

  </div> <!-- /main -->

  <script>
    $("#ddl_tds_type").on("change", function() {
      var vendor_type = $("#ddl_tds_type").val();
      if (vendor_type !== "") {
        $.ajax({
          url: "ajax/project_get_vendor_data.php",
          data: {
            type: vendor_type
          },
          success: function(response) {
            response = JSON.parse(response);
            $("#ddl_vendor_id").empty();
            $("#ddl_vendor_id").append("<option value=''>Select Vendor</option>");
            for (var i = 0; i < response.length; i++) {
              if (vendor_type == "Machine") {
                var id = response[i]['project_machine_vendor_master_id'];
                var name = response[i]['project_machine_vendor_master_name'];
              } else {
                var id = response[i]['project_manpower_agency_id'];
                var name = response[i]['project_manpower_agency_name'];
              }
              $("#ddl_vendor_id").append("<option value='" + id + "'>" + name + "</option>");
            }
          }
        });
      }
    });
  </script>

</body>

</html>
