<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_accont_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn account for customer withdrawals

*/



/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Query String Data
	if(isset($_REQUEST["material_id"]))

	{

		$material_id = $_REQUEST["material_id"];

	}

	else

	{

		$material_id = "";

	}
	if(isset($_POST['stock_search_submit']))

	{
		if(isset($_POST["cb_closing_stock"]))
		{
			$closing_stock_value   = '0';
		}
		else
		{
			$closing_stock_value   = '-1';
		}
		$project   = $_POST["ddl_project"];
	}

	else

	{
		$project   = $_REQUEST["ddl_project"];
		$closing_stock_value = '-1';
	}
	$start_date = "";
	$start_date_filter = "";
	if(isset($_REQUEST["dt_start_date"]))
	{
		$start_date = $_REQUEST["dt_start_date"];
		if($start_date != '')
		{
			$start_date_filter = $start_date.' 00:00:00';
		}
	}
	else
	{
		//$start_date_filter = date("Y-m-01").' 00:00:00';
	}

	$end_date = "";
	$end_date_filter = "";
	if(isset($_REQUEST["dt_end_date"]))
	{
		$end_date = $_REQUEST["dt_end_date"];
		if($end_date != '')
		{
			$end_date_filter = $end_date.' 23:59:59';
		}
	}
	else
	{
		//$end_date_filter = date("Y-m-d").' 23:59:59';
	}

	//Get Material Master List

	$stock_history_search_data = array('active'=>'1',"material_id"=>$material_id,"project"=>$project,"start_date"=>$start_date_filter,"end_date"=>$end_date_filter);

	$stock_material_history_list = i_get_stock_history($stock_history_search_data);

	if($stock_material_history_list["status"] == SUCCESS)

	{

		$stock_material_history_list_data  = $stock_material_history_list["data"];
		$material_code = $stock_material_history_list_data[0]["stock_material_code"];
		$material_name= $stock_material_history_list_data[0]["stock_material_name"];
		$uom = $stock_material_history_list_data[0]["stock_unit_name"];
	}
	else {
		$material_name = "";
		$uom = "";
		$material_code = "";
	}

	// Get Project List
	$stock_project_search_data = array();
	$project_result_list = i_get_project_list($stock_project_search_data);
	if($project_result_list['status'] == SUCCESS)
	{
		$project_result_list_data = $project_result_list['data'];
	}

	else

	{
		$alert = $project_result_list["data"];
		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Stock Report</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>





<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">



          <div class="span6" style="width:100%;">



          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3><a href="stock_material_report_export.php?project=<?php echo $project; ?>&start_date=<?php echo $start_date_filter; ?>&end_date=<?php echo $end_date_filter; ?>&material_id=<?php echo $material_id ;?>" target="_blank">Export Live to Excel</a></h3>

            </div>

            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">

			  <form method="post" id="file_search_form" action="stock_material_report.php">
			  <input type="hidden" id="material_id" name="material_id"  value="<?php echo $material_id ;?>">

			  <span style="padding-right:20px;">
			  <select name="ddl_project">
				<option value="">- - Select Project - -</option>
				<?php
				for($count = 0; $count < count($project_result_list_data); $count++)
				{
				?>
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				<?php
				}
				?>
			  </select>
			  </span>

			   <span style="padding-left:10px; padding-right:10px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date; ?>" />
			  </span>

			  <span style="padding-left:10px; padding-right:10px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date; ?>" />
			  </span>

			  <input type="submit" name="stock_search_submit" />

			  </form>

            </div>

            <div class="widget-content">



              <table class="table table-bordered">

                <thead>

				<tr>
				<th style="font-size:15px;" colspan="5">Material Name : <?php echo $material_name;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Material Code : <?php echo $material_code;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UOM : <?php echo $uom;?> </th>
				<th> <span id="span_total_opening_stock"></span></th>
				<th> <span id="span_total_purchase_qty"></span></th>
				<th> <span id="span_total_issued_qty"></span></th>

				<th> <span id="span_total_closing_stock"></span></th>
				<th> <span id="span_total_closing_stock"></span></th>
				<th> <span id="span_total_value"></span></th>
				</tr>

                  <tr>

				    <th>SL No</th>

				    <th>Date</th>

				    <th>Transaction Type</th>

				    <th>Transaction Details</th>

				    <th>Project</th>

					<th>Opening Stock</th>

					<th>Receipts</th>

					<th>Issues</th>

					<th>Closing Stock</th>

					<th>Rate</th>

					<th>Value</th>

				</tr>

				</thead>

				<tbody>


				<?php

				$total_value = 0;


				if($stock_material_history_list["status"] == SUCCESS)

				{

					$sl_no = 0;
					$total_opening_qty = 0;
					$total_issue_qty = 0;
					$total_purchase_qty = 0;
					$total_closing_stock = 0;
					$material_history = [];
					$start = 0;

					$end_date = date('Y-m-d',strtotime($start_date_filter.' -1 day'));
					$start_date_cl_st = $end_date.' 23:59:59';
					//Get closing Stock
					$total_closing_stock_value = 	i_get_stock_closing_stock($material_id,$project,"1990-01-01 00:00:00",$start_date_cl_st);

					if($start_date_filter != '')
					{
						$total_closing_stock = $total_closing_stock_value;
						$start = 1;

						/* Opening stock for this duration */
						$material_array[0]['added_on'] = "";
						$material_array[0]['transaction_type'] = "";
						$material_array[0]['project_name'] = "";
						$material_array[0]['opening_qty'] = $total_closing_stock_value;
						$material_array[0]['purchase_qty'] = 0;
						$material_array[0]['issue_qty'] = 0;
						$material_array[0]['total_closing_stock'] = $total_closing_stock_value;
						$material_array[0]['value'] = "";
						$material_array[0]['transaction_details'] = "Opening Stock";
						$material_array[0]['sl_no'] = "";
						$material_array[0]['uom'] = "";
					}

					for($count = 0; $count < count($stock_material_history_list_data); $count++)
					{
						$opening_qty = 0;
						$purchase_qty = 0;
						$issue_qty = 0;
						$transaction_type = $stock_material_history_list_data[$count]["stock_history_transaction_type"];
						if($transaction_type == "Purchase")
						{
							//Get Grn Inspectioned List
							$stock_grn_engineer_inspection_search_data = array("inspection_id"=>$stock_material_history_list_data[$count]["stock_history_reference_id"]);
							$stock_grn_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);
							if($stock_grn_inspection_list["status"] == SUCCESS)
							{
								$stock_grn_inspection_list_data = $stock_grn_inspection_list["data"];
								$grn_qty = $stock_grn_inspection_list_data[0]["stock_grn_engineer_inspection_approved_quantity"];
								$grn_number = $stock_grn_inspection_list_data[0]["stock_grn_no"];
								$vendor = $stock_grn_inspection_list_data[0]["stock_vendor_name"];
								$grn_by = $stock_grn_inspection_list_data[0]["user_name"];
								$grn_inspectioned_date = date("d-M-Y",strtotime($stock_grn_inspection_list_data[0]["stock_grn_engineer_inspection_added_on"]));
							}
							else
							{
								$grn_qty = 0;
								$grn_number = "";
								$grn_inspectioned_date = 0;
								$vendor = "";
								$grn_by = "";
							}


						}
						else if($transaction_type == "Issue")
						{
							//Get Indent Issue List
							$stock_issue_item_search_data = array("issue_item_id"=>$stock_material_history_list_data[$count]["stock_history_reference_id"]);
							$stock_issued_items_list = i_get_stock_issued_item_without_sum($stock_issue_item_search_data);
							if($stock_issued_items_list["status"] == SUCCESS)
							{
								$stock_issued_items_list_data = $stock_issued_items_list["data"];
								$stock_issued_no = $stock_issued_items_list_data[0]["stock_issue_no"];
								$stock_issued_qty = $stock_issued_items_list_data[0]["stock_issue_item_qty"];
								$stock_issued_date = $stock_issued_items_list_data[0]["stock_issue_item_issued_on"];
								$machine_name = $stock_issued_items_list_data[0]["stock_machine_master_name"];
								$machine_number = $stock_issued_items_list_data[0]["stock_machine_master_id_number"];
								$issued_by  = $stock_issued_items_list_data[0]["added_by"];
								$issued_to  = $stock_issued_items_list_data[0]["user_name"];
							}
							else
							{
								$stock_issued_no = "";
								$stock_issued_qty = "";
								$stock_issued_date = "";
								$machine_name = "";
								$machine_number = "";
								$stock_issued_by = "";
								$issued_by = "";
								$issued_to = "";
							}
						}
						if(($transaction_type == "Transfer Out") || ($transaction_type == "Transfer In"))
						{
							$stock_transfer_search_data = array("transfer_id"=>$stock_material_history_list_data[$count]["stock_history_reference_id"]);
							$stock_transfer_list = i_get_stock_transfer($stock_transfer_search_data);
							if($stock_transfer_list["status"] == SUCCESS)
							{
								$stock_transfer_list_data = $stock_transfer_list["data"];
								$stock_source_project = $stock_transfer_list_data[0]["source"];
								$stock_destination_project = $stock_transfer_list_data[0]["dest"];
							}
							else
							{
								$stock_source_project = "";
								$stock_destination_project = "";
							}
						}
						else
						{
							// Do nothing
						}
						switch($transaction_type)
						{
							case  "Opening" :
							$opening_qty = $stock_material_history_list_data[$count]["stock_history_quantity"];
							$transaction_details =  'Added By : '.' '.$stock_material_history_list_data[$count]["user_name"];

							break;

							case  "Purchase" :
							$purchase_qty = $stock_material_history_list_data[$count]["stock_history_quantity"];
							$transaction_details =  'Grn no : '.' '.$grn_number.' <br>'.'Vendor  : '.' '.$vendor.' <br>'.'Grn By  : '.' '.$grn_by ;

							break;

							case  "Issue" :
							$issue_qty = $stock_material_history_list_data[$count]["stock_history_quantity"];
							$transaction_details = 'Issue No : '.' '.$stock_issued_no.' <br>'.'Machine  : '.' '.$machine_name.' '.$machine_number.' <br>'.'Issued By  : '.' '.$issued_by.' <br>'.'Issued to  : '.' '.$issued_to ;

							break;

							case  "Transfer Out" :
							$issue_qty = $issue_qty + $stock_material_history_list_data[$count]["stock_history_quantity"];$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;

							break;

							case  "Transfer In" :
							$purchase_qty = $purchase_qty + $stock_material_history_list_data[$count]["stock_history_quantity"];
							$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;
							break;
						}

						/*if($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Purchase"){
							$transaction_details =  'Grn no : '.' '.$grn_number.' <br>'.'Vendor  : '.' '.$vendor.' <br>'.'Grn By  : '.' '.$grn_by ;

						}
						if($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Issue")
						{
							$transaction_details = 'Issue No : '.' '.$stock_issued_no.' <br>'.'Machine  : '.' '.$machine_name.' '.$machine_number.' <br>'.'Issued By  : '.' '.$issued_by.' <br>'.'Issued to  : '.' '.$issued_to ;
						}
						if(($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Transfer Out") ||($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Transfer In"))
						{
							$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;
						}*/

						$total_opening_qty = $total_opening_qty + $opening_qty;
						$total_issue_qty = $total_issue_qty + $issue_qty;
						$total_purchase_qty = $total_purchase_qty + $purchase_qty;

						$rate = $stock_material_history_list_data[$count]["stock_material_price"];
						$closing_stock = ($opening_qty + $purchase_qty - $issue_qty);
						$total_closing_stock = $total_closing_stock + $closing_stock;

						//$value = $rate * $total_closing_stock;
						$value = $rate * $total_closing_stock;
						$material_array[$count+$start]['added_on'] = date("d-M-Y",strtotime($stock_material_history_list_data[$count]["stock_history_added_on"]));
						$material_array[$count+$start]['transaction_type'] = $stock_material_history_list_data[$count]["stock_history_transaction_type"];
						$material_array[$count+$start]['project_name'] = $stock_material_history_list_data[$count]["stock_project_name"];
						$material_array[$count+$start]['opening_qty'] = $opening_qty;
						$material_array[$count+$start]['purchase_qty'] = $purchase_qty;
						$material_array[$count+$start]['issue_qty'] = $issue_qty;
						$material_array[$count+$start]['total_closing_stock'] = $total_closing_stock;
						$material_array[$count+$start]['value'] = $rate * $total_closing_stock;
						$material_array[$count+$start]['transaction_details'] = $transaction_details;
						$material_array[$count+$start]['sl_no'] = $sl_no;
						$material_array[$count+$start]['uom'] = $stock_material_history_list_data[$count]["stock_unit_name"];
						$total_value = $total_value + $value;

					}

					for($display_count = (count($material_array) - 1) ; $display_count >= 0 ; $display_count--)
					{
						$sl_no++;
					?>
						<tr>

						<td><?php echo $sl_no ; ?></td>

						<td><?php echo $material_array[$display_count]['added_on'] ; ?></td>

						<td><?php echo $material_array[$display_count]['transaction_type']; ?></td>

						<td><?php echo $material_array[$display_count]['transaction_details'] ?></td>

						<td><?php echo $material_array[$display_count]['project_name']; ?></td>

						<td><?php echo $material_array[$display_count]['opening_qty']; ?></td>

						<td><?php echo $material_array[$display_count]['purchase_qty']; ?></td>

						<td><?php echo $material_array[$display_count]['issue_qty']; ?></td>

						<td><?php echo $material_array[$display_count]['total_closing_stock']; ?></td>

						<td><?php echo $rate; ?></td>

						<td><?php echo $material_array[$display_count]['value']; ?></td>

						</tr>
						<?php
					}

				}

				else

				{

				?>

				<td colspan="9"></td>

				<?php

				}

				 ?>



                </tbody>

              </table>

			  <script>

			  document.getElementById('span_total_value').innerHTML = 'Total Value: ' + '<?php echo $total_value; ?>';
			  document.getElementById('span_total_opening_stock').innerHTML = 'Total Opening: ' + '<?php echo $total_opening_qty; ?>';
			  document.getElementById('span_total_issued_qty').innerHTML = 'Total Issued: ' + '<?php echo $total_issue_qty; ?>';
			  document.getElementById('span_total_purchase_qty').innerHTML = 'Total Purchase: ' + '<?php echo $total_purchase_qty; ?>';
			  document.getElementById('span_total_closing_stock').innerHTML = 'Total Closing: ' + '<?php echo $total_closing_stock; ?>';

			  </script>

            </div>

            <!-- /widget-content -->

          </div>

          <!-- /widget -->



          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 -->

      </div>

      <!-- /row -->

    </div>

    <!-- /container -->

  </div>

  <!-- /main-inner -->

</div>









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_account(account_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else

					{

					 window.location = "stock_grn_account_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_account.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("account_id=" + account_id + "&action=0");

		}

	}

}

function get_material_list()

{

	var searchstring = document.getElementById('stxt_material').value;



	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);

	}

	else

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;



	document.getElementById('search_results').style.display = 'none';

}

</script>



  </body>



</html>
