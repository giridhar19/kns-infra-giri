<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_BUDGET_LIST_FUNC_ID', '365');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '6', '1');

    $project_id = '';
    if (isset($_GET["Project_id"])) {
      $project_id   = $_GET["Project_id"];
    }
    if (isset($_GET["search_process"])) {
      $search_process   = $_GET["search_process"];
    }
    else {
      $search_process = '';
    }
    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    function upload($file_id, $user_id)
    {
    	if($_FILES[$file_id]["name"]!="")
    	{
    		if ($_FILES[$file_id]["error"] > 0)
    		{
    			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
    		}
    		else
    		{
    			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
    			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);
    		}
    	}

    	return $_FILES[$file_id]["name"];
    }

    // background submit remarks
  	if(isset($_POST["process_id"])) {
      $project_id = $_POST["project_id"];
      $process_id = $_POST["process_id"];
      $task_id    = $_POST["task_id"];
      $uom        = $_POST["uom"];
      $road_id    = $_POST["road_id"];
  		$manpower   = $_POST["mp_budget"];
  		$machine	  = $_POST["mc_budget"];
  		$contract   = $_POST["cw_budget"];
  		$material   = $_POST["material_budget"];
  		$process    = $_POST["process_name"];
  		$mp_cost    = $_POST["total_mp_cost"];
  		$mc_cost    = $_POST["total_mc_cost"];
  		$material_cost   = $_POST["total_material_cost"];
    	$cw_cost  = $_POST["total_cw_cost"];
    	$project_name  = $_POST["project_name"];
    	$task_name  = $_POST["task_name"];
    	$road_name  = $_POST["road_name"];
    	$prev_planned_contract  = $_POST["planned_contract"];
    	$prev_planned_machine  = $_POST["planned_machine"];
    	$prev_planned_manpower  = $_POST["planned_manpower"];
    	$prev_planned_material  = $_POST["planned_material"];
    	$remarks  = $_POST["remarks"];
      $doc  = upload("file_remarks_doc",$user);
      $task_iresult  = i_add_project_task_method_planning($process_id,$project_id,$doc,"1",$remarks,$user);
      if($_POST["road_name"] != null || $_POST["road_name"] != "")
      {
    	   $road_name  = $_POST["road_name"];
       }
       else {
         $road_name  = "No Roads";
       }
      $manpower =   $mp_cost > $manpower ? $mp_cost + 1 : $manpower ;
      $machine  =   $mc_cost > $machine ? $mc_cost + 1 : $machine ;
      $contract =  $cw_cost > $contract ? $mc_cost + 1 : $contract ;
      $material =   $material_cost > $material ? $material_cost + 1 : $material ;

      if($machine !=0 || $material !=0 || $contract || $manpower)
      {
        $project_planned_data = array("task_id"=>$task_id,"road_id"=>$road_id);
        $get_planned_data = db_get_planned_data($project_planned_data);
        if($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS)
        {
          $project_delete_data = array("task_id"=>$task_id,"road_id"=>$road_id);
          $delete_planned_data = db_delete_planned_data($project_delete_data);
        }
        //Add data into table
  			$budget_plan_iresult = db_add_planned_data($project_id,
                                              $process_id,
                                              $task_id,
                                              $uom,
                                              $road_id,
                                              $manpower,
                                              $machine,
                                              $contract,
                                              $material,
                                              $user);
  			if($budget_plan_iresult["status"] == SUCCESS)
  			{
          $budget_history_plan_iresult = db_add_budget_history_data($project_id,
          $process_id,
          $task_id,
          $road_id,
          $manpower,
          $machine,
          $contract,
          $material,
          $doc,
          $remarks,
          $user);
          $manpower_data = "";
          $machine_data = "";
          $contract_data = "";
          $material_data = "";
          $change_status = false;
          //Get Email list
          $project_email_search_data = array("project_id"=>$project_id);
          $email_list = db_get_project_email_list($project_email_search_data);
          if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
              $email_list_data = $email_list["data"];
              $to = $email_list_data[0]["project_email_details_to"];
              $cc = $email_list_data[0]["project_email_details_cc"];
              $bcc = $email_list_data[0]["project_email_details_bcc"];
          }
          $subject = "Budget amount has been changed for '.$project_name.'";
          $heading = "<br>
          <table style='border: 1px solid black;'>
          <tr>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Project</td>
            <td style='border: 1px solid #ddd;width:20%'>".$project_name ."</td>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Process</td>
            <td style='border: 1px solid #ddd;width:20%'>".$process."</td>
          </tr>
          <tr>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Changed By</td>
            <td style='border: 1px solid #ddd;width:20%'>". $loggedin_name ."</td>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Changed on</td>
            <td style='border: 1px solid #ddd;width:20%'>". date('d-m-y h:m A') ."</td>
          </tr>
          <tr>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Task</td>
            <td style='border: 1px solid #ddd;width:20%'>".$task_name."</td>
            <td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Roads</td>
            <td style='border: 1px solid #ddd;width:20%'>".$road_name."</td>
          </tr>
          </table><br>";

            $table_content = "
            <table style='border: 1px solid #1C6EA4;
              background-color: E9F1E3;
              width: 100%;
              text-align: left;
              border-collapse: collapse;'>
            <thead style='background: #00ba8b;
              border-bottom: 2px solid #444444;'>
            <tr>
            <th style='font-size: 15px;
              font-weight: bold;
              color: #FFFFFF;
              border-left: 2px solid #D0E4F5;padding:8px'>Type</th>
            <th style='font-size: 15px;
                font-weight: bold;
                color: #FFFFFF;
                border-left: 2px solid #D0E4F5;padding:8px'>Old Amount</th>
            <th style='font-size: 15px;
                  font-weight: bold;
                  color: #FFFFFF;
                  border-left: 2px solid #D0E4F5;padding:8px'>New Amount</th>
            <th style='font-size: 15px;
                  font-weight: bold;
                  color: #FFFFFF;
                  border-left: 2px solid #D0E4F5;padding:8px'>Variance</th>
                  <th style='font-size: 15px;
                        font-weight: bold;
                        color: #FFFFFF;
                        border-left: 2px solid #D0E4F5;padding:8px'>Status</th>
            <th style='font-size: 15px;
                  font-weight: bold;
                  color: #FFFFFF;
                  border-left: 2px solid #D0E4F5;padding:8px'>Remarks</th>
            </tr>
            </thead>
            <tbody>";
            if($manpower != $prev_planned_manpower)
            {
              $manpower_varience = $manpower - $prev_planned_manpower;
              $change_status = true ;
              if($manpower_varience < 0)
              {
                $css = "red";
                $manpower_status = "Decreased";
              }
              else {
                $manpower_status =  "Increased" ;
              }
              $manpower_data= "<tr>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>Manpower</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$prev_planned_manpower."</td>
            <td style='border: 1px solid #ddd;;width:20%;font-size: 13px;padding:8px'>".$manpower."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$manpower_varience."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$manpower_status."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$remarks."</td></tr>";
            }
            if($machine != $prev_planned_machine)
            {
              $machine_varience = $machine - $prev_planned_machine;
              $change_status = true ;

              if($machine_varience < 0)
              {
                $css = "red";
                $machine_status = "Decreased";
              }
              else {
                $css =  "green" ;
                $machine_status =  "Increased" ;
              }
           $machine_data="<tr>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>Machine</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$prev_planned_machine."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$machine."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$machine_varience."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$machine_status."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$remarks."</td></tr>";
        }
        if($contract != $prev_planned_contract)
        {
          $contract_varience = $contract - $prev_planned_contract;
          $change_status = true ;
          if($contract_varience < 0)
          {
            $css = "red";
            $contract_status = "Decreased";
          }
          else {
            $css =  "green" ;
            $contract_status =  "Increased" ;
          }
          $contract_data="<tr>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>Contract</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$prev_planned_contract."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$contract."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$contract_varience."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$contract_status."</td>
          <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$remarks."</td></tr>";
          }
          if($material != $prev_planned_material)
          {
            $material_varience = $material - $prev_planned_material;
            $change_status = true ;
            if($material_varience < 0)
            {
              $css = "red";
              $material_status = "Decreased";
            }
            else {
              $css =  "green" ;
              $material_status =  "Increased" ;
            }
            $material_data="<tr>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>Material</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$prev_planned_material."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$material."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$material_varience."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$material_status."</td>
            <td style='border: 1px solid #ddd;width:20%;font-size: 13px;padding:8px'>".$remarks."</td></tr>";
        }
          $table_content .= "</tbody>";
          $table_end = "</table>";
      		$message = $heading;
      		// $message .= $table_header;
      		$message .= $table_content;
      		$message .= $manpower_data;
      		$message .= $machine_data;
      		$message .= $contract_data;
      		$message .= $material_data;
      		$message .= $table_end;
            $cc = explode(',',$cc);
            $cc_string = '[';
          	for($count = 0; $count < count($cc); $count++)
          	{
          		$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
          	}
          	$cc_string = trim($cc_string,',');
          	$cc_string = $cc_string.']';

            $bcc = explode(',',$bcc);
      				$bcc_string = '[';
      	    	for($count = 0; $count < count($bcc); $count++)
      	    	{
      	    		$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
      	    	}
      	    	$bcc_string = trim($bcc_string,',');
      	    	$bcc_string = $bcc_string.']';

          	$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
          		"personalizations": [
          			{
          				"to": [
          					{
          					  "email": "'.$to.'"
          					}
          				],
          				"cc": '.$cc_string.',
          				"bcc": '.$bcc_string.',
          				"subject": "'.$subject.'"
          			}
          		],
          		"from": {
          			"email": "venkataramanaiah@knsgroup.in"
          		},
          		"content": [
          			{
          				"type": "text/html",
          				"value": "'.$message.'"
          			}
          		]
          	}'));
            if($change_status == true)
            {
            	$apiKey = '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';
            	$sg = new \SendGrid($apiKey);
            	$response = $sg->client->mail()->send()->post($request_body);
            }
  				$display_message = "Budget Plan Updated for".' '.$process. ' ' .'process..';
  			}
  			else
  			{
  				$display_message = "Failed to update";
  			}
      }
      else {
        $display_message = "Failed to update";
      }
      if($display_message == "Failed to update")
      echo '<script>window.parent.closeModalWindow("Failed to update")</script>';
      else
      echo '<script>window.parent.closeModalWindow("Budget Plan Updated for Process:")</script>';
      exit;
  	}
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Project Budget Report List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/project_budget_report.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Change Budget Plan</h4>
      </div>


      <div class="widget-header" style="height:auto;">
        <div style="border-bottom: 1px solid #C0C0C0;">
          <span class="header-label">Project Name: </span><span id="header_project_name"></span>
            </div>
            <div style="border-bottom: 1px solid #C0C0C0;">
          <span class="header-label">Process: </span><span id="header_process_name"></span>
          <span class="header-label">Uom: </span><span id="uom"></span>
        </div>

        <div style="border-bottom: 1px solid #FFFFFF;">
          <span class="header-label">Task: </span><span id="header_task_name"></span>
          <span class="header-label">Road: </span><span id="road"></span>
          <!-- <span class="header-label">Pending Qty: </span><span id="material_pending_quantity"></span> -->
        </div>
      </div>

      <div class="modal-body">
        <iframe src="about:blank" id="iframe_file_upload" name="iframe_file_upload" style="width:0px; height:0px;"></iframe>
        <form  name="project_budget_plan_form" action="project_budget_report_list.php" method="POST" target="iframe_file_upload" enctype="multipart/form-data">
        <input type="hidden" id="selected_row_id">
        <input type="hidden" id="process_id" class="form-control" name="process_id" />
        <input type="hidden" id="project_id" name="project_id" />
        <input type="hidden" id="task_id" name="task_id" >
        <input type="hidden" id="uom" name="uom">
        <input type="hidden" id="road_id" name="road_id">
        <input type="hidden" id="process_name" name="process_name">
        <input type="hidden" id="total_mp_cost" name="total_mp_cost">
        <input type="hidden" id="total_mc_cost" name="total_mc_cost">
        <input type="hidden" id="total_material_cost" name="total_material_cost">
        <input type="hidden" id="total_cw_cost" name="total_cw_cost">
        <input type="hidden" id="project_name" name="project_name">
        <input type="hidden" id="task_name" name="task_name">
        <input type="hidden" id="road_name" name="road_name">
        <input type="hidden" id="planned_contract" name="planned_contract">
        <input type="hidden" id="planned_machine" name="planned_machine">
        <input type="hidden" id="planned_manpower" name="planned_manpower">
        <input type="hidden" id="planned_material" name="planned_material">

        <input type="hidden" id="view_perm" value="<?= $view_perms_list['status'];?>">
        <input type="hidden" id="edit_perm" value="<?= $edit_perms_list['status'];?>">
        <input type="hidden" id="ok_perm" value="<?= $ok_perms_list['status'];?>">
        <input type="hidden" id="delete_perm" value="<?= $delete_perms_list['status'];?>">
        <input type="hidden" id="approve_perm" value="<?= $approve_perms_list['status'];?>">

        <!-- <input type="hidden" id="hd_machine_id"/> -->
        <div class="row">
            <div class="col-md-6">
        <div class="form-group">
          <label class="control-label" for="mp_budget">Manpower</label>
          <div class="controls">
            <input type="number" value=0 minlength="3" class="form-control" name="mp_budget" id="mp_budget">
          </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="mc_budget">Machine</label>
            <div class="controls">
              <input type="number" value=0 name="mc_budget" id="mc_budget" class="form-control">
            </div> <!-- /controls -->
          </div> <!-- /form-group -->
        </div>
        </div>
        <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="cw_budget">Contract</label>
            <div class="controls">
              <input type="number" value=0 name="cw_budget" id="cw_budget" class="form-control">
            </div> <!-- /controls -->
          </div> <!-- /form-group -->
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="material_budget">Material</label>
            <div class="controls">
              <input type="number" value=0 name="material_budget" id="material_budget" class="form-control">
            </div> <!-- /controls -->
          </div> <!-- /form-group -->
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="remarks">Remarks</label>
            <div class="controls">
              <textarea name="remarks" id="remarks" class="form-control" rows="4" cols="50" minlength="15" required></textarea>
            </div> <!-- /controls -->
          </div> <!-- /form-group -->
        </div>
        <div class="col-md-6" style="padding:20px">
          <div class="form-group">
            <label class="control-label" for="file_remarks_doc">Document*:</label>
            <div class="controls">
              <input  type="file"  name="file_remarks_doc" id="file_remarks_doc" required>
            </div> <!-- /controls -->
          </div> <!-- /form-group -->
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary"/>
        </div>
        </form>
        </div>
        </div>
     </div>
  </div>
  <!-- // modal -->
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Project Budget Report List</h3>
              <?php if(isset($project_id) && !empty($project_id) && $edit_perms_list['status'] == SUCCESS){ ?>
              <!-- Export & Import buttons -->
                <form id="formImport" style="float:right;margin-top: 5px; margin-right: 5px;" class="form-horizontal" action="./import_project_budget_plan.php?project_id=<?php echo $project_id; ?>" method="post" enctype="multipart/form-data">
                  <div class="btn-group btn-group-sm pull-right" role="group">
                    <a href="budget_planning_excel.php?project_id=<?php echo $project_id; ?>" class="btn btn-primary">
                      <span class="glyphicon glyphicon-download"></span> Export
                    </a>
                  <label class="btn btn-default" style="margin-top: 0px;">
                      <span class="glyphicon glyphicon-upload"></span>
                      Import <input type="file" name="file" id="file" accept=".xls" onchange="checkFileAttached()" hidden>
                  </label>
                </div>
                </form>
            <?php } ?>
            </div>
            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                <input type="hidden" name="ddl_process_id"  id="ddl_process_id" value=<?php echo $search_process ;?>>
                <select name="Project_id" id="Project_id" class="form-control">
                 <option value="">- - Select Project - -</option>
              <?php
                for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) { ?>
                 <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>"
                   <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                    ?> selected="selected" <?php } ?>>
                    <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
                 </option>
              <?php } ?>
              </select>
              <select name="search_process"  id="search_process"  class="form-control">
                <opton value="0">------Select Process-----</option>
      			  </select>
              <input type="submit" class="btn btn-primary" />
              </form>
            </div>
            <br>
            <div id="alert_show" class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong></strong><span id="alert_msg">'+dialog_title+'</span>
            </div>
          </div>
          <?php if($view_perms_list['status'] == SUCCESS){ ?>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example">
               <thead>
                 <tr>
                    <th colspan="6">&nbsp;</th>
                    <th colspan="4" class="center blue">Planned</th>
                    <th colspan="4" class="center">Actual</th>
                    <th colspan="4" class="center red">Variance</th>
                    <th>&nbsp;</th>
                 </tr>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Process</th>
                   <th>Task Name</th>
                   <th>Road Name</th>
                   <th>UOM</th>
                   <th class="blue">Manpower</th>
                   <th class="blue">Machine</th>
                   <th class="blue">Contract</th>
                   <th class="blue">Material</th>
                   <th>Manpower</th>
                   <th>Machine</th>
                   <th>Contract</th>
                   <th>Material</th>
                   <th class="red">Manpower</th>
                   <th class="red">Machine</th>
                   <th class="red">Contract</th>
                   <th class="red">Material</th>
                   <th>Action</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
          <?php }
          else{ ?>
            <div class="widget-content">
             <h1>Access Denied</h1>
             <h3>You dont have permissions to view the Document</h3>
           </div>
         <?php } ?>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
