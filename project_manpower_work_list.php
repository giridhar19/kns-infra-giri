<?php

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

// What is the date today
$start_date = date("Y-m-d 00:00:00");
$end_date = date("Y-m-d 23:59:59");
// Get list of approved bookings with no profile
$max_wait_days = 30;
// Get Project Task BOQ modes already added

// Project data
$project_management_master_search_data = array("active"=>'1',"status"=>"all");
$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
if($project_management_master_list["status"] == SUCCESS)
{
	$project_management_master_list_data = $project_management_master_list["data"];
}

	for($pcount = 0 ; $pcount < count($project_management_master_list_data) ; $pcount++)
	{
		$sl_no = 0;
		$project_email_search_data = array("project_id"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$email_list = db_get_project_email_list($project_email_search_data);
		if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
				$email_list_data = $email_list["data"];
				$to  = $email_list_data[0]["project_email_details_to"];
				$cc  = $email_list_data[0]["project_email_details_cc"];
				$bcc = $email_list_data[0]["project_email_details_bcc"];
		}
		//Contract Data
		$man_power_search_data = array("active"=>'1',"work_start_date"=>$start_date,"work_end_date"=>$end_date,"project"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$man_power_list = i_get_man_power_list($man_power_search_data);
		if($man_power_list['status'] == SUCCESS)
		{
			$total_no_of_men   = 0 ;
			$total_no_of_women = 0 ;
			$total_no_of_mason = 0 ;
			$total_men_cost 	 = 0 ;
			$total_women_cost  = 0 ;
			$total_mason_cost  = 0 ;
			$grand_total  = 0 ;
			$message_heading = "" ;
			$message_content = "" ;
			$heading = "" ;
			// $message_end = "" ;
			$man_power_list_data = $man_power_list['data'];
			$subject = $project_management_master_list_data[$pcount]["project_master_name"].'  '.'Manpower Work List';
			$message = 'Dear Sir/Madam,<br><br>Daily Manpower work list:<br><br>';

			$message_heading .="<table border='1' style='border-collapse:collapse; border-width:2px;'>";
				// Header row - start
			$message_heading .= "<tr style='border-width:2px;text-align:center;'>
			<td style='border-width:2px;text-align:center;'><strong>SL No.</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Agency</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Process</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Task</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Road</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>WorkType</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Men</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Women</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Mason</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>No of People<strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Amount<strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Remarks<strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added By</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added On</strong></td>";

			$message_heading = $message_heading.'</tr>';
			for($count = 0; $count < count($man_power_list_data); $count++)
			{
				$sl_no++;

				$man_power_men_rate = $man_power_list_data[$count]["project_task_actual_manpower_men_rate"];
				$man_power_women_rate = $man_power_list_data[$count]["project_task_actual_manpower_women_rate"];
				$man_power_mason_rate = $man_power_list_data[$count]["project_task_actual_manpower_mason_rate"];

				$men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
				$women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
				$mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
				$total_cost = $men_cost + $women_cost + $mason_cost;

				$men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
				$women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
				$mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

				$no_of_men = $men_hrs/8;
				$no_of_women = $women_hrs/8;
				$no_of_mason = $mason_hrs/8;

				$total_hrs = $men_hrs + $women_hrs + $mason_hrs;
				$no_of_people = $total_hrs/8;
				$grand_total = $grand_total + $total_cost;

				// Compose the message
				$project	     = $man_power_list_data[$count]["project_master_name"];
				$process  	   = $man_power_list_data[$count]["project_process_master_name"];
				$task	  	     = $man_power_list_data[$count]["project_task_master_name"];
				$vendor	 			 = $man_power_list_data[$count]["project_manpower_agency_name"];
				$work_type	   = $man_power_list_data[$count]["project_task_actual_manpower_work_type"];
				if($man_power_list_data[$count]["project_task_actual_manpower_road_id"] == "No Roads")
				{
					$location = "No Roads";
				}
				else {
					$location	 = $man_power_list_data[$count]["project_site_location_mapping_master_name"];
				}
				$remarks 					= $man_power_list_data[$count]["project_task_actual_manpower_remarks"];
				$user	  	  			= $man_power_list_data[$count]["user_name"];
				$added_on	  	 	  = date("d-M-Y",strtotime($man_power_list_data[$count]["project_task_actual_manpower_added_on"]));

				$total_no_of_men   += $no_of_men ;
				$total_no_of_women += $no_of_women ;
				$total_no_of_mason += $no_of_mason ;
				$total_men_cost 	 += $men_cost ;
				$total_women_cost  += $women_cost ;
				$total_mason_cost  += $mason_cost ;

				if($work_type == "Rework")
				{
						$css = "yellow";
				}
				else {
					$css = "";
				}
				$message_content .= "<tr style='border-width:2px; text-align:center;'>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$sl_no."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$vendor."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$process."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$task."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$location."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$work_type."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$no_of_men."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$no_of_women."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$no_of_mason."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".($no_of_men + $no_of_women + $no_of_mason)."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$total_cost."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$remarks."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$user."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$added_on."</td>
				</tr>";
			}
			$heading = $heading."<br>
			<table style='border: 1px solid black;'>
			<tr>
				<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Project : ".$project_management_master_list_data[$pcount]["project_master_name"] ."</th>
				<td style='border: 1px solid #ddd;width:20%'>&nbsp;</td>
				<td style='border: 1px solid #ddd;width:20%'>Date : ".date("d-M-Y")."</td>
			</tr>
			<tr>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Manpower</th>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Head Count</th>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Amount</th>
			</tr>
			<tr>
				<td style='border: 1px solid #ddd;width:20%'>Man</td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_no_of_men."</span></td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_men_cost."</td>
			</tr>
			<tr>
				<td style='border: 1px solid #ddd;width:20%'>Woman</td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_no_of_women."</span></td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_women_cost."</td>
			</tr>
			<tr>
				<td style='border: 1px solid #ddd;width:20%'>Mason</td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_no_of_mason."</span></td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_mason_cost."</td>
			</tr>
			<tr>
				<th  style='color:#00ba8b;border: 1px solid #ddd;colspan:2;width:20%'>Total Amount</th>
				<td style='border: 1px solid #ddd;width:20%'>".($total_no_of_men+$total_no_of_women+$total_mason_cost)."</span></td>
				<td style='border: 1px solid #ddd;width:20%'>".$grand_total."</span></td>
			</tr>
			</table><br>";
			$message_end = "</table>";
			$message_end = $message_end.'<br>Regards,<br>KNS ERP';

			$message .= $heading ;
			$message .= $message_heading ;
			$message .= $message_content;
			$message .= $message_end;
			$cc = explode(',',$cc);
			$cc_string = '[';
			for($count = 0; $count < count($cc); $count++)
			{
				$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
			}
			$cc_string = trim($cc_string,',');
			$cc_string = $cc_string.']';

			$bcc = explode(',',$bcc);
				$bcc_string = '[';
				for($count = 0; $count < count($bcc); $count++)
				{
					$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
				}
				$bcc_string = trim($bcc_string,',');
				$bcc_string = $bcc_string.']';

			$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
				"personalizations": [
					{
						"to": [
							{
								"email": "'.$to.'"
							}
						],
						"cc": '.$cc_string.',
						"bcc": '.$bcc_string.',
						"subject": "'.$subject.'"
					}
				],
				"from": {
					"email": "venkataramanaiah@knsgroup.in"
				},
				"content": [
					{
						"type": "text/html",
						"value": "'.$message.'"
					}
				]
			}'));
			$apiKey = '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';
			$sg = new \SendGrid($apiKey);
			$response = $sg->client->mail()->send()->post($request_body);
		}
	}
?>
