<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_man_power_rate_master_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID','247');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MAN_POWER_RATE_FUNC_ID,'1','1');

	// Query String Data
	// Nothing
	$search_vendor   	 = "";

	if(isset($_POST["search_vendor"]))
	{
		$search_vendor   = $_POST["search_vendor"];
	}
	// Temp data
	// Get Project Man Power Rate modes already added
	$project_man_power_rate_search_data = array("active"=>'1',"vendor_id"=>$search_vendor);
	$project_man_power_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
	if($project_man_power_rate_list['status'] == SUCCESS)
	{
		$project_man_power_rate_list_data = $project_man_power_rate_list['data'];
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
	 else
	{

	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Master Man Power Rate List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Master Man Power Rate List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="project_master_add_man_power_rate.php">Project Master Add Man Power Rate</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_master_man_power_rate_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_manpower_agency_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if($search_vendor == $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>

              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Power Type</th>
					<th>Vendor</th>
					<th>Cost Per Hours</th>
					<th>Applicable Date</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
					<th>Updated By</th>
					<th>Updated On</th>
					<th colspan="2" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_man_power_rate_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_man_power_rate_list_data); $count++)
					{
						$sl_no++;
						if($project_man_power_rate_list_data[$count]["project_man_power_type_id"] == 1)
						{
							$man_type = "Male";
						}
						if($project_man_power_rate_list_data[$count]["project_man_power_type_id"] == 2)
						{
							$man_type = "Female";
						}
						if($project_man_power_rate_list_data[$count]["project_man_power_type_id"] == 3)
						{
							$man_type = "Mason";
						}


					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $man_type; ?></td>
					<td><?php echo $project_man_power_rate_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $project_man_power_rate_list_data[$count]["project_man_power_rate_cost_per_hours"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_man_power_rate_list_data[$count]["project_man_power_rate_applicable_date"])); ?></td>
					<td><?php echo $project_man_power_rate_list_data[$count]["project_man_power_rate_remarks"]; ?></td>
					<td><?php echo $project_man_power_rate_list_data[$count]["added_by"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_man_power_rate_list_data[$count][
					"project_man_power_rate_added_on"],"d-M-Y"); ?></td>
					<td><?php echo $project_man_power_rate_list_data[$count]["updated_by"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_man_power_rate_list_data[$count][
					"project_man_power_rate_updated_on"],"d-M-Y"); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_man_power_rate_master('<?php echo $project_man_power_rate_list_data[$count]["project_man_power_rate_master_id"]; ?>');">Edit </a></div><?php } ?></td>
					<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_man_power_rate_list_data[$count]["project_man_power_rate_active"] == "1")){?><a href="#" onclick="return project_delete_man_power_rate_master(<?php echo $project_man_power_rate_list_data[$count]["project_man_power_rate_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>

					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
		   <?php
		    }
		   ?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_man_power_rate_master(rate_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_master_man_power_rate_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_master_delete_man_power_rate.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("rate_id=" + rate_id + "&action=0");
		}
	}
}
function go_to_project_edit_man_power_rate_master(rate_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_master_edit_man_power_rate.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","rate_id");
	hiddenField1.setAttribute("value",rate_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
