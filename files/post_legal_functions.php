<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_post_legal.php');

/*
PURPOSE : To add site project 
INPUT 	: File ID, Project ID, Added By
OUTPUT 	: Message, success or failure message
BY 		: Punith
*/
function i_add_site_project_list($file_id,$project_id,$added_by)
{
	$site_project_sresult = db_get_site_project($file_id,'','','1','','','','','');
	if($site_project_sresult["status"] == DB_NO_RECORD)
	{
		$site_project_iresult = db_add_site_project($file_id,$project_id,$added_by);
		
		if($site_project_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Site project Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Site already mapped to project";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site project  list
INPUT 	: File ID, Project ID, Survey No, Status, Added By, Start Date, End Date, 
OUTPUT 	: BD site_project List or Error Details, success or failure message
BY 		: Punith
*/
function i_get_site_project_list($file_id,$project_id,$survey_no,$status,$added_by,$start_date,$end_date,$updated_by,$updated_on)
{
	$site_project_sresult = db_get_site_project($file_id,$project_id,$survey_no,$status,$added_by,$start_date,$end_date,$updated_by,$updated_on);
	
	if($site_project_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_project_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No sites added to this project yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update site project mapping
INPUT 	: id,file_id,project_id,status,updated_by,updated_on,added_by,added_on
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_site_project($site_project_mapping_id,$file_id,$project_id,$status,$updated_by)
{
	$site_project_uresult = db_update_site_project($site_project_mapping_id,$file_id,$project_id,$status,$updated_by);
		
	if($site_project_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Site Project Mapping Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add site approval
INPUT 	: File ID, Status, Approving Authority, Added_by
OUTPUT 	: Message, success or failure message
BY 		: Puniths
*/
function i_add_site_approval_list($file_id,$approving_authority,$added_by)
{
	$site_approval_sresult = db_get_site_approval($file_id,'1',$approving_authority,'','','');
	if($site_approval_sresult["status"] == DB_NO_RECORD)
	{
		$site_approval_iresult = db_add_site_approval($file_id,$approving_authority,$added_by);
		
		if($site_approval_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Site approval Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Site approval Data already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get BD site_approval request list
INPUT 	: File ID, Status, Approving Authority, Added By, Start date, End date, Project ID
OUTPUT 	: Site_approval List or Error Details, success or failure message
BY 		: Punith
*/
function i_get_site_approval_list($file_id,$status,$approving_authority,$added_by,$start_date,$end_date,$project_id='',$approval_id='')
{
	$site_approval_sresult = db_get_site_approval($file_id,$status,$approving_authority,$added_by,$start_date,$end_date,$project_id,$approval_id);
	
	if($site_approval_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_approval_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site_approvals added yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update site approval
INPUT 	: Approval ID, Status, Approving Authority, Updated By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_site_approval($approval_id,$status,$remarks,$approving_authority,$added_by)
{
	$site_approval_uresult = db_update_site_approval($approval_id,$status,$remarks,$approving_authority,$added_by);
	
	if($site_approval_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Site approval Successfully updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
			
	return $return;
}
?>