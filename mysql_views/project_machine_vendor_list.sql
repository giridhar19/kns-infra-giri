-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2018 at 07:03 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_machine_vendor_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_machine_vendor_list`  AS  select `PMVM`.`project_machine_vendor_master_id` AS `project_machine_vendor_master_id`,`PMVM`.`project_machine_vendor_master_name` AS `project_machine_vendor_master_name`,`PMVM`.`project_machine_vendor_master_active` AS `project_machine_vendor_master_active`,`PMVM`.`project_machine_vendor_master_code` AS `project_machine_vendor_master_code`,`PMVM`.`project_machine_vendor_master_address` AS `project_machine_vendor_master_address`,`PMVM`.`project_machine_vendor_master_contact_person` AS `project_machine_vendor_master_contact_person`,`PMVM`.`project_machine_vendor_master_contact_number` AS `project_machine_vendor_master_contact_number`,`PMVM`.`project_machine_vendor_master_email` AS `project_machine_vendor_master_email`,`PMVM`.`project_machine_vendor_master_pan_number` AS `project_machine_vendor_master_pan_number`,`PMVM`.`project_machine_vendor_master_tin_number` AS `project_machine_vendor_master_tin_number`,`PMVM`.`project_machine_vendor_master_gst_number` AS `project_machine_vendor_master_gst_number`,`PMVM`.`project_machine_vendor_master_account_holder_name` AS `project_machine_vendor_master_account_holder_name`,`PMVM`.`project_machine_vendor_master_bank_name` AS `project_machine_vendor_master_bank_name`,`PMVM`.`project_machine_vendor_master_branch` AS `project_machine_vendor_master_branch`,`PMVM`.`project_machine_vendor_master_account_number` AS `project_machine_vendor_master_account_number`,`PMVM`.`project_machine_vendor_master_ifsc_code` AS `project_machine_vendor_master_ifsc_code`,`PMVM`.`project_machine_vendor_master_secondary_acc_holder_name` AS `project_machine_vendor_master_secondary_acc_holder_name`,`PMVM`.`project_machine_vendor_master_secondary_acc_bank` AS `project_machine_vendor_master_secondary_acc_bank`,`PMVM`.`project_machine_vendor_master_secondary_acc_branch` AS `project_machine_vendor_master_secondary_acc_branch`,`PMVM`.`project_machine_vendor_master_secondary_acc_number` AS `project_machine_vendor_master_secondary_acc_number`,`PMVM`.`project_machine_vendor_master_secondary_acc_ifsc_code` AS `project_machine_vendor_master_secondary_acc_ifsc_code`,`PMVM`.`project_machine_vendor_master_last_updated_by` AS `project_machine_vendor_master_last_updated_by`,`PMVM`.`project_machine_vendor_master_last_updated_on` AS `project_machine_vendor_master_last_updated_on`,`PMVM`.`project_machine_vendor_master_remarks` AS `project_machine_vendor_master_remarks`,`PMVM`.`project_machine_vendor_master_added_by` AS `project_machine_vendor_master_added_by`,`PMVM`.`project_machine_vendor_master_added_on` AS `project_machine_vendor_master_added_on`,`AU`.`user_name` AS `updated_by`,`U`.`user_name` AS `added_by` from ((`project_machine_vendor_master` `PMVM` join `users` `U` on((`U`.`user_id` = `PMVM`.`project_machine_vendor_master_added_by`))) left join `users` `AU` on((`AU`.`user_id` = `PMVM`.`project_machine_vendor_master_last_updated_by`))) ;

--
-- VIEW  `project_machine_vendor_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
