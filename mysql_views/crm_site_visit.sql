-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2018 at 07:27 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `crm_site_visit`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `crm_site_visit`  AS  select `PM`.`project_id` AS `project_id`,`PM`.`project_name` AS `project_name`,`SVP`.`crm_site_visit_plan_id` AS `crm_site_visit_plan_id`,`SVP`.`crm_site_visit_plan_enquiry` AS `crm_site_visit_plan_enquiry`,`SVP`.`crm_site_visit_plan_date` AS `crm_site_visit_plan_date`,`SVP`.`crm_site_visit_plan_time` AS `crm_site_visit_plan_time`,`SVP`.`crm_site_visit_plan_project` AS `crm_site_visit_plan_project`,`SVP`.`crm_site_visit_plan_confirmation` AS `crm_site_visit_plan_confirmation`,`SVP`.`crm_site_visit_plan_drive` AS `crm_site_visit_plan_drive`,`SVP`.`crm_site_visit_plan_status` AS `crm_site_visit_plan_status`,`SVP`.`crm_site_visit_pickup_location` AS `crm_site_visit_pickup_location`,`SVP`.`crm_site_visit_plan_added_by` AS `crm_site_visit_plan_added_by`,`SVP`.`crm_site_visit_plan_added_on` AS `crm_site_visit_plan_added_on`,`SVP`.`crm_site_visit_status` AS `crm_site_visit_status`,`STP`.`crm_site_travel_plan_id` AS `crm_site_travel_plan_id`,`STP`.`crm_site_travel_plan_svp` AS `crm_site_travel_plan_svp`,`STP`.`crm_site_travel_plan_cab` AS `crm_site_travel_plan_cab`,`STP`.`crm_site_travel_plan_project` AS `crm_site_travel_plan_project`,`STP`.`crm_site_travel_plan_date` AS `crm_site_travel_plan_date`,`STP`.`crm_site_travel_plan_status` AS `crm_site_travel_plan_status`,`STP`.`crm_site_travel_actual_cab` AS `crm_site_travel_actual_cab`,`STP`.`crm_site_travel_plan_added_by` AS `crm_site_travel_plan_added_by`,`STP`.`crm_site_travel_plan_added_on` AS `crm_site_travel_plan_added_on`,`CM`.`crm_cab_id` AS `crm_cab_id`,`CM`.`crm_cab_travels` AS `crm_cab_travels`,`CM`.`crm_cab_number` AS `crm_cab_number`,`CE`.`project_id` AS `crm_project_id`,`CE`.`name` AS `name`,`CE`.`assigned_to` AS `assigned_to`,`CE`.`added_by` AS `added_by`,`CE`.`enquiry_number` AS `enquiry_number`,`CE`.`cell` AS `cell`,`U`.`user_name` AS `travel_plan_added_by`,`U1`.`user_name` AS `site_visit_planned_by`,`U2`.`user_name` AS `travel_plan_assigned_to` from (((((((`crm_site_travel_plan` `STP` join `crm_site_visit_plan` `SVP` on((`SVP`.`crm_site_visit_plan_id` = `STP`.`crm_site_travel_plan_svp`))) join `crm_enquiry` `CE` on((`CE`.`enquiry_id` = `SVP`.`crm_site_visit_plan_enquiry`))) join `crm_project_master` `PM` on((`PM`.`project_id` = `SVP`.`crm_site_visit_plan_project`))) left join `crm_cab_master` `CM` on((`CM`.`crm_cab_id` = `STP`.`crm_site_travel_plan_cab`))) join `users` `U` on((`U`.`user_id` = `CE`.`added_by`))) join `users` `U1` on((`U1`.`user_id` = `SVP`.`crm_site_visit_plan_added_by`))) join `users` `U2` on((`U2`.`user_id` = `CE`.`assigned_to`))) ;

--
-- VIEW  `crm_site_visit`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
