-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 29, 2018 at 09:41 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `stock_indent_items_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_indent_items_list`  AS  select `si`.`stock_indent_id` AS `indent_id`,`si`.`stock_indent_no` AS `indent_no`,`si`.`stock_indent_project` AS `project_id`,`sp`.`stock_project_name` AS `project_name`,`smm`.`stock_material_code` AS `material_code`,`smm`.`stock_material_name` AS `material_name`,`smm`.`stock_material_price` AS `material_price`,`summ`.`stock_unit_name` AS `uom`,`u`.`user_name` AS `indent_by`,`si`.`stock_indent_added_on` AS `added_on`,`sii`.`stock_indent_item_id` AS `indent_item_id`,`sii`.`stock_indent_item_material_id` AS `material_id`,`sii`.`stock_indent_id` AS `stock_indent_id`,`sii`.`stock_indent_item_quantity` AS `material_qty`,`sii`.`stock_indent_item_uom` AS `stock_indent_item_uom`,`sii`.`stock_indent_item_active` AS `indent_item_active`,`sii`.`stock_indent_item_status` AS `indent_item_status`,`sii`.`stock_indent_item_required_date` AS `stock_indent_item_required_date`,`sii`.`stock_indent_item_requested_date` AS `stock_indent_item_requested_date`,`sii`.`stock_indent_item_remarks` AS `stock_indent_item_remarks`,`sii`.`stock_indent_item_approved_by` AS `stock_indent_item_approved_by`,`sii`.`stock_indent_item_approved_on` AS `stock_indent_item_approved_on`,`sii`.`stock_indent_item_added_by` AS `stock_indent_item_added_by`,`sii`.`stock_indent_item_added_on` AS `stock_indent_item_added_on` from ((((((`stock_indent_items` `sii` join `users` `u` on((`sii`.`stock_indent_item_added_by` = `u`.`user_id`))) join `stock_material_master` `smm` on((`sii`.`stock_indent_item_material_id` = `smm`.`stock_material_id`))) join `stock_indent` `si` on((`si`.`stock_indent_id` = `sii`.`stock_indent_id`))) join `stock_unit_measure_master` `summ` on((`summ`.`stock_unit_id` = `smm`.`stock_material_unit_of_measure`))) join `stock_project` `sp` on((`sp`.`stock_project_id` = `si`.`stock_indent_project`))) left join `stock_location_master` `slm` on((`slm`.`stock_location_id` = `si`.`stock_indent_location`))) ;

--
-- VIEW  `stock_indent_items_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
