-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2018 at 01:33 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_process_budget_details`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_process_budget_details`  AS  select `PMPM`.`project_master_name` AS `project_master_name`,`PMPM`.`project_management_master_id` AS `project_id`,`PPM`.`project_process_master_name` AS `project_process_master_name`,`PPP`.`project_plan_process_id` AS `project_plan_process_id`,`PPP`.`project_plan_process_active` AS `project_plan_process_active`,`U`.`user_name` AS `added_by`,`U`.`user_id` AS `user_id`,`AU`.`user_name` AS `assigned_user`,`AP`.`user_name` AS `approved_by`,`PPM`.`project_process_master_id` AS `project_process_master_id` from ((((((`project_plan_process` `PPP` join `users` `U` on((`U`.`user_id` = `PPP`.`project_plan_process_assigned_user`))) join `users` `AU` on((`AU`.`user_id` = `PPP`.`project_plan_process_assigned_user`))) join `project_process_master` `PPM` on((`PPM`.`project_process_master_id` = `PPP`.`project_plan_process_name`))) left join `users` `AP` on((`AP`.`user_id` = `PPP`.`project_plan_process_approved_by`))) join `project_plan` `PP` on((`PP`.`project_plan_id` = `PPP`.`project_plan_process_plan_id`))) join `project_management_project_master` `PMPM` on((`PMPM`.`project_management_master_id` = `PP`.`project_plan_project_id`))) ;

--
-- VIEW  `project_process_budget_details`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
