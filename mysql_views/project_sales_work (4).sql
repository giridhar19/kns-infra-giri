-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2018 at 07:30 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_sales_work`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_sales_work`  AS  select `pt`.`enquiry_follow_up_id` AS `enquiry_follow_up_id`,`pt`.`enquiry_id_for_follow_up` AS `enquiry_id_for_follow_up`,`pt`.`enquiry_follow_up_remarks` AS `enquiry_follow_up_remarks`,`pt`.`enquiry_follow_up_int_status` AS `enquiry_follow_up_int_status`,`pt`.`enquiry_follow_up_date` AS `enquiry_follow_up_date`,`pt`.`enquiry_follow_up_added_by` AS `enquiry_follow_up_added_by`,`pt`.`enquiry_follow_up_added_on` AS `enquiry_follow_up_added_on`,`cis`.`crm_cust_interest_status_id` AS `crm_cust_interest_status_id`,`cis`.`crm_cust_interest_status_name` AS `crm_cust_interest_status_name`,`cis`.`crm_cust_interest_status_active` AS `crm_cust_interest_status_active`,`cis`.`crm_customer_interest_status_order` AS `crm_customer_interest_status_order`,`cis`.`crm_cust_interest_status_added_by` AS `crm_cust_interest_status_added_by`,`cis`.`crm_cust_interest_status_added_on` AS `crm_cust_interest_status_added_on`,`ce`.`enquiry_id` AS `enquiry_id`,`ce`.`enquiry_number` AS `enquiry_number`,`ce`.`project_id` AS `project_id`,`ce`.`site_id` AS `site_id`,`ce`.`name` AS `name`,`ce`.`cell` AS `cell`,`ce`.`email` AS `email`,`ce`.`company` AS `company`,`ce`.`location` AS `location`,`ce`.`source` AS `source`,`ce`.`remarks` AS `remarks`,`ce`.`interest_status` AS `interest_status`,`ce`.`walk_in` AS `walk_in`,`ce`.`follow_up_date` AS `follow_up_date`,`ce`.`assigned_to` AS `assigned_to`,`ce`.`assigned_on` AS `assigned_on`,`ce`.`added_by` AS `added_by`,`ce`.`added_on` AS `added_on`,`cpm`.`project_name` AS `project_name`,`u`.`user_name` AS `user_name`,`u`.`user_id` AS `user_id`,`au`.`user_name` AS `assigned_by`,`esm`.`enquiry_source_master_name` AS `enquiry_source_master_name`,`esm`.`enquiry_source_master_id` AS `enquiry_source_master_id` from ((((((((`crm_enquiry_follow_up` `pt` left join `crm_enquiry_follow_up` `tt` on(((`pt`.`enquiry_id_for_follow_up` = `tt`.`enquiry_id_for_follow_up`) and (`pt`.`enquiry_follow_up_date` < `tt`.`enquiry_follow_up_date`)))) join `crm_enquiry` `ce` on((`ce`.`enquiry_id` = `pt`.`enquiry_id_for_follow_up`))) join `crm_customer_interest_status` `cis` on((`cis`.`crm_cust_interest_status_id` = `pt`.`enquiry_follow_up_int_status`))) left join `crm_project_master` `cpm` on((`cpm`.`project_id` = `ce`.`project_id`))) join `crm_enquiry_source_master` `esm` on((`esm`.`enquiry_source_master_id` = `ce`.`source`))) join `crm_customer_interest_status` `ciss` on((`ciss`.`crm_cust_interest_status_id` = `ce`.`interest_status`))) join `users` `u` on((`u`.`user_id` = `ce`.`assigned_to`))) join `users` `au` on((`au`.`user_id` = `ce`.`added_by`))) where isnull(`tt`.`enquiry_id_for_follow_up`) ;

--
-- VIEW  `project_sales_work`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
