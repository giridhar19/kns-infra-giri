-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2018 at 08:19 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `weekly_manpower_payment_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `weekly_manpower_payment_list`  AS  select `PAPMP`.`project_actual_payment_manpower_id` AS `project_actual_payment_manpower_id`,`PAPMP`.`project_actual_payment_task_manpower_id` AS `project_actual_payment_task_manpower_id`,`PAPMP`.`project_actual_payment_manpower_vendor_id` AS `project_actual_payment_manpower_vendor_id`,`PAPMP`.`project_actual_payment_manpower_hrs` AS `project_actual_payment_manpower_hrs`,`PAPMP`.`project_actual_payment_men_hrs` AS `project_actual_payment_men_hrs`,`PAPMP`.`project_actual_payment_women_hrs` AS `project_actual_payment_women_hrs`,`PAPMP`.`project_actual_payment_mason_hrs` AS `project_actual_payment_mason_hrs`,`PAPMP`.`project_actual_payment_others_hrs` AS `project_actual_payment_others_hrs`,`PAPMP`.`project_actual_payment_manpower_amount` AS `project_actual_payment_manpower_amount`,`PAPMP`.`project_actual_payment_manpower_tds` AS `project_actual_payment_manpower_tds`,`PAPMP`.`project_actual_payment_manpower_status` AS `project_actual_payment_manpower_status`,`PAPMP`.`project_actual_payment_manpower_bill_no` AS `project_actual_payment_manpower_bill_no`,`PAPMP`.`project_actual_payment_manpower_billing_address` AS `project_actual_payment_manpower_billing_address`,`PAPMP`.`project_actual_payment_manpower_from_date` AS `project_actual_payment_manpower_from_date`,`PAPMP`.`project_actual_payment_manpower_to_date` AS `project_actual_payment_manpower_to_date`,`PAPMP`.`project_actual_payment_manpower_active` AS `project_actual_payment_manpower_active`,`PAPMP`.`project_actual_payment_manpower_remarks` AS `project_actual_payment_manpower_remarks`,`PAPMP`.`project_actual_payment_manpower_accepted_by` AS `project_actual_payment_manpower_accepted_by`,`PAPMP`.`project_actual_payment_manpower_accepted_on` AS `project_actual_payment_manpower_accepted_on`,`PAPMP`.`project_actual_payment_manpower_approved_by` AS `project_actual_payment_manpower_approved_by`,`PAPMP`.`project_actual_payment_manpower_approved_on` AS `project_actual_payment_manpower_approved_on`,`PAPMP`.`project_actual_payment_manpower_added_by` AS `project_actual_payment_manpower_added_by`,`PAPMP`.`project_actual_payment_manpower_added_on` AS `project_actual_payment_manpower_added_on`,`SCM`.`stock_company_master_id` AS `stock_company_master_id`,`SCM`.`stock_company_master_name` AS `stock_company_master_name`,`SCM`.`stock_company_master_contact_person` AS `stock_company_master_contact_person`,`SCM`.`stock_company_master_active` AS `stock_company_master_active`,`PMA`.`project_manpower_agency_id` AS `project_manpower_agency_id`,`PMA`.`project_manpower_agency_name` AS `project_manpower_agency_name`,`PMA`.`project_manpower_agency_active` AS `project_manpower_agency_active`,`U`.`user_name` AS `added_by`,`U`.`user_id` AS `user_added_by`,`AU`.`user_id` AS `user_approved_by`,`AU`.`user_name` AS `approved_by` from ((((`project_actual_payment_manpower` `PAPMP` join `users` `U` on((`U`.`user_id` = `PAPMP`.`project_actual_payment_manpower_added_by`))) join `project_manpower_agency` `PMA` on((`PMA`.`project_manpower_agency_id` = `PAPMP`.`project_actual_payment_manpower_vendor_id`))) left join `stock_company_master` `SCM` on((`SCM`.`stock_company_master_id` = `PAPMP`.`project_actual_payment_manpower_billing_address`))) left join `users` `AU` on((`AU`.`user_id` = `PAPMP`.`project_actual_payment_manpower_approved_by`))) ;

--
-- VIEW  `weekly_manpower_payment_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
