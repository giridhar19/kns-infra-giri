<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: reason_list.php
CREATED ON	: 28-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	$task = $_GET["task"];

	// Temp data
	$alert = "";

	// Get Task Details
	$gen_task_data = i_get_gen_task_plan_list($task,'','','','','','','',''); // Get task plan for this task plan ID
	if($gen_task_data["status"] == SUCCESS)
	{
		$gen_task_data_details = $gen_task_data["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$gen_task_data["data"];
	}	

	// Get reason details
	$remarks_details = i_get_remarks($task);
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Remarks List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Remarks List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<form action="task_list.php" method="post" id="task_update_form">			
			<span style="padding-left:50px;">
			Task Details: <strong><?php if($gen_task_data["status"] == SUCCESS)
			{
				echo $gen_task_data_details[0]["general_task_details"]; 
			}
			else
			{
				echo 'DELETED TASK';
			}?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>Remarks</th>
					<th>File</th>
					<th>Remarks Date</th>
					<th>Subtask</th>
					<th>Status</th>
					<th>Action</th>					
				</tr>
				</thead>
				<tbody>
				 <?php
				 if($remarks_details["status"] == SUCCESS)
				 {					
					for($count = 0; $count < count($remarks_details["data"]); $count++)
					{					
					?>				
					<tr>
						<td><?php echo $remarks_details["data"][$count]["general_task_remarks"]; ?></td>
						<td><?php if($remarks_details["data"][$count]["general_task_remarks_file"] != "") { ?><a href="documents/<?php echo $remarks_details["data"][$count]["general_task_remarks_file"]; ?>" target="_blank">DOWNLOAD</a><?php }
						else
						{
						?>
						File Not Added
						<?php
						}?></td>
						<td><?php echo date("d-M-Y H:i",strtotime($remarks_details["data"][$count]["general_task_remarks_added_on"])); ?></td>	
						<td><?php if($remarks_details["data"][$count]["general_task_remarks_subtask"] != '0') 
						{ 
					    ?>
					    YES
						<?php 
						}
						else
						{
						?>
						NO
						<?php
						}?></td>
						<td><?php 
						if($remarks_details["data"][$count]["general_task_remarks_subtask"] != '0')
						{							
							if($remarks_details["data"][$count]["general_task_remarks_status"] != '0') 
							{
								$action = '0';
							?>
							Completed
							<?php 
							}
							else
							{
								$action = '1';
							?>
							Pending
							<?php
							}
						}
						else
						{
							?>
							NA
							<?php
						}?></td>	
						<td><?php
						if(($remarks_details["data"][$count]["general_task_remarks_subtask"] != '0')  && ($gen_task_data_details[0]["general_task_user"] == $user))
						{
						?><a href="#" onclick="return update_status(<?php echo $remarks_details["data"][$count]["general_task_remarks_id"] ?>,<?php echo $action; ?>,<?php echo $task; ?>);">UPDATE</a>
						<?php
						}
						else
						{
							?>
							<i>NA</i>
							<?php
						}
						?></td>						
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="2">No remarks added</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />			
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function update_status(remarks_id,action,task)
{
	var ok = confirm("Are you sure you want to change the status?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if(xmlhttp.responseText != "SUCCESS")
				{					
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
				}
				else(xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "view_gen_task_remarks.php?task=" + task;
				}
			}

			xmlhttp.open("POST", "change_task_remarks_status.php?remarks=" + remarks_id + "&status=" + action);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>

  </body>

</html>