<?php

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

// What is the date today
$start_date = date('Y-m-d 00:00:00',strtotime("-1 days"));
$end_date = date('Y-m-d 23:59:59',strtotime("-1 days"));
// Get list of approved bookings with no profile
$max_wait_days = 30;
// Get Project Task BOQ modes already added

// Project data
$project_management_master_search_data = array("active"=>'1',"status"=>"all");
$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
if($project_management_master_list["status"] == SUCCESS)
{
	$project_management_master_list_data = $project_management_master_list["data"];
}

	for($pcount = 0 ; $pcount < count($project_management_master_list_data) ; $pcount++)
	{
		$sl_no = 0;
		$project_email_search_data = array("project_id"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$email_list = db_get_project_email_list($project_email_search_data);
		if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
				$email_list_data = $email_list["data"];
				$to  = $email_list_data[0]["project_email_details_to"];
				$cc  = $email_list_data[0]["project_email_details_cc"];
				$bcc = $email_list_data[0]["project_email_details_bcc"];
		}
		$project_stock_module_mapping_search_data = array("mgmnt_project_id"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$project_stock_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);
		if ($project_stock_mapping_list["status"] == SUCCESS) {
				$stock_project = $project_stock_mapping_list["data"][0]["project_stock_module_mapping_stock_project_id"];
				$material_stock_search_data = array("project"=>$stock_project,"material_id"=>'461');
				$material_stock_list = i_get_material_stock($material_stock_search_data);
				if($material_stock_list['status'] == SUCCESS)
				{
					$material_stock_list_data = $material_stock_list['data'];
					$material_qty = $material_stock_list_data[0]["material_stock_quantity"];
				}
				//Contract Data
				$stock_issued_data = array("material_id"=>"461","issued_start_date"=>$start_date,"issued_end_date"=>$end_date,"project_id"=>$stock_project);
				$issued_material_list = db_get_issued_material($stock_issued_data);
				if($issued_material_list['status'] == DB_RECORD_ALREADY_EXISTS)
				{
					$total_amount  = 0 ;
					$message_heading = "" ;
					$message_content = "" ;
					$heading = "" ;
					// $message_end = "" ;
					$issued_material_list_data = $issued_material_list['data'];
					$subject = $project_management_master_list_data[$pcount]["project_master_name"].'  '.'Diesel Consumption List';
					$message = 'Dear Sir/Madam,<br><br>Daily Diesel list:<br><br>';

					$message_heading .="<table border='1' style='border-collapse:collapse; border-width:2px;'>";
						// Header row - start
					$message_heading .= "<tr style='border-width:2px;text-align:center;'>
					<td style='border-width:2px;text-align:center;'><strong>SL No.</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Process</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Task</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Road</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Issued By</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>UOM</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Issued qty</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Machine</strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Machine Number<strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Amount<strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Issue No<strong></td>
					<td style='border-width:2px;text-align:center;'><strong>Issued Date</strong></td>";

					$message_heading = $message_heading.'</tr>';
					for($count = 0; $count < count($issued_material_list_data); $count++)
					{
						$sl_no++;

						// Compose the message
						$project	     = $issued_material_list_data[$count]["stock_project_name"];
						$process  	   = $issued_material_list_data[$count]["process_name"];
						$task	  	     = $issued_material_list_data[$count]["task_name"];
						$issued_by	   = $issued_material_list_data[$count]["user_name"];
						$uom	 			   = $issued_material_list_data[$count]["uom_name"];
						$issued_qty	   = $issued_material_list_data[$count]["stock_issue_item_qty"];
						$machine	     = $issued_material_list_data[$count]["stock_machine_master_name"];
						$machine_number= $issued_material_list_data[$count]["stock_machine_master_id_number"];
						$issue_no	     = $issued_material_list_data[$count]["stock_issue_no"];
						$issued_on	   = $issued_material_list_data[$count]["stock_issue_item_issued_on"];
						$material_price	   = $issued_material_list_data[$count]["stock_material_price"];
						$total_amount += ($issued_qty * $material_price) ;
						if($issued_material_list_data[$count]["road_id"] == "No Roads")
						{
							$location = "No Roads";
						}
						else {
							$location	 = $issued_material_list_data[$count]["road_name"];
						}

						$message_content .= "<tr style='border-width:2px; text-align:center;'>
							<td style='border-width:2px;text-align:center;'>".$sl_no."</td>
							<td style='border-width:2px;text-align:center;'>".$process."</td>
							<td style='border-width:2px;text-align:center;'>".$task."</td>
							<td style='border-width:2px;text-align:center;'>".$location."</td>
							<td style='border-width:2px;text-align:center;'>".$issued_by."</td>
							<td style='border-width:2px;text-align:center;'>".$uom."</td>
							<td style='border-width:2px;text-align:center;'>".$issued_qty."</td>
							<td style='border-width:2px;text-align:center;'>".$machine."</td>
							<td style='border-width:2px;text-align:center;'>".$machine_number."</td>
							<td style='border-width:2px;text-align:center;'>".($issued_qty*$material_price)."</td>
							<td style='border-width:2px;text-align:center;'>".$issue_no."</td>
						<td style='border-width:2px;text-align:center;'>".$issued_on."</td>
						</tr>";
					}
					$heading = $heading."<br>
					<table style='border: 1px solid black;width:30%;'>
					<tr>
						<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Project</th>
						<td style='border: 1px solid #ddd;width:20%;colspan:2;'>".$project_management_master_list_data[$pcount]["project_master_name"] ."</td>
					</tr>
					<tr>
					<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Date</th>
					<td style='border: 1px solid #ddd;width:20%'>".date("d-M-Y")."</td>
					</tr>
					<tr>
						<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Material</th>
						<td style='border: 1px solid #ddd;width:20%'>Diesel</td>
					</tr>
					<tr>
						<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Issued Value</th>
						<td style='border: 1px solid #ddd;width:20%'>".$total_amount."</td>
					</tr>
					<tr>
					<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Stock As on ".date("d-M-Y")."</th>
					<td style='border: 1px solid #ddd;width:20%'>".$material_qty."</td>
					</tr>
					</table><br>";
					$message_end = "</table>";
					$message_end = $message_end.'<br>Regards,<br>KNS ERP';

					$message .= $heading ;
					$message .= $message_heading ;
					$message .= $message_content;
					$message .= $message_end;
					$cc = explode(',',$cc);
					$cc_string = '[';
					for($count = 0; $count < count($cc); $count++)
					{
						$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
					}
					$cc_string = trim($cc_string,',');
					$cc_string = $cc_string.']';

					$bcc = explode(',',$bcc);
						$bcc_string = '[';
						for($count = 0; $count < count($bcc); $count++)
						{
							$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
						}
						$bcc_string = trim($bcc_string,',');
						$bcc_string = $bcc_string.']';

					$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
						"personalizations": [
							{
								"to": [
									{
										"email": "'.$to.'"
									}
								],
								"cc": '.$cc_string.',
								"bcc": '.$bcc_string.',
								"subject": "'.$subject.'"
							}
						],
						"from": {
							"email": "venkataramanaiah@knsgroup.in"
						},
						"content": [
							{
								"type": "text/html",
								"value": "'.$message.'"
							}
						]
					}'));
					$apiKey = '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';
					$sg = new \SendGrid($apiKey);
					// $response = $sg->client->mail()->send()->post($request_body);
					var_dump($message);
				}
		}
		 else {

		}
	}
?>
