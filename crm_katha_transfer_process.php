<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23rd Feb 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["site"]))
	{
		$site_id = $_GET["site"];
	}
	else
	{
		$site_id = "";
	}
	if(isset($_GET["type"]))
	{
		$transaction_type = $_GET["type"];
	}
	else
	{
		$transaction_type = "";
	}
	// Capture the form data
	if(isset($_POST["add_kt_transaction_submit"]))
	{
		$site_id          = $_POST["hd_site_id"];
		$transaction_type = $_POST["hd_trans_type"];
		$transaction_date = $_POST["dt_transaction_date"];
		$remarks          = $_POST["txt_remarks"];		
		
		// Check for mandatory fields
		if(($site_id !="") && ($transaction_date !="") && ($transaction_type !=""))
		{
			if(strtotime($transaction_date) > strtotime(date("Y-m-d")))
			{
				$alert_type = 0;
				$alert      = "Transaction cannot be greater than current date";
			}
			else
			{
				$kt_transaction_iresult = i_add_kt_transaction($site_id,$transaction_type,$transaction_date,$remarks,$user);
				
				if($kt_transaction_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
					header("location:crm_site_status_report.php");
				}
				else
				{
					$alert_type = 0;
				}
				
				$alert = $kt_transaction_iresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Enquiry Follow Up List
	$kt_transaction_data = array("site_id"=>$site_id);
	$kt_transaction_list = i_get_kt_transactions($kt_transaction_data);
	if($kt_transaction_list["status"] == SUCCESS)
	{
		$kt_transaction_list_data = $kt_transaction_list["data"];
	}
	
	// Site Data
	$site_list = i_get_site_list('','','','','','','',$site_id);
	if($site_list["status"] == SUCCESS)
	{
		$site_list_data = $site_list["data"];
		$site_no = $site_list_data[0]["crm_site_no"];
		$project = $site_list_data[0]["project_name"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_list["data"];
		
		$site_no = "<i>Invalid Site No</i>";
		$project = "<i>Invalid Project</i>";
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Katha Transfer Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
	      				<h3>Site: <?php echo $site_no; ?>&nbsp;&nbsp;&nbsp;&nbsp;Project: <?php echo $project; ?></h3>						
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <?php
							if($transaction_type == "1") // Applied
							{
								echo "Add Katha Transfer Application Details";
							}
							else if($transaction_type == "2") // Received
							{
								echo "Add Katha Transfer Received Details";
							}
							else if($transaction_type == "3")
							{
								echo "View Katha Transfer Details";
							}
							?>								
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
							<?php 
							if(($transaction_type == "1") || ($transaction_type == "2"))
							{?>
								<div class="tab-pane active" id="formcontrols">
								<form id="add_kt_transaction_form" class="form-horizontal" method="post" action="crm_katha_transfer_process.php">
								<input type="hidden" name="hd_site_id" value="<?php echo $site_id; ?>" />
								<input type="hidden" name="hd_trans_type" value="<?php echo $transaction_type; ?>" />
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="dt_transaction_date">Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_transaction_date" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_kt_transaction_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								<?php
								}
								?>
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Activity</th>
									<th>Date</th>					
									<th>Remarks</th>					
									<th>Added By</th>
									<th>Added On</th>									
								</tr>
								</thead>
								<tbody>							
								<?php
								if($kt_transaction_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($kt_transaction_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php if($kt_transaction_list_data[$count]["crm_katha_transfer_transaction_type"] == "1") 
									{
										echo "Katha Application Submitted";
									}
									else
									{
										echo "Katha Documents Received";
									}?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($kt_transaction_list_data[$count]["crm_katha_transfer_transaction_date"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $kt_transaction_list_data[$count]["crm_katha_transfer_transaction_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $kt_transaction_list_data[$count]["user_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($kt_transaction_list_data[$count]["crm_katha_transfer_transaction_added_on"])); ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="4">Katha Transfer not initiated yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
