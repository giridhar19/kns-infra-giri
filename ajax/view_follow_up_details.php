<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

  $start_date = $_GET["start_date"];
  $end_date = $_GET["end_date"];
  $user_id = $_GET["user_id"];
  $user_name = $_GET["user_name"];
  $follow_up_data = i_get_enquiry_fup_list('','',$user_id,'','','',$start_date,$end_date,'assignee_only','','','');
  if($follow_up_data["status"] == SUCCESS)
  {

		$answered_fup=array();
		$not_answered_fup=array();
		$follow_up_list_data=$follow_up_data["data"];
		for($count=0;$count<count($follow_up_list_data);$count++){
			if($follow_up_list_data[$count]["enquiry_follow_up_call_status"]==""){
				continue;
			}
			else if($follow_up_list_data[$count]["enquiry_follow_up_call_status"]=="answered"){
				array_push($answered_fup,$follow_up_list_data[$count]);
			}
			else{
				array_push($not_answered_fup,$follow_up_list_data[$count]);
			}
		}
  }
}
else
{
	header("location:login.php");
}
?>
<div class="widget-header" style="height:auto; margin-bottom:15px;">
	<div style="border-bottom: 1px solid #FFFFFF;">
		<span class="header-label">Start Date: </span><span><?php echo date("d-m-Y h:i:s A ",strtotime($start_date));?></span>
	</div>
	<div style="border-bottom: 1px solid #FFFFFF;">
		<span class="header-label">End Date: </span><span><?php echo date("d-m-Y h:i:s A",strtotime($end_date));?></span>
	</div>
	<div style="border-bottom: 1px solid #FFFFFF;">
		<span class="header-label">Crm User: </span><span><?php echo $user_name ?></span>
 </div>
 <h3>Showing records for Call Answered Status</h3>
</div>

<table id="example1" class="table table-striped table-bordered" cellspacing="0" width="100%" style="margin-bottom:15px">
 <thead>
	 <tr>
		 <th>Added On</th>
		 <th>Enquiry Number</th>
		 <th>Name</th>
		 <th>cell</th>
		 <th>Call Status</th>
		 <th>Remarks</th>
		 <th>Follow up Date</th>
		 <th>Status</th>
 </tr>
 </thead>
 <tbody>
	 <?php
	 if(isset($answered_fup) && count($answered_fup)>0){
	for($count=0;$count<count($answered_fup);$count++){
	?>
	<tr>
		<td>
			<?php echo date("d-m-Y h:i:s A",strtotime($answered_fup[$count]["enquiry_follow_up_added_on"])); ?>
		</td>
	<td>
			<?php echo $answered_fup[$count]["enquiry_number"];?>
	</td>
	<td>
		<?php echo $answered_fup[$count]["name"] ?>
	</td>
	<td>
		<?php echo $answered_fup[$count]["cell"] ?>
	</td>
	<td>
	<?php echo $answered_fup[$count]['enquiry_follow_up_call_status']?>
 </td>
	<td>
		<?php echo $answered_fup[$count]["enquiry_follow_up_remarks"] ?>
	</td>
	<td>
		<?php echo date("d-m-Y h:i:s A",strtotime($answered_fup[$count]["enquiry_follow_up_date"])); ?>
	</td>
	<td>
		<?php echo $answered_fup[$count]["crm_cust_interest_status_name"] ?>
	</td>
	</tr>
	<?php
}
}
?>
</tbody>
</table>
	<div style="height:auto; margin:10px 0px 14px 0px;" class="widget-header">
 	 <h3>Showing records for Call Not Answered Status</h3>
  </div>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
 <thead>
	 <tr>
		 <th>Added On</th>
		 <th>Enquiry Number</th>
		 <th>Name</th>
		 <th>cell</th>
		 <th>Call Status</th>
		 <th>Remarks</th>
		 <th>Follow up Date</th>
		 <th>Status</th>
 </tr>
 </thead>
 <tbody>
	 <?php
	 if(isset($not_answered_fup) && count($not_answered_fup)>0){
	for($count=0;$count<count($not_answered_fup);$count++){
?>
	<tr>
		<td>
			<?php echo date("d-m-Y h:i:s A",strtotime($not_answered_fup[$count]["enquiry_follow_up_added_on"])); ?>
		</td>
	<td>
			<?php echo $not_answered_fup[$count]["enquiry_number"];?>
	</td>
	<td>
		<?php echo $not_answered_fup[$count]["name"] ?>
	</td>
	<td>
		<?php echo $not_answered_fup[$count]["cell"] ?>
	</td>
	<td>
	<?php echo $not_answered_fup[$count]['enquiry_follow_up_call_status']?>
 </td>
	<td>
		<?php echo $not_answered_fup[$count]["enquiry_follow_up_remarks"] ?>
	</td>
	<td>
		<?php echo date("d-m-Y h:i:s A",strtotime($not_answered_fup[$count]["enquiry_follow_up_date"])); ?>
	</td>
	<td>
		<?php echo $not_answered_fup[$count]["crm_cust_interest_status_name"] ?>
	</td>
	</tr>
	<?php
}
}
?>
</tbody>
</table>
