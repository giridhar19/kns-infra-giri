<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_masters.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$material_id     = $_GET["material_id"];
	$project_id      = $_GET["project_id"];
	$material_stock_search_data=array("material_id"=>$material_id,"project"=>$project_id);
	$stock_material = i_get_material_stock($material_stock_search_data);
	if($stock_material["status"] == SUCCESS)
	{
		$qunatity = 0;
		for($stock_qty = 0; $stock_qty< count($stock_material[ "data"]) ; $stock_qty++)
		{
			$stock_material_data = $stock_material[ "data"];
			$stock_qunatity = $qunatity + $stock_material_data[$stock_qty][ "material_stock_quantity"];
		}
	}
	else
	{
		$stock_qunatity = "0" ;
	}

	//Get Percentage of stock
	$stock_material_percenatge_search_data = array("material_id"=>$material_id);
	$material_percentage_list = db_get_stock_material_percentage_list($stock_material_percenatge_search_data);
	if($material_percentage_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$material_percentage_list_data = $material_percentage_list["data"];
		$material_percentage = $material_percentage_list_data[0]["stock_percentage"];
	}
	else {
		$material_percentage = 0;
	}

	$stock_data = array("stock_qty"=>$stock_qunatity,"purchaseable_qty"=>$material_percentage);
	echo json_encode($stock_data);
}
else
{
	header("location:login.php");
}
?>
