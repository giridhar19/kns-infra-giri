<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $enquiry_id = $_GET["enquiry_id"];
	$prospective_closure_date="";
	if(isset($_GET["prospective_profile_id"])){
		$prospective_closure_date_data = i_get_prospective_clients($_GET["prospective_profile_id"],'','','','');
		$prospective_closure_date=$prospective_closure_date_data["data"][0]["crm_prospective_profile_tentative_closure_date"];
	}
  $latest_follow_up_data = i_get_enquiry_fup_list($enquiry_id,'','','desc','0','1');
  $follow_up_data = i_get_enquiry_fup_list($enquiry_id,'','','','','');
  if($follow_up_data["status"] == SUCCESS)
  {
    $fup_count = count($follow_up_data["data"]);
  }
  else
  {
    $fup_count = 0;
  }
  // Site Visit Data
  $site_visit_data = i_get_site_travel_plan_list('','',$enquiry_id,'','','','2','','');
  if($site_visit_data["status"] == SUCCESS)
  {
    $sv_count = count($site_visit_data["data"]);
  }
  else
  {
    $sv_count = 0;
  }

 $sales_result = i_get_site_booking('','','','','','1','','','','','','',$enquiry_id,'','','','');
 if($sales_result["status"]== SUCCESS)
 {
  $isSold='SOLD';
}
else
{
  $isSold='UNSOLD';
}
$unqualified_data = i_get_unqualified_leads('',$enquiry_id,'','','','','');
$status="";
if($unqualified_data["status"]==SUCCESS){
	$status="unqualified";
}

  $output = array("follow_up_added_by"=>
	$latest_follow_up_data["data"][0]["user_name"],"remarks"=>
	$latest_follow_up_data["data"][0]["enquiry_follow_up_remarks"],"follow_up_added_on"=>
	$latest_follow_up_data["data"][0]["enquiry_follow_up_added_on"],"follow_up_date"=>
	$latest_follow_up_data["data"][0]["enquiry_follow_up_date"],"fup_count"=>
	$fup_count,"sv_count"=>$sv_count,"isSold"=>$isSold,"closure_date"=>$prospective_closure_date);
	echo json_encode($output);
}
 else{
   echo "FAILURE";
 }
?>
