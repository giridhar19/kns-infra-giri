<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management_masters.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['task_id'])) {
        $task_id = $_REQUEST['task_id'];
    } else {
        $task_id = "";
    }
    if (isset($_REQUEST['road_id'])) {
        $road_id = $_REQUEST['road_id'];
    } else {
        $road_id = "";
    }

    $work_type = 'Regular';

    $project_process_task_search_data = array("task_id"=>$task_id,"active"=>'1');
    $project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if ($project_plan_process_task_list["status"] == SUCCESS) {
        $project_plan_process_task_list_data = $project_plan_process_task_list["data"];
        $task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
        $project = $project_plan_process_task_list_data[0]["project_master_name"];
        $process = $project_plan_process_task_list_data[0]["project_process_master_name"];
    } else {
        $alert = $project_plan_process_task_list["data"];
        $alert_type = 0;
        $task_name = "";
        $project = "";
        $process = "";
    }

    // Get Project task planning list
    $project_task_planning_search_data = array("active"=>'1',"task_id"=>$task_id,"no_of_roads"=>$road_id);
    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];
        $planned_msmrt = $project_task_planning_list_data[0]["project_task_planning_measurment"];
        $uom = $project_task_planning_list_data[0]["project_uom_name"];
        if ($road_id == "No Roads") {
            $road = "No Roads";
        } else {
            $road = $project_task_planning_list_data[0]["project_site_location_mapping_master_name"];
        }
    } else {
        $planned_msmrt = "";
        $uom = "";
        $road = "";
    }
    //Get Man power List
    $man_power_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>'1',"work_type"=>$work_type);
    $man_power_actual_data = i_get_man_power_list($man_power_search_data);
    if ($man_power_actual_data["status"] == SUCCESS) {
        $total_mp_completed_msmrt = 0;
        for ($mp_count = 0 ; $mp_count < count($man_power_actual_data["data"]) ; $mp_count++) {
            $total_mp_completed_msmrt = $total_mp_completed_msmrt + $man_power_actual_data["data"][$mp_count]["project_task_actual_manpower_completed_msmrt"];
        }
        $manpower_status = "";
    } else {
        $total_mp_completed_msmrt = 0;
        $manpower_status = "NotStarted";
    }

    //Get Machine List
    $actual_machine_plan_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>'1',"work_type"=>$work_type);
    $machine_latest_list = i_get_machine_planning_list($actual_machine_plan_search_data);
    if ($machine_latest_list['status'] == SUCCESS) {
        $machine_latest_list_data = $machine_latest_list["data"];
        $total_mc_completed_msmrt = 0;
        for ($mc_count = 0 ; $mc_count < count($machine_latest_list_data) ; $mc_count++) {
            $total_mc_completed_msmrt = $total_mc_completed_msmrt + $machine_latest_list_data[$mc_count]["project_task_actual_machine_msmrt"];
        }
        $machine_status = "";
    } else {
        $total_mc_completed_msmrt = 0;
        $machine_status = "NotStarted";
    }

    //Get Contract List
    $project_task_actual_boq_search_data = array("task_id"=>$task_id, "active"=>'1', "work_type"=>$work_type, "road_id"=>$road_id);
    $contract_latest_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);

    if ($contract_latest_list['status'] == SUCCESS) {
        $contract_status = "" ;
        $contract_latest_list_data = $contract_latest_list["data"];
        $total_cw_completed_msmrt = 0;
        for ($cw_count = 0 ; $cw_count < count($contract_latest_list["data"]) ; $cw_count++) {
            $total_cw_completed_msmrt = $total_cw_completed_msmrt + $contract_latest_list["data"][$cw_count]["project_task_actual_boq_total_measurement"];
        }
    } else {
        $contract_status = "NotStarted";
        $total_cw_completed_msmrt = '0';
    }
    $overall_completed_msmrt = $total_mp_completed_msmrt + $total_mc_completed_msmrt + $total_cw_completed_msmrt;

    $data = array( "project" => $project,
                           "process" => $process,
                           "task" => $task_name,
                           "road"=>$road,
                           "uom" => $uom,
                           "planned_msmrt" => $planned_msmrt,
                           "mp_msmrt" => $total_mp_completed_msmrt,
                           "mc_msmrt" => $total_mc_completed_msmrt,
                           "cw_msmrt" => $total_cw_completed_msmrt,
                           "manpower_status" => $manpower_status,
                           "machine_status" =>  $machine_status,
                           "contract_status" => $contract_status,
                           "overall_msmrt" => $overall_completed_msmrt
                         );

    echo json_encode($data);
} else {
    header("location:login.php");
}
