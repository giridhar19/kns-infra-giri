<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'general_config.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_master_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'projectmgmnt' . DIRECTORY_SEPARATOR . 'project_management_functions.php');
include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    $search_vendor = $_GET["vendor_id"];
    $type          = $_GET["type"];

    // Get Project  Payment ManPower modes already added
    $project_actual_payment_manpower_search_data = array(
        "active" => '1',
        "vendor_id" => $search_vendor,
        "start" => '-1'
    );
    $project_actual_payment_manpower_list  = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
    if ($project_actual_payment_manpower_list['status'] == SUCCESS) {
        $project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
    }

    $total_issued_amount = 0;
    $total_balance       = 0;
    $total_amount        = 0;
    if ($project_actual_payment_manpower_list["status"] == SUCCESS) {

        for ($count = 0; $count < count($project_actual_payment_manpower_list_data); $count++) {
            if ($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_status"] == 'Accepted' || $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_status"] == 'Approved' || $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_status"] == 'Payment Issued') {
                //Get Delay
                $start_date = date("d-M-Y");
                $end_date   = date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_approved_on"]));
                $delay      = get_date_diff($end_date, $start_date);

                //Get total amount
                $amount_before_tds = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_amount"];
                $manpower_tds      = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_tds"];
                $tds_amount        = ($manpower_tds / 100) * $amount_before_tds;
                $amount            = $amount_before_tds - $tds_amount;


                //Get Project Machine Vendor master List
                $issued_amount                               = 0;
                $project_man_power_issue_payment_search_data = array(
                    "active" => '1',
                    "man_power_id" => $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]
                );
                $project_man_power_issue_payment_list        = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
                if ($project_man_power_issue_payment_list["status"] == SUCCESS) {
                    $project_actual_machine_payment_issue_list_data = $project_man_power_issue_payment_list["data"];
                    for ($issue_count = 0; $issue_count < count($project_actual_machine_payment_issue_list_data); $issue_count++) {
                        $issued_amount = $issued_amount + $project_actual_machine_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_amount"];
                        $total_issued_amount = $total_issued_amount + $project_actual_machine_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_amount"];
                    }
                } else {
                    $issued_amount = 0;
                }
                $balance_amount  = round(($amount - $issued_amount), 2);
                $project_payment_manpower_mapping_search_data = array('payment_id' => $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]);
                $project_name_sresult = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
                if ($project_name_sresult['status'] == SUCCESS) {
                    $project_name = $project_name_sresult['data'][0]['project_master_name'];
                    $project_id   = $project_name_sresult['data'][0]['project_management_master_id'];
                } else {
                    $project_name = 'NOT VALID';
                    $project_id   = '-1';
                }
                    $total_issued_amount = $total_issued_amount;
                    $total_balance       = $total_balance + $balance_amount;
                    $total_amount        = $total_amount + $amount;
                $vendor = $project_actual_payment_manpower_list_data[$count]["project_manpower_agency_name"];
            }
        }
        $output= array('total_amount' => $total_amount, 'issued_amount'=>$total_issued_amount,'balance_amount'=>$total_balance);
    }
    $output= array('total_amount' => $total_amount, 'issued_amount'=>$total_issued_amount,'balance_amount'=>$total_balance);
    echo json_encode($output);
} else {
    echo "FAILURE";
}

?>
