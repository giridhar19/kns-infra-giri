<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 8th July 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

// Session Data
$user = $_SESSION["loggedin_user"];
$role = $_SESSION["loggedin_role"];

/* FORM DATA - START */
$type        = $_POST["type"];
if($type == 'single')
{
	$process_id  = $_POST["process"];
	$bprocess_id = '';
}
else
{
	$process_id  = '';
	$bprocess_id = $_POST["process"];
}
/* FORM DATA - END */

// Get tasks for this process
$task_result = i_get_task_plan_list('','',$process_id,'','','','','',$bprocess_id);

if($task_result['status'] == SUCCESS)
{	
	$return = '
	<table class="table table-bordered" style="table-layout: fixed;">
        <thead>
            <tr>
				<th>SL No</th>	
				<th>Task</th>					
				<th>Start Date</th>
				<th>End Date</th>	
				<th>Variance</th>
				<th>Remarks</th>
			</tr>
		</thead>
		<tbody>';
		
	for($count = 0; $count < count($task_result['data']); $count++)
	{
		$start_date = $task_result['data'][$count]['task_plan_actual_start_date'];
		$end_date   = $task_result['data'][$count]['task_plan_actual_end_date'];
		
		if(($start_date == '') || ($start_date == '0000-00-00'))
		{
			$start_date_display = '';
			$start_date         = date('Y-m-d');
		}
		else
		{
			$start_date_display = date('d-M-Y',strtotime($start_date));
		}
		
		if(($end_date == '') || ($end_date == '0000-00-00'))
		{
			$end_date_display = '';
			$end_date         = date('Y-m-d');
		}
		else
		{
			$end_date_display = date('d-M-Y',strtotime($end_date));
		}
		
		$variance = get_date_diff($start_date,$end_date);
		
		$return = $return.'
		<tr>
			<td style="word-wrap:break-word;">'.($count + 1).'</td>
			<td style="word-wrap:break-word;">'.$task_result['data'][$count]['task_type_name'].'</td>
			<td style="word-wrap:break-word;">'.$start_date_display.'</td>
			<td style="word-wrap:break-word;">'.$end_date_display.'</td>
			<td style="word-wrap:break-word;">'.$variance['data'].'</td>
			<td style="word-wrap:break-word;"><a href="reason_list.php?task='.$task_result['data'][$count]['task_plan_legal_id'].'" target="_blank">Remarks</a></td>
		</tr>';
	}
			
	$return  = $return.'</tbody>
	</table>';
}
else
{
	$return = 'No tasks for this process';
}

echo $return;
?>