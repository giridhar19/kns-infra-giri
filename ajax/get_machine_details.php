<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$payment_id  = $_GET["payment_id"];
	$bata=0;
	// Get machine payment mapping for project name
	$project_payment_machine_mapping_search_data = array("payment_id"=>$payment_id);
	$project_name_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
	if($project_name_sresult['status'] == SUCCESS)
	{
		for ($count = 0; $count < count($project_name_sresult['data']); $count++) {
		$bata   = $bata+$project_name_sresult['data'][$count]['project_task_actual_machine_bata'];
	}
		$actual_machine_plan_search_data = array('plan_id'=>$project_name_sresult['data'][0]['project_payment_machine_mapping_machine_actuals_id']);
		$project_name_data = i_get_machine_planning_list($actual_machine_plan_search_data);
		$project_name = $project_name_data['data'][0]['project_master_name'];
		$machine   = $project_name_data['data'][0]['project_machine_master_id_number'];
		$machine_name   = $project_name_data['data'][0]['project_machine_master_name'];
	}
	else
	{
		$project_name = 'INVALID PROJECT';
		$machine = "";
	}
	$output = array("machine"=>$machine,"project"=>$project_name,"bata_amount"=>$bata,"machine_name"=>$machine_name);
  echo json_encode($output);
}
else
{
	header("location:login.php");
}
?>
