<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$task_id      = $_GET["task_id"];
	$road_id      = $_GET["road_id"];

	$total_actual_cw_cost = 0 ;
	$project_budget_contract_search_data = array("task_id"=>$task_id,"road_id"=>$road_id);
	$budget_cw_list =  db_get_project_budget_contract($project_budget_contract_search_data);
	if($budget_cw_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			for($cw_count = 0 ; $cw_count < count($budget_cw_list["data"]) ; $cw_count++)
			{
				$total_actual_cw_cost = $total_actual_cw_cost + $budget_cw_list["data"][$cw_count]["total_amount"];
			}
	}
	else {
		$total_actual_cw_cost = "";
	}
	echo $total_actual_cw_cost ;
}
else
{
	header("location:login.php");
}
?>
