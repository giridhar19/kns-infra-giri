<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$material_id    = $_REQUEST["material_id"];
	$project_id     = $_REQUEST["project_id"];
	$status     = $_REQUEST["search_status"];

	// Get Indent Item Details
	// Get Indent Item Details
	if($status == "Waiting")
	{
		$is_quote = 1;
	}
	else {
		$is_quote = '' ;
	}
	$stock_indent_search_data = array("active"=>'1',"is_quote"=>'1','approved_on'=>$is_quote,'project'=>$project_id,"material_id"=>$material_id,"status"=>'Approved');
	$indent_item_list = db_get_indent_qty($stock_indent_search_data);
	if($indent_item_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$indent_item_list_data = $indent_item_list["data"];
		$indent_qty = 0 ;
		$indent_qty += $indent_item_list_data[0]["total_stock_indent_item_quantity"];
	}
	else
	{
		$indent_qty = 0;
	}
	echo $indent_qty ;
}
else
{
	header("location:login.php");
}
?>
