<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$po_id      = $_GET["po_id"];
	$material_id    = $_GET["material_id"];

	$po_qty    = 0;
	$stock_purchase_order_items_search_data = array("order_id" => $po_id,"item" => $material_id);
	$stock_purchase_item_list        = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);
	if ($stock_purchase_item_list["status"] == SUCCESS) {
			$po_qty += $stock_purchase_item_list["data"][0]["stock_purchase_order_item_quantity"];
	}
	else
	{
		$po_qty = 0;
	}
	echo $po_qty ;
}
else
{
	header("location:login.php");
}
?>
