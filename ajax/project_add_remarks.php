<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['remarks'])) {
        $remarks = $_REQUEST['remarks'];
    } else {
        $remarks = "";
    }

    if (isset($_REQUEST['remarks_id'])) {
        $remarks_id = $_REQUEST['remarks_id'];
    } else {
        $remarks_id = "";
    }

    if (isset($_REQUEST['status'])) {
        $status = $_REQUEST['status'];
    } else {
        $status = "";
    }
    if($status == "Completed")
    {
      $update_status = "Pending";
    }
    else {
      $update_status = "Completed";
    }
    $process_remarks_update_data = array("status"=>$update_status,"completed_remarks"=>$remarks,"closed_by"=>$user,"closed_om"=>date("d-m-y h:i:s"));
    $payment_manpower_mapping_iresult = db_update_process_remarks_task($remarks_id,$process_remarks_update_data);
    echo json_encode($payment_manpower_mapping_iresult);
} else {
    header("location:login.php");
}
