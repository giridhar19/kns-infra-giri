<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['user_id'])) {
        $user_id = $_REQUEST['user_id'];
    } else {
        $user_id = "";
    }

    $user_list = db_get_pending_loan($user_id);
    if ($user_list['status'] == 'DB_RECORD_ALREADY_EXISTS') {
        $user_list_data = $user_list['data'][0];

        $monthly_amount =   ceil($user_list_data['pending_loan_total_amount'] / $user_list_data['pending_loan_total_tenure']);
        $deduction_amount = ceil($user_list_data['pending_loan_pending_amount'] / $monthly_amount);
        $monthly_amount = $deduction_amount >=1 ? $monthly_amount : 0;
    } else {
      $monthly_amount = 0;
    }
    echo json_encode($monthly_amount);
} else {
    header("location:login.php");
}
