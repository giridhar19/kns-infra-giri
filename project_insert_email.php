<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 26-May-2017

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];



include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */

	$alert_type = -1;
	$alert = "";

	/* DATA INITIALIZATION - END */
	if(isset($_REQUEST['ddl_project_id']))
	{
		$project = $_REQUEST['ddl_project_id'];
	}
	else
	{
		$project = "";
	}
	if(isset($_REQUEST['email_to']))
	{
		$email_to = $_REQUEST['email_to'];
	}
	else
	{
		$email_to = "";
	}
	$email_cc = '';
 	foreach($_POST["framework"] as $row)
 	{
  	$email_cc .= $row . ', ';
 	}
  $email_cc = substr($email_cc, 0, -2);

	$email_bcc = '';
 	foreach($_POST["email_bcc"] as $row)
 	{
  	$email_bcc .= $row . ', ';
 	}
  $email_bcc = substr($email_bcc, 0, -2);
	$project_data = explode(',',$project);
	$project_id 	= $project_data[0];
	$project_name = $project_data[1];

	$project_email_search_data = array("project_id"=>$project_id);
	$email_list = db_get_project_email_list($project_email_search_data);
	if($email_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$project_email_list_update_data = array("project_id"=>$project_id,"project_name"=>$project_name,"to"=>$email_to,"cc"=>$email_cc,"bcc"=>$email_bcc);
		$project_email_uresults = db_update_project_email_list($project_id,$project_email_list_update_data);
		echo "Email Updated Successfully";
	}
	else {
		//insert
		// Check for mandatory fields
		if($project_id != "")
		{
			$project_email_iresult = db_add_project_email($project_id,$project_name,$email_to,$email_cc,$email_bcc);
			if($project_email_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				echo "Email Added Successfully";
			}
			else
			{
				$alert_type = 0;
			}
			$alert = $project_email_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}
else
{
	header("location:login.php");
}

?>
