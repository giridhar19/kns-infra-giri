<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */
    $measurment = $_POST['quantity'];
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $no_of_objects = $_POST['no_of_objects'];
    $total_days = $_POST['total_days'];
    $planning_id = $_POST["planning_id"];
    $resource_type = $_POST["resource_type"];
    $resource_name = $_POST["resource_name"];
    $project_id = $_POST["project_id"];
    $task_id = $_POST["task_id"];
    $road_id = $_POST["road_id"];
    $table_end="";
    $message="";
    $table_content="";
    $table_header = "<table  style=border-collapse:collapse;width:100%>
		<tr>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Process</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Task</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Road</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>UOM</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Old<br> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>New<br> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Diff  <br/> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Old <br/> End date</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>New <br/> End date</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Diff  <br/> Days</th>
		</tr>";

    //Get Email list
    $project_email_search_data = array("project_id"=>$project_id);
    $email_list = db_get_project_email_list($project_email_search_data);
    if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $email_list_data = $email_list["data"];
        $to = $email_list_data[0]["project_email_details_to"];
        $cc = $email_list_data[0]["project_email_details_cc"];
        $bcc = $email_list_data[0]["project_email_details_bcc"];
    }
    $project_task_planning_search_data = array("active"=>'1',"planning_id"=>$planning_id);
    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    // print_r($project_task_planning_list); exit;
    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];
        $old_measurment = $project_task_planning_list_data[0]["project_task_planning_measurment"];
        $old_end_date  = $project_task_planning_list_data[0]["project_task_planning_end_date"];
        $old_start_date  = $project_task_planning_list_data[0]["project_task_planning_start_date"];
        $project_name  = $project_task_planning_list_data[0]["project_master_name"];
        $process_name  = $project_task_planning_list_data[0]["project_process_master_name"];
        $task_name  = $project_task_planning_list_data[0]["project_task_master_name"];
        $road_name  = $project_task_planning_list_data[0]["project_site_location_mapping_master_name"];
        $uom  = $project_task_planning_list_data[0]["project_uom_name"];
    }
 else {
    $planning_id = "";
}
// Machine Type List
$project_machine_type_master_search_data = array("active"=>'1',"name"=>$resource_name);
$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
if ($project_machine_type_master_list["status"] == SUCCESS) {
    $project_machine_type_master_list_data = $project_machine_type_master_list["data"];
    $object_name_id = $project_machine_type_master_list_data[0]["project_machine_type_master_id"];
} else {
    // Get Project CW Master List
    $project_cw_master_search_data = array("active"=>'1',"name"=>$resource_name);
    $project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
    if ($project_cw_master_list['status'] == SUCCESS) {
        $project_cw_master_list_data = $project_cw_master_list["data"];
        $object_name_id = $project_cw_master_list_data[0]["project_cw_master_id"];
    }
}

$start_date_formatted = get_formatted_date($start_date, "Y-m-d");
$end_date_formatted = get_formatted_date($end_date, "Y-m-d");
$diff = abs($old_measurment - $measurment);
$date_diff = get_date_diff($old_end_date,$end_date_formatted);
$project_task_planning_update_data= array("measurment"=>$measurment,"no_of_object"=>$no_of_objects,"total_days"=>$total_days,"plan_start_date"=>$start_date,"plan_end_date"=>$end_date,"object_type"=>$resource_type,"machine_type"=>$resource_name);
$project_task_planning_iresult = i_update_project_task_planning($planning_id, '', $project_task_planning_update_data);
if (($project_task_planning_iresult["status"] == SUCCESS)) {
  if (($start_date != $old_start_date) || ($old_end_date!=$end_date)) {
    $task_plan_history_iresults = db_add_project_task_planning_shadow($planning_id,$project_id,$task_id,$road_id,$old_end_date, $end_date_formatted, $old_start_date, $start_date_formatted, $user);
  }
//Data Insertion
// if ($measurment != 0 && $total_days>=1 ) {
if (($measurment != $old_measurment) || ($old_end_date!=$end_date)) {
?>
<script>
  console.log('measurement ', <?= $measurment; ?>, <?= $old_measurment; ?>);
</script>
  <?php
        // Compose
        $table_content .= "<tr>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$process_name."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$task_name."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$road_name."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$uom."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$old_measurment."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".$measurment."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; color: red;'>".$diff."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".date("d-M-Y",strtotime($old_end_date))."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; '>".date("d-M-Y",strtotime($end_date_formatted))."</td>
        <td  style='border: 1px solid #ddd;padding: 8px; color:red;'>".abs($date_diff["data"])."</td>
        </tr>
";

          $subject = '[Notice] Project '.$project_name.' Measurment and End dates have been changed';
    }
}


$heading = "<h3>
        Project: <span style='color:#00ba8b;'>".$project_name ."</span>
        </h3>
        <h3>
        Changed By: <span style='color:#00ba8b;margin-right:30px;'>". $loggedin_name ."</span>

        Changed On: <span style='color:#00ba8b;'>". date('d-m-y h:m A') ."</span>
        </h3>
        <br/>";

$table_end .= "</table>";
$message .= $table_end;
$message = $heading;
$message .= $table_header;
$message .= $table_content;
$message .= $table_end;

$cc = explode(',',$cc);

$cc_string = '[';
for($count = 0; $count < count($cc); $count++)
{
$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
}
$cc_string = trim($cc_string,',');
$cc_string = $cc_string.']';

$bcc = explode(',',$bcc);
$bcc_string = '[';
for($count = 0; $count < count($bcc); $count++)
{
  $bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
}
$bcc_string = trim($bcc_string,',');
$bcc_string = $bcc_string.']';

$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
"personalizations": [
  {
    "to": [
      {
        "email": "'.$to.'"
      }
    ],
    "cc": '.$cc_string.',
    "bcc": '.$bcc_string.',
    "subject": "'.$subject.'"
  }
],
"from": {
  "email": "venkataramanaiah@knsgroup.in"
},
"content": [
  {
    "type": "text/html",
    "value": "'.$message.'"
  }
]
}'));

$apiKey = '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';
$sg = new \SendGrid($apiKey);


$response = $sg->client->mail()->send()->post($request_body);

    echo json_encode($project_task_planning_iresult["data"]); exit;
  }
   else {
    header("location:login.php");
}
