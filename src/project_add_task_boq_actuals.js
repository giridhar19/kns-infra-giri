function calculate_msrmnt(total_msrmnt, completed_msrmnt, rework) {

  var new_msrmnt = document.getElementById("total_sqft").value;
  document.getElementById("add_project_task_boq_actual_submit").disabled = false;
  var isRework = document.getElementById('rework').checked;

  // For selected work_type is rework
  if(rework >=0 && isRework) {
    total_msrmnt = completed_msrmnt;
    completed_msrmnt = rework;
  }

	if((completed_msrmnt + parseFloat(new_msrmnt)) > total_msrmnt ) {
		alert('Entered Msrmnt ('+ new_msrmnt + ') exceeds Total msrmnt ('+ total_msrmnt + ') and Completed (' + completed_msrmnt +')');
    document.getElementById("add_project_task_boq_actual_submit").disabled = true;
  }
}

function total_sqft_no()
{
	var length = parseFloat(document.getElementById('length').value);
	var breadth = parseFloat(document.getElementById('breadth').value);
	var depth = parseFloat(document.getElementById('depth').value);
	var number = parseFloat(document.getElementById('number').value);

	var total_sqft = (length * breadth * depth * number);

	document.getElementById("total_sqft").value = parseFloat(total_sqft).toFixed(2);

	var rate = document.getElementById("amount1").value;
	document.getElementById("amount").value = parseFloat(total_sqft * rate).toFixed(2);
}

function total_cost()
{

	var total_area = document.getElementById("total_sqft").value;
	var rate 	   = document.getElementById("amount1").value;

	document.getElementById("amount").value = parseFloat(total_area * rate).toFixed(2);
}

function get_contract_vendor_rate()
{
	var contract_task_id = document.getElementById("ddl_task").value;
	var contract_vendor_id = document.getElementById("ddl_venodr").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			alert(xmlhttp.responseText);
			var object = JSON.parse(xmlhttp.responseText);

			document.getElementById("amount").value = object.contract_rate;
			document.getElementById("uom").value = object.uom;
			document.getElementById("amount1").value = object.contract_rate;
			document.getElementById("unit").value = object.uom_id;
		}
	}

	xmlhttp.open("POST", "ajax/get_contract_vendor_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("contract_task_id=" + contract_task_id + "&contract_vendor_id=" + contract_vendor_id);
}
