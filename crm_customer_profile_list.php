<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/
/* DEFINES - START */define('CRM_CUSTOMER_PROFILE_LIST_FUNC_ID','94');define('CRM_CUSTOMER_PROFILE_DETAILS_FUNC_ID','313');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_LIST_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_LIST_FUNC_ID,'3','1');	$delete_perms_list = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_LIST_FUNC_ID,'4','1');
// Get permission settings for this user for this page	$add_perms_profile_list    = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_DETAILS_FUNC_ID,'1','1');		$view_perms_profile_list   = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_DETAILS_FUNC_ID,'2','1');	$edit_perms_profile_list   = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_DETAILS_FUNC_ID,'3','1');	$delete_perms_profile_list = i_get_user_perms($user,'',CRM_CUSTOMER_PROFILE_DETAILS_FUNC_ID,'4','1');

	// Query String / Get form Data
	// Nothing here
	
	if(isset($_POST["search_cust_profile_submit"]))
	{
		$project_id  = $_POST["ddl_project"];
		$site_status = $_POST["ddl_site_status"];
	}
	else
	{
		$project_id  = "-1";
		$site_status = "";
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('','','','','','','','','','',$site_status,$user);
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// Project List
	$site_status_list = i_get_site_status_list('','1');
	if($site_status_list["status"] == SUCCESS)
	{
		$site_status_list_data = $site_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_status_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Customer Profile List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Customer Profile List (Total: <?php if($cust_details["status"] == SUCCESS)
			  {
				echo count($cust_details_list); 
			  }
			  else
			  {
				echo "0";
			  }?>)</h3>
            </div>
			
			<div class="widget-header" style="height:80px; padding-top:10px;"> 
			<form method="post" id="pending_booked_list" action="crm_customer_profile_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_site_status">
			  <option value="">- - Select Status - -</option>
			  <?php
				for($count = 0; $count < count($site_status_list_data); $count++)
				{
					?>
					<option value="<?php echo $site_status_list_data[$count]["status_id"]; ?>" <?php 
					if($site_status == $site_status_list_data[$count]["status_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $site_status_list_data[$count]["status_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_cust_profile_submit" />
			  </span>
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Site No</th>
					<th>Site Dimension (Area)</th>
					<th>Status</th>
					<th>Booked By</th>
					<th>Booking Added On</th>
					<th>Name</th>
					<th>Contact No</th>
					<th>Email ID</th>
					<th>DOB</th>
					<th>Anniversary</th>
					<th>Company</th>
					<th>Designation</th>					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($cust_details["status"] == SUCCESS)
				{
					$sl_no = 0;					
					for($count = 0; $count < count($cust_details_list); $count++)
					{	
						if(($cust_details_list[$count]["crm_project_id"] == $project_id) || ($project_id ==""))
						{
						$sl_no++;
						?>
						<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["enquiry_number"]; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["project_name"]; ?>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_site_no"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_dimension_name"]." (".$cust_details_list[$count]["crm_site_area"]." sq. ft)"; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["status_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($cust_details_list[$count]["crm_booking_added_on"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_customer_name_one"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_customer_contact_no_one"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_customer_email_id_one"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($cust_details_list[$count]["crm_customer_dob_one"])); ?></td>
						<td style="word-wrap:break-word;"><?php if($cust_details_list[$count]["crm_customer_anniversary_one"] != "0000-00-00") 
						{
							echo date("d-M-Y",strtotime($cust_details_list[$count]["crm_customer_anniversary_one"])); 
						}
						else
						{
							echo "";
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_customer_company"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $cust_details_list[$count]["crm_customer_designation"]; ?></td>
						<td style="word-wrap:break-word;"><?php						if($view_perms_profile_list['status'] == SUCCESS)						{						?><a href="crm_view_customer_profile.php?profile=<?php echo $cust_details_list[$count]["crm_customer_details_id"]; ?>">View</a>						<?php						}						else						{							?>							<div class="form-actions">								You are not authorized to View							</div> <!-- /form-actions -->							<?php						}?>						</td>
						<td style="word-wrap:break-word;"><?php						if($edit_perms_list['status'] == SUCCESS)						{						?><a href="crm_edit_customer_profile.php?profile=<?php echo $cust_details_list[$count]["crm_customer_details_id"]; ?>">Edit</a>						<?php						}						else						{							?>							<div class="form-actions">								You are not authorized to View							</div> <!-- /form-actions -->							<?php						}?></td>
						</tr>
						<?php
						}
					}
				}
				else
				{
				?>
				<tr><td colspan="14">No customer profiles added yet!</td></tr>
				<?php
				}			
				?>					
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>