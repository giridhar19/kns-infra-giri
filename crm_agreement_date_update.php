<?php
$_SESSION['module'] = 'CRM Reports';


// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

//Get Booking date from Crm Booking List
$crm_booking_list = i_get_site_booking('','','','','','','','','','','','','','','','','','','','','','','');
if($crm_booking_list["status"] == SUCCESS)
{
	$crm_booking_list_data = $crm_booking_list["data"];
	for($booking_count = 0 ; $booking_count < count($crm_booking_list_data)  ; $booking_count++)
	{
		$booking_id = $crm_booking_list_data[$booking_count]["crm_booking_id"];
		$booking_date = $crm_booking_list_data[$booking_count]["crm_booking_date"];
		if($crm_booking_list_data[$booking_count]["crm_booking_consideration_area"] != "0")
		{
			$consideration_area = $crm_booking_list_data[$booking_count]["crm_booking_consideration_area"];
		}
		else
		{
			$consideration_area = $crm_booking_list_data[$booking_count]["crm_site_area"];
		}
		$total_payment_to_be_done = ($consideration_area * $crm_booking_list_data[$booking_count]["crm_booking_rate_per_sq_ft"]);
		
		//Get Pay Schedule data
		$pay_schedule_ag_list = i_get_pay_schedule($booking_id,'','','','AGREEMENT','','','');
		$agreement_date = date('Y-m-d',strtotime($booking_date.' +20 days'));
		$agreement_amt = ($total_payment_to_be_done * 0.4);
		if($pay_schedule_ag_list["status"] == SUCCESS)
		{
			$payment_schedule_data = array("date"=>$agreement_date,"amount"=>$agreement_amt);
			$payment_uresults = db_update_payment_scheduled_date($booking_id,'AGREEMENT',$payment_schedule_data);
		}
		else
		{
			if($booking_date != "0000-00-00")
			{
				$payment_irseluts = i_add_pay_schedule($booking_id,$agreement_date,$agreement_amt,'AGREEMENT','',"143620071466608200");
			}		
		}
		
		//Get Pay Schedule data
		$pay_schedule_reg_list = i_get_pay_schedule($booking_id,'','','','REGISTRATION','','','');
		$registration_date = date('Y-m-d',strtotime($booking_date.' +50 days'));
		$registartion_amt = ($total_payment_to_be_done * 0.5);
		if($pay_schedule_reg_list["status"] == SUCCESS)
		{
			$payment_schedule_data = array("date"=>$registration_date,"amount"=>$registartion_amt);
			$payment_uresults = db_update_payment_scheduled_date($booking_id,'REGISTRATION',$payment_schedule_data);
		}
		else
		{
			if($booking_date != "0000-00-00")
			{
				$payment_irseluts = i_add_pay_schedule($booking_id,$registration_date,$registartion_amt,'REGISTRATION','',"143620071466608200");
			}	
		}
		
		//Get Pay Schedule data
		$pay_schedule_reg_list = i_get_pay_schedule($booking_id,'','','','KHATHA TRANSFER','','','');
		$khatha_date = date('Y-m-d',strtotime($booking_date.' +120 days'));
		if($pay_schedule_reg_list["status"] == SUCCESS)
		{
			$payment_schedule_data = array("date"=>$khatha_date);
			$payment_uresults = db_update_payment_scheduled_date($booking_id,'KHATHA TRANSFER',$payment_schedule_data);
		}
		else
		{
			if($booking_date != "0000-00-00")
			{
				$payment_irseluts = i_add_pay_schedule($booking_id,$khatha_date,'','KHATHA TRANSFER','',"143620071466608200");
			}	
		}
	}
}
?>