<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_site_travel_plan_list.php
CREATED ON	: 12-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Site Travel Plans
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	if(isset($_GET["site_travel_actual_search_submit"]))
	{
		$travel_date = $_GET["dt_st_actual_date"];
		$enquiry_id  = $_GET["hd_enquiry"];
		$project     = $_GET["ddl_search_project"];				
	}
	else
	{
		$travel_date = "";
		if(isset($_GET["enquiry"]))
		{
			$enquiry_id  = $_GET["enquiry"];
		}
		else		
		{
			$enquiry_id = "";
		}
		$project     = "";
	}

	// Temp data
	$alert = "";

	if(($role == 1) || ($role == 5)|| ($enquiry_id != ""))
	{
		$added_by = "";
	}
	else
	{
		$added_by = $user;
	}
	
	// Get project list
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	$site_travel_actual_list = i_get_site_travel_actual_list('',$travel_date,$enquiry_id,$project,$added_by,'','');
	if($site_travel_actual_list["status"] == SUCCESS)
	{
		$site_travel_actual_list_data = $site_travel_actual_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$site_travel_actual_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Site Travel Actual List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Site Travel Actual List</h3>
            </div>
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="get" id="site_travel_search_form" action="crm_site_travel_actual_list.php">
			  <input type="hidden" name="hd_enquiry" value="" />
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($count = 0; $count < count($project_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_st_actual_date" value="<?php echo $travel_date; ?>" />
			  </span>	
			  <input type="submit" name="site_travel_actual_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>	
					<th>Enquiry No.</th>	
					<th>Project</th>					
					<th>Date</th>
					<th>Cab</th>
					<th>Opening Km</th>					
					<th>Closing Km</th>
					<th>Travel Distance (in Km)</th>
					<th>Driver</th>
					<th>No. Passengers</th>	
					<th>Remarks</th>
					<th>Entered By</th>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($site_travel_actual_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($site_travel_actual_list_data); $count++)
					{
						$sl_no++;						
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["enquiry_number"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["project_name"]; ?></td>					
					<td><?php echo date("d-M-Y",strtotime($site_travel_actual_list_data[$count]["crm_site_travel_actual_date"])); ?></td>	
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_vehicle_number"]; ?></td>					
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_opening_km"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_closing_km"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_closing_km"] - $site_travel_actual_list_data[$count]["crm_site_travel_actual_opening_km"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_driver"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_num_passengers"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["crm_site_travel_actual_remarks"]; ?></td>
					<td><?php echo $site_travel_actual_list_data[$count]["user_name"]; ?></td>					
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="7">No site travel plan!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>