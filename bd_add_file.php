<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 25th June 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */



/* TBD - START */

// 

/* TBD - END */
$_SESSION['module'] = 'BD';


/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');

/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert = "";

	$alert_type = -1; // No alert

	/* DATA INITIALIZATION - END */



	// Capture the form data

	if(isset($_POST["bd_add_file_submit"]))

	{

		$project_id       = $_POST["ddl_project"];

		$survey_no        = $_POST["stxt_survey_no"];
		
		$survey_alias     = $_POST["stxt_survey_alias"];
		
		$survey_alias_date = $_POST["stxt_survey_alias_date"];

		$owner            = $_POST["stxt_owner"];

		$owner_address    = $_POST["txt_owner_address"];

		$owner_phone      = $_POST["num_phone_no"];

		$village          = $_POST["ddl_village"];

		$extent           = $_POST["stxt_extent"];

		$owner_status     = $_POST["ddl_land_status"];

		$cost             = $_POST["stxt_cost"];

		$broker_name	  =	$_POST["stxt_broker_name"];

		$broker_address	  =	$_POST["txt_broker_address"];

		$process_status   = $_POST["ddl_process_status"];

		$jd_share_percent = $_POST["stxt_share_percent"];

		$own_account      = $_POST["ddl_own_account"];

		$remarks          = $_POST["txt_remarks"];

		$brokerage_amount = $_POST["num_brokerage_amt"];

		

		// Check for mandatory fields

		if(($project_id != "") && ($survey_no != "") && ($owner != "") && ($village != "") && ($extent != ""))

		{

			$add_bd_file_result = i_add_file_to_bd_project($project_id,$survey_no,'sales_deed_'.$survey_alias,$survey_alias_date,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$user,$brokerage_amount);

			

			if($add_bd_file_result["status"] == SUCCESS)

			{

				$alert = $add_bd_file_result["data"];

				$alert_type = 1;

			}

			else

			{

				$alert = $add_bd_file_result["data"];

				$alert_type = 0;

			}			

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}



	// Get list of BD projects

	$bd_project_list = i_get_bd_project_list('','','','','1');

	if($bd_project_list["status"] == SUCCESS)

	{

		$bd_project_list_data = $bd_project_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$bd_project_list["data"];

		$alert_type = 0; // Failure

	}

	

	// Get list of villages

	$village_list = i_get_village_list('');

	if($village_list["status"] == SUCCESS)

	{

		$village_list_data = $village_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$village_list["data"];

		$alert_type = 0; // Failure

	}

	

	// Get list of owner status

	$owner_status_list = i_get_owner_status_list('');

	if($owner_status_list["status"] == SUCCESS)

	{

		$owner_status_list_data = $owner_status_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$owner_status_list["data"];

		$alert_type = 0; // Failure

	}

	

	// Get list of process status

	$process_status_list = i_get_process_type_list('','');

	if($process_status_list["status"] == SUCCESS)

	{

		$process_status_list_data = $process_status_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$process_status_list["data"];

		$alert_type = 0; // Failure

	}

	

	// Get list of KNS accounts

	$own_account_list = i_get_account_list('','1');

	if($own_account_list["status"] == SUCCESS)

	{

		$own_account_list_data = $own_account_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$own_account_list["data"];

		$alert_type = 0; // Failure

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add BD File</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Add BD File</h3>

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add BD File</a>

						  </li>						  

						</ul>

						

						<br>

						    <div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="add_bd_file_form" class="form-horizontal" method="post" action="bd_add_file.php">

									<fieldset>										

															

										<div class="control-group">											

											<label class="control-label" for="ddl_project">Project *</label>

											<div class="controls">

												<select name="ddl_project" required>

												<?php

												for($count = 0; $count < count($bd_project_list_data); $count++)

												{

												?>

												<option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>"><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>								

												<?php

												}

												?>														

												</select>												

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_survey_no">Survey Number *</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_survey_no" placeholder="Dont use spaces. Use only / symbol" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										

										<div class="control-group">											

											<label class="control-label" for="stxt_survey_alias">Survey Alias *</label>

											<div class="controls">

												<input type="number" class="span6" name="stxt_survey_alias" placeholder="Sales Deed :" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										
										<div class="control-group">											

											<label class="control-label" for="stxt_survey_alias_date">Survey Alias Date *</label>

											<div class="controls">

												<input type="date" class="span6" name="stxt_survey_alias_date" required="required">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										

										<div class="control-group">											

											<label class="control-label" for="stxt_owner">Land Owner *</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_owner" required="required">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_broker_address">Owner Address</label>

											<div class="controls">

												<textarea name="txt_owner_address"></textarea>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_phone_no">Land Owner Phone No *</label>

											<div class="controls">

												<input type="number" class="span6" name="num_phone_no" required="required" placeholder="10 digit mobile number / Landline number with STD code">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_village">Village *</label>

											<div class="controls">

												<select name="ddl_village" required>

												<?php

												for($count = 0; $count < count($village_list_data); $count++)

												{

												?>

												<option value="<?php echo $village_list_data[$count]["village_id"]; ?>"><?php echo $village_list_data[$count]["village_name"]; ?></option>								

												<?php

												}

												?>														

												</select>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_extent">Extent *</label>

											<div class="controls">

												<input type="number" class="span6" name="stxt_extent" required="required" min="0" step="0.01">

												<p class="help-block">Enter in guntas</p>

											</div> <!-- /controls -->												

										</div> <!-- /control-group -->																				

										

										<div class="control-group">											

											<label class="control-label" for="ddl_owner_status">Land Status *</label>

											<div class="controls">

												<select name="ddl_land_status" required>

												<?php

												for($count = 0; $count < count($owner_status_list_data); $count++)

												{

												?>

												<option value="<?php echo $owner_status_list_data[$count]["bd_file_owner_status_id"]; ?>"><?php echo $owner_status_list_data[$count]["bd_file_owner_status_name"]; ?></option>								

												<?php

												}

												?>														

												</select>												

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_cost">Cost</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_cost" value="0" >

												<p class="help-block">Full land cost. Not per acre cost.<br /> <span style="color:red;">To be finalized by finance team</span></p>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_broker_name">Broker Name *</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_broker_name" required="required">

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_broker_address">Broker Address *</label>

											<div class="controls">

												<textarea name="txt_broker_address"></textarea>

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->



										<div class="control-group">											

											<label class="control-label" for="num_brokerage_amt">Brokerage Amount</label>

											<div class="controls">

												<input type="number" class="span6" name="num_brokerage_amt" min="0" step="0.01">

											</div> <!-- /controls -->												

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_process_status">Process Status *</label>

											<div class="controls">

												<select name="ddl_process_status" required>

												<?php

												for($count = 0; $count < count($process_status_list_data); $count++)

												{

												?>

												<option value="<?php echo $process_status_list_data[$count]["process_master_id"]; ?>"><?php echo $process_status_list_data[$count]["process_name"]; ?></option>								

												<?php

												}

												?>														

												</select>												

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="ddl_own_account">KNS Account</label>

											<div class="controls">

												<select name="ddl_own_account" required>

												<option value=""> - - Select Account - -</option>

												<?php

												for($count = 0; $count < count($own_account_list_data); $count++)

												{

												?>

												<option value="<?php echo $own_account_list_data[$count]["bd_own_accunt_master_id"]; ?>"><?php echo $own_account_list_data[$count]["bd_own_account_master_account_name"]; ?></option>								

												<?php

												}

												?>														

												</select>												

											</div> <!-- /controls -->				

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="stxt_share_percent">JD %</label>

											<div class="controls">

												<input type="number" class="span6" name="stxt_share_percent" min="0" step="0.01">%	

												<p class="help-block">DONT include % symbol. Only mention the number.<br /> <span style="color:red;">To be finalized by finance team</span></p>

											</div> <!-- /controls -->												

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<textarea name="txt_remarks"></textarea>

											</div> <!-- /controls -->												

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />

										

											

										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="bd_add_file_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>																

								

							</div>

						  

						  

						</div>

						

						

						

						

						

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      	

	      	

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>




  </body>



</html>