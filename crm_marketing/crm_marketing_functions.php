<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_marketing.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_config.php');

/*
PURPOSE : To add Marketing expenses
INPUT 	: Source,Date,Amount,Leads,Contact Person,Contact No,Email,Remarks,Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_marketing_expenses($marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$contact_person,$contact_no,$contact_email,$remarks,$added_by)
{
	// Check if there is an approved requestalready for this source for these dates
	$marketing_expenses_list = db_get_marketing_expenses_list('',$marketing_source,$applicable_start_date,$applicable_end_date,'','','1','','','');
	
	if($marketing_expenses_list['status'] == DB_NO_RECORD)
	{
		$marketing_iresult = db_add_marketing_expenses($marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$contact_person,$contact_no,$contact_email,$remarks,$added_by);
			
		if($marketing_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Marketing Expenses Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "There seems to be an approved expense for this source during this duration. Please cross check";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get Marketing Expenses
INPUT 	: Expense ID, Source, Applicable Start Date, Applicable End Date, Amount, No. of leads, Status, Added By
OUTPUT 	: Bank List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_marketing_expenses_list($expense_id,$marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$status,$added_by,$start_date,$end_date)
{
	$marketing_sresult = db_get_marketing_expenses_list($expense_id,$marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$status,$added_by,$start_date,$end_date);
	
	if($marketing_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $marketing_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Marketing Expenses added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Marketing expenses payment details
INPUT 	: Mode,Instrument Details,Instrument Date,Paid To,Amount,Expense ID,Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_marketing_expenses_payment($mode,$instrument_details,$date,$paid_to,$amount,$expenses_id,$added_by)
{
	// Initialization
	$paid_amount    = 0;
	$payable_amount = 0;
	
	/* To Check if amount is exceeding planned amount - Start */
	// Get done payments
	$marketing_payment_sresult = db_get_marketing_payment_list('','','','','','1',$expenses_id,'','','');
	if($marketing_payment_sresult['status'] != DB_RECORD_ALREADY_EXISTS)
	{
		$paid_amount = 0;
	}
	else
	{
		for($count = 0; $count < count($marketing_payment_sresult['data']); $count++)
		{
			$paid_amount = $paid_amount + $marketing_payment_sresult['data'][$count]['crm_marketing_expenses_payment_details_amount'];
		}
	}
	
	// Get payable
	$marketing_expense_sresult = i_get_marketing_expenses_list($expenses_id,'','','','','','','');
	if($marketing_expense_sresult['status'] == SUCCESS)
	{
		$payable_amount = $marketing_expense_sresult['data'][0]['crm_marketing_expenses_amount'];
	}
	else
	{
		$payable_amount = 0;
	}	
	
	/* To Check if amount is exceeding planned amount - Start */
		
	if($payable_amount >= ($paid_amount + $amount))
	{
		$marketing_payment_iresult = db_add_marketing_expenses_payment($mode,$instrument_details,$date,$paid_to,$amount,$expenses_id,$added_by);
		
		if($marketing_payment_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Marketing Expenses Payment Details Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Payment exceeds the planned amount!";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get Marketing Expenses Payment Details
INPUT 	: Expenses_id,Mode,Status,Date,
OUTPUT 	: Bank List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_marketing_payment_list($mode,$instrument_details,$date,$paid_to,$amount,$status,$expenses_id,$added_by)
{
	$marketing_payment_sresult = db_get_marketing_payment_list($mode,$instrument_details,$date,$paid_to,$amount,$status,$expenses_id,$added_by,'','');
	
	if($marketing_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $marketing_payment_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Marketing Expenses Payment Details added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update the status of Marketing Expenses
INPUT 	: Marketing Expenses ID, Status, Updated By
OUTPUT 	: Success or Failure Message
BY 		: Sonakshi D
*/
function i_approve_reject_marketing_expenses($marketing_expenses_id,$status,$updated_by)
{
	$allow_update = true;
	if($status == '1')
	{
		$marketing_expense_sresult = db_get_marketing_expenses_list($marketing_expenses_id,'','','','','','','','','');
		
		// Check if there is no conflict before approving		
		$marketing_expenses_list = db_get_marketing_expenses_list('',$marketing_expense_sresult['data'][0]['crm_marketing_expenses_source'],$marketing_expense_sresult['data'][0]['crm_marketing_expenses_applicable_start_date'],$marketing_expense_sresult['data'][0]['crm_marketing_expenses_applicable_end_date'],'','','1','','','');
		
		if($marketing_expenses_list['status'] == DB_RECORD_ALREADY_EXISTS)
		{
			$allow_update = false;
		}
	}
	
	if($allow_update == true)
	{
		$marketing_expenses_details = array("status"=>$status,"updated_by"=>$updated_by);
		$marketing_expenses_uresult = db_change_marketing_expenses($marketing_expenses_id,$marketing_expenses_details);
		if($marketing_expenses_uresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = "Status updated successfully";
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an internal error. Please try again later!";
		}
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There seems to be an approved marketing plan for this source during these dates. Please check";
	}
	
	return $return;
}

/*
PURPOSE : To update Marketing Expenses Details
INPUT 	: Marketing Expenses ID, Marketing Expenses Data Array
OUTPUT 	: Success or Failure Message
BY 		: Nitin Kashyap
*/
function i_update_marketing_expenses($marketing_expenses_id,$marketing_expenses_data)
{	
	$marketing_expenses_uresult = db_change_marketing_expenses($marketing_expenses_id,$marketing_expenses_data);
	if($marketing_expenses_uresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = "Marketing Expenses Data updated successfully";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There was an internal error. Please try again later!";
	}
	
	return $return;
}
?>