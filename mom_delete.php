<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: general_task_delete.php
CREATED ON	: 08-November-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Delete a general task
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["mom"]))
	{
		$mom_id = $_GET["mom"];
	}
	else
	{
		$mom_id = "";
	}
	
	if(isset($_GET["meeting"]))
	{
		$meeting = $_GET["meeting"];
	}
	else
	{
		$meeting = "";
	}

	// Check whether this user is authorized to do this change
	$meeting_data = array("meeting_id"=>$meeting,"user"=>$user);
	$meeting_list = i_get_meetings($meeting_data);
	if($meeting_list["status"] == SUCCESS)
	{
		// Delete the MOM item
		$mom_data = array("status"=>'2');
		$mom_delete_result = i_update_meeting_mom($mom_id,$mom_data);
		
		// Get MOM details
		$mom_data = array("mom_id"=>$mom_id);		
		$mom_details = i_get_meeting_mom($mom_data);
		$task_plan_id = $mom_details["data"][0]["mom_task_id"];
		
		if($mom_delete_result["status"] == SUCCESS)
		{
			$msg = "MOM Item was successfully deleted";
			
			if($task_plan_id != "")
			{				
				$task_dresult = i_delete_gen_task_plan($task_plan_id);
				if($task_dresult["status"] == SUCCESS)
				{
					$msg = "MOM item successfully deleted!";
				}
				else
				{
					$msg = "MOM item successfully deleted, but task was not deleted!";
				}
			}
		}
		else
		{
			$msg = "There was an internal error in deleting the task. Please try after some time";
		}
	}
	else
	{
		$msg = "You are not authorized to delete this MOM item!";
	}

	header("location:meeting_details.php?msg=$msg&meeting=".$meeting);
}
else
{
	header("location:login.php");
}	
?>