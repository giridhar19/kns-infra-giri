<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_indent_list.php
CREATED ON	: 28-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of indent for customer withdrawals
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
/*include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_masters_functions.php');*/
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";

	$project			 = "";
	// Query String Data
	// Query String Data
	if(isset($_GET["issue_id"]))
	{
		$issue_id = $_GET["issue_id"];
	}	
	else
	{
		$issue_id = "";
	}
	if(isset($_GET["indent_id"]))
	{
		$indent_id = $_GET["indent_id"];
	}	
	else
	{
		$indent_id = "";
	}
	// Nothing
	//Temp Data
	
	// Temp data
	$stock_issue_item_search_data = array("active"=>'1',"issue_id"=>$issue_id);
	$issue_item_list = i_get_stock_issue_item($stock_issue_item_search_data);
	if($issue_item_list["status"] == SUCCESS)
	{
		$issue_item_list_data = $issue_item_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$issue_item_list["data"];
	}
	
	//Get Project List
	$stock_project_search_data = array();
	$project_list = i_get_project_list($stock_project_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $project_list["data"];
		$alert_type = 0;
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Indent List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Issue List</h3><span style="float:right; padding-right:20px;"><strong><a href="#" onclick="return go_to_issue_print('<?php echo $issue_id; ?>',<?php echo $indent_id; ?>);">Print</a></strong></span>
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Issue No</th>
					<th>Issue Date</th>
					<th>Indent No</th>
					<th>Indent Date</th>
					<th>Indent By</th>
					<th>Indent Qty</th>
					<th>Issued Item</th>
					<th>Issued Code</th>
					<th>Issued Qty</th>								
					<th>Item Rate</th>								
					<th>Value</th>								
					<th>Project</th>								
					<th>Issued By</th>								
				</tr>
				</thead>
				<tbody>							
				<?php
				if($issue_item_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($issue_item_list_data); $count++)
					{
						$sl_no++;
						$issued_qty = $issue_item_list_data[$count]["stock_issue_item_qty"];
						$price 		= $issue_item_list_data[$count]["stock_material_price"];
						$value = $issued_qty * $price ;
						// Get Indent Item Details
						$stock_indent_search_data = array("indent_id"=>$issue_item_list_data[$count]["stock_issue_indent_id"],"active"=>'1',"material_id"=>$issue_item_list_data[$count]["stock_issue_item_material_id"]);
						$indent_item_list = i_get_indent_items_list($stock_indent_search_data);
						if($indent_item_list["status"] == SUCCESS)
						{
							$indent_item_list_data = $indent_item_list["data"];
							$indent_qty = $indent_item_list_data[0]["stock_indent_item_quantity"];
						}
						else
						{
							$alert = $alert."Alert: ".$indent_item_list["data"];
							$indent_qty = "";
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $issue_item_list_data[$count]["stock_issue_no"]; ?></td>
					<td><?php echo date('d-M-Y',strtotime($issue_item_list_data[$count]["stock_issue_item_issued_on"])); ?></td>
					<td><?php echo $issue_item_list_data[$count]["stock_indent_no"]; ?></td>
					<td><?php echo date('d-M-Y',strtotime($issue_item_list_data[$count]["stock_indent_added_on"])); ?></td>
					<td><?php echo $issue_item_list_data[$count]["user_name"]; ?></td>
					<td><?php echo $indent_qty ; ?></td>
					<td><?php echo $issue_item_list_data[$count]["stock_material_name"]; ?></td>
					<td><?php echo $issue_item_list_data[$count]["stock_material_code"]; ?></td>
					<td><?php echo $issued_qty ; ?></td>
					<td><?php echo $price ; ?></td>
					<td><?php echo $value ; ?></td>
					<td><?php echo $issue_item_list_data[$count]["stock_project_name"] ; ?></td>
					<td><?php echo $issue_item_list_data[$count]["user_name"]; ?></td>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="6">No indent added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_issue_item(indent_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "stock_indent_items_issue.php");
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","indent_id");
	hiddenField2.setAttribute("value",indent_id);
	
	form.appendChild(hiddenField2);	
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_issue_print(issue_id,indent_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "stock_issue_slip.php");
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","indent_id");
	hiddenField2.setAttribute("value",indent_id);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","issue_id");
	hiddenField3.setAttribute("value",issue_id);
	
	form.appendChild(hiddenField2);	
	form.appendChild(hiddenField3);	
	
	document.body.appendChild(form);
    form.submit();
}
function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results').style.display = 'none';

}

</script>

  </body>

</html>