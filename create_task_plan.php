<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 11th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/*
TBD: 
1. Insertion of other task type
2. Session management
3. Calendar Object
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	$process = $_GET['process'];

	// Temp data
	$alert = "";

	// Get list of task types
	$task_type_list = i_get_task_type_list('',$process,1); // Get task types
	if($task_type_list["status"] == SUCCESS)
	{
		$task_type_list_data = $task_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$task_type_list["data"];
	}

	if(isset($_POST["create_task_plan_submit"]))
	{
		// Capture all form data
		$task_type          = $_POST["task_type"];
		$other_task_type    = $_POST["other_task_type"];	
		$planned_end_date   = $_POST["planned_end_date"];
		$start_date         = $_POST["start_date"];
		$process_plan_id    = $_POST["process_plan"];	
		
		if(($task_type != "") && ($planned_end_date != ""))
		{
			if($task_type == 0) // if task type is other
			{
				$task_type = $other_task_type;
				$task_type = 0; // to be removed
			}
			
			$task_plan_legal_result = i_add_task_plan_legal($task_type,$process_plan_id,$planned_end_date,$start_date,$user);
			
			if($task_plan_legal_result["status"] != SUCCESS)
			{
				$disp_msg = $task_plan_legal_result["data"];
				$disp_class = "";
			}
			else
			{
				$disp_msg = $task_plan_legal_result["data"];
				$disp_class = "";
			}
		}
		else	
		{
			$disp_msg = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$disp_class = "";
		}
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Create Task Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	
  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Create Process Plan</a>
						  </li>						  
						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="create_task_plan" class="form-horizontal" method="post" action="create_task_plan.php" enctype="multipart/form-data">
									<fieldset>
										<input type="hidden" name="process_plan" value="<?php echo $process; ?>" />
										<div class="control-group">											
											<label class="control-label" for="task_type">Task Type</label>
											<div class="controls">
												<select name="task_type" id="task_type" onchange="enable_others_text_entry()">	
												<option value="">- - Select Task Type - -</option>
												<?php 
												for($count = 0; $count < count($task_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $task_type_list_data[$count]["task_type_id"]; ?>"><?php echo $task_type_list_data[$count]["task_type_name"]; ?></option>												
												<?php
												}
												?>
												<option value="0">Other</option>
												</select>
												<p class="help-block">Choose Other if the task falls under none of the other categories</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="other_task_type">Other Task Type</label>
											<div class="controls">
												<input type="text" class="span6" name="other_task_type" id="other_task_type" disabled="disabled">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="planned_end_date">Planned End Date</label>
											<div class="controls">
												<input type="text" class="span6" name="planned_end_date" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="start_date">Start Date</label>
											<div class="controls">
												<input type="text" class="span6" name="start_date" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="create_task_plan_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
	function enable_others_text_entry()
	{
		var task_type = document.getElementById("task_type").value;
		
		if(task_type == 0)
		{
			alert("test");
			document.getElementById("other_task_type").removeAttribute("disabled");
			document.getElementById("other_task_type").required = true;
		}	
	}
	</script>

  </body>

</html>
