<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 04-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_REQUEST["delay_id"]))
	{
		$delay_id = $_REQUEST["delay_id"];
	}
	else
	{
		$delay_id = "";
	}
	
	if(isset($_REQUEST["process_id"]))
	{
		$process_id = $_REQUEST["process_id"];
	}
	else
	{
		$process_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_survey_process_delay_submit"]))
	{
		$delay_id             = $_POST["hd_delay_id"];
		$reason_id            = $_POST["ddl_reason_id"];
		$process_id           = $_POST["hd_process_id"];
		$remarks              = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($reason_id != ""))
		{
			$survey_process_delay_update_data = array("reason_id"=>$reason_id,"remarks"=>$remarks);
			$survey_process_delay_iresult = i_update_survey_process_delay($delay_id,$survey_process_delay_update_data);
			
			if($survey_process_delay_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
		    
				header("location:survey_add_process_delay.php?process_id=$process_id");
			}
			else
			{
				$alert 		= $survey_process_delay_iresult["data"];
				$alert_type = 0;	
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
		
	 // Get Delay Reason Master modes already added
	$survey_delay_reason_master_search_data = array("active"=>'1');
	$survey_delay_reason_master_list = i_get_survey_delay_reason_master($survey_delay_reason_master_search_data);
	if($survey_delay_reason_master_list["status"] == SUCCESS)
	{
		$survey_delay_reason_master_list_data = $survey_delay_reason_master_list["data"];
	}
	else
	{
		$alert = $survey_delay_reason_master_list["data"];
		$alert_type = 0;
	}
	
	 // Get Survey Process modes already added
	$survey_process_search_data = array("active"=>'1');
	$survey_process_list = i_get_survey_process($survey_process_search_data);
	if($survey_process_list['status'] == SUCCESS)
	{
		$survey_process_list_data = $survey_process_list['data'];
	}
	else
	{
		$alert = $survey_process_list["data"];
		$alert_type = 0;
	}
	
	// Get Survey Process  Delay modes already added
	$survey_process_delay_search_data = array("active"=>'1',"delay_id"=>$delay_id,"process_id"=>$process_id);
	$survey_process_delay_list = i_get_survey_process_delay($survey_process_delay_search_data);
	if($survey_process_delay_list['status'] == SUCCESS)
	{
		$survey_process_delay_list_data = $survey_process_delay_list['data'];
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Edit Process Delay</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Survey - Edit Process Delay</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Survey Edit Process Delay</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_edit_process_delay_form" class="form-horizontal" method="post" action="survey_edit_process_delay.php">
								<input type="hidden" name="hd_delay_id" value="<?php echo $delay_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
									<fieldset>										
																
											 <div class="control-group">											
											<label class="control-label" for="ddl_reason_id">Delay reason*</label>
											<div class="controls">
												<select class="span6" name="ddl_reason_id" required>
												<option value="">- - Select Reason - -</option>
												<?php
												for($count = 0; $count < count($survey_delay_reason_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_delay_reason_master_list_data[$count]["survey_delay_reason_master_id"]; ?>" <?php if($survey_delay_reason_master_list_data[$count]["survey_delay_reason_master_id"] == $survey_process_delay_list_data[0]["survey_process_delay_reason_id"]){ ?> selected="selected" <?php } ?>><?php echo $survey_delay_reason_master_list_data[$count]["survey_delay_reason_master_name"]; ?></option>
												<?php
												}
												?>	
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $survey_process_delay_list_data[0]["survey_process_delay_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_survey_process_delay_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
