<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Query String
	if(isset($_REQUEST["enquiry"]))
	{
		$enquiry_id = $_REQUEST["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}

	// Capture the form data
	if(isset($_POST["add_site_visit_plan_submit"]))
	{
		$enquiry_id      = $_POST["hd_enquiry"];
		$site_visit_date = $_POST["dt_site_visit"];
		$site_visit_time = $_POST["stxt_visit_time"];
		$location        = $_POST["stxt_pickup_location"];
		$site_project    = $_POST["ddl_project"];
		$confirm_status  = $_POST["rd_confirm_status"];
		$drive_type      = $_POST["rd_drive_type"];

		// Check for mandatory fields
		if(($enquiry_id !="") && ($site_visit_date !="") && ($site_project !="") && ($site_visit_time != "") && ($location !=""))
		{
			$date_valid_array = get_date_diff($site_visit_date,date("Y-m-d"));

			if($date_valid_array["status"] == 0)
			{
				$site_visit_plan_iresult = i_add_site_visit_plan($enquiry_id,$site_visit_date,$site_visit_time,$site_project,$confirm_status,$drive_type,$location,$user);
				// print_r($site_visit_plan_iresult); exit;
				if($site_visit_plan_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
					if(($confirm_status == "1") && ($drive_type == "1"))
					{
						$site_travel_iresult = i_add_site_travel_plan($site_visit_plan_iresult["data"],'1',$site_project,$site_visit_date,$user);
						if($site_travel_iresult["status"] == SUCCESS)
						{
							$svp_uresult = i_update_site_visit_plan($enquiry_id,'');
							if($svp_uresult["status"] == SUCCESS)
							{
								$alert="";
							}
							else
							{
								$alert_type = 0;
								$alert      = "Travel Plan added successfully, but with minor issues. Please contact the admin!";
							}
						}
						else
						{
							$alert_type = 0;
							$alert      = "Tentative Site Visit Plan added successfully, but travel plan not added. Please contact the admin!";
						}
					}
				}
				else
				{
					$alert_type = 0;
					// $alert      = $site_visit_plan_iresult["data"];
				}
			}
			else
			{
				$alert = "Tentative Site Visit Date cannot be before today";
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
		echo $alert; exit;
	}

	// Project
	$project_list = i_get_project_list('','1');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}

	// Enquiry List
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}
?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Add Site Visit Plan</h4>
								</div>
									<div class="widget-header" style="height:auto;">
										<div style="border-bottom: 1px solid #C0C0C0;">
											<span class="header-label">Enquiry Number: </span><span><?php echo $enquiry_list_data[0]["enquiry_number"]; ?></span>
												</div>
												<div style="border-bottom: 1px solid #C0C0C0;">
											<span class="header-label">Source: </span><span><?php echo $enquiry_list_data[0]["enquiry_source_master_name"]; ?></span>
										</div>

										<div style="border-bottom: 1px solid #FFFFFF;">
											<span class="header-label">Customer Name: </span><span><?php echo $enquiry_list_data[0]["name"]; ?></span>
										</div>
										<div style="border-bottom: 1px solid #FFFFFF;">
											<span class="header-label">Customer Mobile: </span><span><?php echo $enquiry_list_data[0]["cell"]; ?></span>
										</div>
									</div>
								<div class="modal-body container-fluid" >
									<form id="add_site_visit_plan" class="form-group">
									<input type="hidden" id="hd_enquiry" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
											<div class="row">
											<div class="form-group col-md-4">
												<label class="control-label" for="ddl_project">Project*</label>
													<select id="ddl_project" name="ddl_project" class="form-control">
													<option value="">- - Select a project - -</option>
													<?php
													foreach ($project_list_data as $item) {?>
													<option value="<?php echo  $item["project_id"]; ?>"><?php echo $item["project_name"]; ?></option>
													<?php
													}
													?>
													</select>
											</div>
											<div class="form-group col-md-4 ml-auto">
												<label class="control-label" for="dt_site_visit">Date*</label>
												<div class="form-group">
													<input class="form-control" type="date" id="dt_site_visit" name="dt_site_visit" required="required">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
												<label class="control-label" for="stxt_visit_time">Time*</label>
												<div class="form-group">
													<input class="form-control" type="text" id="stxt_visit_time" name="dt_site_visit" required>
												</div>
											</div>
											<div class="form-group col-md-4 ml-auto">
												<label class="control-label" for="stxt_pickup_location">Pickup Location*</label>
												<div class="form-group">
													<input class="form-control" type="text" id="stxt_pickup_location" name="stxt_pickup_location" required>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label class="control-label" for="rd_confirm_status">Confirmation Status*</label>
												<div class="form-group">
													<input type="radio" id="rd_confirm_status" name="rd_confirm_status" value="0" checked>&nbsp;&nbsp;&nbsp;Tentative
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" id="rd_confirm_status" name="rd_confirm_status" value="1">&nbsp;&nbsp;&nbsp;Confirm
												</div>
											</div>

											<div class="form-group col-md-6 ml-auto">
												<label class="control-label" for="rd_drive_type">Drive Type*</label>
												<div class="form-group">
													<input type="radio" id="rd_drive_type" name="rd_drive_type" value="0" checked>&nbsp;&nbsp;&nbsp;KNS
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" id="rd_drive_type" name="rd_drive_type" value="1">&nbsp;&nbsp;&nbsp;Self-Drive
												</div>
											</div>
										</div>
										</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" onclick="submitPlan()">Submit</button>
							</div>
