<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 09-Sept-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_RESPONSE_LIST_FUNC_ID','336');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',APF_RESPONSE_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_RESPONSE_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_RESPONSE_LIST_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',APF_RESPONSE_LIST_FUNC_ID,'1','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['query_id']))
	{
		$query_id = $_GET['query_id'];
	}
	else
	{
		$query_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["add_response_submit"]))
	{
		
		$query_id         = $_POST["hd_query_id"];
		$response         = $_POST["txt_response"];
		$follow_up_date   = $_POST["follow_up_date"];
		$status           = $_POST["status"];
		$remarks          = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($response != "") && ($follow_up_date != ""))
		{
			$apf_response_iresult = i_add_apf_response($query_id,$response,$follow_up_date,$status,$remarks,$user);
			
			if($apf_response_iresult["status"] == SUCCESS)				
			{	
				"APF Query Successfully Added";
			}
			
			$alert = $apf_response_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}	
	
	// Get Response modes already added
	$apf_response_search_data = array("active"=>'1',"query_id"=>$query_id);
	$apf_response_list = i_get_apf_response($apf_response_search_data);
	if($apf_response_list['status'] == SUCCESS)
	{
		$apf_response_list_data = $apf_response_list['data'];
	}	
}
else
{
	header("location:login.php");
}	

?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>APF - Add Response</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>APF - Add Response</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">APF - Add Response</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="apf_add_response_form" class="form-horizontal" method="post" action="apf_add_response.php">
								<input type="hidden" name="hd_query_id" value="<?php echo $query_id; ?>" />
									<fieldset>
									
									<div class="control-group">											
											<label class="control-label" for="txt_response">Response*</label>
											<div class="controls">
												<textarea rows="4" cols="50" class="span6" name="txt_response" placeholder="Response" required="required"></textarea>
											</div> <!-- /controls -->	
										</div> <!-- /control-group -->

									
										 <div class="control-group">											
											<label class="control-label" for="follow_up_date">Follow Up Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="follow_up_date" placeholder="Date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="status">Status</label>
											<div class="controls">
												<input type="radio" name="status" value="Completed" />&nbsp;&nbsp;&nbsp;Completed&nbsp;&nbsp;&nbsp;
												<input type="radio" name="status" checked value="Not Completed" />&nbsp;&nbsp;&nbsp;&nbsp;Not Completed&nbsp;&nbsp;
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
										<?php if($add_perms_list["status"] == SUCCESS) {?>
											<input type="submit" class="btn btn-primary" name="add_response_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php } ?>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							<?php 
							if($view_perms_list["status"] == SUCCESS)
							{
							?>
							
							<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Response</th>
					<th>Follow Up Date</th>
					<th>Status</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($apf_response_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_response_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $apf_response_list_data[$count]["apf_response"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_response_list_data[$count][
					"apf_response_follow_up_date"])); ?></td>
					<td><?php echo $apf_response_list_data[$count]["apf_response_status"]; ?></td>
					<td><?php echo $apf_response_list_data[$count]["apf_response_remarks"]; ?></td>
					<td><?php echo $apf_response_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_response_list_data[$count][
					"apf_response_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_apf_edit_response('<?php echo $apf_response_list_data[$count]["apf_response_id"]; ?>','<?php echo $apf_response_list_data[$count]["apf_response_query_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($apf_response_list_data[$count]["apf_response_active"] == "1") || ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return apf_delete_response('<?php echo $apf_response_list_data[$count]["apf_response_id"]; ?>','<?php echo $apf_response_list_data[$count]["apf_response_query_id"]; ?>');">Delete</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No APF Response condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  
			<?php
			}
			?>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
function apf_delete_response(response_id,query_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "apd_add_response.php?query_id=" +query_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/apf_delete_response.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("response_id=" + response_id + "&action=0");
		}
	}	
}
function go_to_apf_edit_response(response_id,query_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "Get");
    form.setAttribute("action", "apf_edit_response.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","response_id");
	hiddenField1.setAttribute("value",response_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","query_id");
	hiddenField2.setAttribute("value",query_id);
	
	form.appendChild(hiddenField1);
	
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>


  </body>

</html>
