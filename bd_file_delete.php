<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: bd_file_delete.php
CREATED ON	: 15-March-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Delete a BD file
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["file"]))
	{
		$file = $_GET["file"];
	}
	else
	{
		$file = "";
	}

	// Check whether this user is authorized to do this change
	$file_list = i_get_bd_files_list($file,'','','','','','','');
	if($file_list["status"] == SUCCESS)
	{
		$file_status = $file_list["data"][0]["bd_project_file_mapping_active"];
		if($role == "1")
		{
			// Delete the file
			$file_delete_result = i_delete_bd_file($file,$user);
			
			if($file_delete_result["status"] == SUCCESS)
			{
				$msg = "File was successfully deleted";
			}
			else
			{
				$msg = $file_delete_result["data"];
			}
		}
		else
		{
			$msg = "You are not authorized to delete this file!";
		}
	}
	else
	{
		$msg = "This seems to be an invalid file. Please check!";
		$file_status = "";
	}

	if($file_status == "0")
	{
		header("location:bd_orphan_file_list.php?msg=$msg");
	}
	else	
	{
		header("location:bd_file_list.php?msg=$msg");
	}
}
else
{
	header("location:login.php");
}	
?>