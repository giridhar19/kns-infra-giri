<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';
/* DEFINES - START */
define('INDENT_FUNC_ID', '165');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', INDENT_FUNC_ID, '2', '1');
		$edit_perms_list   	= i_get_user_perms($user, '', INDENT_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', INDENT_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', INDENT_FUNC_ID, '1', '1');
    ?>

    <script>
      window.permissions = {
          view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

      <?php
    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
    }

    if (isset($_GET["search_status"])) {
      $search_status  = $_GET["search_status"];
    }
    else {
      $search_status = '';
    }

    // Get Project Management Master modes already added
		$stock_project_search_data = array('active'=>'1');
		$project_list = i_get_project_list($stock_project_search_data);
		if($project_list["status"] == SUCCESS)
		{
			$project_list_data = $project_list["data"];
		}
} else {
    header("location:login.php");
}
?>

    <html>

    <head>
      <meta charset="utf-8">
      <title>List of Indent Items for Approval</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
      <script src="datatable/stock_indent_item_for_approval.js?<?php echo time();?>"></script>
      <link href="./css/style.css?<?php echo time();?>" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
      <link href="./bootstrap_aku.min.css" rel="stylesheet">
    </head>

    <body>
      <?php
      include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'menu_header.php');?>
        <div class="main margin-top">
          <div class="main-inner">
            <div class="container">
              <div class="row">
                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3 style="padding-right:3px;">List of Indent Items for Approval</h3>
                    <button  style="float:right;margin:3px;" class="btn btn-danger" onclick="delete_indent_item_multiple()">
                      <span class="glyphicon glyphicon-trash"></span> Delete Multiple
                    </button>
                    <?php if($edit_perms_list['status'] == SUCCESS){ ?>
                    <iframe src="about:blank" id="iframe_file_upload" name="iframe_file_upload" style="width:0px; height:0px;"></iframe>
                    <form id="formImport" style="float:right;margin-top: 5px; margin-right: 5px;" class="form-horizontal" target="iframe_file_upload" method="post" enctype="multipart/form-data">
                      <input type="hidden" id="hd_project_id" name="hd_project_id" class="form-control" />
                      <input type="hidden" id="hd_status" name="hd_status" class="form-control" />
                      <div class="btn-group btn-group-sm pull-right" role="group">
                        <button class="btn btn-primary" onclick="exportData()">
                         <span class="glyphicon glyphicon-download"></span> Export
                        </button>
                      </div>
                    </form>
                    <?php } ?>
                    <!-- Export & Import buttons -->
                  </div>
                  <div class="widget-header widget-toolbar" style="height:auto">
                    <form class="form-inline">
                      <select name="project_id" id="project_id" style="max-width:250px" class="form-control input-sm">
                                 <option value="">- - Select Project - -</option>
                              <?php
                                for ($project_count = 0; $project_count < count($project_list_data); $project_count++) { ?>
                                 <option value="<?php echo $project_list_data[$project_count]["stock_project_id"]; ?>"
                                   <?php if ($project_id == $project_list_data[$project_count]["stock_project_id"]) {
                                    ?> selected="selected" <?php } ?>>
                                    <?php echo $project_list_data[$project_count]["stock_project_name"]; ?>
                                 </option>
                              <?php } ?>
                      </select>
                      <select name="status" id="status" style="max-width:250px" class="form-control input-sm">
                          <option value="Pending">Pending</option>
                          <option value="Approved">Approved</option>
                          <option value="Rejected">Rejected</option>
                          <option value="Cancelled">Cancelled</option>
                      </select>
                      <button id="submit_button" type="button" class="btn btn-primary" onclick="redrawTable()">Submit</button>
                    </form>
                  </div>
                  <?php if($view_perms_list['status'] == SUCCESS){ ?>
                  <div class="widget-content">
                    <table class="table table-striped table-bordered display nowrap" id="example" width="100%">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Indent No</th>
                          <th>Indent Date</th>
                          <th>Project Name</th>
                          <th>Material</th>
                          <th>Material Code</th>
                          <th>UOM</th>
                          <th>Quantity</th>
                          <th>Stock Quantity</th>
                          <th>Status</th>
                          <th>Remarks</th>
                          <th>Required Date</th>
                          <th>Requested By</th>
                          <th>A</th>
                          <th>D</th>
                          <th>R</th>
                          <th><input type="checkbox" id="toggle_checkbox"/ onchange="toggleSelection(this)"></th>
                        </tr>
                      </thead>
                      </tbody>
                    </table>
                  </div>
                  <?php }
                else{ ?>
                  <div class="widget-content">
                    <h1>Access Denied</h1>
                    <h3>You dont have permissions to view the Document</h3>
                  </div>
                  <?php } ?>
                  <!-- widget-content -->
                </div>
              </div>
            </div>
          </div>
    </body>

    </html>
