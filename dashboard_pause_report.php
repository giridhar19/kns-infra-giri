<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 11th May 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Role dropdown should be from database
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	/* DATA INITIALIZATION - END */

	/* QUERY STRING DATA - START */
	if(isset($_GET["process"]))
	{
		$process = $_GET["process"];
	}
	else
	{
		$process = "";
	}

	$search_project   	 = "";

	if(isset($_POST["search_project"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_process   	 = "";

	if(isset($_POST["search_process"]))
	{
		$search_process   = $_POST["search_process"];
	}

	$search_reason   	 = "";

	if(isset($_POST["search_reason"]))
	{
		$search_reason   = $_POST["search_reason"];
	}

	$search_status   	 = "0";

	if(isset($_POST["search_status"]))
	{
		$search_status   = $_POST["search_status"];
		if($search_status == "0")
		{
			$delay_end_date = "0000-00-00 00:00:00";
			$search_status  = "";
		}
		else if($search_status == "1")
		{
			$delay_end_date = "";
			$search_status  = "1";
		}
	}
	else
	{
		$delay_end_date = "0000-00-00 00:00:00";
		$search_status  = "";
	}
	/* QUERY STRING DATA - END */

	/* Get file count for each process */
	//Get Delay Reason List
	$delay_reason_search_data = array("delay_end_date"=>$delay_end_date,"project"=>$search_project,"name"=>$search_reason,"process_id"=>$search_process,"status"=>$search_status);
	$delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
	if($delay_reason_list["status"] == SUCCESS)
	{
		$delay_reason_list_data = $delay_reason_list["data"];
	}
	// Project data
	$project_management_master_search_data = array("active"=>'1',"user"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}

	// Get Project Delay Reason
	$project_reason_master_search_data = array("active"=>'1');
	$project_delay_reason_list = i_get_project_reason_master($project_reason_master_search_data);
	if($project_delay_reason_list["status"] == SUCCESS)
	{
		$project_delay_reason_list_data = $project_delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_delay_reason_list["data"];
	}

	// Get Already added process
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

}

else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Paused Report</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Paused Report</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="dashboard_pause_report.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_reason">
			  <option value="">- - Select Delay - -</option>
			  <?php
			  for($count = 0; $count < count($project_delay_reason_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $project_delay_reason_list_data[$count]["project_reason_master_id"]; ?>"<?php if($search_reason == $project_delay_reason_list_data[$count]["project_reason_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_delay_reason_list_data[$count]["project_reason_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span>
			  <select name="search_status">
			  <option value="0" <?php if($search_status == '0') { ?> selected="selected" <?php } ?>>Paused</option>
			  <option value="1" <?php if($search_status == '1') { ?> selected="selected" <?php } ?>>Released</option>

			  </select>
			  </span>


			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
			          <th>Project</th>
					  <th>Process</th>
					  <th>Task</th>
					  <th>Road</th>
					  <th>Pause Date </th>
						<?php if($search_status == 1) { ?>
					  <th>Release Date</th>
					<?php } ?>
					  <th>Days</th>
					  <th>Reason</th>
					  <th>Remarks</th>
					  <th>Paused By</th>
					  <th>Actions</th>
						<?php if($search_status == 0) { ?>
					  <th>Actions</th>
							<?php } ?>
				  </tr>
				</thead>
				<tbody>
			<?php
			if($delay_reason_list["status"] == SUCCESS)
			{
				$sl_no = 0;
				for($count = 0; $count < count($delay_reason_list_data); $count++)
				{
					$sl_no++;
					$pause_date = date("d-M-Y g:i A", strtotime($delay_reason_list["data"][$count]["project_task_delay_reason_start_date"]));
					$pause_reason = $delay_reason_list["data"][$count]["project_reason_master_name"];
					$pause_remarks = $delay_reason_list["data"][$count]["project_task_delay_reason_remarks"];
					$paused_by = $delay_reason_list["data"][$count]["user_name"];
					$end_date = date('d-M-Y g:i A',strtotime($delay_reason_list["data"][$count]["project_task_delay_reason_end_date"]));
					$today = date("Y-m-d");
					if($delay_reason_list["data"][$count]["project_task_delay_reason_end_date"] != "0000-00-00 00:00:00" )
					{
						$pause_days = get_date_diff($pause_date,$delay_reason_list["data"][$count]["project_task_delay_reason_end_date"]);
					}
					else
					{
						$pause_days = get_date_diff($pause_date,$today);
						$end_date = '';
					}
					?>
					<tr>
					<td><?php echo $delay_reason_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $delay_reason_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $delay_reason_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $delay_reason_list_data[$count]["project_site_location_mapping_master_name"]; ?></td>
					<td><?php echo $pause_date ; ?></td>
					<?php if($search_status == 1) { ?>
					<td><?php echo $end_date ; ?></td>
				<?php } ?>
					<td><?php echo abs($pause_days["data"] + 1) ;?></td>
					<td><?php echo $pause_reason ;?></td>
					<td><?php echo $pause_remarks ; ?></td>
					<td><?php echo $paused_by ; ?></td>
					<td><a style="padding-right:10px" href="#" onclick="return go_to_edit_reason_master('<?php echo $delay_reason_list_data[$count]["project_task_delay_reason_task_id"]; ?>','<?php echo $delay_reason_list_data[$count]["project_task_delay_reason_id"] ;?>','<?php echo $delay_reason_list_data[$count]["project_management_master_id"]; ?>');">Edit</a></td>
					<?php if($search_status == 0) { ?><td><?php if(($delay_reason_list_data[$count]["project_task_delay_reason_active"] == "1")){?><a style="padding-right:10px" href="#" onclick="return go_to_release_reason_master('<?php echo $delay_reason_list_data[$count]["project_task_delay_reason_task_id"]; ?>','<?php echo $delay_reason_list_data[$count]["project_task_delay_reason_id"] ;?>','<?php  echo $delay_reason_list_data[$count]["project_management_master_id"]; ;?>','<?php echo $delay_reason_list_data[$count]["project_task_delay_reason_road_id"] ; ?>');">Release</a><?php } ?></td> <?php } ?>

					</tr>
				<?php
				}

			}
			?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_edit_reason_master(project_process_task_id,reason_id,project_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_edit_task_delay_reason.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","reason_id");
	hiddenField2.setAttribute("value",reason_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","project_id");
	hiddenField3.setAttribute("value",project_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}

function go_to_release_reason_master(project_process_task_id,reason_id,project_id,road_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_release_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","reason_id");
	hiddenField2.setAttribute("value",reason_id);

	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","project_id");
	hiddenField4.setAttribute("value",project_id);

	var hiddenField5 = document.createElement("input");
	hiddenField5.setAttribute("type","hidden");
	hiddenField5.setAttribute("name","road_id");
	hiddenField5.setAttribute("value",road_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);








	form.appendChild(hiddenField4);
	form.appendChild(hiddenField5);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
