<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: user_list.php

CREATED ON	: 05-July-2015

CREATED BY	: Nitin Kashyap

PURPOSE     : List of Users

*/



/* DEFINES - START */

define('USER_LIST_FUNC_ID','2');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',USER_LIST_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',USER_LIST_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',USER_LIST_FUNC_ID,'4','1');



	// Query String Data

	// Nothing here



	// Temp Data

	$user_id    = "";

	$user_name  = "";

	$user_email = "";

	$user_role  = "";



	// Get list of users

	$user_list = i_get_user_list($user_id,$user_name,$user_email,$user_role);



	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>User List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>     



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>User List</h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			<?php

			if($view_perms_list['status'] == SUCCESS)

			{

			?>

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>User Name</th>

					<th>Email ID</th>

				    <th>Role</th>

					<th>Active</th>	

					<th>Manager</th>

					<th>Department</th>	

					<th>Location</th>	

					<th>User Created On</th>

					<?php

					if($delete_perms_list['status'] == SUCCESS)

					{

					?>

					<th>&nbsp;</th>

					<?php

					}

					?>

					<?php

					if($edit_perms_list['status'] == SUCCESS)

					{

					?>

					<th>&nbsp;</th>

					<?php

					}

					?>

					<?php

					if($edit_perms_list['status'] == SUCCESS)

					{

					?>

					<th>&nbsp;</th>

					<?php

					}

					?>

				</tr>

				</thead>

				<tbody>

				 <?php

				for($count = 0; $count < count($user_list_data); $count++)

				{

					if($user_list_data[$count]["user_active"] == 1)

					{

						$action = 0;

						$text   = "Disable";

					}

					else

					{

						$action = 1;

						$text   = "Enable";

					}

				?>

				<tr>

				    <td><?php echo $user_list_data[$count]["user_name"]; ?></td>

					<td><?php echo $user_list_data[$count]["user_email_id"]; ?></td>

					<td><?php echo i_get_user_role($user_list_data[$count]["user_role"]); ?></td>

					<td><?php echo i_get_user_active_status($user_list_data[$count]["user_active"]); ?></td>

					<td><?php $manager_result = i_get_user_list($user_list_data[$count]["user_manager"],'','','');

					echo $manager_result["data"][0]["user_name"]; ?></td>

					<td><?php echo $user_list_data[$count]["general_task_department_name"]; ?></td>

					<td><?php echo $user_list_data[$count]["stock_location_name"]; ?></td>

					<td><?php echo get_formatted_date($user_list_data[$count]["user_added_on"],"d-M-Y"); ?></td>

					<?php

					if($delete_perms_list['status'] == SUCCESS)

					{

					?><td><a href="user_enable_disable.php?user=<?php echo $user_list_data[$count]["user_id"]; ?>&action=<?php echo $action; ?>"><?php echo $text; ?></a></td>

					<?php

					}

					?>

					<?php

					if($edit_perms_list['status'] == SUCCESS)

					{

					?>

					<td><a href="edit_user.php?user=<?php echo $user_list_data[$count]["user_id"]; ?>">Edit User</a></td>

					<?php

					}

					?>

					<?php

					if($edit_perms_list['status'] == SUCCESS)

					{

					?>

					<td><a href="reset_password.php?user=<?php echo $user_list_data[$count]["user_id"]; ?>">Reset Password</a></td>

					<?php

					}

					?>

				</tr>

				<?php 

				}

				 ?>	



                </tbody>

              </table>

			<?php

			}

			else

			{

				echo 'You are not authorized to view the user list';

			}

			?>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>





  </body>



</html>

