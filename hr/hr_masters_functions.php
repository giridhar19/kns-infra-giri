<?php
//ini_set('memory_limit',$ '-1');
/**
 * @author Perumal
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_masters.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_config.php');

/* INTERFACE FUNCTIONS - START */

/*
PURPOSE : To add a holiday
INPUT 	: Holiday Date, Holiday Name, Type, Added By
OUTPUT 	: Holiday ID, success or failure message
BY 		: Perumal
*/
function i_add_holiday($date,$name,$type,$added_by)
{
	$holiday_filter_data = array("date_start"=>$date,"date_end"=>$date);
	$holiday_sresult = db_get_holiday_list($holiday_filter_data);
	
	if($holiday_sresult['status'] == DB_NO_RECORD) // If a holiday has not been added for this date already
    {
        $holiday_iresult = db_add_holiday($date,$name,$type,$added_by);
		
		if($holiday_iresult["status"] == SUCCESS)
		{
			$return["data"]   = "Holiday has been added!";
			$return["status"] = SUCCESS;
		}
		else
		{
			$return["data"]   = "Internal error. Please inform the admin";
			$return["status"] = FAILURE;
		}
    }
    else
    {
		$return["data"]   = "A holiday has already been added for ".date("d-M-Y",strtotime($date));
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To get holiday list
INPUT   : Holiday Filter Array
OUTPUT 	: Holiday List, success or failure message
BY 		: Nitin
*/
function i_get_holiday($holiday_filter_data)
{
	$holiday_sresult = db_get_holiday_list($holiday_filter_data);
	
	if($holiday_sresult['status'] == DB_RECORD_ALREADY_EXISTS) // If a holiday has been added for this search already
    {
		$return["data"]   = $holiday_sresult["data"];
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No holiday matching your search";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To update a holiday
INPUT   : Holiday Filter Array
OUTPUT 	: Holiday List, success or failure message
BY 		: Nitin
*/
function i_update_holiday($holiday_id,$holiday_filter_data)
{
	$holiday_uresult = db_update_holiday_details($holiday_id,$holiday_filter_data);
	
	if($holiday_uresult['status'] == SUCCESS) // If a holiday has been added for this search already
    {
		$return["data"]   = "Holiday details updated successfully";
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "Internal Error. Please contact the admin";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To enable/disable a holiday
INPUT   : Holiday ID, Action
OUTPUT 	: success or failure message
BY 		: Nitin
*/
function i_enable_disable_holiday($holiday_id,$action)
{
	$holiday_filter_data = array("status"=>$action);
	$holiday_uresult = db_update_holiday_details($holiday_id,$holiday_filter_data);
	
	if($holiday_uresult['status'] == SUCCESS)
    {
		$return["data"]   = "Holiday details updated successfully";
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "Internal Error. Please contact the admin";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}
/* INTERFACE FUNCTIONS - END */
?>